package com.million.project.context;


import com.million.project.context.inteceptor.LoginInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.LinkedList;
import java.util.Stack;

@Configuration
public class MillionWebMvcConfigurer implements WebMvcConfigurer {

    @Autowired
    private LoginInterceptor loginInterceptor;

    /**
     * 添加拦截器
     * @param registry
     */
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        // 添加一个拦截器，拦截所有路径
//        registry.addInterceptor(loginInterceptor)
//            .addPathPatterns("/**") // 拦截所有路径
//            .excludePathPatterns("/login/**"); // 除了/login
//    }
    /**
     * 解决跨域问题
     * @param registry
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**") // 允许跨域访问的路径
            .allowCredentials(true)  // 是否发送cookie
            .allowedHeaders("*") // 允许头部设置
            .allowedOrigins("*") // 允许跨域访问的源
            .allowedMethods("*"); // 允许请求方法
//.allowedOriginPatterns("*") // 允许跨域访问的源
    }

}
