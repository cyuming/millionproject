package com.million.project.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;

/**
 * 配置redis
 */
@Configuration
public class RedisConfig {
    // JacksonJsonRedisSerializer：jackson-json工具提供了javabean与json之间的转换能力，
    // 可以将pojo实例序列化成json格式存储在redis中，也可以将json格式的数据转换成pojo实例。
    // 因为jackson工具在序列化和反序列化时，需要明确指定Class类型

    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory){

        // 创建一个 RedisTemplate 实例
        RedisTemplate<String, Object> template = new RedisTemplate<>();

        // 设置连接工厂
        template.setConnectionFactory(connectionFactory);

        // 设置 key 的序列化器为 Redis 的字符串序列化器
        template.setKeySerializer(RedisSerializer.string());
        // 设置 hash key 的序列化器为 Redis 的字符串序列化器
        template.setHashKeySerializer(RedisSerializer.string());

        // 创建一个通用的 Jackson2JsonRedisSerializer 作为值的序列化器
        GenericJackson2JsonRedisSerializer jsonRedisSerializer = new GenericJackson2JsonRedisSerializer();

        // 设置 value 的序列化器为 Jackson2JsonRedisSerializer
        template.setValueSerializer(jsonRedisSerializer);
        // 设置 hash value 的序列化器为 Jackson2JsonRedisSerializer
        template.setHashValueSerializer(jsonRedisSerializer);

        // 返回设置好的 RedisTemplate 实例
        return template;
    }


}
