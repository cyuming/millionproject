package com.million.project.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.ServletOutputStream;
import org.springframework.web.multipart.MultipartFile;

/**
 * 方便对文件处理
 *
 * @Author: zhencym
 * @DATE: 2022/11/18
 */
public class MyFileUtils {

  //单位
  public static final long TG = 1024 * 1024 * 1024 * 1024;
  public static final long GB = 1024 * 1024 * 1024;
  public static final long MB = 1024 * 1024;
  public static final long KB = 1024;
  public static final long B = 1;

  //默认头像路径
  public static final String driverImage0 = "/usr/image/driver/default0.jpg";
      //"src/main/resources/image/driver/default0.jpg";
  public static final String auditorImage0 = "/usr/image/auditor/default0.jpg";
          //"src/main/resources/image/auditor/default0.jpg";
  public static final String managerImage0 = "/usr/image/manager/default0.jpg";
              //"src/main/resources/image/manager/default0.jpg";

  public static String getdefaultImage(String userType) {
    if (userType.equals("司机")) {
      return driverImage0();
    } else if (userType.equals("审核员")) {
      return auditorImage0();
    } else if (userType.equals("管理员")) {
      return managerImage0();
    } else {
      return null;
    }
  }

  public static String driverImage0() {
    return driverImage0;
  }

  public static String auditorImage0() {
    return auditorImage0;
  }

  public static String managerImage0() {
    return managerImage0;
  }

  /**
   * 上传头像且限制文件大小
   *
   * @param file
   * @param savePath
   */
  public static boolean clientUpload(MultipartFile file, String savePath, int size, long unit) {
    if (file.getSize() >= (size * unit)) {
      return false;
    } else {
      clientUpload(file, savePath);
      return true;
    }
  }

  /**
   * 上传图片
   *
   * @param file
   * @param savePath
   */
  public static void clientUpload(MultipartFile file, String savePath) {
    //接收文件
    FileInputStream fis = null;
    FileOutputStream fos = null;
    try {
      fis = (FileInputStream) file.getInputStream();
      fis.available();
      fos = new FileOutputStream(new File(savePath));
      byte[] buffer = new byte[1024];
      int len = 0;
      while ((len = fis.read(buffer, 0, buffer.length)) != -1) {
        fos.write(buffer, 0, buffer.length);
      }
      fis.close();
      fos.close();
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 下载 参数为输出流 和 请求资源路径
   */
  public static void clientDownload(ServletOutputStream outputStream, String getPath) {
    // 读到流中
    InputStream inputStream = null;// 文件的存放路径
    try {
      inputStream = new FileInputStream(getPath);
      byte[] b = new byte[1024];
      int len;
      while ((len = inputStream.read(b)) > 0) {
        outputStream.write(b, 0, len);
      }
      inputStream.close();
    } catch (FileNotFoundException e) {
      throw new RuntimeException(e);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
  }

}
