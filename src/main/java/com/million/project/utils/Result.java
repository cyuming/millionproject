package com.million.project.utils;

import java.util.Map;

/**
 * 结果实体：用于统一后端向前端返回数据的格式
 */
public class Result {

  private Object data;
  private String code;
  private String message;

  @Override
  public String toString() {
    return "Result{" +
        "data=" + data +
        ", code='" + code + '\'' +
        ", message='" + message + '\'' +
        '}';
  }

  public void setData(Object data) {
    this.data = data;
  }

  public Object getData() {
    return data;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public Result() {
  }

  public Result(String code, String message, Object data) {
    this.code = code;
    this.message = message;
    this.data = data;
  }

  /**
   * 成功返回无数据
   *
   * @return
   */
  public static Result OK() {
    return new Result("1", "success", null);
  }

  /**
   * 成功返回有数据
   */
  public static Result OK(Object data) {
    return new Result("1", "success", data);
  }

  /**
   * 失败返回默认错误信息无数据
   */
  public static Result NOK() {
    return new Result("0", "error", null);
  }

  /**
   * 失败返回自定义错误信息无数据
   */
  public static Result NOK(String errorMsg) {
    return new Result("0", errorMsg, null);
  }

  /**
   * 自定义成功或不成功
   */
  public static Result OR(boolean OKorNOK, Object data) {
    if (OKorNOK) {
      return new Result("1", "success", data);
    } else {
      return new Result("0", "error", data);
    }
  }

  /**
   * 自定义成功或不成功,并附加msg
   */
  public static Result OR(boolean OKorNOK, String msg, Object data) {
    if (OKorNOK) {
      return new Result("1", msg, data);
    } else {
      return new Result("0", msg, data);
    }
  }

}
