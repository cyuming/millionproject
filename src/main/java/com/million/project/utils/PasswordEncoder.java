package com.million.project.utils;


import cn.hutool.core.util.RandomUtil;
import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;


public class PasswordEncoder {


  public static String encode(String password) {

    String salt = RandomUtil.randomString(20);

    return encode(password, salt);
  }

  private static String encode(String password, String salt) {

    return salt + "@" + DigestUtils.md5DigestAsHex(
        (password + salt).getBytes(StandardCharsets.UTF_8));
  }

  /**
   * 校验加密后的密码与加密前的密码是否正确
   *
   * @param encodedPassword
   * @param rawPassword
   * @return
   */
  public static Boolean matches(String encodedPassword, String rawPassword) {
    if (encodedPassword == null || rawPassword == null) {
      return false;
    }
    if (!encodedPassword.contains("@")) {
      throw new RuntimeException("密码格式不正确！");
    }
    String[] arr = encodedPassword.split("@");

    String salt = arr[0];

    return encodedPassword.equals(encode(rawPassword, salt));
  }
}
