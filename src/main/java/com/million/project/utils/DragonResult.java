package com.million.project.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DragonResult {

  private Boolean success; // 返回成功

  private String errorMsg; // 返回错误信息指示

  private Object data; // 返回对象

  private Long total;  // 返回结果个数（当返回对象列表时）

  private String type; // 在用户登录时给前端，返回用户类型

  private HashMap<String, String> mapData; // 打包零散数据

  /**
   * @return 只返回成功
   */
  public static DragonResult ok() {
    return new DragonResult(true, null, null, null, null, null);
  }

  public static DragonResult ok(Boolean success, Object data) {
    return new DragonResult(success, null, data, null, null, null);
  }

  /**
   * @param data
   * @return 成功并返回一个对象
   */
  public static DragonResult ok(Object data, String type) {
    return new DragonResult(true, null, data, null, type, null);
  }

  public static DragonResult ok(Object data) {
    return new DragonResult(true, null, data, null, null, null);
  }

  /**
   * @param data
   * @return
   */
  public static DragonResult ok(HashMap<String, String> data) {
    return new DragonResult(true, null, null, null, null, data);
  }

  /**
   * @param data
   * @param total
   * @return 成功并返回对象列表
   */
  public static DragonResult ok(List<?> data, Long total) {
    return new DragonResult(true, null, data, total, null, null);
  }

  /**
   * @param errorMsg
   * @return 失败并返回失败信息，失败信息需要手动传入
   */
  public static DragonResult fail(String errorMsg) {
    return new DragonResult(false, errorMsg, null, null, null, null);
  }

  @Override
  public String toString() {
    return "Result{" +
        "success=" + success +
        ", errorMsg='" + errorMsg + '\'' +
        ", data=" + data +
        ", total=" + total +
        ", type='" + type + '\'' +
        '}';
  }
}
