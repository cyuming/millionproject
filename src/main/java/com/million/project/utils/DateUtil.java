package com.million.project.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import org.assertj.core.util.Strings;

/**
 * 日期工具类
 *
 * @author 11921
 */
public class DateUtil {
    public static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    public static final String YYYY_MM_DD_HH = "yyyy-MM-dd HH";
    public static final String YYYY_MM_DD = "yyyy-MM-dd";

    /**
     * 获取到当前的日期时间
     *
     * @return
     */
    public static String getTime() {
        return date2Str(new Date(),YYYY_MM_DD_HH_MM_SS);

    }

    /**
     * 几天后
     * @param date
     * @param day
     * @return
     */
    public static Date getSomeDay(Date date, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DATE, day);
        return calendar.getTime();
    }

    /**
     * 字符串转Date
     * @param strDate
     * @return
     */
    public static Date strToDate(String strDate) {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return formatter.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Date转字符串
     * @param date
     * @param pattern
     * @return
     */
    public static String date2Str(Date date, String pattern) {
        return !Objects.isNull(date) && !Strings.isNullOrEmpty(pattern) ? Optional.of((new SimpleDateFormat(pattern)).format(date)).get() : (String) Optional.empty().get();
    }

    /**
     * 获取日期所在星期
     *
     * @param date 日期
     * @return 星期
     */
    public static int getDayOfWeek(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_WEEK) - 1;
    }

    /**
     * 获取日期所在月份
     *
     * @param date 日期
     * @return 月份
     */
    public static int getDayOfMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.DAY_OF_MONTH);
    }

    /**
     * 获取日期所在年份
     *
     * @param date 日期
     * @return 年份
     */
    public static int getDayOfYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static String getMinTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getMinTime(calendar);
    }

    /**
     * 设置最小时间
     *
     * @param calendar
     */
    private static String getMinTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return date2Str(calendar.getTime(), YYYY_MM_DD_HH_MM_SS);
    }

    public static String getMaxTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        return getMaxTime(calendar);
    }

    /**
     * 设置最大时间
     *
     * @param calendar
     */
    private static String getMaxTime(Calendar calendar) {
        calendar.set(Calendar.HOUR_OF_DAY, calendar.getActualMaximum(Calendar.HOUR_OF_DAY));
        calendar.set(Calendar.MINUTE, calendar.getActualMaximum(Calendar.MINUTE));
        calendar.set(Calendar.SECOND, calendar.getActualMaximum(Calendar.SECOND));
        calendar.set(Calendar.MILLISECOND, calendar.getActualMaximum(Calendar.MILLISECOND));
        return date2Str(calendar.getTime(), YYYY_MM_DD_HH_MM_SS);
    }

    /**
     * 获取多少小时前
     * @param hour 小时
     * @return 时间
     */
    public static String getBeforeHourTime(int hour) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) - hour);
        return date2Str(calendar.getTime(), YYYY_MM_DD_HH_MM_SS);
    }
    /**
     * 获取多少个月前
     * @param month 个月
     * @return 时间
     */
    public static String getBeforeMonthTime(int month) {
        Calendar calendar = Calendar.getInstance();
        int num = calendar.get(Calendar.MONTH) + 1 - month; //月份从0开始，所以+1
        System.err.println(calendar.get(Calendar.YEAR) + " "+calendar.get(Calendar.MONTH) +" "+calendar.get(Calendar.DATE) + " " +date2Str( calendar.getTime(), YYYY_MM_DD) +" "+month+" "+ num);
        //减一年
        if ( num <= 0 ) {
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
            calendar.set(Calendar.MONTH, 11 + num); //11表示12月份，num是否负数
            calendar.set(Calendar.DATE, 1); //本月第一天
        } else {
            calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR));
            calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - month);
            calendar.set(Calendar.DATE, 1); //本月第一天
        }
        System.err.println(calendar.get(Calendar.YEAR) + " "+calendar.get(Calendar.MONTH) +" "+calendar.get(Calendar.DATE) + " " +date2Str(calendar.getTime(), YYYY_MM_DD) +" "+month+" "+ num);
        return date2Str(calendar.getTime(), YYYY_MM_DD);
    }
}
