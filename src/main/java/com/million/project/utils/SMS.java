package com.million.project.utils;

import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.PostMethod;

/**
 * 用于用户注册时，真实短信发送到手机上。
 */
public class SMS {
    public static void sendMessage(String phone, String code) throws Exception {
        HttpClient client = new HttpClient();
        PostMethod post = new PostMethod("http://gbk.api.smschinese.cn");
        System.out.println("+++");
        System.out.println(2);
        post.addRequestHeader("Content-Type",
                "application/x-www-form-urlencoded;charset=gbk");//在头文件中设置转码
        NameValuePair[] data = {
                new NameValuePair("Uid", "xiaolong1"),
                new NameValuePair("Key", "2CE2CD3564EB6060408D7A4DB1CB31BE"),
                new NameValuePair("smsMob", phone),
                new NameValuePair("smsText", "尊敬的用户" + phone + "您好，您本次手机6位验证码为：" + code)};
        post.setRequestBody(data);
        System.out.println(1);
        client.executeMethod(post);
        Header[] headers = post.getResponseHeaders();
        int statusCode = post.getStatusCode();
        System.out.println("statusCode:" + statusCode);
        for (Header h : headers) {
            System.out.println(h.toString());
        }
        String result = new String(post.getResponseBodyAsString().getBytes("gbk"));
        System.out.println(result); //打印返回消息状态
        post.releaseConnection();
    }
}
