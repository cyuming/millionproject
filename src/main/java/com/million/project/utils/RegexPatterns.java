package com.million.project.utils;

/**
 * 该类用于各种正则校验校验码的存储。
 *
 * @author 虎哥？？虎哥是谁？
 */
public abstract class RegexPatterns {

  /**
   * 手机号正则
   */
  public static final String PHONE_REGEX = "^1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\\d{8}$";
  //"^1([38][0-9]|4[579]|5[0-3,5-9]|6[6]|7[0135678]|9[89])\\d{8}$";
  /**
   * 邮箱正则
   */
  public static final String EMAIL_REGEX = "^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$";
  /**
   * 密码正则。4~32位的字母、数字、下划线
   */
  public static final String PASSWORD_REGEX = "^\\w{4,32}$";
  /**
   * 验证码正则, 6位数字
   */
  public static final String VERIFY_CODE_REGEX = "^\\d{6}$";

  public static final String ID_CARD = "^[1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx]$";

}
