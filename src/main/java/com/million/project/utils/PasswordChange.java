package com.million.project.utils;

import lombok.Data;

@Data
public class PasswordChange {

  private String oldPassword;

  private String newPassword;

  private String setPassword;
}
