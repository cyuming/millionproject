package com.million.project.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * 结果实体：用于统一后端向前端返回数据的格式
 */
public class ResultMap {

  // 返回状态码
  private String code;
  // 返回信息
  private String message;
  // 返回数据集
  private Map<String, Object> data = new HashMap<>();

  public ResultMap() {
  }

  public ResultMap(String code, String message, Map<String, Object> data) {
    this.code = code;
    this.message = message;
    this.data = data;
  }

  /**
   * 成功返回无数据
   *
   * @return
   */
  public static ResultMap OK() {
    return new ResultMap("1", "success", null);
  }

  /**
   * 成功返回有数据
   */
  public static ResultMap OK(Map<String, Object> data) {
    return new ResultMap("1", "success", data);
  }

  /**
   * 失败返回默认错误信息无数据
   */
  public static ResultMap NOK() {
    return new ResultMap("0", "error", null);
  }

  /**
   * 失败返回自定义错误信息无数据
   */
  public static ResultMap NOK(String errorMsg) {
    return new ResultMap("0", errorMsg, null);
  }


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  /**
   * 可以直接获取里面的map往里put数据
   *
   * @return
   */
  public Map<String, Object> getData() {
    return data;
  }

  /**
   * 也可以在外面new一个map,put完数据后，重新复赋值map
   *
   * @return
   */
  public void setData(Map<String, Object> data) {
    this.data = data;
  }

  @Override
  public String toString() {
    return "ResultMap{" +
        "code=" + code +
        ", message='" + message + '\'' +
        ", data=" + data +
        '}';
  }
}