package com.million.project.utils;

public class Base64 {
    private static final java.util.Base64.Decoder decoder = java.util.Base64.getDecoder(); // base64解码器
    private static final java.util.Base64.Encoder encoder = java.util.Base64.getEncoder(); // base64编码器

    public static void main(String[] args) {
        String path = new String("[{\"name\":\"长沙\",\"position\":[112.938882,28.228304],\"note\":\"长沙特别好\",\"img\":\"\"},{\"name\":\"衡阳\",\"position\":[112.572016,26.894216],\"note\":\"衡阳还是好\",\"img\":\"\"}]");
        String paths = encoder.encodeToString(path.getBytes());
        System.out.println("编码后元素为："+paths);

        String decodePaths = new String(decoder.decode(paths));
        System.out.println("解码后数据为："+decodePaths);
    }
}
