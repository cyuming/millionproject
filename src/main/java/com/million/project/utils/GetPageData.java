package com.million.project.utils;

import lombok.Data;

@Data
public class GetPageData {

  private Integer currentPage; //当前页码

  private Integer pageSize; // 页大小
}
