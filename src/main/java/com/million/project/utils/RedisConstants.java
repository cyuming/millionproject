package com.million.project.utils;

/**
 * 定义一些Redis中用到的前缀常量、定时时间常量
 */
public class RedisConstants {
    // 登录验证码有效时间与前缀
    public static final String LOGIN_CODE_KEY = "login:code:";
    public static final Long LOGIN_CODE_TTL = 15L; // 时间有效期

    // 注册验证码有效时间与前缀
    public static final String Regis_CODE_KEY = "regis:code:";
    public static final Long Regis_CODE_TTL = 15L; // 时间有效期

    // 忘记密码时的密码找回验证码有效时间与前缀
    public static final String ForgetPassword_CODE_KEY = "forgetPassword:code:";
    public static final Long ForgetPassword_CODE_TTL = 15L; // 时间有效期


    // 用户信息token的有效期与前缀
    public static final String LOGIN_USER_KEY = "login:token:";
    public static final Long LOGIN_USER_TTL = 30L; // 时间有效期



    public static final Long CACHE_NULL_TTL = 2L; // 缓存击穿问题中，设置缓存空对象null的有效期

    public static final Long CACHE_SHOP_TTL = 30L;

    public static final String CACHE_SHOP_TYPE_KEY = "cache:shopType:";
    public static final String CACHE_SHOP_KEY = "cache:shop:"; // 店铺缓存信息前缀

    public static final String LOCK_SHOP_KEY = "lock:shop:";
    public static final Long LOCK_SHOP_TTL = 10L;

    public static final String SECKILL_STOCK_KEY = "seckill:stock:";
    public static final String BLOG_LIKED_KEY = "blog:liked:";
    public static final String FEED_KEY = "feed:";
    public static final String SHOP_GEO_KEY = "shop:geo:";
    public static final String USER_SIGN_KEY = "sign:";
}
