package com.million.project.service;

import com.github.pagehelper.PageInfo;
import com.million.project.pojo.RoadNodes;
import com.million.project.pojo.RoadStone;

/**
 * @Author: zhencym
 * @DATE: 2022/11/10
 */
public interface RoadStoneService {

  PageInfo<RoadStone> getRoadStone(int pageNum, int pageSize);

  RoadStone selectRoadStone(int id);

}
