package com.million.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.million.project.pojo.Trucks;
import com.million.project.pojo.Users;
import com.million.project.utils.DragonResult;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 小龙
 * @since 2022-10-26
 */
public interface ITrucksService extends IService<Trucks> {

  List<Trucks> getAllTruckPages(Users userDTO);

  DragonResult updateTruck(String truckLicense, String currentCondition);

  List<Trucks> selectPartTruck(String truckLicense, Float truckWeight, Users userDTO);

  DragonResult selectByIdcard(String idCard);
}
