package com.million.project.service;

import com.github.pagehelper.PageInfo;
import com.million.project.pojo.IsCode;
import com.million.project.pojo.Users;
import com.million.project.utils.DragonResult;
import com.million.project.utils.PasswordChange;
import com.million.project.utils.Result;

import com.million.project.utils.ResultMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public interface UserService {

  int getUserNumByType(String userType);

  public Users LoginByPassword(String username, String password);

  public int Register(String username, String password, String user_type);

  public int Insert(Users users);

  public ResultMap addUser(String username, String password, String userType);

  Result deleteUser(Integer id);

  public Users SelectByUsername(String username);

  Result sendforgetCode(String username, HttpSession session) throws Exception;

  Result checkForgetCode(IsCode isCode, HttpServletRequest request);

  Result changeForgetPassword(Users user, HttpServletRequest request);


  /**
   * 根据页号，页大小，用户类型（司机、审核员），返回用户信息
   *
   * @param pageNum
   * @param pageSize
   * @param userType
   * @return
   */
  PageInfo<Users> getUserByType(int pageNum, int pageSize, String userType);

  PageInfo<Users> getUserByType(int pageNum, int pageSize, String userType, String condition);

  /**
   * 根据ID返回用户具体数据
   *
   * @param userId
   * @return
   */
  Users getUserById(int userId);

  DragonResult changeUserInfo(Users userDTO);

  DragonResult changePassword(PasswordChange passwordChange, String username);

  int changeImage(Users users);
}
