package com.million.project.service;

import com.github.pagehelper.PageInfo;
import com.million.project.mapper.RoadsMapper;
import com.million.project.pojo.BlockedDetail;
import com.million.project.pojo.BlockedSegs;
import com.million.project.pojo.HighRoads;
import com.million.project.utils.Result;
import com.million.project.utils.ResultMap;
import com.million.project.vo.BlockVO;
import java.util.List;

/**
 * @Author: zhencym
 * @DATE: 2022/11/3
 */
public interface BlockedSegsService {

  ResultMap getBlockedSegs(int pageNum, int pageSize);

  ResultMap selectBlockedSegs(int id);

  Result insertBlockedSegs(BlockedSegs blockedSegs);

  int deleteBlockSeg(BlockedSegs blockedSegs);
}
