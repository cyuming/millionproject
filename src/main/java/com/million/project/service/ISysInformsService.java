package com.million.project.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.million.project.pojo.SysInforms;
import com.million.project.pojo.Users;
import com.million.project.utils.DragonResult;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 小龙
 * @since 2022-10-26
 */
public interface ISysInformsService extends IService<SysInforms> {

  PageInfo<SysInforms> getSentSysInfo(Integer pageNum, Integer pageSize,String username);

  IPage<SysInforms> getDriverSysInfo(Integer currentPage, Integer pageSize, Users userDTO);

  DragonResult deleteById(SysInforms sysInforms);

  DragonResult getDeepInfo(SysInforms sysInforms);

  int sendSysInfoByType(String username, String userType, String message);
}
