package com.million.project.service;

import com.million.project.utils.ResultMap;
import com.million.project.pojo.AuditLogs;
import com.million.project.utils.Result;

public interface AuditService {

  Result getById(Integer id);

  ResultMap getPageApply(Integer pageNumber, Integer listNumber);

  int CheckRoads(AuditLogs auditLogs);

  ResultMap getPageApplyLogs(Integer pageNumber, Integer listNumber, String username, Integer type);

  int deleteLogs(Integer auditLogsId);
}
