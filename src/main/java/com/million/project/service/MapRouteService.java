package com.million.project.service;

import com.million.project.utils.DragonResult;
import com.million.project.vo.mapRouteNodeVO;
import com.million.project.vo.mapRouteVO;

/**
 * 此接口用于处理智慧生成
 */
public interface MapRouteService {

    DragonResult addRoute(mapRouteVO mapRouteVO);

    int addRouteNode(mapRouteNodeVO[] nodeVOS);

    DragonResult getAIRoute(String begin, String end, String policy, String cargoType);
}
