package com.million.project.service;

import com.github.pagehelper.PageInfo;
import com.million.project.pojo.RoadNodes;
import com.million.project.utils.ResultMap;
import com.million.project.pojo.Roads;
import com.million.project.utils.DragonResult;
import com.million.project.vo.RoadVO;

/**
 * @Author: zhencym
 * @DATE: 2022/11/3
 */
public interface RoadsService {

  PageInfo<RoadVO> getRoads(int pageNum, int pageSize);

  Roads selectRoad(int id);

  ResultMap updateBase(Roads roads);

  ResultMap insertRoad(Roads roads);

  ResultMap deleteByPK(Integer id);


  DragonResult selectSuggestRoads(String begin, String end);


}
