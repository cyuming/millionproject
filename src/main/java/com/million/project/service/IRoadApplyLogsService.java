package com.million.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.million.project.pojo.NodeLngLat;
import com.million.project.pojo.RoadApplyLogs;
import com.million.project.pojo.RoadApplySum;
import com.million.project.utils.DragonResult;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 小龙
 * @since 2022-10-26
 */
public interface IRoadApplyLogsService extends IService<RoadApplyLogs> {

    List<RoadApplyLogs> getAllApplyLogsPage(String username);

    DragonResult getDeepApply(RoadApplyLogs roadApplyLogs, String username);

    DragonResult retractApply(RoadApplyLogs roadApplyLogs, String username);

    List<RoadApplyLogs> getHistoryPage(String username);

    DragonResult deleteHistoryLog(RoadApplyLogs roadApplyLogs);

    List<RoadApplyLogs> getPassPage(String username) throws IOException;

    List<RoadApplyLogs> getNotPassPage(String username) throws IOException;

    List <Integer> getNumByDate();

    DragonResult submitRoadApply(RoadApplyLogs roadApplyLogs, NodeLngLat nodeLngLat);

    int getApplyNum();

    int getPassApplyNum();

    List<RoadApplySum> getPopular();


}
