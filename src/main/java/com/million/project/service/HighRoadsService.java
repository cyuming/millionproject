package com.million.project.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.million.project.pojo.HighRoads;
import com.million.project.pojo.Users;
import java.util.List;

/**
 * @Author: zhencym
 * @DATE: 2022/11/3
 */
public interface HighRoadsService {

  PageInfo<HighRoads> getHighRoads(int pageNum, int pageSize);

  HighRoads selectHighRoads(int id);


}
