package com.million.project.service;

import com.million.project.pojo.MapRouteEdge;

import java.util.List;

public interface MapRouteEdgeService {
    List<MapRouteEdge> getAllEdges();
}
