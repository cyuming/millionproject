package com.million.project.service;

import com.million.project.pojo.RoadSegs;

/**
 * @Author: zhencym
 * @DATE: 2022/11/19
 */
public interface RoadSegService {

  RoadSegs getRoadSegsById(Integer roadSegsId);

  RoadSegs getRoadsegsByBeginEnd(int begin, int end);

  int updateRoadsegs(RoadSegs roadsegs);

}
