package com.million.project.service;

import com.github.pagehelper.PageInfo;
import com.million.project.pojo.RoadNodes;
import com.million.project.pojo.Roads;
import com.million.project.vo.RoadNodeVO;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * @Author: zhencym
 * @DATE: 2022/11/10
 */
public interface RoadNodesService {

  PageInfo<RoadNodeVO> getRoadNodes(int pageNum, int pageSize);

  RoadNodes selectRoadNode(int id);

  RoadNodes selectByPrimaryKey(int id);

  int selectByName(String name);

  int insertRoadNodeByFile() throws IOException;

}
