package com.million.project.service;

import com.million.project.pojo.MapRouteNode;

import java.util.List;

public interface MapRouteNodeService {
    List<MapRouteNode> getAllNodes();
}
