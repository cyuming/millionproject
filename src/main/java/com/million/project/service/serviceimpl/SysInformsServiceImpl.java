package com.million.project.service.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.million.project.mapper.SysInformsMapper;
import com.million.project.mapper.UsersMapper;
import com.million.project.pojo.SysInforms;
import com.million.project.pojo.Users;
import com.million.project.service.ISysInformsService;
import com.million.project.utils.DragonResult;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 系统通知服务实现类
 * </p>
 *
 * @author 小龙
 * @since 2022-10-26
 */
@Service
public class SysInformsServiceImpl extends ServiceImpl<SysInformsMapper, SysInforms> implements
        ISysInformsService {

    @Autowired
    SysInformsMapper sysInformsDao;
    @Autowired
    UsersMapper usersMapper;

    /**
     * 查看管理员自己发出的通知
     */
    public PageInfo<SysInforms> getSentSysInfo(Integer pageNum, Integer pageSize, String username) {
        PageHelper.startPage(pageNum, pageSize); //设置分页
        List<SysInforms> informsList = sysInformsDao.selectBysender(username);
        PageInfo<SysInforms> pageInfo = new PageInfo<>(informsList); //将数据交给分页helper
        System.out.println(pageInfo);
        return pageInfo;
    }

    /**
     * 查看所有司机收到的通知
     *
     * @param currentPage
     * @param pageSize
     * @param userDTO     用户信息
     * @return
     */
    @Override
    public IPage<SysInforms> getDriverSysInfo(Integer currentPage, Integer pageSize, Users userDTO) {

        Page page = new Page(currentPage, pageSize);

        LambdaQueryWrapper<SysInforms> lqw = new LambdaQueryWrapper<>();

        lqw.eq((Strings.isNotEmpty(userDTO.getUsername()))
                , SysInforms::getReceiverUsername, userDTO.getUsername());
        // 3.执行
        IPage<SysInforms> sysInformsIPage = sysInformsDao.selectPage(page, lqw);

/*       可选：
        // 最后将每个对象的发送者统一改为管理员
        // for循环遍历修改
        List<SysInforms> records = sysInformsIPage.getRecords();
        for (int i = 0; i < records.size(); i++) {
            // 取出SysInforms对象
            SysInforms sysInforms = records.get(i);
            sysInforms.setSenderUsername("管理员");
        }*/
        return sysInformsIPage;
    }

    /**
     * 根据传入的通知信息中的通知id，删除该通知即可
     *
     * @param sysInforms
     * @return
     */
    @Override
    public DragonResult deleteById(SysInforms sysInforms) {

        int numbers = sysInformsDao.deleteById(sysInforms);

        if (numbers >= 1) {
            return DragonResult.ok();
        } else {
            return DragonResult.fail("删除失败！");
        }
    }

    /**
     * 根据传入系统通知的id得到详细信息
     *
     * @param sysInforms
     * @return
     */
    @Override
    public DragonResult getDeepInfo(SysInforms sysInforms) {

        SysInforms sysInform = (SysInforms) query()
                .eq("sys_informs_id", sysInforms.getSysInformsId()).one();

        return DragonResult.ok(sysInform != null, sysInform);
    }

    /**
     * 根据用户类型，将通知发送给用户
     * @param username
     * @param userType
     * @param message
     * @return
     */
    @Override
    @Transactional
    public int sendSysInfoByType(String username, String userType, String message) {
        int num = 0;
        List<Users> userList;
        if (userType.equals("all")) {
            // 查询所有用户
            userList = usersMapper.selectAll();
        } else {
            //根据类型查询
            userList = usersMapper.selectAllByType(userType);
        }
        for (int i = 0; i < userList.size(); i++) {
            String toUsername = (userList.get(i)).getUsername();
            Date date = new Date();
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            //测试
            System.out.println(f.format(date));
            SysInforms sysInforms = new SysInforms(0, username, toUsername,
                    date, message, 0);
            sysInformsDao.insertNoId(sysInforms);
            num++;
        }
        return num;
    }
}
