package com.million.project.service.serviceimpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.million.project.mapper.BlockedSegsMapper;
import com.million.project.pojo.BlockedSegs;
import com.million.project.utils.BeanCopyUtil;
import com.million.project.utils.ResultMap;
import com.million.project.mapper.RoadNodesMapper;
import com.million.project.mapper.RoadsMapper;
import com.million.project.pojo.Roads;
import com.million.project.service.RoadsService;

import com.million.project.vo.BlockVO;
import com.million.project.vo.RoadVO;
import java.util.ArrayList;
import java.util.List;

import com.million.project.utils.DragonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 路线实现类
 *
 * @Author: zhencym
 * @DATE: 2022/11/3
 */
@Service
public class RoadsServiceImpl implements RoadsService {

    @Autowired
    RoadsMapper roadsMapper;

    @Autowired
    RoadNodesMapper roadNodesMapper;
    @Autowired
    BlockedSegsMapper blockedSegsMapper;

    /**
     * 分页得到所有路线,如果阻塞，获得阻塞信息
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageInfo<RoadVO> getRoads(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize); //设置分页
        List<Roads> roadsList = roadsMapper.selectAllDetail();
        // 处理
        List<RoadVO> roadVOs = new ArrayList<>();
        for (Roads item : roadsList) {
            //查名字
            String beginName = roadNodesMapper.getNodeName(item.getRoadBegin());
            String endName = roadNodesMapper.getNodeName(item.getRoadEnd());
            BlockVO blockVO = null;
            //查阻塞信息
            if(item.getRoadStatus() == false) {
                BlockedSegs blockedSegs = blockedSegsMapper.selectByRoadId(item.getRoadsId());

                if (blockedSegs != null) {
                    System.err.println(blockedSegs);
                    blockVO = BeanCopyUtil.copyObject(blockedSegs,BlockVO.class);
                }
            }
            //组装数据
            RoadVO roadVO =  RoadVO.builder()
                .roadsId(item.getRoadsId())
                .BeginName(beginName)
                .EndName(endName)
                .suggestRoad(item.getSuggestRoad())
                .passCities(item.getPassCities())
                .roadStatus(item.getRoadStatus())
                .blockVO(blockVO)
                .build();
            roadVOs.add(roadVO);
        }
        PageInfo<RoadVO> pageInfo = new PageInfo<>(roadVOs); //将数据交给分页helper
        return pageInfo;
    }

    /**
     * 根据id查找出路线
     *
     * @param id
     * @return
     */
    @Override
    public Roads selectRoad(int id) {
        return roadsMapper.selectByPrimaryKeyDetail(id);
    }

    /**
     * 更新路线
     *
     * @param roads
     * @return
     */
    @Override
    public ResultMap updateBase(Roads roads) {
        //检查所有字段 是否为空
        boolean existEmpty = (roads.getRoadStatus() == null ||
                roads.getRoadsId() == null || roads.getRoadComment() == null ||
                roads.getPassCities() == null || roads.getRoadCost() == null ||
                roads.getSuggestRoad() == null || roads.getTotalMileage() == null ||
                roads.getTruckWidth() == null || roads.getTruckAxes() == null ||
                roads.getTruckHeight() == null || roads.getTruckLength() == null ||
                roads.getTruckTyres() == null || roads.getTruckTyres() == null);
        if (existEmpty) {
            return ResultMap.NOK("不允许空值！");
        }
        int num = roadsMapper.updateBase(roads);
        if (num > 0) {
            return ResultMap.OK();
        } else {
            return ResultMap.NOK("更新失败");
        }
    }

    @Override
    public ResultMap insertRoad(Roads roads) {
        return null;
    }

    /**
     * 根据id删除路线
     *
     * @param id
     * @return
     */
    @Override
    public ResultMap deleteByPK(Integer id) {
        if (id < 1) {
            return ResultMap.NOK("非法id");
        }
        int num = roadsMapper.deleteByPrimaryKey(id);
        if (num > 0) {
            return ResultMap.OK();
        } else {
            return ResultMap.NOK("删除失败");
        }
    }

    /**
     * 添加新的推荐路线
     */
    public ResultMap insertRoad(RoadVO roadVO) {
        //检查所有字段 是否为空
        if (roadVO.getSuggestRoad()== null || roadVO.getBeginName() == null || roadVO.getEndName() == null) {
            return ResultMap.NOK("不允许空值！");
        }
        Roads roads = new Roads();
        // 查询名字id
        int num = roadsMapper.insertNoPK(roads);
        if (num > 0) {
            return ResultMap.OK();
        } else {
            return ResultMap.NOK("插入失败");
        }
    }

    /**
     * 根据用户输入的起点和终点，找出推荐路线
     *
     * @param begin
     * @param end
     * @return
     */
    @Override
    public DragonResult selectSuggestRoads(String begin, String end) {

        if (begin == null || "".equals(begin)) {
            return DragonResult.fail("起始地不能为空！");
        }
        if (end == null || "".equals(end)) {
            return DragonResult.fail("终点地不能为空！");
        }
        // 1.首先从收费站结点表找出id
        int beginId = roadNodesMapper.selectByName(begin);
        int endId = roadNodesMapper.selectByName(end);

        System.out.println(beginId);
        System.out.println(endId);
        // 2.再根据两个id查询出推荐路线

        List<Roads> roads = roadsMapper.selectByBeginAndEnd(beginId, endId);

        System.out.println(roads);

        return DragonResult.ok(roads != null && !roads.isEmpty(), roads);
    }

}
