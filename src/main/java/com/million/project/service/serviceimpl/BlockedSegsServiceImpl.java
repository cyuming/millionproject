package com.million.project.service.serviceimpl;

import static java.lang.Boolean.FALSE;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.million.project.mapper.BlockedSegsMapper;
import com.million.project.mapper.RoadNodesMapper;
import com.million.project.mapper.RoadSegsMapper;
import com.million.project.mapper.RoadsMapper;
import com.million.project.pojo.BlockedDetail;
import com.million.project.pojo.BlockedSegs;
import com.million.project.pojo.RoadNodes;
import com.million.project.pojo.RoadSegs;
import com.million.project.service.BlockedSegsService;
import com.million.project.utils.Result;
import com.million.project.utils.ResultMap;

import com.million.project.vo.BlockVO;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 阻断路段功能实现
 *
 * @Author: zhencym
 * @DATE: 2022/11/3
 */
@Service
public class BlockedSegsServiceImpl implements BlockedSegsService {

    @Autowired
    BlockedSegsMapper blockedSegsMapper;
    @Autowired
    RoadSegsMapper roadSegsMapper;
    @Autowired
    RoadNodesMapper roadNodesMapper;
    @Autowired
    RoadsMapper roadsMapper;


    /**
     * 返回所有阻断路段的信息
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public ResultMap getBlockedSegs(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize); //设置分页
        List<BlockedSegs> blockedSegList = blockedSegsMapper.selectAllDetail();
        PageInfo<BlockedSegs> pageInfo = new PageInfo<>(blockedSegList); //将数据交给分页helper
        Map<String, Object> map = new HashMap<>();
        List list = pageInfo.getList();
        if (list == null) {
            return ResultMap.NOK("没有数据");
        } else {
            //进行数据重装
            List<BlockedDetail> listdetail = new ArrayList<>();
            for (int i = 0; i < list.size(); i++) {
                BlockedSegs blockSeg = (BlockedSegs) list.get(i);
                int setSegId = blockSeg.getBlockedSegId(); //路段id
                //查询起点终点id
                RoadSegs roadSegs = roadSegsMapper.selectByPrimaryKey(setSegId);
                int beginid = roadSegs.getRoadBegin();
                int endid = roadSegs.getRoadEnd();
                //查询起点终点名字
                RoadNodes beginNode = roadNodesMapper.selectByPrimaryKey(beginid);
                RoadNodes endNode = roadNodesMapper.selectByPrimaryKey(endid);
                String beginName = beginNode.getNodeName();
                String endName = endNode.getNodeName();
                //组装数据
                BlockedDetail blockedDetail = new BlockedDetail(
                        blockSeg.getBlockedSegId(), roadSegs.getTotalMileage(),
                        beginName, endName,
                        new Date(blockSeg.getBeginDate()), new Date(blockSeg.getEndDate()),
                        blockSeg.getDirection(), blockSeg.getReason()
                );
                listdetail.add(blockedDetail);

            }
            map.put("blockedSegsList", listdetail);
            map.put("total", pageInfo.getTotal());
        }
        return ResultMap.OK(map);
    }

    /**
     * 根据id返回具体路段的信息
     *
     * @param id
     * @return
     */
    @Override
    public ResultMap selectBlockedSegs(int id) {
        Map<String, Object> map = new HashMap<>();
        // 先根据id找到路段
        BlockedSegs blockedSegs = blockedSegsMapper.selectById(id);
        if (blockedSegs == null) {
            return ResultMap.NOK("没有数据");
        } else {
            //进行数据重装
            int setSegId = blockedSegs.getBlockedSegId(); //路段id
            //查询起点终点id
            RoadSegs roadSegs = roadSegsMapper.selectByPrimaryKey(setSegId);
            int beginId = roadSegs.getRoadBegin();
            int endId = roadSegs.getRoadEnd();
            //查询起点终点名字
            RoadNodes beginNode = roadNodesMapper.selectByPrimaryKey(beginId);
            RoadNodes endNode = roadNodesMapper.selectByPrimaryKey(endId);
            String beginName = beginNode.getNodeName();
            String endName = endNode.getNodeName();
            //组装数据
            BlockedDetail blockedDetail = new BlockedDetail(
                    blockedSegs.getBlockedSegId(), roadSegs.getTotalMileage(),
                    beginName, endName,
                    new Date(blockedSegs.getBeginDate()), new Date(blockedSegs.getEndDate()),
                    blockedSegs.getDirection(), blockedSegs.getReason()
            );
            map.put("blockedSegs", blockedDetail);
        }
        return ResultMap.OK(map);
    }

    /**
     * 添加阻断路段信息
     * 添加失败时需要回滚
     *
     * @param blockedSegs
     * @return
     */
    @Override
    @Transactional
    public Result insertBlockedSegs(BlockedSegs blockedSegs) {
        // 更新阻塞状态为false
        roadsMapper.updateRoadStatusInt(blockedSegs.getSegId(),FALSE);
        // 插入阻塞信息
        blockedSegsMapper.insert(blockedSegs);
        return Result.OK();
    }

    /**
     * 删除阻断路段信息。即关闭阻塞
     * @param blockedSegs,为推荐路线的主键
     * @return
     */
    @Override
    @Transactional
    public int deleteBlockSeg(BlockedSegs blockedSegs) {
        // 更新为true
        roadsMapper.updateRoadStatusInt(blockedSegs.getSegId(), Boolean.TRUE);
        // 删除阻塞信息
        return blockedSegsMapper.deleteById(blockedSegs.getBlockedSegId());
    }



}
