package com.million.project.service.serviceimpl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.million.project.mapper.TrucksMapper;
import com.million.project.pojo.Trucks;
import com.million.project.pojo.Users;
import com.million.project.service.ITrucksService;
import com.million.project.utils.DragonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 小龙
 * @since 2022-10-26
 */
@Service
public class TrucksServiceImpl extends ServiceImpl<TrucksMapper, Trucks> implements ITrucksService {

    @Autowired
    private TrucksMapper trucksMapper;


    /**
     * 获取当前页的车辆展示信息
     *
     * @return 返回分页对象
     */
    @Override
    public List<Trucks> getAllTruckPages(Users userDTO) {

        List<Trucks> trucks = trucksMapper.selectByIdcard(userDTO.getIdCard());
        return trucks;
    }

    /**
     * 修改车辆信息：只能修改一项车辆近况（currentCondition）
     *
     * @param truckLicense     传入车牌号
     * @param currentCondition 传入的修改信息
     * @return
     */
    @Override
    public DragonResult updateTruck(String truckLicense, String currentCondition) {

        int numbers = trucksMapper.updateCurrentCondition(truckLicense, currentCondition);
        if (numbers > 0) {
            return DragonResult.ok();
        } else {
            return DragonResult.fail("更新失败");
        }
    }

    /**
     * 条件查询车辆：车牌、吨位。并最终分页显示。
     *
     * @return
     */
    @Override
    public List<Trucks> selectPartTruck(String truckLicense, Float truckWeight, Users userDTO) {

        // 1.这里传入的limit的第一个参数要变为：currentPage = currentPage-1

        if (truckLicense != null && truckWeight != null && truckLicense != ""
                && truckWeight > 0) {

            System.out.println(trucksMapper.selectAndPage(truckLicense, truckWeight,
                    userDTO.getIdCard()));
            return trucksMapper.selectAndPage(truckLicense, truckWeight, userDTO.getIdCard());
        } else {
            System.out.println(trucksMapper.selectAndPage(truckLicense, truckWeight,
                    userDTO.getIdCard()));
            return trucksMapper.selectOrPage(truckLicense, truckWeight, userDTO.getIdCard());
        }
    }

    /**
     * 根据用户身份证号查询用户的所有车辆
     * @param idCard
     * @return
     */
    @Override
    public DragonResult selectByIdcard(String idCard) {
        if (idCard == null || "".equals(idCard)) {
            return DragonResult.fail("idCard无数据，车辆查询失败");
        }
        List<Trucks> trucks = trucksMapper.selectByIdcard(idCard);

        return DragonResult.ok(trucks != null && !trucks.isEmpty(), trucks);
    }
}
