package com.million.project.service.serviceimpl;

import com.million.project.mapper.MapRouteNodeMapper;
import com.million.project.pojo.MapRouteNode;
import com.million.project.service.MapRouteNodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MapRouteNodeServiceImpl implements MapRouteNodeService {

    @Autowired
    MapRouteNodeMapper nodeMapper;
    @Override
    public List<MapRouteNode> getAllNodes() {

        return nodeMapper.getAllNodes();
    }
}
