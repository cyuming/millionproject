package com.million.project.service.serviceimpl;

import com.million.project.mapper.RoadSegsMapper;
import com.million.project.pojo.RoadSegs;
import com.million.project.service.RoadSegService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 路段方法实现
 * @Author: zhencym
 * @DATE: 2022/11/19
 */
@Service
public class RoadSegServiceImpl implements RoadSegService {

  @Autowired
  RoadSegsMapper roadSegsMapper;

  /**
   * 根据id获得路段信息
   * @param roadSegsId
   * @return
   */
  public RoadSegs getRoadSegsById(Integer roadSegsId) {
    return roadSegsMapper.selectByPrimaryKey(roadSegsId);
  }

  /**
   * 根据路段起点和终点找到一条路段。
   * @param begin
   * @param end
   * @return
   */
  public RoadSegs getRoadsegsByBeginEnd(int begin,int end){
    return roadSegsMapper.getRoadsegsByBeginEnd(begin,end);
  }

  /**
   * 更新某一条路段信息
   * @param roadsegs
   * @return
   */
  @Override
  public int updateRoadsegs(RoadSegs roadsegs) {
    return roadSegsMapper.updateByPrimaryKey(roadsegs);
  }


}
