package com.million.project.service.serviceimpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.million.project.mapper.HighRoadsMapper;
import com.million.project.pojo.HighRoads;
import com.million.project.service.HighRoadsService;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 高速公路功能实现
 *
 * @Author: zhencym
 * @DATE: 2022/11/3
 */
@Service
public class HighRoadsServiceImpl implements HighRoadsService {

    @Autowired
    HighRoadsMapper highRoadsMapper;

    /**
     * 分页获得所有高速公路信息
     * @param pageNum
     * @param pageSize
     * @return
     */
    @Override
    public PageInfo<HighRoads> getHighRoads(int pageNum, int pageSize) {
        PageHelper.startPage(pageNum, pageSize); //设置分页
        List<HighRoads> highRoadsList = highRoadsMapper.selectAllDetail();
        PageInfo<HighRoads> pageInfo = new PageInfo<>(highRoadsList); //将数据交给分页helper
        return pageInfo;
    }

    /**
     * 根据id返回某一条高速公路
     * @param id
     * @return
     */
    @Override
    public HighRoads selectHighRoads(int id) {
        return highRoadsMapper.selectByPrimaryKeyDetail(id);
    }
}
