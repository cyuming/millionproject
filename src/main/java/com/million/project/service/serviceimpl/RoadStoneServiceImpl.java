package com.million.project.service.serviceimpl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.million.project.mapper.RoadStoneMapper;
import com.million.project.pojo.RoadStone;
import com.million.project.service.RoadStoneService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 路线结点实现类
 * @Author: zhencym
 * @DATE: 2022/11/10
 */
@Service
public class RoadStoneServiceImpl implements RoadStoneService {

  @Autowired
  RoadStoneMapper roadStoneMapper;

  /**
   * 分页得到所有节点信息
   * @param pageNum
   * @param pageSize
   * @return
   */
  @Override
  public PageInfo<RoadStone> getRoadStone(int pageNum, int pageSize) {
    PageHelper.startPage(pageNum, pageSize); //设置分页
    List<RoadStone> roadStoneList = roadStoneMapper.selectAllDetail();
    PageInfo<RoadStone> pageInfo = new PageInfo<>(roadStoneList); //将数据交给分页helper
    return pageInfo;
  }

  /**
   * 根据id查找节点详细信息
   * @param id
   * @return
   */
  @Override
  public RoadStone selectRoadStone(int id) {
    return roadStoneMapper.selectByPrimaryKeyDetail(id);
  }
}
