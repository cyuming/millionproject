package com.million.project.service.serviceimpl;

import com.million.project.mapper.RoadNodesMapper;
import com.million.project.pojo.*;
import com.million.project.utils.ResultMap;
import com.million.project.mapper.AuditLogsMapper;
import com.million.project.mapper.RoadApplyLogsMapper;
import com.million.project.service.AuditService;
import com.million.project.utils.Result;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuditServiceImpl implements AuditService {
    @Autowired
    RoadNodesMapper roadNodesMapper;
    @Autowired
    RoadApplyLogsMapper roadApplyLogsMapper;
    @Autowired
    AuditLogsMapper auditLogsMapper;

    /**
     * 根据id查询路线
     *
     * @param id
     * @return
     */
    @Override
    public Result getById(Integer id) {
        Result result = new Result();
        if (roadApplyLogsMapper.selectByPrimaryKey(id) != null) {
            result.setData(roadApplyLogsMapper.selectByPrimaryKey(id));
            result.setMessage("查找成功");
            result.setCode("1");
        } else {
            result.setData(roadApplyLogsMapper.selectByPrimaryKey(id));
            result.setMessage("失败");
            result.setCode("0");
        }
        return result;
    }

    /**
     * 分页查询路线信息
     *
     * @param pageNumber
     * @param listNumber
     * @return
     */
    @Override
    public ResultMap getPageApply(Integer pageNumber, Integer listNumber) {
        List<ApplyRoads> applyRoads = roadApplyLogsMapper.selectAllPage();
        List<ApplyRoadsMap> applyRoadsMap =new ArrayList<>();
        System.out.println(applyRoads);
        for (int i=0;i<applyRoads.size();i++){
            System.out.println(applyRoads.get(i));
            if(applyRoads.get(i).getSuggestRoad()==null){
                applyRoads.get(i).setSuggestRoad(applyRoads.get(i).getInitRoad());
            }
            else{
                applyRoads.get(i).setInitRoad(applyRoads.get(i).getSuggestRoad());
            }

            ApplyRoadsMap applyRoadsMap1 =new ApplyRoadsMap();
            applyRoadsMap1.setRoadBegin(applyRoads.get(i).getRoadBegin());
            applyRoadsMap1.setRoadEnd(applyRoads.get(i).getRoadEnd());
            applyRoadsMap1.setRoadApplyLogsId(applyRoads.get(i).getRoadApplyLogsId());
            applyRoadsMap1.setRealName(applyRoads.get(i).getRealName());
            applyRoadsMap1.setIdCard(applyRoads.get(i).getIdCard());
            applyRoadsMap1.setApplyTimes(applyRoads.get(i).getApplyTimes());
            applyRoadsMap1.setCargoType(applyRoads.get(i).getCargoType());
            applyRoadsMap1.setSuggestRoad(applyRoads.get(i).getSuggestRoad());
            applyRoadsMap1.setWhetherUseAdvice(applyRoads.get(i).getWhetherUseAdvice());
            applyRoadsMap1.setEndDate(applyRoads.get(i).getEndDate());
            applyRoadsMap1.setBeginDate(applyRoads.get(i).getBeginDate());
            applyRoadsMap1.setApplyDate(applyRoads.get(i).getApplyDate());
            applyRoadsMap1.setPassCities(applyRoads.get(i).getPassCities());
            applyRoadsMap1.setInitRoad(applyRoads.get(i).getInitRoad());
            applyRoadsMap1.setCargoWeight(applyRoads.get(i).getCargoWeight());
            applyRoadsMap1.setTotalWeight(applyRoads.get(i).getTotalWeight());
            applyRoadsMap1.setTruckAxes(applyRoads.get(i).getTruckAxes());
            applyRoadsMap1.setTruckTyres(applyRoads.get(i).getTruckTyres());
            applyRoadsMap1.setCurrentCondition(applyRoads.get(i).getCurrentCondition());
            applyRoadsMap1.setPaths(applyRoads.get(i).getPaths()); // 将策略也加入其中
            applyRoadsMap.add(applyRoadsMap1);
        }
        // for循环填充起始点、终点的经纬度
        for (int i=0;i<applyRoadsMap.size();i++){
            Double d1=roadNodesMapper.getLong(applyRoadsMap.get(i).getRoadBegin());

            Double d2=roadNodesMapper.getLat(applyRoadsMap.get(i).getRoadBegin());

            Double d3=roadNodesMapper.getLong(applyRoadsMap.get(i).getRoadEnd());
            Double d4=roadNodesMapper.getLat(applyRoadsMap.get(i).getRoadEnd());
            Double []D1= new Double[]{d1,d2};
            Double []D2= new Double[]{d3,d4};


            applyRoadsMap.get(i).setBeginLnglat(D1);
            applyRoadsMap.get(i).setEndLnglat(D2);
        }
        System.out.println(applyRoadsMap);
//        System.out.println(applyRoads.get(0).getApplyDate());

        int num = applyRoads.size();
        int paNumber = (int) Math.ceil((double) num / listNumber);
        ResultMap resultMap = new ResultMap();
        if (applyRoads != null) {
            List<ApplyRoadsMap> applyRoads1 = new ArrayList<>();
            for (int i = 0; i < listNumber; i++) {

                if ((pageNumber - 1) * listNumber + i + 1 <= num) {
                    applyRoads1.add(applyRoadsMap.get((pageNumber - 1) * listNumber + i));
                }
            }
            HashMap<String, Object> HM = new HashMap<>();
            HM.put("data", applyRoads1);
            HM.put("number", num);
            resultMap.setData(HM);
            resultMap.setCode("1");
            resultMap.setMessage("查询成功");
        } else {
            resultMap.setCode("0");
            resultMap.setMessage("查询失败");
        }
        return resultMap;
    }

    /**
     * 进行审核路线
     *
     * @param auditLogs
     * @return
     */
    @Override
    public int CheckRoads(AuditLogs auditLogs) {
        int number1 = roadApplyLogsMapper.checkRoads(auditLogs.getRoadApplyId());
        Date date = new Date();
        System.out.println(date);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dateNowStr = sdf.format(date);
        int number2 = auditLogsMapper.checkRoads(auditLogs.getRoadApplyId(),
                auditLogs.getAuditorUsername(), auditLogs.getWhetherPass(),
                auditLogs.getWhetherPassReason(), dateNowStr);
        if (number1 != 0 && number2 != 0) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * 得到审核记录表
     *
     * @param pageNumber
     * @param listNumber
     * @param username
     * @param type
     * @return
     */
    @Override
    public ResultMap getPageApplyLogs(Integer pageNumber, Integer listNumber, String username,
                                      Integer type) {
        if (type == 0) { //全部
            //获取所有审核记录
            List<AuditLogsPage> applyRoadLogs = roadApplyLogsMapper.selectAllPageLogs(username);
            List<AuditLogsPageMap> auditLogsPageMapList=new ArrayList<>();
            for (int i=0;i<applyRoadLogs.size();i++){
//                applyRoadLogs.get(i).setInitRoad(applyRoadLogs.get(i).getSuggestRoad());
                if(applyRoadLogs.get(i).getSuggestRoad()==null){
                    applyRoadLogs.get(i).setSuggestRoad(applyRoadLogs.get(i).getInitRoad());
                }
                else{
                    applyRoadLogs.get(i).setInitRoad(applyRoadLogs.get(i).getSuggestRoad());
                }
                AuditLogsPageMap auditLogsPageMap=new AuditLogsPageMap();
                auditLogsPageMap.setWhetherUseAdvice(applyRoadLogs.get(i).getWhetherUseAdvice());
                auditLogsPageMap.setRoadBegin(applyRoadLogs.get(i).getRoadBegin());
                auditLogsPageMap.setRoadEnd(applyRoadLogs.get(i).getRoadEnd());
                auditLogsPageMap.setSuggestRoad(applyRoadLogs.get(i).getSuggestRoad());
                auditLogsPageMap.setWhetherPassReason(applyRoadLogs.get(i).getWhetherPassReason());
                auditLogsPageMap.setAuditLogsId(applyRoadLogs.get(i).getAuditLogsId());
                auditLogsPageMap.setRealName(applyRoadLogs.get(i).getRealName());
                auditLogsPageMap.setUsername(applyRoadLogs.get(i).getUsername());
                auditLogsPageMap.setAuditDate(applyRoadLogs.get(i).getAuditDate());
                auditLogsPageMap.setAuditorRealName(applyRoadLogs.get(i).getAuditorRealName());
                auditLogsPageMap.setWhetherPass(applyRoadLogs.get(i).getWhetherPass());
                auditLogsPageMap.setWhetherPassReason(applyRoadLogs.get(i).getWhetherPassReason());
                auditLogsPageMap.setAuditorUsername(applyRoadLogs.get(i).getAuditorUsername());
                auditLogsPageMap.setIdCard(applyRoadLogs.get(i).getIdCard());
                auditLogsPageMap.setApplyTimes(applyRoadLogs.get(i).getApplyTimes());
                auditLogsPageMap.setCargoType(applyRoadLogs.get(i).getCargoType());
                auditLogsPageMap.setEndDate(applyRoadLogs.get(i).getEndDate());
                auditLogsPageMap.setBeginDate(applyRoadLogs.get(i).getBeginDate());
                auditLogsPageMap.setApplyDate(applyRoadLogs.get(i).getApplyDate());
                auditLogsPageMap.setPassCities(applyRoadLogs.get(i).getPassCities());
                auditLogsPageMap.setInitRoad(applyRoadLogs.get(i).getInitRoad());
                auditLogsPageMap.setCargoWeight(applyRoadLogs.get(i).getCargoWeight());
                auditLogsPageMap.setTotalWeight(applyRoadLogs.get(i).getTotalWeight());
                auditLogsPageMap.setTruckAxes(applyRoadLogs.get(i).getTruckAxes());
                auditLogsPageMap.setTruckTyres(applyRoadLogs.get(i).getTruckTyres());
                auditLogsPageMap.setCurrentCondition(applyRoadLogs.get(i).getCurrentCondition());
                auditLogsPageMapList.add(auditLogsPageMap);
            }
            for(int i=0;i<auditLogsPageMapList.size();i++){
                Double d1=roadNodesMapper.getLong(auditLogsPageMapList.get(i).getRoadBegin());
                Double d2=roadNodesMapper.getLat(auditLogsPageMapList.get(i).getRoadBegin());
                Double d3=roadNodesMapper.getLong(auditLogsPageMapList.get(i).getRoadEnd());
                Double d4=roadNodesMapper.getLat(auditLogsPageMapList.get(i).getRoadEnd());
                Double []D1= new Double[]{d1,d2};
                Double []D2= new Double[]{d3,d4};

                auditLogsPageMapList.get(i).setBeginLnglat(D1);
                auditLogsPageMapList.get(i).setEndLnglat(D2);
            }
            System.out.println(applyRoadLogs);
            int number = applyRoadLogs.size();
            int paNumber = (int) Math.ceil((double) number / listNumber);
            ResultMap resultMap = new ResultMap();
            if (applyRoadLogs != null) {
                List<AuditLogsPageMap> applyRoads1 = new ArrayList<>();
                for (int i = 0; i < listNumber; i++) {
                    if ((pageNumber - 1) * listNumber + i + 1 <= number) {
                        applyRoads1.add(auditLogsPageMapList.get((pageNumber - 1) * listNumber + i));
                    }
                }
                HashMap<String, Object> HM = new HashMap<>();
                HM.put("data", applyRoads1);
                HM.put("number", number);
                resultMap.setData(HM);
                resultMap.setCode("1");
                resultMap.setMessage("查询成功");
            } else {
                resultMap.setCode("0");
                resultMap.setMessage("查询失败");
            }
            return resultMap;
        } else if (type == 1) { //通过
            System.out.println(pageNumber);
            List<AuditLogsPage> applyRoadLogs = roadApplyLogsMapper.selectPassPageLogs(username);
            List<AuditLogsPageMap> auditLogsPageMapList=new ArrayList<>();
            for (int i=0;i<applyRoadLogs.size();i++){
                if(applyRoadLogs.get(i).getSuggestRoad()==null){
                    applyRoadLogs.get(i).setSuggestRoad(applyRoadLogs.get(i).getInitRoad());
                }
                else{
                    applyRoadLogs.get(i).setInitRoad(applyRoadLogs.get(i).getSuggestRoad());
                }
                AuditLogsPageMap auditLogsPageMap=new AuditLogsPageMap();
                auditLogsPageMap.setWhetherUseAdvice(applyRoadLogs.get(i).getWhetherUseAdvice());
                auditLogsPageMap.setRoadBegin(applyRoadLogs.get(i).getRoadBegin());
                auditLogsPageMap.setRoadEnd(applyRoadLogs.get(i).getRoadEnd());
                auditLogsPageMap.setSuggestRoad(applyRoadLogs.get(i).getSuggestRoad());
                auditLogsPageMap.setWhetherPassReason(applyRoadLogs.get(i).getWhetherPassReason());
                auditLogsPageMap.setAuditLogsId(applyRoadLogs.get(i).getAuditLogsId());
                auditLogsPageMap.setRealName(applyRoadLogs.get(i).getRealName());
                auditLogsPageMap.setUsername(applyRoadLogs.get(i).getUsername());
                auditLogsPageMap.setAuditDate(applyRoadLogs.get(i).getAuditDate());
                auditLogsPageMap.setAuditorRealName(applyRoadLogs.get(i).getAuditorRealName());
                auditLogsPageMap.setWhetherPass(applyRoadLogs.get(i).getWhetherPass());
                auditLogsPageMap.setWhetherPassReason(applyRoadLogs.get(i).getWhetherPassReason());
                auditLogsPageMap.setAuditorUsername(applyRoadLogs.get(i).getAuditorUsername());
                auditLogsPageMap.setIdCard(applyRoadLogs.get(i).getIdCard());
                auditLogsPageMap.setApplyTimes(applyRoadLogs.get(i).getApplyTimes());
                auditLogsPageMap.setCargoType(applyRoadLogs.get(i).getCargoType());
                auditLogsPageMap.setEndDate(applyRoadLogs.get(i).getEndDate());
                auditLogsPageMap.setBeginDate(applyRoadLogs.get(i).getBeginDate());
                auditLogsPageMap.setApplyDate(applyRoadLogs.get(i).getApplyDate());
                auditLogsPageMap.setPassCities(applyRoadLogs.get(i).getPassCities());
                auditLogsPageMap.setInitRoad(applyRoadLogs.get(i).getInitRoad());
                auditLogsPageMap.setCargoWeight(applyRoadLogs.get(i).getCargoWeight());
                auditLogsPageMap.setTotalWeight(applyRoadLogs.get(i).getTotalWeight());
                auditLogsPageMap.setTruckAxes(applyRoadLogs.get(i).getTruckAxes());
                auditLogsPageMap.setTruckTyres(applyRoadLogs.get(i).getTruckTyres());
                auditLogsPageMap.setCurrentCondition(applyRoadLogs.get(i).getCurrentCondition());
                auditLogsPageMapList.add(auditLogsPageMap);
            }
            for(int i=0;i<auditLogsPageMapList.size();i++){
                Double d1=roadNodesMapper.getLong(auditLogsPageMapList.get(i).getRoadBegin());
                Double d2=roadNodesMapper.getLat(auditLogsPageMapList.get(i).getRoadBegin());
                Double d3=roadNodesMapper.getLong(auditLogsPageMapList.get(i).getRoadEnd());
                Double d4=roadNodesMapper.getLat(auditLogsPageMapList.get(i).getRoadEnd());
                Double []D1= new Double[]{d1,d2};
                Double []D2= new Double[]{d3,d4};
                auditLogsPageMapList.get(i).setBeginLnglat(D1);
                auditLogsPageMapList.get(i).setEndLnglat(D2);
            }
            int num = applyRoadLogs.size();
            int paNumber = (int) Math.ceil((double) num / listNumber);
            ResultMap resultMap = new ResultMap();
            if (applyRoadLogs != null) {
                List<AuditLogsPageMap> applyRoads1 = new ArrayList<>();
                for (int i = 0; i < listNumber; i++) {
                    if ((pageNumber - 1) * listNumber + i + 1 <= num) {
                        applyRoads1.add(auditLogsPageMapList.get((pageNumber - 1) * listNumber + i));
                    }
                }
                HashMap<String, Object> HM = new HashMap<>();
                HM.put("data", applyRoads1);
                HM.put("number", num);
                resultMap.setData(HM);
                resultMap.setCode("1");
                resultMap.setMessage("查询成功");
            } else {
                resultMap.setCode("0");
                resultMap.setMessage("查询失败");
            }
            return resultMap;
        } else if(type==2){  // 未通过
            System.out.println("*******************");
            List<AuditLogsPage> applyRoadLogs = roadApplyLogsMapper.selectNotPassPageLogs(username);
            List<AuditLogsPageMap> auditLogsPageMapList=new ArrayList<>();
            System.out.println("--------------------------------");
            for (int i=0;i<applyRoadLogs.size();i++){
                if(applyRoadLogs.get(i).getSuggestRoad()==null){
                    applyRoadLogs.get(i).setSuggestRoad(applyRoadLogs.get(i).getInitRoad());
                }
                else{
                    applyRoadLogs.get(i).setInitRoad(applyRoadLogs.get(i).getSuggestRoad());
                }
                AuditLogsPageMap auditLogsPageMap=new AuditLogsPageMap();
                auditLogsPageMap.setWhetherUseAdvice(applyRoadLogs.get(i).getWhetherUseAdvice());
                auditLogsPageMap.setRoadBegin(applyRoadLogs.get(i).getRoadBegin());
                auditLogsPageMap.setRoadEnd(applyRoadLogs.get(i).getRoadEnd());
                auditLogsPageMap.setSuggestRoad(applyRoadLogs.get(i).getSuggestRoad());
                auditLogsPageMap.setWhetherPassReason(applyRoadLogs.get(i).getWhetherPassReason());
                auditLogsPageMap.setAuditLogsId(applyRoadLogs.get(i).getAuditLogsId());
                auditLogsPageMap.setRealName(applyRoadLogs.get(i).getRealName());
                auditLogsPageMap.setUsername(applyRoadLogs.get(i).getUsername());
                auditLogsPageMap.setAuditDate(applyRoadLogs.get(i).getAuditDate());
                auditLogsPageMap.setAuditorRealName(applyRoadLogs.get(i).getAuditorRealName());
                auditLogsPageMap.setWhetherPass(applyRoadLogs.get(i).getWhetherPass());
                auditLogsPageMap.setWhetherPassReason(applyRoadLogs.get(i).getWhetherPassReason());
                auditLogsPageMap.setAuditorUsername(applyRoadLogs.get(i).getAuditorUsername());
                auditLogsPageMap.setIdCard(applyRoadLogs.get(i).getIdCard());
                auditLogsPageMap.setApplyTimes(applyRoadLogs.get(i).getApplyTimes());
                auditLogsPageMap.setCargoType(applyRoadLogs.get(i).getCargoType());
                auditLogsPageMap.setEndDate(applyRoadLogs.get(i).getEndDate());
                auditLogsPageMap.setBeginDate(applyRoadLogs.get(i).getBeginDate());
                auditLogsPageMap.setApplyDate(applyRoadLogs.get(i).getApplyDate());
                auditLogsPageMap.setPassCities(applyRoadLogs.get(i).getPassCities());
                auditLogsPageMap.setInitRoad(applyRoadLogs.get(i).getInitRoad());
                auditLogsPageMap.setCargoWeight(applyRoadLogs.get(i).getCargoWeight());
                auditLogsPageMap.setTotalWeight(applyRoadLogs.get(i).getTotalWeight());
                auditLogsPageMap.setTruckAxes(applyRoadLogs.get(i).getTruckAxes());
                auditLogsPageMap.setTruckTyres(applyRoadLogs.get(i).getTruckTyres());
                auditLogsPageMap.setCurrentCondition(applyRoadLogs.get(i).getCurrentCondition());
                auditLogsPageMapList.add(auditLogsPageMap);
            }
            for(int i=0;i<auditLogsPageMapList.size();i++){
                Double d1=roadNodesMapper.getLong(auditLogsPageMapList.get(i).getRoadBegin());
                Double d2=roadNodesMapper.getLat(auditLogsPageMapList.get(i).getRoadBegin());
                Double d3=roadNodesMapper.getLong(auditLogsPageMapList.get(i).getRoadEnd());
                Double d4=roadNodesMapper.getLat(auditLogsPageMapList.get(i).getRoadEnd());
                Double []D1= new Double[]{d1,d2};
                Double []D2= new Double[]{d3,d4};

                auditLogsPageMapList.get(i).setBeginLnglat(D1);
                auditLogsPageMapList.get(i).setEndLnglat(D2);
            }
            int num = applyRoadLogs.size();
            int paNumber = (int) Math.ceil((double) num / listNumber);
            ResultMap resultMap = new ResultMap();
            if (applyRoadLogs != null) {
                List<AuditLogsPageMap> applyRoads1 = new ArrayList<>();
                for (int i = 0; i < listNumber; i++) {
                    if ((pageNumber - 1) * listNumber + i + 1 <= num) {
                        applyRoads1.add(auditLogsPageMapList.get((pageNumber - 1) * listNumber + i));
                    }
                }
                HashMap<String, Object> HM = new HashMap<>();
                HM.put("data", applyRoads1);
                HM.put("number", num);
                resultMap.setData(HM);
                resultMap.setCode("1");
                resultMap.setMessage("查询成功");
            } else {
                resultMap.setCode("0");
                resultMap.setMessage("查询失败");
            }
            return resultMap;
        }
        else {  // 错误
            return null;
        }
    }

    /**
     * 删除审核日志
     *
     * @param auditLogsId
     * @return
     */
    @Override
    public int deleteLogs(Integer auditLogsId) {
        if (auditLogsMapper.deleteLogs(auditLogsId) != 0) {
            return 1;
        }
        return 0;
    }
}
