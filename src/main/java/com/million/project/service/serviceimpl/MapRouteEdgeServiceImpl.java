package com.million.project.service.serviceimpl;

import com.million.project.mapper.MapRouteEdgeMapper;
import com.million.project.pojo.MapRouteEdge;
import com.million.project.service.MapRouteEdgeService;
import com.million.project.service.MapRouteNodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MapRouteEdgeServiceImpl implements MapRouteEdgeService {

    @Autowired
    private MapRouteEdgeMapper edgeMapper;
    @Override
    public List<MapRouteEdge> getAllEdges() {
        return edgeMapper.getAllEdge();
    }
}
