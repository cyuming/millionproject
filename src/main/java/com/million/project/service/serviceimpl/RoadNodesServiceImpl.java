package com.million.project.service.serviceimpl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.million.project.mapper.RoadNodesMapper;
import com.million.project.pojo.RoadNodes;
import com.million.project.service.RoadNodesService;

import com.million.project.vo.RoadNodeVO;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @Author: zhencym
 * @DATE: 2022/11/10
 */
@Service
public class RoadNodesServiceImpl implements RoadNodesService {

  @Autowired
  RoadNodesMapper roadNodesMapper;

  /**
   * 分页获得所有收费站结点
   *
   * @param pageNum
   * @param pageSize
   * @return
   */
  public PageInfo<RoadNodeVO> getRoadNodes(int pageNum, int pageSize) {
    PageHelper.startPage(pageNum, pageSize); // 设置分页
    List<RoadNodeVO> roadNodesList = roadNodesMapper.selectAllDetail();
    PageInfo<RoadNodeVO> pageInfo = new PageInfo<>(roadNodesList); // 将数据交给分页helper
    return pageInfo;
  }

  /**
   * 根据id获得收费站结点全部信息
   *
   * @param id
   * @return
   */
  @Override
  public RoadNodes selectRoadNode(int id) {
    return roadNodesMapper.selectByPrimaryKey(id);
  }

  /**
   * 根据id获得收费站结点部分信息
   *
   * @param id
   * @return
   */
  @Override
  public RoadNodes selectByPrimaryKey(int id) {
    return roadNodesMapper.selectByPrimaryKey(id);
  }

  /**
   * 根据收费站名字获得收费站节点的全部信息
   *
   * @param name
   * @return
   */
  @Override
  public int selectByName(String name) {
    return roadNodesMapper.selectByName(name);
  }

  /**
   * 插入收费站节点数据，不要重复运行重复插入
   *
   */
  @Transactional
  @Override
  public int insertRoadNodeByFile() throws IOException {
    JSONArray jsonArray = JSONArray.parseObject(new FileInputStream(
        "src/main/resources/static/处理后的收费站数据.txt"), JSONArray.class);
    for (int i = 0; i < jsonArray.size(); i++) {
      // System.out.println(jsonArray.get(i));
      JSONObject jsonObject = JSONObject.parseObject(jsonArray.get(i).toString(), JSONObject.class);
      String cityname = (String) jsonObject.get("cityname");
      String latitude = (String) jsonObject.get("latitude");
      String longitude = (String) jsonObject.get("longitude");
      String name = (String) jsonObject.get("name");
      RoadNodes roadNodes = new RoadNodes(name, "收费站", (float) 20, (float) 10, longitude,
          latitude, cityname, true);
      roadNodesMapper.insertKey(roadNodes);
    }
    return 1;

}



}
