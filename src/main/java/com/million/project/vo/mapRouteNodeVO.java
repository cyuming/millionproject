package com.million.project.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class mapRouteNodeVO {
    private String name; // 结点名
    private double[] position; // 经度、维度
    private String note; // 备注
    private String img; // 照片路径

    public void setPosition(double[] position) {
        this.position = position;
    }
}
