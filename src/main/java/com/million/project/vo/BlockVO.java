package com.million.project.vo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: zhencym
 * @DATE: 2023/2/10
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor

public class BlockVO {

  private Integer blockedSegId; //主键


  private Integer segId;  //阻塞的推荐路线id


  private String beginDate;


  private String endDate;


  private String direction;


  private String reason;
}
