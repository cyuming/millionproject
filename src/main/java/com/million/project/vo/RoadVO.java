package com.million.project.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class  RoadVO {

  private Integer roadsId;

  private String BeginName;

  private String EndName;

  private String suggestRoad;

  private String passCities;

  private Boolean roadStatus;

  private BlockVO blockVO; //阻塞信息


}