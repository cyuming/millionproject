package com.million.project.vo;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class RoadNodeVO {

  private Integer roadNodesId;

  private String nodeName;


  private String longitude;


  private String latitude;


  private String indexCities;


}