package com.million.project.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class mapRouteEdgeVO {
       String node1Name; // 结点1的名
       String node2Name; // 结点2的名
       String edgeLength; // 两节点之间的路线距离(KM)
       String edgeTime; // 走过该路段的平均时间
       int truckLimitType; // 该路线的限行车辆类型
    @Override
    public String toString() {
        return "mapRouteEdgeVO{" +
                "node1Name='" + node1Name + '\'' +
                ", node2Name='" + node2Name + '\'' +
                ", edgeLength='" + edgeLength + '\'' +
                ", edgeTime='" + edgeTime + '\'' +
                ", truckLimitType='" + truckLimitType + '\'' +
                '}';
    }

}
