package com.million.project.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.Date;

/**
 * 专门用来承接前端传过来的路线数据(mapRoute数据)
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class mapRouteVO {

    String name; // *路线名
    String area; // *地域
    String distance;// 距离
    String note; //备注
    String seasons; // *[适用季节]
    String paths;// *途径路径点(base64编码存储)
    String policy; // 路线策略
    Integer is_public; // 路线是否可用(0:不可用；1：可用)
    String time; // 该路线花费的时间(单位：分钟)
    String cost; // 该路线的过路费(单位：元)
    String road_type; // 路线类型
    String[] seasonArray; // 装着季节的String数组

    String  truckLimitType; // 车辆类型

    String edges; // 边数据,使用base64进行编码处理

    @Override
    public String toString() {
        return "mapRouteVO{" +
                "name='" + name + '\'' +
                ", area='" + area + '\'' +
                ", distance='" + distance + '\'' +
                ", note='" + note + '\'' +
                ", seasons='" + seasons + '\'' +
                ", paths='" + paths + '\'' +
                ", policy='" + policy + '\'' +
                ", is_public=" + is_public +
                ", time='" + time + '\'' +
                ", cost='" + cost + '\'' +
                ", road_type='" + road_type + '\'' +
                ", seasonArray=" + Arrays.toString(seasonArray) +
                ", truckLimitType='" + truckLimitType + '\'' +
                ", edges='" + edges + '\'' +
                '}';
    }
}
