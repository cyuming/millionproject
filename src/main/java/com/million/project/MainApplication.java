package com.million.project;

import java.net.InetAddress;
import java.net.UnknownHostException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

/**
 * 主程序类
 */
@SpringBootApplication
@Slf4j //简易日志
public class MainApplication {

  public static void main(String[] args) throws UnknownHostException {
    //启动程序，返回IOC容器
    ConfigurableApplicationContext run = SpringApplication.run(MainApplication.class, args);
    //返回项目启动的网址
    Environment env = run.getEnvironment();
    String ip = InetAddress.getLocalHost().getHostAddress();
    String port = env.getProperty("server.port");
    String path = env.getProperty("server.servlet.context-path");
    if (path == null) {
      path = "";
    }
    log.info("\n----------------------------------------------------------\n\t" +
        "Application  is running! Access URLs:\n\t" +
        "Local访问网址: \t\thttp://localhost:" + port + path + "\n\t" +
        "External访问网址: \thttp://" + ip + ":" + port + path + "\n\t" +
        "----------------------------------------------------------");
  }
}