package com.million.project.pojo;

public class RoadStone {


  private Integer roadNodesId;


  private Integer roadSegsId;


  private Integer highRoadsId;


  private String nodeName;


  private String longitude;


  private String latitude;


  private String indexCities;


  private String nodeComment;
  /**
   * 所属路段，高速公路的详细信息
   */
  private RoadSegs roadSegs;
  private HighRoads highRoads;

  public RoadSegs getRoadSegs() {
    return roadSegs;
  }

  public void setRoadSegs(RoadSegs roadSegs) {
    this.roadSegs = roadSegs;
  }

  public HighRoads getHighRoads() {
    return highRoads;
  }

  public void setHighRoads(HighRoads highRoads) {
    this.highRoads = highRoads;
  }


  public Integer getRoadNodesId() {
    return roadNodesId;
  }


  public void setRoadNodesId(Integer roadNodesId) {
    this.roadNodesId = roadNodesId;
  }


  public Integer getRoadSegsId() {
    return roadSegsId;
  }


  public void setRoadSegsId(Integer roadSegsId) {
    this.roadSegsId = roadSegsId;
  }


  public Integer getHighRoadsId() {
    return highRoadsId;
  }


  public void setHighRoadsId(Integer highRoadsId) {
    this.highRoadsId = highRoadsId;
  }


  public String getNodeName() {
    return nodeName;
  }


  public void setNodeName(String nodeName) {
    this.nodeName = nodeName == null ? null : nodeName.trim();
  }


  public String getLongitude() {
    return longitude;
  }


  public void setLongitude(String longitude) {
    this.longitude = longitude == null ? null : longitude.trim();
  }


  public String getLatitude() {
    return latitude;
  }


  public void setLatitude(String latitude) {
    this.latitude = latitude == null ? null : latitude.trim();
  }


  public String getIndexCities() {
    return indexCities;
  }


  public void setIndexCities(String indexCities) {
    this.indexCities = indexCities == null ? null : indexCities.trim();
  }


  public String getNodeComment() {
    return nodeComment;
  }

  public void setNodeComment(String nodeComment) {
    this.nodeComment = nodeComment == null ? null : nodeComment.trim();
  }
}