package com.million.project.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BlockedSegs {


  private Integer blockedSegId;


  private Integer segId;


  private Date beginDate;


  private Date endDate;


  private String direction;


  private String reason;

  private RoadSegs roadSegs;

  public RoadSegs getRoadSegs() {
    return roadSegs;
  }

  public void setRoadSegs(RoadSegs roadSegs) {
    this.roadSegs = roadSegs;
  }


  public Integer getBlockedSegId() {
    return blockedSegId;
  }


  public void setBlockedSegId(Integer blockedSegId) {
    this.blockedSegId = blockedSegId;
  }


  public Integer getSegId() {
    return segId;
  }


  public void setSegId(Integer segId) {
    this.segId = segId;
  }


  public String getBeginDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(beginDate);
  }
  public Date getBDate() {
    return beginDate;
  }
  public Date getEDate() {
    return endDate;
  }

  public void setBeginDate(String beginDate) {
    try {
      this.beginDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(beginDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }


  public String getEndDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endDate);

  }


  public void setEndDate(String endDate) {
    try {
      this.endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }


  public String getDirection() {
    return direction;
  }


  public void setDirection(String direction) {
    this.direction = direction == null ? null : direction.trim();
  }


  public String getReason() {
    return reason;
  }


  public void setReason(String reason) {
    this.reason = reason == null ? null : reason.trim();
  }

  public BlockedSegs(Integer blockedSegId, Integer segId, Date beginDate, Date endDate,
      String direction, String reason) {
    this.blockedSegId = blockedSegId;
    this.segId = segId;
    this.beginDate = beginDate;
    this.endDate = endDate;
    this.direction = direction;
    this.reason = reason;
  }

  public BlockedSegs() {
  }
}