package com.million.project.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.neo4j.ogm.annotation.NodeEntity;

@Data
@TableName("tbl_map_route_node") // MYSQL中的表
public class MapRouteNode {

    private int Id; // 结点id
    private String name; // 结点名
    private String longitude; // 经度
    private String latitude; // 维度
    private String note; // 备注
}
