package com.million.project.pojo;

import java.util.Arrays;

public class NodeLngLat {

    String[] beginLngLat;


    String[] endLngLat;

    public String[] getBeginLngLat() {
        return beginLngLat;
    }

    public void setBeginLngLat(String[] beginLngLat) {
        this.beginLngLat = beginLngLat;
    }

    public String[] getEndLngLat() {
        return endLngLat;
    }

    public void setEndLngLat(String[] endLngLat) {
        this.endLngLat = endLngLat;
    }

    @Override
    public String toString() {
        return "NodeLngLat{" +
                "beginLngLat=" + Arrays.toString(beginLngLat) +
                ", endLngLat=" + Arrays.toString(endLngLat) +
                '}';
    }
}
