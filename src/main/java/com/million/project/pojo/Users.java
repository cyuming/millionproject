package com.million.project.pojo;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * 用户信息实体，包括司机、审核员、管理员
 */
public class Users {

  private Integer userId;

  private String username;

  @Override
  public String toString() {
    return "Users{" +
        "userId=" + userId +
        ", username='" + username + '\'' +
        ", password='" + password + '\'' +
        ", realname='" + realName + '\'' +
        ", idCard='" + idCard + '\'' +
        ", registerDate=" + registerDate +
        ", applyTimes=" + applyTimes +
        ", userType='" + userType + '\'' +
        ", image=" + image +
        '}';
  }

  private String password;

  private String realName;

  private String idCard;

  private Date registerDate;

  private Integer applyTimes;

  private String userType;

  private String image;

  public Integer getUserId() {
    return userId;
  }

  public void setUserId(Integer userId) {
    this.userId = userId;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username == null ? null : username.trim();
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password == null ? null : password.trim();
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realname) {
    this.realName = realname == null ? null : realname.trim();
  }

  public String getIdCard() {
    return idCard;
  }

  public void setIdCard(String idCard) {
    this.idCard = idCard == null ? null : idCard.trim();
  }

  public String getRegisterDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(registerDate);
  }

  public void setRegisterDate(String registerDate) {
    try {
      this.registerDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(registerDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public Integer getApplyTimes() {
    return applyTimes;
  }

  public void setApplyTimes(Integer applyTimes) {
    this.applyTimes = applyTimes;
  }

  public String getUserType() {
    return userType;
  }

  public void setUserType(String userType) {
    this.userType = userType == null ? null : userType.trim();
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }
}