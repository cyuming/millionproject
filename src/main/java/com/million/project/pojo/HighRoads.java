package com.million.project.pojo;

public class HighRoads {


  private Integer highRoadsId;


  private String highRoadName;


  private Integer beginNodeId;


  private Integer endNodeId;


  private Float truckLength;


  private Float truckWidth;


  private Float truckHeight;


  private Float truckWeight;


  private Short truckAxes;


  private Short truckTyres;


  private String roadSegStatus;


  private String roadSegComment;

  /**
   * 将查询到的结点id 替换成名字
   */
  private RoadNodes beginNode;
  private RoadNodes endNode;

  public RoadNodes getBeginNode() {
    return beginNode;
  }

  public void setBeginNode(RoadNodes beginNode) {
    this.beginNode = beginNode;
  }

  public RoadNodes getEndNode() {
    return endNode;
  }

  public void setEndNode(RoadNodes endNode) {
    this.endNode = endNode;
  }


  public Integer getHighRoadsId() {
    return highRoadsId;
  }


  public void setHighRoadsId(Integer highRoadsId) {
    this.highRoadsId = highRoadsId;
  }


  public String getHighRoadName() {
    return highRoadName;
  }


  public void setHighRoadName(String highRoadName) {
    this.highRoadName = highRoadName == null ? null : highRoadName.trim();
  }


  public Integer getBeginNodeId() {
    return beginNodeId;
  }


  public void setBeginNodeId(Integer beginNodeId) {
    this.beginNodeId = beginNodeId;
  }


  public Integer getEndNodeId() {
    return endNodeId;
  }


  public void setEndNodeId(Integer endNodeId) {
    this.endNodeId = endNodeId;
  }


  public Float getTruckLength() {
    return truckLength;
  }


  public void setTruckLength(Float truckLength) {
    this.truckLength = truckLength;
  }


  public Float getTruckWidth() {
    return truckWidth;
  }


  public void setTruckWidth(Float truckWidth) {
    this.truckWidth = truckWidth;
  }


  public Float getTruckHeight() {
    return truckHeight;
  }


  public void setTruckHeight(Float truckHeight) {
    this.truckHeight = truckHeight;
  }


  public Float getTruckWeight() {
    return truckWeight;
  }


  public void setTruckWeight(Float truckWeight) {
    this.truckWeight = truckWeight;
  }


  public Short getTruckAxes() {
    return truckAxes;
  }


  public void setTruckAxes(Short truckAxes) {
    this.truckAxes = truckAxes;
  }


  public Short getTruckTyres() {
    return truckTyres;
  }


  public void setTruckTyres(Short truckTyres) {
    this.truckTyres = truckTyres;
  }


  public String getRoadSegStatus() {
    return roadSegStatus;
  }


  public void setRoadSegStatus(String roadSegStatus) {
    this.roadSegStatus = roadSegStatus == null ? null : roadSegStatus.trim();
  }


  public String getRoadSegComment() {
    return roadSegComment;
  }


  public void setRoadSegComment(String roadSegComment) {
    this.roadSegComment = roadSegComment == null ? null : roadSegComment.trim();
  }
}