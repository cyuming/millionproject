package com.million.project.pojo;

public class Roads {


  private Integer roadsId;


  private Integer roadBegin;


  private Integer roadEnd;


  private String suggestRoad;


  private Float totalMileage;


  private String passCities;


  private Float roadCost;


  private Float truckLength;


  private Float truckWidth;


  private Float truckHeight;


  private Float truckWeight;


  private Short truckAxes;


  private Short truckTyres;


  private Boolean roadStatus;


  private String roadComment;

  /**
   * 将查询到的结点id 替换成名字
   */
  private RoadNodes beginNode;
  private RoadNodes endNode;

  public RoadNodes getBeginNode() {
    return beginNode;
  }

  public void setBeginNode(RoadNodes beginNode) {
    this.beginNode = beginNode;
  }

  public RoadNodes getEndNode() {
    return endNode;
  }

  public void setEndNode(RoadNodes endNode) {
    this.endNode = endNode;
  }


  public Integer getRoadsId() {
    return roadsId;
  }


  public void setRoadsId(Integer roadsId) {
    this.roadsId = roadsId;
  }


  public Integer getRoadBegin() {
    return roadBegin;
  }


  public void setRoadBegin(Integer roadBegin) {
    this.roadBegin = roadBegin;
  }


  public Integer getRoadEnd() {
    return roadEnd;
  }


  public void setRoadEnd(Integer roadEnd) {
    this.roadEnd = roadEnd;
  }


  public String getSuggestRoad() {
    return suggestRoad;
  }


  public void setSuggestRoad(String suggestRoad) {
    this.suggestRoad = suggestRoad == null ? null : suggestRoad.trim();
  }


  public Float getTotalMileage() {
    return totalMileage;
  }


  public void setTotalMileage(Float totalMileage) {
    this.totalMileage = totalMileage;
  }


  public String getPassCities() {
    return passCities;
  }


  public void setPassCities(String passCities) {
    this.passCities = passCities == null ? null : passCities.trim();
  }


  public Float getRoadCost() {
    return roadCost;
  }


  public void setRoadCost(Float roadCost) {
    this.roadCost = roadCost;
  }


  public Float getTruckLength() {
    return truckLength;
  }


  public void setTruckLength(Float truckLength) {
    this.truckLength = truckLength;
  }


  public Float getTruckWidth() {
    return truckWidth;
  }


  public void setTruckWidth(Float truckWidth) {
    this.truckWidth = truckWidth;
  }


  public Float getTruckHeight() {
    return truckHeight;
  }


  public void setTruckHeight(Float truckHeight) {
    this.truckHeight = truckHeight;
  }


  public Float getTruckWeight() {
    return truckWeight;
  }


  public void setTruckWeight(Float truckWeight) {
    this.truckWeight = truckWeight;
  }


  public Short getTruckAxes() {
    return truckAxes;
  }


  public void setTruckAxes(Short truckAxes) {
    this.truckAxes = truckAxes;
  }


  public Short getTruckTyres() {
    return truckTyres;
  }


  public void setTruckTyres(Short truckTyres) {
    this.truckTyres = truckTyres;
  }


  public Boolean getRoadStatus() {
    return roadStatus;
  }


  public void setRoadStatus(Boolean roadStatus) {
    this.roadStatus = roadStatus;
  }


  public String getRoadComment() {
    return roadComment;
  }


  public void setRoadComment(String roadComment) {
    this.roadComment = roadComment == null ? null : roadComment.trim();
  }
}