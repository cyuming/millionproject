package com.million.project.pojo;

public class RoadNodes {


  private Integer roadNodesId;


  private Integer roadSegsId;


  private Integer highRoadsId;


  private String nodeName;


  private String nodeType;


  private Float truckHeight;


  private Float truckWidth;


  private String longitude;


  private String latitude;


  private String indexCities;


  private Boolean nodeState;


  private String nodeComment;

  public RoadNodes(){

  }

  public RoadNodes(String nodeName, String nodeType, Float truckHeight, Float truckWidth,
      String longitude, String latitude, String indexCities, Boolean nodeState) {
    this.nodeName = nodeName;
    this.nodeType = nodeType;
    this.truckHeight = truckHeight;
    this.truckWidth = truckWidth;
    this.longitude = longitude;
    this.latitude = latitude;
    this.indexCities = indexCities;
    this.nodeState = nodeState;
  }
  /**
   * 结点所属路段，高速公路信息
   */
  private RoadSegs roadSegs;
  private HighRoads highRoads;

  public RoadSegs getRoadSegs() {
    return roadSegs;
  }

  public void setRoadSegs(RoadSegs roadSegs) {
    this.roadSegs = roadSegs;
  }

  public HighRoads getHighRoads() {
    return highRoads;
  }

  public void setHighRoads(HighRoads highRoads) {
    this.highRoads = highRoads;
  }


  public Integer getRoadNodesId() {
    return roadNodesId;
  }


  public void setRoadNodesId(Integer roadNodesId) {
    this.roadNodesId = roadNodesId;
  }


  public Integer getRoadSegsId() {
    return roadSegsId;
  }


  public void setRoadSegsId(Integer roadSegsId) {
    this.roadSegsId = roadSegsId;
  }


  public Integer getHighRoadsId() {
    return highRoadsId;
  }


  public void setHighRoadsId(Integer highRoadsId) {
    this.highRoadsId = highRoadsId;
  }


  public String getNodeName() {
    return nodeName;
  }


  public void setNodeName(String nodeName) {
    this.nodeName = nodeName == null ? null : nodeName.trim();
  }


  public String getNodeType() {
    return nodeType;
  }


  public void setNodeType(String nodeType) {
    this.nodeType = nodeType;
  }


  public Float getTruckHeight() {
    return truckHeight;
  }


  public void setTruckHeight(Float truckHeight) {
    this.truckHeight = truckHeight;
  }


  public Float getTruckWidth() {
    return truckWidth;
  }


  public void setTruckWidth(Float truckWidth) {
    this.truckWidth = truckWidth;
  }


  public String getLongitude() {
    return longitude;
  }


  public void setLongitude(String longitude) {
    this.longitude = longitude == null ? null : longitude.trim();
  }


  public String getLatitude() {
    return latitude;
  }


  public void setLatitude(String latitude) {
    this.latitude = latitude == null ? null : latitude.trim();
  }


  public String getIndexCities() {
    return indexCities;
  }


  public void setIndexCities(String indexCities) {
    this.indexCities = indexCities == null ? null : indexCities.trim();
  }


  public Boolean getNodeState() {
    return nodeState;
  }


  public void setNodeState(Boolean nodeState) {
    this.nodeState = nodeState;
  }


  public String getNodeComment() {
    return nodeComment;
  }


  public void setNodeComment(String nodeComment) {
    this.nodeComment = nodeComment == null ? null : nodeComment.trim();
  }


}