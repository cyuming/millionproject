package com.million.project.pojo;

public class HighroadNodes {


  private Integer highroadNodeId;


  private Integer highRoadsId;


  private Integer nodeId;

  public Integer getHighroadNodeId() {
    return highroadNodeId;
  }


  public void setHighroadNodeId(Integer highroadNodeId) {
    this.highroadNodeId = highroadNodeId;
  }


  public Integer getHighRoadsId() {
    return highRoadsId;
  }


  public void setHighRoadsId(Integer highRoadsId) {
    this.highRoadsId = highRoadsId;
  }


  public Integer getNodeId() {
    return nodeId;
  }


  public void setNodeId(Integer nodeId) {
    this.nodeId = nodeId;
  }
}