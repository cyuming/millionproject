package com.million.project.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

@Data
@TableName("tbl_trucks")
public class Trucks {

  @TableId(value = "trucks_id", type = IdType.ASSIGN_ID)
  private Integer trucksId;

  private Integer truckType; // 车辆类型

  private String truckLicense;

  private String userIdCard;

  private Float truckLength;

  private Float truckWidth;

  private Float truckHeight;

  private Float truckWeight;

  private Float truckCargoWeight;

  private Short truckAxes;

  private Short truckTyres;


  private Date initDate;

  private String currentCondition;

  public Integer getTrucksId() {
    return trucksId;
  }

  public void setTrucksId(Integer trucksId) {
    this.trucksId = trucksId;
  }

  public String getTruckLicense() {
    return truckLicense;
  }

  public void setTruckLicense(String truckLicense) {
    this.truckLicense = truckLicense == null ? null : truckLicense.trim();
  }

  public String getUserIdCard() {
    return userIdCard;
  }

  public void setUserIdCard(String userIdCard) {
    this.userIdCard = userIdCard == null ? null : userIdCard.trim();
  }

  public Float getTruckLength() {
    return truckLength;
  }

  public void setTruckLength(Float truckLength) {
    this.truckLength = truckLength;
  }

  public Float getTruckWidth() {
    return truckWidth;
  }

  public void setTruckWidth(Float truckWidth) {
    this.truckWidth = truckWidth;
  }

  public Float getTruckHeight() {
    return truckHeight;
  }

  public void setTruckHeight(Float truckHeight) {
    this.truckHeight = truckHeight;
  }

  public Float getTruckWeight() {
    return truckWeight;
  }

  public void setTruckWeight(Float truckWeight) {
    this.truckWeight = truckWeight;
  }

  public Float getTruckCargoWeight() {
    return truckCargoWeight;
  }

  public void setTruckCargoWeight(Float truckCargoWeight) {
    this.truckCargoWeight = truckCargoWeight;
  }

  public Short getTruckAxes() {
    return truckAxes;
  }

  public void setTruckAxes(Short truckAxes) {
    this.truckAxes = truckAxes;
  }

  public Short getTruckTyres() {
    return truckTyres;
  }

  public void setTruckTyres(Short truckTyres) {
    this.truckTyres = truckTyres;
  }

  public String getInitDate() {
    return new SimpleDateFormat(
        "yyyy-MM-dd HH:mm:ss").format(initDate);
  }

  public void setInitDate(String initDate) {
    try {
      this.initDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(initDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public String getCurrentCondition() {
    return currentCondition;
  }
  
  public void setCurrentCondition(String currentCondition) {
    this.currentCondition = currentCondition == null ? null : currentCondition.trim();
  }
}