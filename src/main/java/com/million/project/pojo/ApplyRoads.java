package com.million.project.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.Timestamp;
import net.sf.jsqlparser.expression.DateTimeLiteralExpression.DateTime;

public class ApplyRoads {
  public String getPaths() {
    return paths;
  }

  public void setPaths(String paths) {
    this.paths = paths;
  }

  private String paths;
  private Integer roadBegin;
  private Integer roadEnd;
  private Integer roadApplyLogsId;

  public Integer getRoadApplyLogsId() {
    return roadApplyLogsId;
  }

  public Integer getRoadBegin() {
    return roadBegin;
  }

  public void setRoadBegin(Integer roadBegin) {
    this.roadBegin = roadBegin;
  }

  public Integer getRoadEnd() {
    return roadEnd;
  }

  public void setRoadEnd(Integer roadEnd) {
    this.roadEnd = roadEnd;
  }

  public void setRoadApplyLogsId(Integer roadApplyLogsId) {
    this.roadApplyLogsId = roadApplyLogsId;
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  private String realName;
  private String username;
  private String idCard;
  private Integer applyTimes;
  private String cargoType;
  private String suggestRoad;
  private Integer whetherUseAdvice;

  public String getSuggestRoad() {
    return suggestRoad;
  }

  public void setSuggestRoad(String suggestRoad) {
    this.suggestRoad = suggestRoad;
  }

  public Integer getWhetherUseAdvice() {
    return whetherUseAdvice;
  }

  public void setWhetherUseAdvice(Integer whetherUseAdvice) {
    this.whetherUseAdvice = whetherUseAdvice;
  }

  public void setApplyDate(Date applyDate) {
    this.applyDate = applyDate;
  }

  private Date endDate;
  private Date beginDate;

  public String getBeginDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(beginDate);
  }

  public void setBeginDate(String beginDate) {
    try {
      this.beginDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(beginDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public String getEndDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endDate);

  }

  public void setEndDate(String endDate) {
    try {
      this.endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public String toString() {
    return "ApplyRoads{" +
            "roadBegin=" + roadBegin +
            ", roadEnd=" + roadEnd +
            ", roadApplyLogsId=" + roadApplyLogsId +
            ", realName='" + realName + '\'' +
            ", username='" + username + '\'' +
            ", idCard='" + idCard + '\'' +
            ", applyTimes=" + applyTimes +
            ", cargoType='" + cargoType + '\'' +
            ", suggestRoad='" + suggestRoad + '\'' +
            ", whetherUseAdvice=" + whetherUseAdvice +
            ", endDate=" + endDate +
            ", beginDate=" + beginDate +
            ", applyDate=" + applyDate +
            ", passCities='" + passCities + '\'' +
            ", initRoad='" + initRoad + '\'' +
            ", cargoWeight=" + cargoWeight +
            ", totalWeight=" + totalWeight +
            ", truckAxes=" + truckAxes +
            ", truckTyres=" + truckTyres +
            ", currentCondition='" + currentCondition + '\'' +
            '}';
  }


//  @Override
//  public String toString() {
//    return "ApplyRoads{" +
//            "roadApplyLogsId=" + roadApplyLogsId +
//            ", realName='" + realName + '\'' +
//            ", username='" + username + '\'' +
//            ", idCard='" + idCard + '\'' +
//            ", applyTimes=" + applyTimes +
//            ", cargoType='" + cargoType + '\'' +
//            ", suggestRoad='" + suggestRoad + '\'' +
//            ", whetherUseAdvice=" + whetherUseAdvice +
//            ", endDate=" + endDate +
//            ", beginDate=" + beginDate +
//            ", applyDate=" + applyDate +
//            ", passCities='" + passCities + '\'' +
//            ", initRoad='" + initRoad + '\'' +
//            ", cargoWeight=" + cargoWeight +
//            ", totalWeight=" + totalWeight +
//            ", truckAxes=" + truckAxes +
//            ", truckTyres=" + truckTyres +
//            ", currentCondition='" + currentCondition + '\'' +
//            '}';
//  }

  //  public Date getApplyDate() {
//    return applyDate;
//  }
  public String getApplyDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(applyDate);
  }

  //  public void setApplyDate(Date applyDate) {
//    this.applyDate = applyDate;
//  }
  public void setApplyDate(String applyDate) {
    try {
      this.applyDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(applyDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  private Date applyDate;
  private String passCities;
  private String initRoad;
  private Float cargoWeight;
  private Float totalWeight;
  private Short truckAxes;
  private Short truckTyres;
  private String currentCondition;


  public String getIdCard() {
    return idCard;
  }

  public void setIdCard(String idCard) {
    this.idCard = idCard;
  }

  public Integer getApplyTimes() {
    return applyTimes;
  }

  public void setApplyTimes(Integer applyTimes) {
    this.applyTimes = applyTimes;
  }

  public String getCargoType() {
    return cargoType;
  }

  public void setCargoType(String cargoType) {
    this.cargoType = cargoType;
  }

//  public Date getBeginDate() {
//    return beginDate;
//  }

  public void setBeginDate(Date beginDate) {
    this.beginDate = beginDate;
  }

//  public Date getEndDate() {
//    return endDate;
//  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getPassCities() {
    return passCities;
  }

  public void setPassCities(String passCities) {
    this.passCities = passCities;
  }

  public String getInitRoad() {
    return initRoad;
  }

  public void setInitRoad(String initRoad) {
    this.initRoad = initRoad;
  }

  public Float getCargoWeight() {
    return cargoWeight;
  }

  public void setCargoWeight(Float cargoWeight) {
    this.cargoWeight = cargoWeight;
  }

  public Float getTotalWeight() {
    return totalWeight;
  }

  public void setTotalWeight(Float totalWeight) {
    this.totalWeight = totalWeight;
  }

  public Short getTruckAxes() {
    return truckAxes;
  }

  public void setTruckAxes(Short truckAxes) {
    this.truckAxes = truckAxes;
  }

  public Short getTruckTyres() {
    return truckTyres;
  }

  public void setTruckTyres(Short truckTyres) {
    this.truckTyres = truckTyres;
  }

  public String getCurrentCondition() {
    return currentCondition;
  }

  public void setCurrentCondition(String currentCondition) {
    this.currentCondition = currentCondition;
  }
}
