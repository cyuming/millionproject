package com.million.project.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

@Data
@TableName("tbl_road_apply_logs")
public class RoadApplyLogs {


  @TableId(value = "road_apply_logs_id", type = IdType.AUTO)
  private Integer roadApplyLogsId;


  private String username;


  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
  private Date applyDate;


  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
  private Date beginDate;


  @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
  @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
  private Date endDate;


  private String passCities;


  private Integer roadBegin;

  @Override
  public String toString() {
    return "RoadApplyLogs{" +
            "roadApplyLogsId=" + roadApplyLogsId +
            ", username='" + username + '\'' +
            ", applyDate=" + applyDate +
            ", beginDate=" + beginDate +
            ", endDate=" + endDate +
            ", passCities='" + passCities + '\'' +
            ", roadBegin=" + roadBegin +
            ", paths='" + paths + '\'' +
            ", beginLngLat=" + Arrays.toString(beginLngLat) +
            ", endLngLat=" + Arrays.toString(endLngLat) +
            ", roadBeginName='" + roadBeginName + '\'' +
            ", roadEndName='" + roadEndName + '\'' +
            ", passOrNotPassReason='" + passOrNotPassReason + '\'' +
            ", roadEnd=" + roadEnd +
            ", suggestRoad='" + suggestRoad + '\'' +
            ", initRoad='" + initRoad + '\'' +
            ", truckLicense='" + truckLicense + '\'' +
            ", whetherUseAdvice=" + whetherUseAdvice +
            ", cargoType=" + cargoType +
            ", cargoWeight=" + cargoWeight +
            ", auditStatus='" + auditStatus + '\'' +
            ", deleted=" + deleted +
            ", isPass='" + isPass + '\'' +
            '}';
  }

  private String paths; // 途经点数据(Base64加密)

  @TableField(exist = false)
  String[] beginLngLat;

  @TableField(exist = false)
  String[] endLngLat;

  @TableField(exist = false)
  private String roadBeginName;

  @TableField(exist = false)
  private String roadEndName;

  @TableField(exist = false)
  private String passOrNotPassReason;



  public String getPassOrNotPassReason() {
    return passOrNotPassReason;
  }

  public void setPassOrNotPassReason(String passOrNotPassReason) {
    this.passOrNotPassReason = passOrNotPassReason;
  }
  public String[] getBeginLngLat() {
    return beginLngLat;
  }

  public void setBeginLngLat(String[] beginLngLat) {
    this.beginLngLat = beginLngLat;
  }

  public String[] getEndLngLat() {
    return endLngLat;
  }

  public void setEndLngLat(String[] endLngLat) {
    this.endLngLat = endLngLat;
  }

  public String getRoadBeginName() {
    return roadBeginName;
  }

  public void setRoadBeginName(String roadBeginName) {
    this.roadBeginName = roadBeginName;
  }

  public String getRoadEndName() {
    return roadEndName;
  }

  public void setRoadEndName(String roadEndName) {
    this.roadEndName = roadEndName;
  }


  private Integer roadEnd;


  private String suggestRoad;


  private String initRoad;


  private String truckLicense;


  private Integer whetherUseAdvice;


  private Short cargoType;


  private Float cargoWeight;


  private String auditStatus;

  private Integer deleted;

  public Integer getDeleted() {
    return deleted;
  }

  public void setDeleted(Integer deleted) {
    this.deleted = deleted;
  }

  @TableField(exist = false)
  private String isPass;

  public String getIsPass() {
    return isPass;
  }

  public void setIsPass(String isPass) {
    this.isPass = isPass;
  }


  public Integer getRoadApplyLogsId() {
    return roadApplyLogsId;
  }


  public void setRoadApplyLogsId(Integer roadApplyLogsId) {
    this.roadApplyLogsId = roadApplyLogsId;
  }


  public String getUsername() {
    return username;
  }


  public void setUsername(String username) {
    this.username = username == null ? null : username.trim();
  }


  public String getApplyDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(applyDate);
  }


  public void setApplyDate(String applyDate) {
    try {
      this.applyDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(applyDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }


  public String getBeginDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(beginDate);
  }


  public void setBeginDate(String beginDate) {
    try {
      this.beginDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(beginDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }


  public String getEndDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endDate);
  }


  public void setEndDate(String endDate) {
    try {
      this.endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }


  public String getPassCities() {
    return passCities;
  }


  public void setPassCities(String passCities) {
    this.passCities = passCities == null ? null : passCities.trim();
  }


  public Integer getRoadBegin() {
    return roadBegin;
  }


  public void setRoadBegin(Integer roadBegin) {
    this.roadBegin = roadBegin;
  }


  public Integer getRoadEnd() {
    return roadEnd;
  }


  public void setRoadEnd(Integer roadEnd) {
    this.roadEnd = roadEnd;
  }


  public String getSuggestRoad() {
    return suggestRoad;
  }


  public void setSuggestRoad(String suggestRoad) {
    this.suggestRoad = suggestRoad == null ? null : suggestRoad.trim();
  }


  public String getInitRoad() {
    return initRoad;
  }


  public void setInitRoad(String initRoad) {
    this.initRoad = initRoad == null ? null : initRoad.trim();
  }


  public String getTruckLicense() {
    return truckLicense;
  }


  public void setTruckLicense(String truckLicense) {
    this.truckLicense = truckLicense == null ? null : truckLicense.trim();
  }


  public Integer getWhetherUseAdvice() {
    return whetherUseAdvice;
  }


  public void setWhetherUseAdvice(Integer whetherUseAdvice) {
    this.whetherUseAdvice = whetherUseAdvice;
  }


  public Short getCargoType() {
    return cargoType;
  }


  public void setCargoType(Short cargoType) {
    this.cargoType = cargoType;
  }


  public Float getCargoWeight() {
    return cargoWeight;
  }


  public void setCargoWeight(Float cargoWeight) {
    this.cargoWeight = cargoWeight;
  }


  public String getAuditStatus() {
    return auditStatus;
  }


  public void setAuditStatus(String auditStatus) {
    this.auditStatus = auditStatus == null ? null : auditStatus.trim();
  }
}