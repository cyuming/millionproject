package com.million.project.pojo;

import com.baomidou.mybatisplus.annotation.TableName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@TableName("tbl_audit_logs")
public class AuditLogs {
  @Override
  public String toString() {
    return "AuditLogs{" +
            "auditLogsId=" + auditLogsId +
            ", roadApplyId=" + roadApplyId +
            ", auditorUsername='" + auditorUsername + '\'' +
            ", auditDate=" + auditDate +
            ", whetherPass=" + whetherPass +
            ", whetherPassReason='" + whetherPassReason + '\'' +
            ", deleted=" + deleted +
            '}';
  }

  private Integer auditLogsId;

  private Integer roadApplyId;


  private String auditorUsername;

  private Date auditDate;

  private Boolean whetherPass;

  private String whetherPassReason;

  private Integer deleted;

  public Integer getDeleted() {
    return deleted;
  }

  public void setDeleted(Integer deleted) {
    this.deleted = deleted;
  }

  public Integer getAuditLogsId() {
    return auditLogsId;
  }

  public void setAuditLogsId(Integer auditLogsId) {
    this.auditLogsId = auditLogsId;
  }

  public Integer getRoadApplyId() {
    return roadApplyId;
  }

  public void setRoadApplyId(Integer roadApplyId) {
    this.roadApplyId = roadApplyId;
  }

  public String getAuditorUsername() {
    return auditorUsername;
  }

  public void setAuditorUsername(String auditorUsername) {
    this.auditorUsername = auditorUsername == null ? null : auditorUsername.trim();
  }

  //  public Date getAuditDate() {
//    return auditDate;
//  }
  public String getAuditDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(auditDate);
  }


  //  public void setAuditDate(Date auditDate) {
//    this.auditDate = auditDate;
//  }
  public void setAuditDate(String auditDate) {

    try {
      this.auditDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(auditDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }

  }

  public Boolean getWhetherPass() {
    return whetherPass;
  }


  public void setWhetherPass(Boolean whetherPass) {
    this.whetherPass = whetherPass;
  }


  public String getWhetherPassReason() {
    return whetherPassReason;
  }


  public void setWhetherPassReason(String whetherPassReason) {
    this.whetherPassReason = whetherPassReason == null ? null : whetherPassReason.trim();
  }
}