package com.million.project.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 *
 */
@Data
@TableName("tbl_map_route_edge")
public class MapRouteEdge {
    Integer id; // 边id
    String node1Name; // 结点1的名
    String node2Name; // 结点2的名

    String edgeLength; // 两节点之间的路线距离(KM)
    String edgeCost; // 两节点之间路径的费用
    String edgeTime; // 走过该路段的平均时间
    int truckLimitType; // 该路线的限行车辆类型
}
