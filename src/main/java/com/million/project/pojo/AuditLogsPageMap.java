package com.million.project.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class AuditLogsPageMap {
    private Double [] beginLngLat;
    private Double [] endLngLat;

    public Double[] getBeginLngLat() {
        return beginLngLat;
    }

    public void setBeginLnglat(Double[] beginLnglat) {
        this.beginLngLat = beginLnglat;
    }

    public Double[] getEndLngLat() {
        return endLngLat;
    }

    public void setEndLnglat(Double[] endLnglat) {
        this.endLngLat = endLnglat;
    }

    private Integer roadBegin;
    private Integer roadEnd;

    public Integer getRoadBegin() {
        return roadBegin;
    }

    public void setRoadBegin(Integer roadBegin) {
        this.roadBegin = roadBegin;
    }

    public Integer getRoadEnd() {
        return roadEnd;
    }

    public void setRoadEnd(Integer roadEnd) {
        this.roadEnd = roadEnd;
    }

    private String suggestRoad;

    public String getSuggestRoad() {
        return suggestRoad;
    }

    public void setSuggestRoad(String suggestRoad) {
        this.suggestRoad = suggestRoad;
    }

    public Integer getWhetherUseAdvice() {
        return whetherUseAdvice;
    }

    public void setWhetherUseAdvice(Integer whetherUseAdvice) {
        this.whetherUseAdvice = whetherUseAdvice;
    }

    public void setAuditDate(Date auditDate) {
        this.auditDate = auditDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public void setApplyDate(Date applyDate) {
        this.applyDate = applyDate;
    }

    private Integer whetherUseAdvice;
    private Integer auditLogsId;
    private String realName;
    private String username;
    private Date auditDate;
    private String auditorRealName;
    private boolean whetherPass;
    private String whetherPassReason;
    private String auditorUsername;
    private String idCard;
    private Integer applyTimes;
    private String cargoType;
    private Date endDate;
    private Date beginDate;
    private Date applyDate;
    private String passCities;
    private String initRoad;
    private Float cargoWeight;
    private Float totalWeight;
    private Short truckAxes;
    private Short truckTyres;
    private String currentCondition;

    public Integer getAuditLogsId() {
        return auditLogsId;
    }

    public void setAuditLogsId(Integer auditLogsId) {
        this.auditLogsId = auditLogsId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    //  public Date getAuditDate() {
//    return auditDate;
//  }
    public String getAuditDate() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(auditDate);
    }

    //  public void setAuditDate(Date auditDate) {
//    this.auditDate = auditDate;
//  }
    public void setAuditDate(String auditDate) {
        try {
            this.auditDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(auditDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }


    public String getAuditorRealName() {
        return auditorRealName;
    }

    public void setAuditorRealName(String auditorRealName) {
        this.auditorRealName = auditorRealName;
    }

    public boolean isWhetherPass() {
        return whetherPass;
    }

    public void setWhetherPass(boolean whetherPass) {
        this.whetherPass = whetherPass;
    }

    public String getWhetherPassReason() {
        return whetherPassReason;
    }

    public void setWhetherPassReason(String whetherPassReason) {
        this.whetherPassReason = whetherPassReason;
    }

    public String getAuditorUsername() {
        return auditorUsername;
    }

    public void setAuditorUsername(String auditorUsername) {
        this.auditorUsername = auditorUsername;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public Integer getApplyTimes() {
        return applyTimes;
    }

    public void setApplyTimes(Integer applyTimes) {
        this.applyTimes = applyTimes;
    }

    public String getCargoType() {
        return cargoType;
    }

    public void setCargoType(String cargoType) {
        this.cargoType = cargoType;
    }

    //  public Date getEndDate() {
//    return endDate;
//  }
    public String getEndDate() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endDate);
    }

    //  public void setEndDate(Date endDate) {
//    this.endDate = endDate;
//  }
    public void setEndDate(String endDate) {
        try {
            this.endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }
    //  public Date getBeginDate() {
//    return beginDate;
//  }
    public String getBeginDate() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(beginDate);
    }

    //  public void setBeginDate(Date beginDate) {
//    this.beginDate = beginDate;
//  }
    public void setBeginDate(String beginDate) {
        try {
            this.beginDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(beginDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    //  public Date getApplyDate() {
//    return applyDate;
//  }
    public String getApplyDate() {
        return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(applyDate);
    }

    //  public void setApplyDate(Date applyDate) {
//    this.applyDate = applyDate;
//  }
    public void setApplyDate(String applyDate) {
        try {
            this.applyDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(applyDate);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public String getPassCities() {
        return passCities;
    }

    public void setPassCities(String passCities) {
        this.passCities = passCities;
    }

    public String getInitRoad() {
        return initRoad;
    }

    public void setInitRoad(String initRoad) {
        this.initRoad = initRoad;
    }

    public Float getCargoWeight() {
        return cargoWeight;
    }

    public void setCargoWeight(Float cargoWeight) {
        this.cargoWeight = cargoWeight;
    }

    public Float getTotalWeight() {
        return totalWeight;
    }

    public void setTotalWeight(Float totalWeight) {
        this.totalWeight = totalWeight;
    }

    public Short getTruckAxes() {
        return truckAxes;
    }

    public void setTruckAxes(Short truckAxes) {
        this.truckAxes = truckAxes;
    }

    public Short getTruckTyres() {
        return truckTyres;
    }

    public void setTruckTyres(Short truckTyres) {
        this.truckTyres = truckTyres;
    }

    public String getCurrentCondition() {
        return currentCondition;
    }

    @Override
    public String toString() {
        return "AuditLogsPageMap{" +
                "beginLngLat=" + Arrays.toString(beginLngLat) +
                ", endLngL  at=" + Arrays.toString(endLngLat) +
                ", roadBegin=" + roadBegin +
                ", roadEnd=" + roadEnd +
                ", suggestRoad='" + suggestRoad + '\'' +
                ", whetherUseAdvice=" + whetherUseAdvice +
                ", auditLogsId=" + auditLogsId +
                ", realName='" + realName + '\'' +
                ", username='" + username + '\'' +
                ", auditDate=" + auditDate +
                ", auditorRealName='" + auditorRealName + '\'' +
                ", whetherPass=" + whetherPass +
                ", whetherPassReason='" + whetherPassReason + '\'' +
                ", auditorUsername='" + auditorUsername + '\'' +
                ", idCard='" + idCard + '\'' +
                ", applyTimes=" + applyTimes +
                ", cargoType='" + cargoType + '\'' +
                ", endDate=" + endDate +
                ", beginDate=" + beginDate +
                ", applyDate=" + applyDate +
                ", passCities='" + passCities + '\'' +
                ", initRoad='" + initRoad + '\'' +
                ", cargoWeight=" + cargoWeight +
                ", totalWeight=" + totalWeight +
                ", truckAxes=" + truckAxes +
                ", truckTyres=" + truckTyres +
                ", currentCondition='" + currentCondition + '\'' +
                '}';
    }

    public void setCurrentCondition(String currentCondition) {
        this.currentCondition = currentCondition;
    }


}
