package com.million.project.pojo;

public class SysLogs {

  private Integer sysLogsId;

  private Integer roadApplyId;

  private Integer auditorId;

  private Integer roadId;

  public Integer getSysLogsId() {
    return sysLogsId;
  }

  public void setSysLogsId(Integer sysLogsId) {
    this.sysLogsId = sysLogsId;
  }

  public Integer getRoadApplyId() {
    return roadApplyId;
  }

  public void setRoadApplyId(Integer roadApplyId) {
    this.roadApplyId = roadApplyId;
  }

  public Integer getAuditorId() {
    return auditorId;
  }

  public void setAuditorId(Integer auditorId) {
    this.auditorId = auditorId;
  }

  public Integer getRoadId() {
    return roadId;
  }
  
  public void setRoadId(Integer roadId) {
    this.roadId = roadId;
  }
}