package com.million.project.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 用于返回阻断路线详细信息
 */
public class BlockedDetail {

  private Integer blockedSegId;
  private Float totalMileage; //总里程
  private String roadBegin; //起点名字
  private String roadEnd; //终点名字

  @Override
  public String toString() {
    return "BlockedDetail{" +
        "blockedSegId=" + blockedSegId +
        ", totalMileage=" + totalMileage +
        ", roadBegin='" + roadBegin + '\'' +
        ", roadEnd='" + roadEnd + '\'' +
        ", beginDate=" + beginDate +
        ", endDate=" + endDate +
        ", direction='" + direction + '\'' +
        ", reason='" + reason + '\'' +
        '}';
  }

  public Float getTotalMileage() {
    return totalMileage;
  }

  public void setTotalMileage(Float totalMileage) {
    this.totalMileage = totalMileage;
  }

  public String getRoadBegin() {
    return roadBegin;
  }

  public void setRoadBegin(String roadBegin) {
    this.roadBegin = roadBegin;
  }

  public String getRoadEnd() {
    return roadEnd;
  }

  public void setRoadEnd(String roadEnd) {
    this.roadEnd = roadEnd;
  }

  private Date beginDate;

  private Date endDate;


  public String getBeginDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(beginDate);
  }

  public void setBeginDate(String beginDate) {
    try {
      this.beginDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(beginDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public String getEndDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endDate);

  }

  public void setEndDate(String endDate) {
    try {
      this.endDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(endDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  private String direction;

  private String reason;


  public Integer getBlockedSegId() {
    return blockedSegId;
  }


  public void setBlockedSegId(Integer blockedSegId) {
    this.blockedSegId = blockedSegId;
  }


  public String getDirection() {
    return direction;
  }


  public void setDirection(String direction) {
    this.direction = direction == null ? null : direction.trim();
  }


  public String getReason() {
    return reason;
  }


  public void setReason(String reason) {
    this.reason = reason == null ? null : reason.trim();
  }

  public BlockedDetail() {
  }

  public BlockedDetail(Integer blockedSegId, Float totalMileage, String roadBegin, String roadEnd,
      Date beginDate, Date endDate, String direction, String reason) {
    this.blockedSegId = blockedSegId;
    this.totalMileage = totalMileage;
    this.roadBegin = roadBegin;
    this.roadEnd = roadEnd;
    this.beginDate = beginDate;
    this.endDate = endDate;
    this.direction = direction;
    this.reason = reason;
  }
}