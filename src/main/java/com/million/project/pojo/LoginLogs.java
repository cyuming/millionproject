package com.million.project.pojo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LoginLogs {


  private Integer loginLogId;


  private String username;


  private String userType;


  private Date logTime;


  public Integer getLoginLogId() {
    return loginLogId;
  }


  public void setLoginLogId(Integer loginLogId) {
    this.loginLogId = loginLogId;
  }


  public String getUsername() {
    return username;
  }


  public void setUsername(String username) {
    this.username = username == null ? null : username.trim();
  }


  public String getUserType() {
    return userType;
  }


  public void setUserType(String userType) {
    this.userType = userType == null ? null : userType.trim();
  }


  public String getLogTime() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(logTime);
  }


  public void setLogTime(String logTime) {
    try {
      this.logTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(logTime);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }
}