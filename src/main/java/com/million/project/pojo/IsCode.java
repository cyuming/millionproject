package com.million.project.pojo;

/**
 * 用于手机验证码登录，包装前端数据
 */
public class IsCode {

  private String code;
  private String username;

  @Override
  public String toString() {
    return "IsCode{" +
        "code='" + code + '\'' +
        ", username='" + username + '\'' +
        '}';
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }
}
