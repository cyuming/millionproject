package com.million.project.pojo;

public class RoadSegs {


  private Integer roadSegsId;


  private Integer highRoadsId;


  private Integer roadBegin;


  private Integer roadEnd;


  private Float totalMileage;


  private Float roadSegCost;


  private Float truckLength;


  private Float truckWidth;


  private Float truckHeight;


  private Float truckWeight;


  private Short truckAxes;


  private Short truckTyres;


  private String roadSegStatus;


  private String roadSegComment;


  public Integer getRoadSegsId() {
    return roadSegsId;
  }


  public void setRoadSegsId(Integer roadSegsId) {
    this.roadSegsId = roadSegsId;
  }


  public Integer getHighRoadsId() {
    return highRoadsId;
  }


  public void setHighRoadsId(Integer highRoadsId) {
    this.highRoadsId = highRoadsId;
  }


  public Integer getRoadBegin() {
    return roadBegin;
  }


  public void setRoadBegin(Integer roadBegin) {
    this.roadBegin = roadBegin;
  }


  public Integer getRoadEnd() {
    return roadEnd;
  }


  public void setRoadEnd(Integer roadEnd) {
    this.roadEnd = roadEnd;
  }


  public Float getTotalMileage() {
    return totalMileage;
  }


  public void setTotalMileage(Float totalMileage) {
    this.totalMileage = totalMileage;
  }


  public Float getRoadSegCost() {
    return roadSegCost;
  }


  public void setRoadSegCost(Float roadSegCost) {
    this.roadSegCost = roadSegCost;
  }


  public Float getTruckLength() {
    return truckLength;
  }


  public void setTruckLength(Float truckLength) {
    this.truckLength = truckLength;
  }


  public Float getTruckWidth() {
    return truckWidth;
  }


  public void setTruckWidth(Float truckWidth) {
    this.truckWidth = truckWidth;
  }


  public Float getTruckHeight() {
    return truckHeight;
  }

  public void setTruckHeight(Float truckHeight) {
    this.truckHeight = truckHeight;
  }


  public Float getTruckWeight() {
    return truckWeight;
  }


  public void setTruckWeight(Float truckWeight) {
    this.truckWeight = truckWeight;
  }


  public Short getTruckAxes() {
    return truckAxes;
  }


  public void setTruckAxes(Short truckAxes) {
    this.truckAxes = truckAxes;
  }


  public Short getTruckTyres() {
    return truckTyres;
  }


  public void setTruckTyres(Short truckTyres) {
    this.truckTyres = truckTyres;
  }


  public String getRoadSegStatus() {
    return roadSegStatus;
  }


  public void setRoadSegStatus(String roadSegStatus) {
    this.roadSegStatus = roadSegStatus == null ? null : roadSegStatus.trim();
  }


  public String getRoadSegComment() {
    return roadSegComment;
  }


  public void setRoadSegComment(String roadSegComment) {
    this.roadSegComment = roadSegComment == null ? null : roadSegComment.trim();
  }
}