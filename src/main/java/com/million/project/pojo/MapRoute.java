package com.million.project.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 这是智能生成路线表，是给前端返回的数据格式，途经点信息经过base64编码后，存在paths中。
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("tbl_map_route")
public class MapRoute {
    Integer Id; // 主键
    String name; // *路线名
    String area; // *地域
    String roadType;// *路面类型
    String policy; // 路线规划策略 默认为最短距离
    String seasons; // *[适用季节]
    String paths;// *途径路径点(base64编码存储)
    String note; // 备注
    Date dateInit; // 路线创建时间
    Integer isPublic; // 路线是否可用(0:不可用；1：可用)
    String beginName; // 起点名,均是存在的结点名
    String endName; // 终点名
    String routeDistance; // 该路线的距离(单位：KM)
    String routeTime; // 该路线花费的时间(单位：分钟)
    String routeCost; // 该路线的过路费(单位：元)
    String truckLimitType; // 车辆限制(自上向下兼容)

    String isAdminAdd;

    @Override
    public String toString() {
        return "MapRoute{" +
                "Id=" + Id +
                ", name='" + name + '\'' +
                ", area='" + area + '\'' +
                ", roadType='" + roadType + '\'' +
                ", policy='" + policy + '\'' +
                ", seasons='" + seasons + '\'' +
                ", paths='" + paths + '\'' +
                ", note='" + note + '\'' +
                ", dateInit=" + dateInit +
                ", isPublic=" + isPublic +
                ", beginName='" + beginName + '\'' +
                ", endName='" + endName + '\'' +
                ", routeDistance='" + routeDistance + '\'' +
                ", routeTime='" + routeTime + '\'' +
                ", routeCost='" + routeCost + '\'' +
                ", truckLimitType='" + truckLimitType + '\'' +
                ", isAdminAdd='" + isAdminAdd + '\'' +
                ", policys=" + policys +
                '}';
    }

    @TableField(exist = false)
    Map<Integer,Float> policys = new HashMap<>(); // 作用：根据策略代码，获得该策略对应的属性信息

    /**
     * 根据策略代码，获得该策略对应的属性信息
     * @param policy
     * @return
     */
    public Float getPolicyData(int policy){
        policys.put(0,Float.valueOf(this.routeTime));
        policys.put(1,Float.valueOf(this.routeCost));
        policys.put(2,Float.valueOf(this.routeDistance));
        return policys.get(policy);
    }
    public MapRoute(String mapRouteName, String area, String roadType, String policy, String seasons, String paths, String note, Integer isPublic, String routeDistance, String routeTime, String routeCost
    ,String truckLimitType) {
        this.truckLimitType = truckLimitType;
        this.name = mapRouteName;
        this.area = area;
        this.roadType = roadType;
        this.policy = policy;
        this.seasons = seasons;
        this.paths = paths;
        this.note = note;
        this.isPublic = isPublic;
        this.routeDistance = routeDistance;
        this.routeTime = routeTime;
        this.routeCost = routeCost;
    }


}
