package com.million.project.pojo;

/**
 * @Author: zhencym
 * @DATE: 2022/11/22
 */
public class RoadApplySum {

  private Integer roadBegin;
  private Integer roadEnd;
  private String suggestRoad;
  private String initRoad;
  // 用于统计申请次数
  private Integer applyNum;

  @Override
  public String toString() {
    return "RoadApplySum{" +
        "roadBegin=" + roadBegin +
        ", roadEnd=" + roadEnd +
        ", suggestRoad='" + suggestRoad + '\'' +
        ", initRoad='" + initRoad + '\'' +
        ", applyNum=" + applyNum +
        '}';
  }

  public RoadApplySum() {
  }

  public RoadApplySum( Integer roadBegin, Integer roadEnd,
      String suggestRoad,
      String initRoad, Integer applyNum) {
    this.roadBegin = roadBegin;
    this.roadEnd = roadEnd;
    this.suggestRoad = suggestRoad;
    this.initRoad = initRoad;
    this.applyNum = applyNum;
  }

  public Integer getApplyNum() {
    return applyNum;
  }

  public void setApplyNum(Integer applyNum) {
    this.applyNum = applyNum;
  }

  public Integer getRoadBegin() {
    return roadBegin;
  }

  public void setRoadBegin(Integer roadBegin) {
    this.roadBegin = roadBegin;
  }

  public Integer getRoadEnd() {
    return roadEnd;
  }

  public void setRoadEnd(Integer roadEnd) {
    this.roadEnd = roadEnd;
  }

  public String getSuggestRoad() {
    return suggestRoad;
  }

  public void setSuggestRoad(String suggestRoad) {
    this.suggestRoad = suggestRoad;
  }

  public String getInitRoad() {
    return initRoad;
  }

  public void setInitRoad(String initRoad) {
    this.initRoad = initRoad;
  }
}
