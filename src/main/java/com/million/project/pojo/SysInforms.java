package com.million.project.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@TableName("tbl_sys_informs")
public class SysInforms {

  @Override
  public String toString() {
    return "SysInforms{" +
        "sysInformsId=" + sysInformsId +
        ", senderUsername='" + senderUsername + '\'' +
        ", receiverUsername='" + receiverUsername + '\'' +
        ", sendDate=" + sendDate +
        ", sendInfo='" + sendInfo + '\'' +
        ", deleted=" + deleted +
        '}';
  }


  @TableId(value = "sys_informs_id", type = IdType.AUTO)
  private Integer sysInformsId;

  private String senderUsername;

  private String receiverUsername;

  private Date sendDate;

  private String sendInfo;
  private Integer deleted;

  public SysInforms(Integer sysInformsId, String senderUsername, String receiverUsername,
      Date sendDate, String sendInfo, Integer deleted) {
    this.sysInformsId = sysInformsId;
    this.senderUsername = senderUsername;
    this.receiverUsername = receiverUsername;
    this.sendDate = sendDate;
    this.sendInfo = sendInfo;
    this.deleted = deleted;
  }

  public SysInforms() {
  }

  public Integer getDeleted() {
    return deleted;
  }

  public void setDeleted(Integer deleted) {
    this.deleted = deleted;
  }

  public Integer getSysInformsId() {
    return sysInformsId;
  }

  public void setSysInformsId(Integer sysInformsId) {
    this.sysInformsId = sysInformsId;
  }

  public String getSenderUsername() {
    return senderUsername;
  }

  public void setSenderUsername(String senderUsername) {
    this.senderUsername = senderUsername == null ? null : senderUsername.trim();
  }

  public String getReceiverUsername() {
    return receiverUsername;
  }

  public void setReceiverUsername(String receiverUsername) {
    this.receiverUsername = receiverUsername == null ? null : receiverUsername.trim();
  }

  public String getSendDate() {
    return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(sendDate);
  }

  public void setSendDate(String sendDate) {
    try {
      this.sendDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sendDate);
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  public String getSendInfo() {
    return sendInfo;
  }
  
  public void setSendInfo(String sendInfo) {
    this.sendInfo = sendInfo == null ? null : sendInfo.trim();
  }
}