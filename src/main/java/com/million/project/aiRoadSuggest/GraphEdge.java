package com.million.project.aiRoadSuggest;


import lombok.Data;

import java.math.BigDecimal;

/**
 * 2.邻接表的边结点，存储的实际含义是：两点之间的边，属性有：路长、费用、限行车辆(自上向下兼容)
 *   限行车辆一共四种类型，从上向下兼容
 */
@Data
public class GraphEdge {
        float edgeLength; // 两节点之间的路线距离(KM)
        float edgeCost; // 两节点之间路径的费用

        float edgeTime; // 走过该路段的平均时间
        int truckLimitType; // 该路线的限行车辆类型

        GraphNode from; // 该边的起点结点
        GraphNode to; // 该边的终点结点(该边指向的结点)

        public GraphEdge(float edgeTime,float edgeLength,float edgeCost,int truckLimitType,GraphNode from,GraphNode to){
                this.edgeTime = edgeTime;
                this.edgeCost = edgeCost;
                this.edgeLength = edgeLength;
                this.truckLimitType  = truckLimitType;
                this.from = from;
                this.to = to;
        }
}
