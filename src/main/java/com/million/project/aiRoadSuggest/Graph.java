package com.million.project.aiRoadSuggest;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 本类存储图数据结构，在程序启动时，会将NEO4j中的图数据读取到图中。
 * 无向图、边上有不同的权重(路长、费用)，
 */
@Data
public class Graph {
    int nodeNum; // 结点个数
    int arcNum; // 弧(边)的个数
    List<GraphNode> vertexes; // 顶点表

    public Graph(){};
    public Graph(int nodeNum){
        this.arcNum = arcNum;
        this.nodeNum = 0;
        vertexes = new ArrayList<>(); //初始化顶点表
    }
}

