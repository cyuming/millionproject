package com.million.project.aiRoadSuggest;

import lombok.Data;

/**
 * 1.邻接表的顶结点(图顶点)，存储的实际含义是：城市结点，具有属性：cityName,longitude,latitude,
 */
@Data
public class GraphNode{
    private int nodeId; // 结点id
    private String cityName; // 结点名
    private String longitude; // 经度
    private String latitude; // 维度
    GraphEdge first; // 指向的第一个边结点

    public GraphNode(int nodeId,String cityName,String longitude,String latitude){
        this.nodeId = nodeId;
        this.cityName = cityName;
        this.longitude = longitude;
        this.latitude = latitude;
    }
}