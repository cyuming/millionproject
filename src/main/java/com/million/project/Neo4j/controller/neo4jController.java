package com.million.project.Neo4j.controller;

import com.million.project.Neo4j.entity.Node;
import com.million.project.Neo4j.entity.ShortestPath;
import com.million.project.Neo4j.service.serviceImpl.edgeServiceImpl;
import com.million.project.Neo4j.service.serviceImpl.nodeServiceImpl;
import com.million.project.pojo.MapRouteEdge;
import com.million.project.pojo.MapRouteNode;
import com.million.project.service.serviceimpl.MapRouteEdgeServiceImpl;
import com.million.project.service.serviceimpl.MapRouteNodeServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 操作Neo4j的工具类（封装我们所需要的一些方法）
 * 主要实现：将MYSQL中的结点和边数据读取出来，然后将Neo4j中的数据清空。
 *         再将读到的edge和node信息全部存入Neo4j，实现数据的同步。
 */
@Component
public class neo4jController {
    @Autowired
    nodeServiceImpl nodeService; // 注入

    @Autowired
    edgeServiceImpl edgeService;

    @Autowired
    MapRouteNodeServiceImpl mySQLNodeService; // MYSQL中的结点

    @Autowired
    MapRouteEdgeServiceImpl mySQLEdgeService; // MYSQL中的边


    /**
     * 向Neo4j中插入所有的节点和关系
     * @return
     */
    public void insertNeo4jData(){
        // 1.首先清空Neo4j中的全部数据— 删除全部结点和关系
        nodeService.clearNodeANDEdge();
        System.out.println("删除Neo4j中的全部结点完毕");
        // 2.再将所有的结点和边从MYSQL中读取出来
        List<MapRouteNode> mapRouteNodes = mySQLNodeService.getAllNodes();
        System.out.println("此时读取到的所有节点数据为："+mapRouteNodes.toString());

        List<MapRouteEdge> mapRouteEdges = mySQLEdgeService.getAllEdges();
        System.out.println("此时从MYSQL读取到的所有边数据为:"+mapRouteEdges);

        // 3.整理结点、边的关系，转换为Neo4j中的结点数据，再存入Neo4j中.
        // 3.1 先将所有从数据库中读出来的结点数据转为node对象，并且存入一个map中，键值对数据为：<name,节点对象>
        //     因为要想存关系，就必须找到每个关系对应的结点。那我们就使用name名来找到每一个结点
        Map<String, Node> nodesMap = new HashMap(); //存储向Neo4j插入的结点数据
        for (MapRouteNode m:mapRouteNodes) {
            Node n = new Node();
            n.setName(m.getName());
            n.setLatitude(m.getLatitude());
            n.setLongitude(m.getLongitude());
            n.setNote(m.getNote());
            nodesMap.put(m.getName(),n);
        }
        System.out.println("此时所有的nodeMap为"+nodesMap.toString());

        // 3.2 根据上边的map和从数据库读出来的边信息，存入Neo4j的关系中
        edgeService.saveEdge(nodesMap,mapRouteEdges); // 存入所有的关系
    }

    public Boolean isInSameGraph(String begin, String end) {
        return nodeService.isInSameGraph(begin,end);
    }

    public String getDijkstraPath(String begin, String end, String policy) {
        return nodeService.getDijkstraPath(begin,end,policy);
    }
}
