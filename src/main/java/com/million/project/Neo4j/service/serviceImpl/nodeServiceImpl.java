package com.million.project.Neo4j.service.serviceImpl;

import com.million.project.Neo4j.entity.Node;
import com.million.project.Neo4j.entity.ShortestPath;
import com.million.project.Neo4j.repository.edgeRepository;
import com.million.project.Neo4j.repository.nodeRepository;
import com.million.project.Neo4j.service.nodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class nodeServiceImpl implements nodeService {

    @Autowired
    private nodeRepository nodeRepository;

    @Autowired
    private edgeRepository edgeRepository;
    /**
     * 在Neo4j中删除全部的结点和关系
     */
    @Override
    public void clearNodeANDEdge() {
        System.out.println("进入service开始删除");
        nodeRepository.deleteAll(); // 删除所有节点和关系
        System.out.println("Service删除结束了");
    }

    @Override
    public void saveNode(List<Node> Nodes) {
        // 先测试一下向其中插入一对简单关系
        Node node1 = new Node();
        Node node2 = new Node();
        node1.setName("长沙");
        node2.setName("衡阳");
        nodeRepository.save(node1);
        nodeRepository.save(node2);
        System.out.println("测试节点插入成功");
    }

    /**
     *  判断两个节点是否在同一张图中
     * @param begin
     * @param end
     * @return
     */
    @Override
    public Boolean isInSameGraph(String begin, String end) {
        return nodeRepository.isInSameGraph(begin,end);
    }

    /**
     * 从Neo4j中按条件找到最短路径
     *
     * @param begin
     * @param end
     * @param policy
     * @return
     */
    @Override
    public String getDijkstraPath(String begin, String end, String policy) {

        return nodeRepository.getDijkstraNodes(begin,end,policy);
    }
}
