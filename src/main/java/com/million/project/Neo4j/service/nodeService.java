package com.million.project.Neo4j.service;

import com.million.project.Neo4j.entity.Node;
import com.million.project.Neo4j.entity.ShortestPath;

import java.util.List;

/**
 * 定义对于Neo4j中node数据的一些操作
 */
public interface nodeService {
    void clearNodeANDEdge();

    void saveNode(List<Node> Nodes);

    Boolean isInSameGraph(String begin, String end);

    String getDijkstraPath(String begin, String end, String policy);
}
