package com.million.project.Neo4j.service;

import com.million.project.Neo4j.entity.Node;
import com.million.project.pojo.MapRouteEdge;

import java.util.List;
import java.util.Map;

/**
 *  定义对于Neo4j中edge数据的一些操作
 */
public interface edgeService {
    void saveEdge(Map<String, Node> nodesMap, List<MapRouteEdge> edges);
}
