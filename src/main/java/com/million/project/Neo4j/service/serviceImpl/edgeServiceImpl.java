package com.million.project.Neo4j.service.serviceImpl;

import com.million.project.Neo4j.entity.Node;
import com.million.project.Neo4j.relationship.Edge;
import com.million.project.Neo4j.repository.edgeRepository;
import com.million.project.Neo4j.repository.nodeRepository;
import com.million.project.Neo4j.service.edgeService;
import com.million.project.pojo.MapRouteEdge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

@Service
public class edgeServiceImpl implements edgeService {

    @Autowired
    private nodeRepository nodeRepository;

    @Autowired
    private edgeRepository edgeRepository;
    /**
     *  存入关系
     * @param nodesMap
     * @param edges
     */
    @Override
    public void saveEdge(Map<String, Node> nodesMap, List<MapRouteEdge> edges) {
//        node node1 = new node();
//        node node2 = new node();
//        node node3 = new node();
//        node1.setName("长沙222");
//        node2.setName("衡阳222");
//        node3.setName("岳阳222");
//        edge e1 = new edge();
//        e1.setCost("178");
//        e1.setNodeStart(node1);
//        e1.setNodeEnd(node2);
//        edge e2 = new edge();
//        e2.setCost("33333");
//        e2.setNodeStart(node1);
//        e2.setNodeEnd(node3);
//
//        edge e3 = new edge();
//        e3.setCost("33333");
//        e3.setNodeStart(node2);
//        e3.setNodeEnd(node1);
//        edgeRepository.save(e1);
//        edgeRepository.save(e2);
//        edgeRepository.save(e3);
//        System.out.println("测试关系插入成功");

        // 1.构造出关系对象edge来
        //   思路：每遍历一个MapRouteEdge对象，根据起点、终点，设置好关系的起点和终点
        for (int i = 0; i < edges.size(); i++) {
            MapRouteEdge routeEdge = edges.get(i);
            // 1.1
            Edge e = new Edge();
            Node startNode = nodesMap.get(routeEdge.getNode1Name());
            Node endNode = nodesMap.get(routeEdge.getNode2Name());
            e.setCost(routeEdge.getEdgeCost());
            e.setTime(routeEdge.getEdgeTime());
            e.setLength(routeEdge.getEdgeLength());
            e.setTruckLimitType(routeEdge.getTruckLimitType());
            e.setNodeStart(startNode);
            e.setNodeEnd(endNode);
            edgeRepository.save(e);
        }
    }
}
