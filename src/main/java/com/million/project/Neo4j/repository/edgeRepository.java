package com.million.project.Neo4j.repository;

import com.million.project.Neo4j.relationship.Edge;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

/**
 *  相当于MYSQL中的Mapper层接口，用于与Neo4j交互的底层实现
 */
@Repository // 注入
public interface edgeRepository extends Neo4jRepository<Edge,Long> {
}
