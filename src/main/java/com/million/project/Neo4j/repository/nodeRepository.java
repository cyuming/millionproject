package com.million.project.Neo4j.repository;

import com.million.project.Neo4j.entity.Node;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

/**
 * 封装对Neo4j中node数据的操作；已经封装了很多操作
 */
@Repository
public interface nodeRepository extends Neo4jRepository<Node,Long> {

    @Query("MATCH (n1:node {name: $begin})-[r*]-(m:node {name: $end})\n" +
            "RETURN count(m) > 0")
    Boolean isInSameGraph(String begin, String end);

    @Query("MATCH p=shortestPath((start)-[r*..8]-(end))\n" +
            "WHERE start.name=$begin AND end.name=$end \n" +
            "RETURN [node in nodes(p) | node.name] AS nodeNames\n" +
            "ORDER BY REDUCE(totalCost = 0, r in relationships(p) | totalCost + toInteger(r[$policy]))\n" +
            "LIMIT 1;\n")
    String getDijkstraNodes(String begin, String end, String policy);
}
