package com.million.project.Neo4j.entity;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.voodoodyne.jackson.jsog.JSOGGenerator;
import lombok.Data;
import org.neo4j.ogm.annotation.GeneratedValue;
import org.neo4j.ogm.annotation.Id;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Property;


/**
 * Neo4j中的结点类
 */
@JsonIdentityInfo(generator= JSOGGenerator.class)
@NodeEntity(label = "node") // Neo4j中的结点
@Data
public class Node {

    @Id // 结点的主键(标识符)
    @GeneratedValue// 结点主键自动生成
    private Long Id; // 结点id

    @Property(name = "name")
    private String name; // 结点名
    @Property(name = "longitude")
    private String longitude; // 经度
    @Property(name = "latitude")
    private String latitude; // 维度
    @Property(name = "note")
    private String note; // 备注
}
