package com.million.project.Neo4j.entity;

import com.million.project.Neo4j.relationship.Edge;
import lombok.Data;

import java.util.List;

/**
 * 此类用于：在Neo4j中查找最短路径后的结果.
 */
@Data
public class ShortestPath {
    private List<Node> nodes;
    private List<Edge> edges;
    private int totalCostOfPath;
}
