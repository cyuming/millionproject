package com.million.project.Neo4j.relationship;

import com.million.project.Neo4j.entity.Node;
import lombok.Data;
import org.neo4j.ogm.annotation.*;

/**
 * 这是Neo4j中的关系类，也就是边类。
 * 关系是指：两个节点之间存在这条边
 */
@Data
@RelationshipEntity(type = "CONNECT") // 两节点的关系是相互连接
public class Edge {
    @Id
    @GeneratedValue
    Long id; // 边id
    @StartNode // 关系的开始节点
    Node nodeStart; // 结点1
    @EndNode   // 关系的结束结点
    Node nodeEnd; // 结点2

    @Property("length")
    String length; // 两节点之间的路线距离(KM)
    @Property("cost")
    String cost; // 两节点之间路径的费用
    @Property("time")
    String time; // 走过该路段的平均时间
    @Property("truckLimitType")
    int truckLimitType; // 该路线的限行车辆类型
}
