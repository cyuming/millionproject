package com.million.project.exception;

import com.million.project.utils.DragonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
/*全局异常捕获我们需要用到spring-boot-starter-web中的@ControllerAdvice注解，
        ControllerAdvice本质上是一个Component，因此也会被当成组件扫描

        使用@ControllerAdvice可以将所有的@ExceptionHandler方法都集中在一个类中，统一处理
        1、@ControllerAdvice默认拦截所有控制器
        2、@ControllerAdvice（annotaions={UserController.class}）配置指定需要拦截的控制器
        3、ControllerAdvice(basePackages = “com.demo”) 配置需要拦截的指定路径下的控制器
        所以要是用@ControllerAdvice必须引入springboot-web依赖
     */
@ControllerAdvice
public class GlobalExceptionHandler {
    /**
     * @ExceptionHandler相当于controller的@RequestMapping
     * 如果抛出的的是RuntimeException，就会自动调用该方法
     * 统一抛出“服务器异常”
     * @param
     * @return
     */
      @ExceptionHandler(RuntimeException.class)
      @ResponseBody
      public DragonResult handleRuntimeException(RuntimeException e){
//          log.error(e.toString(), e); //日志输出
          e.printStackTrace(); //控制台输出
          System.out.println("运行时异常捕获成功！");
          return DragonResult.fail("服务器异常");
      }
}
