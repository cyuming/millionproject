package com.million.project.exception;

import com.million.project.utils.DragonResult;
import com.million.project.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@ControllerAdvice
//全局异常处理
public class MvcExpection {
  //登录异常
  @ExceptionHandler(UnLoginException.class)
  public Result unLoginExcepttion(UnLoginException ex) {
    ex.printStackTrace();
    return Result.NOK(ex.getMessage());
  }
  //token异常
  @ExceptionHandler(TokenException.class)
  public Result tokenExcepttion(TokenException ex) {
    ex.printStackTrace();
    return Result.NOK(ex.getMessage());
  }

  // 拦截所有的异常信息
  @ExceptionHandler(Exception.class)
  public DragonResult doException(Exception ex) {
    // 记录日志
    // 通知运维
    // 通知开发
    ex.printStackTrace();// 别忘了向控制台报异常，方便调试
    return DragonResult.fail("服务器异常，请稍后再试！");
  }
}
