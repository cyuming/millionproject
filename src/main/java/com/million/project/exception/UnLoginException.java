package com.million.project.exception;

/**
 * 登录异常
 */
public class UnLoginException extends RuntimeException {

  public UnLoginException() {
    super("请先登录");
  }

  public UnLoginException(String message) {
    super(message);
  }
}
