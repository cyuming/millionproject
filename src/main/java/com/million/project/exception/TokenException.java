package com.million.project.exception;

/**
 * Token过期异常
 */
public class TokenException extends Exception {

  public TokenException(String message) {
    super(message);
  }
}
