package com.million.project.controller;

import com.github.pagehelper.PageInfo;
import com.million.project.Neo4j.controller.neo4jController;
import com.million.project.pojo.SysInforms;
import com.million.project.service.ISysInformsService;
import com.million.project.service.RoadNodesService;
import com.million.project.utils.DragonResult;
import com.million.project.utils.Result;
import com.million.project.utils.ResultMap;
import com.million.project.pojo.Users;
import com.million.project.service.ManagerService;
import com.million.project.service.UserService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 管理员对用户页面处理
 *
 * @Author: zhencym
 * @DATE: 2022/11/2
 */
@RestController
@RequestMapping("/manager")
public class ManagerController {

  @Autowired
  private ManagerService managerService;
  @Autowired
  private UserService userService;
  @Autowired
  private ISysInformsService sysInformsService;
  @Autowired
  RoadNodesService roadNodesService;

  @Autowired
  neo4jController neo4jController;


  /**
   * 向Neo4j插入数据的test方法
   * @return
   */
  @GetMapping("/neo4jTest")
  public DragonResult inertNeo4jTest(){
    System.out.println("开始向Neo4j插入数据");
    neo4jController.insertNeo4jData();
    return DragonResult.ok("插入成功");
  }
  @GetMapping("/neo4jIsSameGraph")
  public DragonResult isSameGraph(@RequestParam("begin") String begin,@RequestParam("end") String end){
    System.out.println("开始进入");
    Boolean inSameGraph = neo4jController.isInSameGraph(begin, end);
    if(inSameGraph){
      return DragonResult.ok("确实在一张图中");
    }else {
      return DragonResult.fail("不在一个图");
    }
  }
  @GetMapping("/neo4jDijkstra")
  public DragonResult getDijkstra(@RequestParam("begin") String begin,@RequestParam("end") String end,@RequestParam("policy") String policy){
    System.out.println("开始进入Dijkstra");
    String dijkstraPath = neo4jController.getDijkstraPath(begin, end, policy);
    System.out.println("此时找到的最短路径为："+dijkstraPath);
    if(dijkstraPath!=null){
      return DragonResult.ok(dijkstraPath);
    }else {
      return DragonResult.fail("找不到路");
    }
  }


  /**
   * 根据页号，页大小，用户类型（司机、审核员），返回用户信息
   *
   * @param pageNum
   * @param pageSize
   * @param user
   * @return
   */
  @RequestMapping("/showUser")
  public ResultMap getUserByType(@RequestParam int pageNum,
      @RequestParam int pageSize, @RequestBody Users user) {
    String type = user.getUserType();
    if (type.equals("司机") || type.equals("审核员") || type.equals("管理员")) {
      PageInfo<Users> pageInfo =
          userService.getUserByType(pageNum, pageSize, type);
      long total = pageInfo.getTotal();
      if (total == 0) {
        return ResultMap.NOK("没有数据！");
      } else {
        Map<String, Object> map = new HashMap<>();
        map.put("usersList", pageInfo.getList());
        map.put("total", total);
        return ResultMap.OK(map);
      }
    } else {
      return ResultMap.NOK("用户类型不存在！");
    }
  }

  /**
   * 根据页号，页大小，用户类型（司机、审核员），指定用户名字，或手机号，返回用户信息
   *
   * @param pageNum
   * @param pageSize
   * @param receive  这个map接收 用户类型 和 查询条件
   * @return
   */
  @RequestMapping("/findUser")
  public ResultMap getUserByTypeCondition(@RequestParam int pageNum,
      @RequestParam int pageSize, @RequestBody Map<String, Object> receive) {
    String type = (String) receive.get("userType");
    String condition = (String) receive.get("condition");
    System.out.println(type + " " + condition);
    if ((type.equals("司机") || type.equals("审核员") ||
        type.equals("管理员")) && condition != null) {
      PageInfo<Users> pageInfo =
          userService.getUserByType(pageNum, pageSize, type, condition);
      long total = pageInfo.getTotal();
      if (total == 0) {
        return ResultMap.NOK("没有数据！");
      } else {
        Map<String, Object> map = new HashMap<>();
        map.put("usersList", pageInfo.getList());
        map.put("total", total);
        return ResultMap.OK(map);
      }
    } else {
      return ResultMap.NOK("参数错误！");
    }
  }

  /**
   * 根据用户id，返回具体用户信息
   *
   * @param user
   * @return
   */
  @RequestMapping("/checkUser")
  public ResultMap getUserById(@RequestBody Users user) {
    int id = user.getUserId();
    if (id <= 0) {
      return ResultMap.NOK("id错误");
    }
    Users user1 = userService.getUserById(user.getUserId());
    if (user1 == null) {
      return ResultMap.NOK("没有数据！");
    } else {
      Map<String, Object> map = new HashMap<>();
      map.put("user", user1);
      return ResultMap.OK(map);
    }
  }

  /**
   * 直接插入新用户
   *
   * @param user
   * @return
   */
  @RequestMapping("/addUser")
  public ResultMap addUser(@RequestBody Users user) {
    if (user.getUsername() == null || user.getPassword() == null
        || user.getUserType() == null) {
      return ResultMap.NOK("账号密码不能为空");
    }
    return userService.addUser(user.getUsername(),
        user.getPassword(), user.getUserType());
  }

  /**
   * 管理员查看自己发送的通知
   */
  @PostMapping("/getSentSysInfo")
  public ResultMap getSentSysInfo(@RequestParam Integer pageNum,
      @RequestParam Integer pageSize, @RequestBody Users users) {
    String username = users.getUsername();
    if (username == null || username.equals("")) {
      return ResultMap.NOK("用户名不能为空");
    }
    PageInfo<SysInforms> pageInfo =
        sysInformsService.getSentSysInfo(pageNum, pageSize, username);
    List<SysInforms> list = pageInfo.getList();
    //输出测试
    for (int i = 0; i < list.size(); i++) {
      System.out.println((SysInforms) list.get(i));
    }
    long total = pageInfo.getTotal();
    if (total == 0) {
      return ResultMap.NOK("没有数据");
    } else {
      Map<String, Object> map = new HashMap<>();
      map.put("total", total);
      map.put("informList", pageInfo.getList());
      return ResultMap.OK(map);
    }
  }

  /**
   * 管理员群发通知
   */
  @PostMapping("/sendSysInfo")
  public Result sendSysInfo(@RequestBody Map<String, String> map) {
    String userType = map.get("userType");
    String message = map.get("message");
    String username = map.get("username");
    if (userType == null || userType.equals("") ||
        message == null || message.equals("")) {
      return Result.NOK("用户类型或消息不能为空");
    }
    if (userType.equals("司机") || userType.equals("审核员") ||
        userType.equals("all")) {
      int total =
          sysInformsService.sendSysInfoByType(username, userType, message);
      if (total == 0) {
        return Result.NOK("发送失败");
      } else {
        return Result.OK();
      }
    } else {
      return Result.NOK("用户类型错误");
    }
  }

  /**
   * 删除用户
   *
   * @param userid
   * @return
   */
  @PostMapping("/deleteUser")
  public Result deleteUser(@RequestParam Integer userid) {
    return userService.deleteUser(userid);
  }

}
