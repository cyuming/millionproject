package com.million.project.controller;

import com.million.project.utils.ResultMap;
import com.million.project.pojo.AuditLogs;
import com.million.project.pojo.RoadApplyLogs;
import com.million.project.service.AuditService;
import com.million.project.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 审核员的业务
 */
@RestController
@RequestMapping("/audit")
public class AuditController {

  @Autowired
  AuditService auditService;

  /**
   * 根据id进行详细查询
   *
   * @param roadApplyLogs
   * @return
   */
  @PostMapping("/getById")
  public Result getCode(@RequestBody RoadApplyLogs roadApplyLogs) {
    Result result = auditService.getById(
        Integer.valueOf(roadApplyLogs.getRoadApplyLogsId()));
    return result;
  }

  /**
   * 根据分页大小进行查询申请的路线信息
   *
   * @param currentPage
   * @param pageSizer
   * @return
   */
  @GetMapping("{currentPage}/{pageSizer}")
  public ResultMap getPageApply(@PathVariable String currentPage,
      @PathVariable String pageSizer) {
    ResultMap resultMap = auditService.getPageApply(Integer.valueOf(currentPage),
        Integer.valueOf(pageSizer));
    return resultMap;
  }

  /**
   * 根据分页来查询审核记录信息。 0代表所有种类 1代表通过的 2代表未通过的
   *
   * @param pageNumber
   * @param listNumber
   * @param username
   * @param type
   * @return
   */
  @GetMapping("/{pageNumber}/{listNumber}/{username}/{type}")
  public ResultMap getPageApplyLogs(@PathVariable String pageNumber,
      @PathVariable String listNumber, @PathVariable String username,
      @PathVariable Integer type) {
    ResultMap resultMap = auditService.getPageApplyLogs(Integer.valueOf(pageNumber),
        Integer.valueOf(listNumber), username, type);
    return resultMap;
  }

  /**
   * 进行路线审核
   *
   * @param auditLogs
   * @return
   */
  @PostMapping("/check")
  public Result check(@RequestBody AuditLogs auditLogs) {
    Result result = new Result();
    if (auditService.CheckRoads(auditLogs) != 0) {
      result.setMessage("审核成功");
      result.setCode("1");
    } else {
      result.setMessage("审核失败");
      result.setCode("0");
    }
    return result;
  }

  /**
   * 删除审核的日志
   *
   * @param auditLogs
   * @return
   */

  @PostMapping("/delete")
  public Result deleteLogs(@RequestBody AuditLogs auditLogs) {
    Result result = new Result();
    if (auditService.deleteLogs(auditLogs.getAuditLogsId()) != 0) {
      result.setMessage("删除成功");
      result.setCode("1");

    } else {
      result.setMessage("删除失败");
      result.setCode("0");
    }
    return result;
  }
}
