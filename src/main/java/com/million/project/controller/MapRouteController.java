package com.million.project.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.million.project.pojo.MapRoute;
import com.million.project.service.MapRouteService;
import com.million.project.service.serviceimpl.MapRouteServiceImpl;
import com.million.project.utils.DragonResult;
import com.million.project.vo.mapRouteNodeVO;
import com.million.project.vo.mapRouteVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.xml.stream.Location;
import java.util.Base64;

/**
 *  此类用于处理智能路线问题
 */
@RestController // @RestController是@ResponseBody和@Controller的组合注解
@RequestMapping( "/MapRoute") // @RequestMapping 配置url映射
// @PathVaribale 获取url中的数据
// @RequestParam 获取请求参数的值
// @GetMapping 组合注解
public class MapRouteController {
    @Resource
    MapRouteService mapRouteService;

    /**
     * 管理员新添自定义路线向前端返回是否添加成功等。具体任务分为以下几点：
     * 1.将新添的路线加入到tbl_map_route表中
     * 2.将各结点信息提取出来，存入到tbl_map_route_node表中
     * 3.将各结点之间的路段信息提取出来，存入到tbl_map_route_edge表中
     * @return
     */
    @PostMapping(path = "/adminAddRoute")
    public DragonResult adminAddRoute(@RequestBody mapRouteVO mapRouteVO){

        return mapRouteService.addRoute(mapRouteVO);
    }




    /**
     * 根据用户输入的起点、终点，智慧生成路线，并返回，有以下几点考虑：
     * 1.首先根据起点、终点，去tbl_map_route表查出所有符合条件的路线，根据策略找到线路，直接返回给前端点亮；
     * 2.如果不存在现有路线，那就首先根据起点、终点模糊查找，如果两个结点均存在，那就可能存在线路，继续向下处理；如果有一点不存在，那就一定不能生成对应线路，直接返回警告即可。
     * 3.起点、终点均存在：将数据读入图中，再根据所选策略，使用Dijkstra生成线路，返回给前端。
     * @param start
     * @param end    终点名
     * @param policy 司机选择的路线策略
     * @param cargoType 大件运输的类型(一共四级，自上向下兼容)
     * @return
     */
    @GetMapping("/getAIRoute")
    public DragonResult getAIRoute(@RequestParam("start") String start, @RequestParam("end") String end,
                                   @RequestParam("policy") String policy,@RequestParam("cargoType") String cargoType){
        System.out.println("后端接收到的信息是"+start+end+policy+cargoType);

        return mapRouteService.getAIRoute(start,end,policy,cargoType);
    }


}
