package com.million.project.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.million.project.pojo.RoadApplyLogs;
import com.million.project.pojo.Users;
import com.million.project.service.IRoadApplyLogsService;
import com.million.project.service.serviceimpl.AuditServiceImpl;
import com.million.project.utils.DragonResult;
import com.million.project.utils.GetPageData;
import com.million.project.utils.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 小龙
 * @since 2022-10-26
 */
@RestController
@RequestMapping("/road_apply_logs")
public class RoadApplyLogsController {

  @Resource
  private IRoadApplyLogsService applyLogsService;

  /**
   * 分页显示所有正在审核中的申报 ,只展示 显示申报时间、起始地、终点地。
   *
   * @return
   */
  @PostMapping("/getAllApplyLogs")
  DragonResult getAllApplyLogsPage(@RequestBody Users users,
      HttpSession session) {
    String username = users.getUsername();
    List<RoadApplyLogs> allApplyLogsPage =
        applyLogsService.getAllApplyLogsPage(username);
    return DragonResult.ok(allApplyLogsPage != null &
        !allApplyLogsPage.isEmpty(), allApplyLogsPage);
  }


  /**
   * 撤回一条申报信息，将申报信息的状态标识改为2（已撤回）
   *
   * @param roadApplyLogs 传入的依旧是id
   * @return
   */
  @PostMapping("/retractApply")
  DragonResult retractApply(@RequestBody RoadApplyLogs roadApplyLogs,
      @RequestParam String username, HttpSession session) {
      /*  String username1 = (String) session.getAttribute("username");
        if (username1 != null && username1 != "") {
            return DragonResult.fail("无操作用户，请先登录");
        }*/
    return applyLogsService.retractApply(roadApplyLogs, username);
  }

  /**
   * 查看审核中的申报详情
   *
   * @param roadApplyLogs 传入某条申报信息的id
   * @param session
   * @return
   */
  @PostMapping("/getDeepApply")
  DragonResult getDeepApply(@RequestBody RoadApplyLogs roadApplyLogs,
      HttpSession session) {
    String username = (String) session.getAttribute("username");
    if (username != null && username != "") {
      return DragonResult.fail("无操作用户，请先登录");
    }
    return applyLogsService.getDeepApply(roadApplyLogs, username);
  }

  // 下边开始实现司机历史申报记录查看页面的相关功能
  /**
   * 传入用户username 申报信息以升序排列。
   *
   * @param users
   * @param session
   * @return
   * @throws IOException
   */
  @PostMapping("/getHsitoryPage")
  DragonResult getHistoryPage(
      @RequestBody Users users, HttpSession session) {

    String username = users.getUsername();
    System.err.println(users);

    List<RoadApplyLogs> historyPage = applyLogsService.getHistoryPage(username);

    return DragonResult.ok(historyPage != null, historyPage);
  }

  /**
   * 根据传入id，将之删除（deleted置为1）
   *
   * @param roadApplyLogs
   * @param session
   * @return
   */
  @PostMapping("/deleteHistoryLog")
  DragonResult deleteHistoryLog(
      @RequestBody RoadApplyLogs roadApplyLogs, HttpSession session) {

    return applyLogsService.deleteHistoryLog(roadApplyLogs);
  }

  /**
   * 查看审核通过的历史申报记录
   *
   * @param users
   * @param session
   * @return
   * @throws IOException
   */
  @PostMapping("/getPassPage")
  DragonResult getPassPage(
      @RequestBody Users users, HttpSession session) throws IOException {

    String username = users.getUsername();

    List<RoadApplyLogs> historyPage = applyLogsService.getPassPage(username);

    return DragonResult.ok(historyPage != null, historyPage);
  }

  /**
   * 查看审核不通过的历史申报记录
   *
   * @param users
   * @param session
   * @return
   * @throws IOException
   */
  @PostMapping("/getNotPassPage")
  DragonResult getNotPassPage(
      @RequestBody Users users, HttpSession session) throws IOException {

    String username = users.getUsername();
    List<RoadApplyLogs> historyPage = applyLogsService.getNotPassPage(username);

    return DragonResult.ok(historyPage != null, historyPage);
  }

  /**
   * 统计各月路线申报数量
   */
  @GetMapping("/getNumByDate")
  Result getNumByDate() {
    return Result.OK(applyLogsService.getNumByDate());
  }

  /**
   * 统计热门路线
   */
  @GetMapping("/getPopular")
  Result getPopular() {
    return Result.OK(applyLogsService.getPopular());
  }

  /**
   * 统计申报数量
   */
  @GetMapping("/getNum")
  Result getNum() {
    return Result.OK(applyLogsService.getApplyNum());
  }

  /**
   * 统计申报通过数量
   */
  @GetMapping("/getPassNum")
  Result getPassNum() {
    return Result.OK(applyLogsService.getPassApplyNum());
  }

}

