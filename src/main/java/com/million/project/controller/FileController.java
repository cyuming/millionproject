package com.million.project.controller;

import com.million.project.pojo.Users;
import com.million.project.service.UserService;
import com.million.project.utils.MyFileUtils;
import com.million.project.utils.Result;
import com.million.project.utils.ResultMap;
import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;


/**
 * 文件上传处理
 *
 * @Author: zhencym
 * @DATE: 2022/11/19
 */
@RestController
public class FileController {

  @Autowired
  UserService userService;

  /**
   * 根据用户账号，来更改头像 参数是 头像，用户名
   */
  @PostMapping("/changeImage")
  Result changeImage(MultipartFile file, String username) {
    if (username == null || "".equals(username)) {
      return Result.NOK("用户名不能为空");
    }
    Users user1 = userService.SelectByUsername(username);
    if (user1 == null) {
      return Result.NOK("用户不存在");
    }
    String imageOld = user1.getImage();
    String userType = user1.getUserType();
    if (userType.equals("司机")) {
      userType = "driver";
    } else if (userType.equals("审核员")) {
      userType = "auditor";
    } else if (userType.equals("管理员")) {
      userType = "manager";
    }
    //限制文件大小
    long size = file.getSize();
    if (size > (2 * 1024 * 1024)) {
      return Result.NOK("文件不能大于2Mb");
    }
//    //文件名
    //本地测试
//    String path = "src/main/resources/image/" + userType + "/"
//        + user1.getUsername() + ".jpg";
    //文件名
    // 服务器
    String path = "/usr/image/" + userType + "/"
        + user1.getUsername() + ".jpg";
    //上传文件,且限制大小为2mb
    MyFileUtils.clientUpload(file, path, 2, MyFileUtils.MB);
    //更新数据库头像地址
    user1.setImage(path);
    int flag = userService.changeImage(user1);
    if (flag == 0) {
      //删除上传的文件
      File fileNew = new File(path);
      boolean deleted = fileNew.delete();
      return Result.NOK("更新失败");
    } else {
      return Result.OK(user1);
    }

  }

  /**
   * 根据图片路径 返回头像
   */
  @GetMapping("/getImage")
  ResultMap getImage(String path, HttpServletResponse response) {
    if (path == null || "".equals(path)) {
      ResultMap.NOK("路径不能为空");
    }
    try {
      //response.reset();
      // 设置内容类型
      //response.setContentType("application/octet-stream");
      // 取得文件名
      String filename = new File(path).getName();
      // 响应头，经过编码
      response.addHeader("Content-Disposition", "attachment; filename=" +
          URLEncoder.encode(filename, "UTF-8"));
      // 获取响应体的输出流,关键
      ServletOutputStream outputStream = response.getOutputStream();
      MyFileUtils.clientDownload(outputStream, path);
    } catch (IOException e) {
      throw new RuntimeException(e);
    }
    return ResultMap.OK();
  }

}
