package com.million.project.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.million.project.pojo.SysInforms;
import com.million.project.pojo.Users;
import com.million.project.service.ISysInformsService;
import com.million.project.utils.DragonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 小龙
 * @since 2022-10-26
 */
@RestController
@RequestMapping("/sys_informs")
public class SysInformsController {

  @Resource
  ISysInformsService informsService;

  /**
   * 查看用户所有收到或发送的通知,包括审核员、系统管理员、司机
   *
   * @param userDTO
   * @param currentPage
   * @param pageSize
   * @return data中存储通知的详细信息
   */
  @PostMapping("/getSysInfos")
  DragonResult getSysInfos(
      @RequestBody Users userDTO, @RequestParam Integer currentPage,
      @RequestParam Integer pageSize) {

    IPage<SysInforms> driverSysInfo =
        informsService.getDriverSysInfo(currentPage, pageSize,
            userDTO);
    // 记得排除删除时候的bug，如下：
    if (currentPage > driverSysInfo.getSize()) {
      driverSysInfo = informsService.getDriverSysInfo(
          (int) driverSysInfo.getSize(), pageSize, userDTO);
    }
    System.out.println(driverSysInfo.getTotal());
    return DragonResult.ok(driverSysInfo != null, driverSysInfo);
  }

  /**
   * 所有用户删除某条通知：根据传入的通知id删除即可。
   *
   * @param sysInforms 某条通知的详细信息，包括id，根据id删除即可。
   * @return
   */
  @PostMapping("/deleteInfo")
  DragonResult deleteInfos(@RequestBody SysInforms sysInforms) {

    return informsService.deleteById(sysInforms);
  }

  /**
   * 显示一条系统通知的详细信息
   *
   * @param sysInforms
   * @return
   */
  @PostMapping("/getDeepInfo")
  DragonResult getDeepInfo(@RequestBody SysInforms sysInforms) {

    return informsService.getDeepInfo(sysInforms);
  }


}

