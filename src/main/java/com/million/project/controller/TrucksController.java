package com.million.project.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.million.project.pojo.Trucks;
import com.million.project.pojo.Users;
import com.million.project.service.ITrucksService;
import com.million.project.utils.DragonResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * 车辆管理类
 * <p>
 * 前端控制器
 * </p>
 *
 * @author 小龙
 * @since 2022-10-26
 */
@RestController
@RequestMapping("/trucks")
public class TrucksController {

  @Resource
  ITrucksService trucksService;

  @GetMapping("/test")
  public String test(){
    return "trucks测试成功！";
  }
  /**
   * 全部查询 显示车辆信息，因为分页展示，因此需要进行分页操作。 条件查询+分页查询
   *
   * @param userDTO
   * @param session
   * @return 返回分页对象IPage
   */
  @PostMapping("/showTrucks")
  DragonResult getAllTrucks(@RequestBody Users userDTO, HttpSession session) {
    List<Trucks> allTruckPages = trucksService.getAllTruckPages(userDTO);

    return DragonResult.ok(allTruckPages != null, allTruckPages);
  }


  /**
   * 修改车的信息：只能修改当前近况. 因为车牌号不可重复（unique），因此只需要传入车牌号即可。
   *
   * @param truckLicense 车牌号
   * @return
   */
  @GetMapping("/updateTruck")
  DragonResult updateTruck(String truckLicense, String currentCondition) {

    return trucksService.updateTruck(truckLicense, currentCondition);
  }

  /**
   * 精确查询，根据 车牌号 或 吨位 或 车牌号和吨位 进行查询。
   * 依旧是 条件查询 + 分页操作。 因此先筛选符合条件的车辆，再分页。
   *
   * @param truckLicense
   * @param truckWeight
   * @param userDTO
   * @return
   */
  @PostMapping("/selectTrucks")
  DragonResult getPartTrucks(
      @RequestParam String truckLicense, @RequestParam Float truckWeight,
      @RequestBody Users userDTO) {
    truckLicense = truckLicense.trim();
    List<Trucks> trucks =
        trucksService.selectPartTruck(truckLicense, truckWeight, userDTO);
    return DragonResult.ok(trucks != null, trucks);
  }

}

