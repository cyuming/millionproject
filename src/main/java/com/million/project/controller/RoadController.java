package com.million.project.controller;

import com.github.pagehelper.PageInfo;
import com.million.project.utils.Result;
import com.million.project.utils.ResultMap;
import com.million.project.pojo.*;
import com.million.project.service.*;


import com.million.project.vo.BlockVO;
import com.million.project.vo.RoadNodeVO;
import com.million.project.vo.RoadVO;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.million.project.utils.DragonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 管理员对高速公路、路线、路段、结点相关处理
 *
 * @Author: zhencym
 * @DATE: 2022/11/3
 */
@RestController
@RequestMapping("/road")
public class RoadController {

    @Autowired
    private BlockedSegsService blockedSegsService;
    @Autowired
    private HighRoadsService highRoadsService;
    @Autowired
    private RoadsService roadsService;
    @Autowired
    private RoadNodesService roadNodesService;
    @Autowired
    private RoadStoneService roadStoneService;

    @Autowired
    private ITrucksService trucksService;
    @Autowired
    private RoadSegService roadSegService;

    @Autowired
    private IRoadApplyLogsService roadApplyLogsService;



    /**
     * 查看所有推荐路段信息
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @PostMapping("/showRoad")
    public ResultMap getRoads(@RequestParam int pageNum, @RequestParam int pageSize) {
        PageInfo<RoadVO> pageInfo = roadsService.getRoads(pageNum, pageSize);
        Map<String, Object> map = new HashMap<>();
        List list = pageInfo.getList();
        map.put("roadsList", list);
        map.put("total", pageInfo.getTotal());
        return ResultMap.OK(map);
    }


    /**
     * 根据id查看阻塞路段
     *
     * @return
     */
    @PostMapping("/selectBlockedSegs")
    public ResultMap selectBlockedSegs(@RequestParam int id) {
        return blockedSegsService.selectBlockedSegs(id);

    }

    /**
     *
     * 根据id删除指定阻断路线信息
     *
     * @return
     */
    @PostMapping("/deleteBlockedSegs")
    public Result deleteBlockedSegs(@RequestBody BlockedSegs blockedSegs) {
        if (blockedSegs.getBlockedSegId() == null || blockedSegs.getSegId() == null) {
            return Result.NOK("id和推荐路线id不能为空");
        }
        blockedSegsService.deleteBlockSeg(blockedSegs);
        return Result.OK();
    }

    /**
     * 新增阻断路线信息
     *
     * @return Result
     */
    @PostMapping("/addBlockedSegs")
    public Result addBlockedSegs(@RequestBody BlockedSegs blockedSegs) {
        // 插入阻塞路段数据
        return blockedSegsService.insertBlockedSegs(blockedSegs);

    }


    /**
     * 查看所有结点信息
     *
     * @param pageNum
     * @param pageSize
     * @return
     */
    @PostMapping("/showNode")
    public ResultMap getNode(@RequestParam int pageNum, @RequestParam int pageSize) {
        PageInfo<RoadNodeVO> pageInfo = roadNodesService.getRoadNodes(pageNum, pageSize);
        Map<String, Object> map = new HashMap<>();
        List list = pageInfo.getList();
        if (list == null) {
            return ResultMap.NOK("没有数据");
        } else {
            map.put("roadNodesList", list);
            map.put("total", pageInfo.getTotal());
        }
        return ResultMap.OK(map);
    }

    /**
     * 根据id删除
     *
     * @param roads
     * @return
     */
    @PostMapping("/deleteRoad")
    public ResultMap deleteRoad(@RequestBody Roads roads) {
        return roadsService.deleteByPK(roads.getRoadsId());
    }

    /**
     * 更新推荐路线，id和器点终点不能更新
     *
     * @param roads
     * @return
     */
    @PostMapping("/updateRoad")
    public ResultMap updateRoad(@RequestBody Roads roads) {
        return roadsService.updateBase(roads);
    }

    /**
     * 新增推荐路线，无需指定主键
     */
    @PostMapping("/insertRoad")
    public ResultMap insertRoad(@RequestBody Roads roads) {
        return roadsService.insertRoad(roads);
    }

    /**
     * 根据用户输入的起点和终点返回推荐路线
     *
     * @param begin
     * @param end
     * @return
     */
    @PostMapping("/suggestRoads")
    public DragonResult suggestRoads(@RequestParam String begin, @RequestParam String end) {

        return roadsService.selectSuggestRoads(begin, end);
    }

    /**
     * 根据用户idcard，返回车辆信息
     *
     * @return
     */
    @PostMapping("/getTrucks")
    public DragonResult getTrucks(@RequestParam String idCard) {

        return trucksService.selectByIdcard(idCard);
    }

    /**
     * 提交用户的路线申报
     *
     * @param roadApplyLogs
     * @return
     */
    @PostMapping("/submitRoadApply")
    public DragonResult submitRoadApply(@RequestBody RoadApplyLogs roadApplyLogs) {

        // 经纬度提取
        NodeLngLat nodeLngLat=new NodeLngLat();
        nodeLngLat.setBeginLngLat(roadApplyLogs.getBeginLngLat());
        nodeLngLat.setEndLngLat(roadApplyLogs.getEndLngLat());
        System.out.println("从前端接收的路线申报数据为:"+roadApplyLogs.toString());

        // 人工输入的路线没有经纬度
        System.out.println("起点经纬度:"+nodeLngLat.getBeginLngLat());
        System.out.println("终点经纬度:"+nodeLngLat.getEndLngLat());
        return roadApplyLogsService.submitRoadApply(roadApplyLogs,nodeLngLat);
    }

}
