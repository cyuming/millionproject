package com.million.project.mapper;

import com.million.project.pojo.RoadSegs;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoadSegsMapper {
  RoadSegs getRoadsegsByBeginEnd(int begin, int end);

  int deleteByPrimaryKey(Integer roadSegsId);

  int insert(RoadSegs record);

  RoadSegs selectByPrimaryKey(Integer roadSegsId);

  List<RoadSegs> selectAll();

  int updateByPrimaryKey(RoadSegs record);
}