package com.million.project.mapper;

import com.million.project.pojo.RoadApplyLogs;
import com.million.project.pojo.Roads;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface RoadsMapper {

  int deleteByPrimaryKey(Integer roadsId);

  int insert(Roads record);

  int insertNoPK(Roads record);

  int updateRoadStatusInt(int id,boolean status);



  Roads selectByPrimaryKey(Integer roadsId);

  Roads selectByPrimaryKeyDetail(Integer roadsId);

  List<Roads> selectAll();

  List<Roads> selectAllDetail();

  int updateByPrimaryKey(Roads record);

  int updateBase(Roads record);

  @Select("select * from tbl_roads where road_begin = #{beginId} "
      + "and road_end = #{endId}")
  List<Roads> selectByBeginAndEnd(int beginId, int endId);


}