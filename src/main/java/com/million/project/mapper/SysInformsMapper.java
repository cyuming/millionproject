package com.million.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.million.project.pojo.SysInforms;
import com.million.project.pojo.Trucks;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SysInformsMapper extends BaseMapper<SysInforms> {

  int deleteByPrimaryKey(Integer sysInformsId);

  int insert(SysInforms record);

  int insertNoId(SysInforms record);

  SysInforms selectByPrimaryKey(Integer sysInformsId);

  List<SysInforms> selectAll();

  int updateByPrimaryKey(SysInforms record);

  List<SysInforms> selectBysender(String senderUsername);
}