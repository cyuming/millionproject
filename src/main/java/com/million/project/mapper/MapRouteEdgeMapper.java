package com.million.project.mapper;

import com.million.project.pojo.MapRouteEdge;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MapRouteEdgeMapper {

    @Select("select * from tbl_map_route_edge")
    List<MapRouteEdge> getAllEdge();

    @Select("select * from tbl_map_route_edge where node1_name=#{curStart} AND node2_name = #{curEnd}")
    MapRouteEdge getEdgeBy2Name(String curStart, String curEnd);
}
