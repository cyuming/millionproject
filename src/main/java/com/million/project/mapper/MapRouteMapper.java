package com.million.project.mapper;

import com.million.project.pojo.MapRoute;
import com.million.project.pojo.MapRouteEdge;
import com.million.project.pojo.MapRouteNode;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MapRouteMapper {
    @Insert("insert into tbl_map_route(name,area,road_type,seasons,paths,note,date_init,policy," +
            "is_public,route_distance,route_time,begin_name,end_name,route_cost,truck_limit_type,is_admin_add) " +
            "VALUES(#{mapRoute.name},#{mapRoute.area},#{mapRoute.roadType},#{mapRoute.seasons},#{mapRoute.paths},#{mapRoute.note}," +
            "NOW(),#{mapRoute.policy},#{mapRoute.isPublic},#{mapRoute.routeDistance},#{mapRoute.routeTime},#{mapRoute.beginName}," +
            "#{mapRoute.endName},#{mapRoute.routeCost},#{mapRoute.truckLimitType},#{mapRoute.isAdminAdd})")
    int addRoute(@Param("mapRoute") MapRoute mapRoute);

    @Select("select id from tbl_map_route t where t.name = #{name}")
    Integer getIsExist(@Param("name") String name);


    int addNode(@Param("list") List<MapRouteNode> list);

    int addEdge(@Param("list") List<MapRouteEdge> list);

    @Select("select * from tbl_map_route Where begin_name = #{begin} AND end_name = #{end} AND truck_limit_type >= #{cargoType} AND " +
            "is_public = 1 AND policy = #{policy}")
    List<MapRoute> getExistAIRoutes(@Param("begin") String begin, @Param("end") String end, @Param("cargoType") String cargoType
                                    ,@Param("policy") String policy);

    @Select("select count(1) from tbl_map_route where is_admin_add = 0")
    int getexistAIroutesNum();
}
