package com.million.project.mapper;

import com.million.project.pojo.BlockedSegs;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface BlockedSegsMapper {

  int insert(BlockedSegs record);

  int insertNoId(BlockedSegs record);

  int deleteById(@Param("id") int id);

  List<BlockedSegs> selectAll();

  List<BlockedSegs> selectAllDetail();

  BlockedSegs selectById(@Param("id") int id);
  BlockedSegs selectByRoadId(@Param("id") int id);


  BlockedSegs selectByIdDetail(@Param("id") int id);

}