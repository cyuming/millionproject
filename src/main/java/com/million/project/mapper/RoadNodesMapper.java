package com.million.project.mapper;

import com.million.project.pojo.RoadNodes;
import com.million.project.pojo.Roads;
import com.million.project.vo.RoadNodeVO;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface RoadNodesMapper {

  int deleteByPrimaryKey(Integer roadNodesId);

  int insert(RoadNodes record);
  int insertKey(RoadNodes record);

  RoadNodes selectByPrimaryKey(Integer roadNodesId);

  RoadNodes selectByPrimaryKeyDetail(Integer roadNodesId);

  List<RoadNodes> selectAll();

  List<RoadNodeVO> selectAllDetail();
  @Select("SELECT longitude from tbl_road_nodes where road_nodes_id=#{id}")
  Double getLong(Integer id);
  @Select("SELECT latitude from tbl_road_nodes where road_nodes_id=#{id}")
  Double getLat(Integer id);

  @Select("SELECT node_name from tbl_road_nodes where road_nodes_id=#{id}")
  String getNodeName(Integer id);

  Integer updateByPrimaryKey(RoadNodes record);

  @Select("SELECT road_nodes_id from tbl_road_nodes where "
      + "node_type = '收费站' " +
      "and node_name like CONCAT('%',#{begin},'%')")
  Integer selectByName(String begin);

  @Select("Select node_name from tbl_road_nodes  where "
      + "road_nodes_id=#{roadBegin}")
  String selectById(Integer roadBegin);

  @Select("SELECT road_nodes_id from tbl_road_nodes where node_name=#{roadBeginName}")
  Integer selectIdByName(String roadBeginName);
}