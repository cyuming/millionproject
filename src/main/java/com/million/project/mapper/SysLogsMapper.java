package com.million.project.mapper;

import com.million.project.pojo.SysLogs;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysLogsMapper {

  int deleteByPrimaryKey(Integer sysLogsId);

  int insert(SysLogs record);

  SysLogs selectByPrimaryKey(Integer sysLogsId);

  List<SysLogs> selectAll();
  
  int updateByPrimaryKey(SysLogs record);
}