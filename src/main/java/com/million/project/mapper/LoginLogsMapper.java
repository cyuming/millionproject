package com.million.project.mapper;

import com.million.project.pojo.LoginLogs;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LoginLogsMapper {

  int deleteByPrimaryKey(Integer loginLogId);

  int insert(LoginLogs record);

  LoginLogs selectByPrimaryKey(Integer loginLogId);

  List<LoginLogs> selectAll();

  int updateByPrimaryKey(LoginLogs record);
}