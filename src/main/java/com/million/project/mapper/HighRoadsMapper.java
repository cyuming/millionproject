package com.million.project.mapper;

import com.million.project.pojo.HighRoads;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HighRoadsMapper {

  int deleteByPrimaryKey(Integer highRoadsId);

  int insert(HighRoads record);

  HighRoads selectByPrimaryKey(Integer highRoadsId);

  HighRoads selectByPrimaryKeyDetail(Integer highRoadsId);

  List<HighRoads> selectAll();

  List<HighRoads> selectAllDetail();

  int updateByPrimaryKey(HighRoads record);
}