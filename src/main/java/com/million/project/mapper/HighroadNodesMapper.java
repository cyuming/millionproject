package com.million.project.mapper;

import com.million.project.pojo.HighroadNodes;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HighroadNodesMapper {

  int deleteByPrimaryKey(Integer highroadNodeId);

  int insert(HighroadNodes record);

  HighroadNodes selectByPrimaryKey(Integer highroadNodeId);

  List<HighroadNodes> selectAll();

  int updateByPrimaryKey(HighroadNodes record);
}