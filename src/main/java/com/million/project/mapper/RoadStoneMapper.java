package com.million.project.mapper;

import com.million.project.pojo.RoadStone;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RoadStoneMapper {

  int deleteByPrimaryKey(Integer roadNodesId);

  int insert(RoadStone record);

  RoadStone selectByPrimaryKey(Integer roadNodesId);

  RoadStone selectByPrimaryKeyDetail(Integer roadNodesId);

  List<RoadStone> selectAll();

  List<RoadStone> selectAllDetail();

  int updateByPrimaryKey(RoadStone record);
}