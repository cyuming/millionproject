package com.million.project.mapper;

import com.million.project.pojo.MapRouteNode;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MapRouteNodeMapper {

    @Select("select * from tbl_map_route_node")
    List<MapRouteNode> getAllNodes();

    @Select("SELECT * from tbl_map_route_node where name = #{s}")
    MapRouteNode getNodeByName(@Param("s") String s);
}
