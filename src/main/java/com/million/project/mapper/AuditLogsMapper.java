package com.million.project.mapper;

import com.million.project.pojo.AuditLogs;
import java.util.List;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface AuditLogsMapper {

  /**
   * 删除审核的日志
   */
  @Update("update tbl_audit_logs set deleted=1 where audit_logs_id=#{auditLogsId}")
  int deleteLogs(Integer auditLogsId);
  /**
   *
   * 分页查询申报记录
   */

  /**
   * 根据审核员审核的情况，修改tbl_audit_logs的内容
   */
  @Insert(
      "insert into tbl_audit_logs(road_apply_id,auditor_username,audit_date,whether_pass,whether_pass_reason,deleted)"
          + "values (#{roadApplyId},#{username},#{date},#{checkCode},#{message},0)")
  int checkRoads(Integer roadApplyId, String username, Boolean checkCode, String message,
      String date);


  int deleteByPrimaryKey(Integer auditLogsId);

  int insert(AuditLogs record);

  AuditLogs selectByPrimaryKey(Integer auditLogsId);

  List<AuditLogs> selectAll();

  int updateByPrimaryKey(AuditLogs record);

  @Select("SELECT * from tbl_audit_logs where road_apply_id = #{roadApplyLogsId}")
  AuditLogs selectByApplyId(Integer roadApplyLogsId);

}
