package com.million.project.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.million.project.pojo.Trucks;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface TrucksMapper extends BaseMapper<Trucks> {

  int deleteByPrimaryKey(Integer trucksId);

  int insert(Trucks record);

  Trucks selectByPrimaryKey(Integer trucksId);

  List<Trucks> selectAll();

  int updateByPrimaryKey(Trucks record);

  @Update("update tbl_trucks set current_condition = #{currentCondition} "
      + "where truck_license = #{truckLicense}")
  int updateCurrentCondition(@Param("truckLicense") String truckLicense,
      @Param("currentCondition") String currentCondition);

  @Select("select * from tbl_trucks where user_id_card = #{userIdCard} " +
      "and (truck_weight = #{truckWeight} or truck_license =#{truckLicense} )")
  List<Trucks> selectOrPage(@Param("truckLicense") String truckLicense,
      @Param("truckWeight") Float truckWeight,
      @Param("userIdCard") String idCard);

  @Select("select * from tbl_trucks where user_id_card = #{userIdCard} " +
      "and (truck_weight = #{truckWeight} and truck_license =#{truckLicense})")
  List<Trucks> selectAndPage(@Param("truckLicense") String truckLicense,
      @Param("truckWeight") Float truckWeight,
      @Param("userIdCard") String idCard);

  @Select("Select * from tbl_trucks where user_id_card = #{idCard}")
  List<Trucks> selectByIdcard(String idCard);
}