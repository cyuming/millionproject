package com.million.project.mapper;

import com.million.project.pojo.Users;

import java.util.List;

import org.apache.catalina.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.web.bind.annotation.PostMapping;

@Mapper
public interface UsersMapper {
    /**
     * 管理员添加用户
     */
    int insertUser(String username, String password, String userType,
        String registerDate, int applyTimes, String image);

    /**
     * 根据用户类型统计用户数量
     */
    int getUserNumByType(@Param("userType") String userType);

    /**
     * 根据密码进行登录
     */
    Users selectByPassword(String username, String password);
    Users login(String username);
    /**
     * 得到加密后的密码
     * @param username
     * @return
     */
    @Select("select password from tbl_users where username= #{username}")
    String selectPassword(String username);
    /**
     * 注册
     */
    int register(String username, String password, String user_type);

    int deleteByPrimaryKey(Integer userId);

    int insert(Users record);

    Users selectByPrimaryKey(Integer userId);

    /**
     * 按照用户名查询，判断该用户是否存在
     *
     * @param username
     * @return
     */
    Users selectByUsername(String username);

    List<Users> selectAll();

    int updateByPrimaryKey(Users record);

    @Update("UPDATE tbl_users set password = #{password} where "
        + "username =#{username}")
    int updatePasswordForget(@Param("username") String username,
        @Param("password") String password);

    /**
     * 根据用户类型返回用户
     */
    List<Users> selectAllByType(@Param("userType") String userType);

    /**
     * 根据用户类型、用户手机号，返回用户
     */
    List<Users> selectByTypeUsername(@Param("userType") String userType,
                                     @Param("username") String username);

    /**
     * 根据用户类型、用户真名，返回用户
     */
    List<Users> selectByTypeRealname(@Param("userType") String userType,
                                     @Param("realname") String realname);

    @Update("UPDATE tbl_users set real_name = #{userDTO.realName}, "
                + "id_card = #{userDTO.idCard}"
                + " where username = #{username}")
    int changeUserInfo(@Param("username") String username,
        @Param("userDTO") Users userDTO);

    @Update("UPDATE tbl_users set password = #{newPassword} "
        + "where username = #{username}")
    int updatePassword(@Param("username") String username,
                       @Param("newPassword") String newPasswordFir);

    @Select("Select * from tbl_users where user_type=#{type}")
    List<Users> selectByType(String type);

    @Select("select username from tbl_users where user_id=#{userId}")
    String getUserNumById(@Param("userId") int userId);
}