/*
 Navicat Premium Data Transfer
 Source Server         : 腾讯云
 Source Server Type    : MySQL
 Source Server Version : 80021
 Source Host           : 43.139.142.79:3306
 Source Schema         : millionproject

 Target Server Type    : MySQL
 Target Server Version : 80021
 File Encoding         : 65001

 Date: 15/02/2023 20:31:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tbl_audit_logs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_audit_logs`;
CREATE TABLE `tbl_audit_logs`  (
  `audit_logs_id` int(0) NOT NULL AUTO_INCREMENT,
  `road_apply_id` int(0) NULL DEFAULT NULL,
  `auditor_username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `audit_date` datetime(0) NULL DEFAULT NULL,
  `whether_pass` tinyint(1) NULL DEFAULT NULL,
  `whether_pass_reason` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`audit_logs_id`) USING BTREE,
  UNIQUE INDEX `road_apply_id`(`road_apply_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 94 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_audit_logs
-- ----------------------------
INSERT INTO `tbl_audit_logs` VALUES (46, 39, '10001', '2023-02-08 23:23:28', 0, '啊', 1);
INSERT INTO `tbl_audit_logs` VALUES (49, 10, '10001', '2023-02-08 23:58:50', 0, '啊啊', 1);
INSERT INTO `tbl_audit_logs` VALUES (56, 58, '10001', '2023-02-09 10:44:13', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (57, 57, '10001', '2023-02-09 10:45:46', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (60, 55, '10001', '2023-02-09 11:21:51', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (62, 54, '10001', '2023-02-09 11:21:56', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (63, 53, '10001', '2023-02-09 11:21:59', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (65, 51, '10001', '2023-02-09 11:22:05', 1, '', 1);
INSERT INTO `tbl_audit_logs` VALUES (67, 61, '10001', '2023-02-09 11:53:29', 1, '', 1);
INSERT INTO `tbl_audit_logs` VALUES (70, 59, '10001', '2023-02-09 11:54:28', 1, '', 1);
INSERT INTO `tbl_audit_logs` VALUES (84, 9, '10001', '2023-02-09 14:20:17', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (85, 1, '10001', '2023-02-09 14:22:28', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (86, 4, '10001', '2023-02-09 14:22:32', 0, '123', 0);
INSERT INTO `tbl_audit_logs` VALUES (87, 6, '10001', '2023-02-09 14:22:35', 0, '123', 0);
INSERT INTO `tbl_audit_logs` VALUES (88, 8, '10001', '2023-02-09 14:22:47', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (89, 68, '10001', '2023-02-11 17:36:11', 0, '不合格！！！', 0);
INSERT INTO `tbl_audit_logs` VALUES (90, 63, '18225374869', '2023-02-12 00:17:53', 0, '你太丑了', 0);
INSERT INTO `tbl_audit_logs` VALUES (91, 67, '10001', '2023-02-12 00:54:54', 1, '', 0);
INSERT INTO `tbl_audit_logs` VALUES (92, 69, '10001', '2023-02-12 01:00:39', 0, '货物过重', 0);
INSERT INTO `tbl_audit_logs` VALUES (93, 73, '10001', '2023-02-12 14:54:13', 1, '', 0);

-- ----------------------------
-- Table structure for tbl_blocked_segs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_blocked_segs`;
CREATE TABLE `tbl_blocked_segs`  (
  `blocked_seg_id` int(0) NOT NULL AUTO_INCREMENT,
  `seg_Id` int(0) NULL DEFAULT NULL,
  `begin_date` datetime(0) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `direction` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `reason` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`blocked_seg_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_blocked_segs
-- ----------------------------
INSERT INTO `tbl_blocked_segs` VALUES (16, 7, '2023-02-01 00:00:00', '2023-03-05 00:00:00', '东北', '撒旦发射点');
INSERT INTO `tbl_blocked_segs` VALUES (17, 8, '2023-02-01 00:00:00', '2023-03-07 00:00:00', '东', '版本');

-- ----------------------------
-- Table structure for tbl_high_roads
-- ----------------------------
DROP TABLE IF EXISTS `tbl_high_roads`;
CREATE TABLE `tbl_high_roads`  (
  `high_roads_id` int(0) NOT NULL AUTO_INCREMENT,
  `high_road_name` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `begin_node_id` int(0) NULL DEFAULT NULL,
  `end_node_id` int(0) NULL DEFAULT NULL,
  `truck_length` float NULL DEFAULT NULL,
  `truck_width` float NULL DEFAULT NULL,
  `truck_height` float NULL DEFAULT NULL,
  `truck_weight` float NULL DEFAULT NULL,
  `truck_axes` smallint(0) NULL DEFAULT NULL,
  `truck_tyres` smallint(0) NULL DEFAULT NULL,
  `road_seg_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `road_seg_comment` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`high_roads_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_high_roads
-- ----------------------------
INSERT INTO `tbl_high_roads` VALUES (1, '小心翼翼', 7, 7, 20, 20, 10, 100, 20, 20, '1', '暴雨');
INSERT INTO `tbl_high_roads` VALUES (2, '燃眉之急', 2, 8, 20, 20, 10, 100, 20, 20, '1', '小雨');
INSERT INTO `tbl_high_roads` VALUES (3, '由来已久', 1, 4, 20, 20, 10, 100, 20, 20, '1', '雪天');
INSERT INTO `tbl_high_roads` VALUES (4, '风口浪尖', 2, 1, 20, 20, 10, 100, 20, 20, '1', '晴天');
INSERT INTO `tbl_high_roads` VALUES (5, '艰苦奋斗', 8, 5, 20, 20, 10, 100, 20, 20, '1', '台风天');
INSERT INTO `tbl_high_roads` VALUES (6, '恶性循环', 4, 8, 20, 20, 10, 100, 20, 20, '1', '雨天');
INSERT INTO `tbl_high_roads` VALUES (7, '对症下药', 9, 6, 20, 20, 10, 100, 20, 20, '1', '雪天');
INSERT INTO `tbl_high_roads` VALUES (8, '数以万计', 2, 3, 20, 20, 10, 100, 20, 20, '1', '雨天');
INSERT INTO `tbl_high_roads` VALUES (9, '任重道远', 2, 5, 20, 20, 10, 100, 20, 20, '1', '小雨');
INSERT INTO `tbl_high_roads` VALUES (10, '精心设计', 4, 7, 20, 20, 10, 100, 20, 20, '1', '冰雹');

-- ----------------------------
-- Table structure for tbl_highroad_nodes
-- ----------------------------
DROP TABLE IF EXISTS `tbl_highroad_nodes`;
CREATE TABLE `tbl_highroad_nodes`  (
  `highroad_node_id` int(0) NOT NULL AUTO_INCREMENT,
  `high_roads_id` int(0) NULL DEFAULT NULL,
  `node_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`highroad_node_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_highroad_nodes
-- ----------------------------
INSERT INTO `tbl_highroad_nodes` VALUES (1, 1, 8);
INSERT INTO `tbl_highroad_nodes` VALUES (2, 4, 6);
INSERT INTO `tbl_highroad_nodes` VALUES (3, 5, 6);
INSERT INTO `tbl_highroad_nodes` VALUES (4, 4, 6);
INSERT INTO `tbl_highroad_nodes` VALUES (5, 4, 3);
INSERT INTO `tbl_highroad_nodes` VALUES (6, 5, 8);
INSERT INTO `tbl_highroad_nodes` VALUES (7, 6, 6);
INSERT INTO `tbl_highroad_nodes` VALUES (8, 8, 3);
INSERT INTO `tbl_highroad_nodes` VALUES (9, 4, 1);
INSERT INTO `tbl_highroad_nodes` VALUES (10, 9, 6);

-- ----------------------------
-- Table structure for tbl_login_logs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_login_logs`;
CREATE TABLE `tbl_login_logs`  (
  `login_log_id` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `log_time` datetime(0) NULL DEFAULT NULL,
  PRIMARY KEY (`login_log_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_login_logs
-- ----------------------------
INSERT INTO `tbl_login_logs` VALUES (1, '17229370572', '审核员', '2022-12-31 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (2, '17766623132', '管理员', '2022-01-16 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (3, '15373033034', '司机', '2022-12-12 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (4, '13548876877', '司机', '2022-06-03 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (5, '17540972172', '管理员', '2022-04-02 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (6, '15539963603', '司机', '2022-05-23 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (7, '15022391130', '管理员', '2022-05-24 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (8, '17147198189', '管理员', '2022-09-25 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (9, '17116973849', '审核员', '2022-12-01 00:00:00');
INSERT INTO `tbl_login_logs` VALUES (10, '17622268114', '审核员', '2022-05-09 00:00:00');

-- ----------------------------
-- Table structure for tbl_road_apply_logs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_road_apply_logs`;
CREATE TABLE `tbl_road_apply_logs`  (
  `road_apply_logs_id` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `apply_date` datetime(0) NULL DEFAULT NULL,
  `begin_date` datetime(0) NULL DEFAULT NULL,
  `end_date` datetime(0) NULL DEFAULT NULL,
  `pass_cities` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `road_begin` int(0) NULL DEFAULT NULL,
  `road_end` int(0) NULL DEFAULT NULL,
  `suggest_road` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `init_road` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `truck_license` char(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `whether_use_advice` tinyint(1) NULL DEFAULT NULL,
  `cargo_type` smallint(0) NULL DEFAULT NULL,
  `cargo_weight` float NULL DEFAULT NULL,
  `audit_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`road_apply_logs_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 80 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_road_apply_logs
-- ----------------------------
INSERT INTO `tbl_road_apply_logs` VALUES (1, '11111111111', '2022-04-02 00:00:01', '2022-03-18 00:00:34', '2022-11-17 00:00:00', '拉萨', 8, 2, '\"教字垭收费站---G5515--S10-G65-G56-6565--会同收费站\"', '\"宁远东收费站-G55二广高速-G60沪昆高速-G65包茂高速-G56杭瑞高速-黄丝桥收费站（往返）\"', '皖E00001', 0, 3, 69, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (2, '11111111111', '2022-03-02 00:00:00', '2022-08-28 00:00:00', '2022-11-17 00:00:00', '自贡', 7, 3, '\"吉首西收费站--G65包茂高速--矮寨收费站（往返）\"', '\"常德高新区（灌溪）收费站---G56杭瑞高速---G5513长张高速---张家界收费站\"', '皖E00002', 0, 3, 48, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (3, '22222222222', '2022-11-15 00:00:00', '2022-11-25 00:00:32', '2022-11-17 00:00:00', '宜宾', 1, 9, '\"金鸡收费站-G4京港澳高速-G0401绕城高速-松雅湖收费站\"', '\"金桥收费站--G0401长沙绕城高速--G6021杭长高速--洞阳收费站\"', '皖E00003', 1, 4, 75, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (4, '33333333333', '2022-05-29 00:00:00', '2022-11-16 00:10:00', '2022-11-17 00:00:00', '开封', 5, 9, '\"邵东收费站---G60沪昆高速---G65包茂高速---S97怀化绕城高速---池回收费站【往返】\"', '\"吉首西收费站--G65包茂高速--矮寨收费站（往返）\"', '皖E00004', 1, 2, 57, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (5, '44444444444', '2022-11-12 00:00:00', '2022-07-25 00:00:10', '2022-11-17 00:00:00', '厦门', 1, 6, '\"常德高新区（灌溪）收费站---G56杭瑞高速---G5513长张高速---张家界收费站\"', '\"吉首西收费站--G65包茂高速--矮寨收费站（往返）\"', '皖E00005', 1, 2, 59, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (6, '55555555555', '2022-01-01 00:00:00', '2022-05-01 00:00:20', '2022-11-17 00:00:00', '宜兴', 6, 4, '\"（往）怀化收费站---G65包茂高速---G60沪昆高速---G4京港澳高速---星沙收费站（返）星沙收费站---G4京港澳高速---G60沪昆高速---G65包茂高速---隆回收费站\"', '\"金鸡收费站-G4京港澳高速-G0401绕城高速-松雅湖收费站\"', '皖E00006', 1, 4, 62, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (7, '66666666666', '2022-09-29 00:00:00', '2022-05-29 00:00:00', '2022-11-17 00:00:00', '海口', 10, 3, '\"流峰收费站--G0421许广高速--G5513长张高速-G56杭瑞高速--G5513长张高速--慈利东收费站\"', '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', '皖E00007', 1, 3, 16, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (8, '77777777777', '2022-09-23 00:00:00', '2022-07-31 00:00:00', '2022-11-17 00:00:00', '呼和浩特', 1, 10, '\"吉首西收费站--G65包茂高速--矮寨收费站（往返）\"', '\"常德高新区（灌溪）收费站---G56杭瑞高速---G5513长张高速---G55二广高速---永州北收费站\"', '皖E00008', 0, 4, 58, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (9, '88888888888', '2022-03-15 00:00:00', '2022-12-13 00:00:00', '2022-11-17 00:00:00', '开封', 8, 3, '\"金桥收费站--G0401长沙绕城高速--G6021杭长高速--洞阳收费站\"', '\"邵东收费站---G60沪昆高速---G65包茂高速---S97怀化绕城高速---池回收费站【往返】\"', '皖E00009', 1, 1, 29, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (10, '99999999999', '2022-06-04 00:00:00', '2022-04-09 00:00:00', '2022-11-17 00:00:00', '佛山', 4, 10, '\"宁远东收费站-G55二广高速-G60沪昆高速-G65包茂高速-G56杭瑞高速-黄丝桥收费站（往返）\"', '\"金桥收费站--G0401长沙绕城高速--G6021杭长高速--洞阳收费站\"', '皖E00010', 1, 3, 89, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (24, '13187029351', '2022-11-21 00:00:00', '2022-11-01 00:00:00', '2022-11-17 00:00:00', '荆门市、长沙市', 1, 2, '\"万家丽南收费站---G56杭瑞高速---G5513长张高速---G55二广高速---常德北收费站\"', NULL, '皖E00001', 1, 1, 3, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (25, '13187029351', '2022-11-21 00:00:00', '2022-11-03 00:00:00', '2022-11-18 00:00:00', '长沙、衡阳', 1, 2, NULL, '长沙市-衡阳市', '皖E00001', 0, 1, 2, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (26, '13187029351', '2022-11-21 00:00:00', '2022-11-03 00:00:00', '2022-11-11 00:00:00', '菏泽市、长沙市', 1, 2, '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', NULL, '皖E00001', 1, 1, 2, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (27, '13187029351', '2022-11-21 00:00:00', '2022-11-03 00:00:01', '2022-11-11 00:00:01', '菏泽市、长沙市', 1, 2, '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', NULL, '皖E00001', 1, 1, 4, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (28, '13187029351', '2022-11-21 00:00:01', '2022-11-01 00:00:01', '2022-11-09 00:00:01', '菏泽市、长沙市', 1, 2, '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', NULL, '皖E00002', 1, 2, 3, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (29, '13187029351', '2022-11-21 20:05:35', '2022-11-03 19:05:06', '2022-11-18 17:05:06', '菏泽市、长沙市', 1, 2, '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', NULL, '皖E00001', 1, 1, 3, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (30, '13187029351', '2022-11-21 20:45:53', '2022-11-10 20:41:52', '2022-11-18 18:44:52', '北京、长沙、衡阳', 4, 5, '长沙收费站---北京---衡阳收费站', '长沙收费站---北京---衡阳收费站', '皖E00002', 0, 2, 2, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (31, '13187029351', '2022-11-22 18:44:00', '2022-11-08 17:43:28', '2022-11-16 18:43:28', '菏泽市、长沙市', 1, 2, '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', NULL, '皖E00003', 1, 2, 5, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (32, '13187029351', '2022-11-22 19:15:57', '2022-11-01 18:15:13', '2022-11-09 18:15:13', '北京、甘肃', 4, 5, '长沙收费站---北京---衡阳收费站', '长沙收费站---北京---甘肃---衡阳收费站', '皖E00004', 0, 1, 5, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (33, '13187029351', '2022-11-22 19:16:40', '2022-11-01 18:16:07', '2022-11-09 18:16:07', '北京', 4, 5, '长沙收费站---北京---衡阳收费站', '衡阳收费站---上海---甘肃收费站', '皖E00003', 0, 1, 2, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (34, '13187029351', '2022-11-23 13:32:24', '2022-11-02 11:31:47', '2022-11-10 13:30:47', '菏泽市、长沙市', 1, 2, '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', NULL, '皖E00002', 1, 1, 3, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (35, '13187029351', '2022-11-23 13:35:40', '2022-11-02 12:34:57', '2022-11-11 11:34:57', '长沙、菏泽', 4, 5, '长沙收费站---北京---衡阳收费站', '长沙收费站---北京---衡阳收费站', '皖E00001', 0, 1, 4.4, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (36, '13187029351', '2022-11-23 15:29:34', '2022-11-02 14:28:51', '2022-11-10 14:28:51', '菏泽市、长沙市', 1, 2, '长沙收费站---北京---甘肃收费站', NULL, '皖E00001', 1, 1, 2, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (37, '13187029351', '2022-11-23 16:53:08', '2022-11-02 15:52:04', '2022-11-10 15:52:04', '菏泽市、长沙市、衡阳', 1, 2, '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', NULL, '皖E00003', 1, 2, 2.2, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (38, '13187029351', '2022-11-23 16:54:25', '2022-11-02 15:53:51', '2022-11-10 15:53:51', '荆门市', 1, 2, '\"万家丽南收费站---G56杭瑞高速---G5513长张高速---G55二广高速---常德北收费站\"', NULL, '皖E00002', 1, 1, 2, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (39, '13187029351', '2022-11-28 21:40:00', '2022-11-03 19:39:36', '2022-11-11 19:39:36', '菏泽市、长沙市', 1, 2, '\"万家丽北收费站---G0401长沙绕城高速---观音岩互通---G5517长常北线高速---苏家坝枢纽---G5513长张高速---石门桥枢纽---G55二广高速---常德收费站（来回往返）\"', NULL, '皖E00005', 1, 1, 5.5, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (49, '13187029351', '2022-12-06 23:29:05', '2022-12-01 22:05:11', '2022-12-03 23:03:11', '长沙市,岳阳市,湖北省,荆门市,随州市,南阳市,驻马店市,平顶山市,洛阳市,焦作市,河南省,晋城市,长治市,晋中市,太原市', 29, 30, '长沙汽车西站-张家界中心汽车站---玉兰路---枫林三路辅路---麓景路---岳麓西大道辅路---岳麓西大道---G5513长张高速---简家坳枢纽---G0421许广高速---九龙山隧道---G0421许广高速---G36宁洛高速---G55二广高速---省庄隧道---雨泼沟隧道---凤凰台1号隧道---凤凰台2号隧道---凤凰台3号隧道---龙凤沟隧道---古铜沟1号隧道---古铜沟2号隧道---四里沟隧道---仙神河隧道---拍盘隧道---月湖泉隧道---柿树掌隧道---朝阳2号隧道---朝阳1号隧道---天井关隧道---李家庄隧道---苇元隧道---东耿窑隧道---G55二广高速---第16隧道---第15隧道---第14隧道---第13隧道---第12隧道---第11隧道---第10隧道---第9隧道---第8隧道---第7隧道---第6隧道---第5隧道---第4隧道---第3隧道---第2隧道---第1隧道---G55二广高速---G55二广高速出口---滨河东路---滨河东路出口---真武路---昌盛街---龙盛街---唐华路---是诚广告图文', NULL, '皖E00005', 1, 1, 2.2, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (50, '13187029351', '2022-12-06 23:37:11', '2022-12-01 22:05:11', '2022-12-03 23:03:11', '长沙市,岳阳市,湖北省,荆门市,随州市,南阳市,驻马店市,平顶山市,洛阳市,焦作市,河南省,晋城市,长治市,晋中市,太原市', 29, 30, '长沙汽车西站-张家界中心汽车站---玉兰路---枫林三路辅路---麓景路---岳麓西大道辅路---岳麓西大道---G5513长张高速---简家坳枢纽---G0421许广高速---九龙山隧道---G0421许广高速---G36宁洛高速---G55二广高速---省庄隧道---雨泼沟隧道---凤凰台1号隧道---凤凰台2号隧道---凤凰台3号隧道---龙凤沟隧道---古铜沟1号隧道---古铜沟2号隧道---四里沟隧道---仙神河隧道---拍盘隧道---月湖泉隧道---柿树掌隧道---朝阳2号隧道---朝阳1号隧道---天井关隧道---李家庄隧道---苇元隧道---东耿窑隧道---G55二广高速---第16隧道---第15隧道---第14隧道---第13隧道---第12隧道---第11隧道---第10隧道---第9隧道---第8隧道---第7隧道---第6隧道---第5隧道---第4隧道---第3隧道---第2隧道---第1隧道---G55二广高速---G55二广高速出口---滨河东路---滨河东路出口---真武路---昌盛街---龙盛街---唐华路---是诚广告图文', NULL, '皖E00005', 1, 1, 2.2, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (51, '10000', '2023-01-21 11:52:52', '2023-01-07 12:00:00', '2023-02-04 12:00:00', '朝阳市,锦州市,葫芦岛市,秦皇岛市,唐山市,天津城区,沧州市,滨州市,淄博市,潍坊市,临沂市,日照市,连云港市', 1, 2, NULL, '朝阳市---玉龙大街---什家河北路---燕都大桥---麒麟大桥---凤凰大街---南外环路---龙山街---G16丹锡高速入口---松山互通---G1京哈高速出口---G25长深高速---京津高速互通立交---G0111秦滨高速出口---秦滨高速互通立交---G0111秦滨高速---S27沾临高速---张店枢纽立交---于家庄枢纽---G25长深高速---穆陵关隧道---城南枢纽---G15沈海高速出口---311国道---洪门路---海连西路---江化北路---新海路---新海路', '皖E00003', 0, 2, 70, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (52, '12345678999', '2023-02-08 16:15:19', '2023-02-09 12:00:00', '2023-03-23 12:00:00', '长沙市,益阳市,常德市,张家界市', 1, 2, NULL, '长沙市---岳麓大道---G5513长张高速---关口垭隧道---狗子滩隧道---张家界互通---永定大道---体育馆路---永昌路---鼎泰路---大庸路---机场路---张家界市', '湘4', 0, 2, 3, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (53, '13187029351', '2023-02-09 10:24:19', '2023-02-09 12:00:00', '2023-02-17 12:00:00', '长沙、衡阳', 3, 4, NULL, '小龙收费站---消灾---小雯收费站', '皖E00003', 0, 2, 6.3, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (54, '13187029351', '2023-02-09 10:26:09', '2023-02-09 12:00:00', '2023-02-17 12:00:00', '长沙、衡阳', 3, 4, NULL, '万家丽收费站---消灾---常德收费站', '皖E00003', 0, 2, 6.3, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (55, '13187029351', '2023-02-09 10:27:45', '2023-02-09 12:00:00', '2023-02-17 12:00:00', '长沙、衡阳', 3, 4, NULL, '云溪收费站(G0421许广高速入口)---消灾---收费站(城陵矶高速出口)', '皖E00003', 0, 2, 6.3, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (56, '13187029351', '2023-02-09 10:28:47', '2023-02-09 12:00:00', '2023-02-17 12:00:00', '长沙、衡阳', 3, 4, NULL, '云溪收费站(G0421许广高速入口)---消灾---收费站(城陵矶高速出口)', '皖E00003', 0, 2, 6.3, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (57, '13187029351', '2023-02-09 10:37:29', '2023-02-09 12:00:00', '2023-02-17 12:00:00', '长沙、衡阳', 3, 4, NULL, '云溪收费站(G0421许广高速入口)---消灾---收费站(城陵矶高速出口)', '皖E00003', 0, 2, 6.3, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (58, '13187029351', '2023-02-09 10:38:54', '2023-02-09 12:00:00', '2023-02-17 12:00:00', '长沙、衡阳', 1222, 1223, '云溪收费站(G0421许广高速入口)---消灾---收费站(城陵矶高速出口)', NULL, '皖E00003', 1, 2, 6.3, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (59, '13187029351', '2023-02-09 11:16:50', '2023-02-02 12:00:00', '2023-02-09 12:00:00', '济南市,德州市,沧州市,廊坊市,北京城区', 2336, 2337, '是小諾吧民宿---尹家巷---按察司街---大明湖路---黑虎泉北路---明湖东路---历黄路---北园立交桥---济南北立交---华山枢纽立交---北野场桥---刘二村桥---S3501大兴机场高速---小白楼桥---晓月隧道---晋元桥---八角东街---时代花园南路', NULL, '皖E00005', 1, 1, 3.6, '1', 1);
INSERT INTO `tbl_road_apply_logs` VALUES (60, '10000', '2023-02-09 11:43:27', '2023-02-03 12:00:00', '2023-02-10 12:00:00', '重庆城区,重庆郊县,恩施土家族苗族自治州,湘西土家族苗族自治州,张家界市,常德市,益阳市,长沙市', 2338, 2339, '重庆市---人民支路---中山四路---中山三路---菜园坝长江大桥---南城隧道---大石路立交---海峡路---江南立交---真武山隧道---G65包茂高速---南环立交---G65包茂高速---南湖隧道---太平隧道---接龙隧道---石龙隧道---丰岩隧道---炉场坡隧道---龙凤山隧道---G65包茂高速---白云隧道---长坝隧道---白马隧道---羊角隧道---大湾隧道---G65包茂高速---黄草岭隧道---武隆隧道---枫香隧道---下石桥隧道---中兴隧道---银盘隧道---佛仙寺隧道---共和隧道---柿子坪隧道---石坎子隧道---胡家湾隧道---董家湾隧道---望江寺隧道---高谷隧道---下塘隧道---G65包茂高速---彭水隧道---长滩隧道---钟山隧道---新开洞隧道---鱼泉隧道---王家堡隧道---贺家堡隧道---大龙洞隧道---蔡家堡隧道---廖家坝隧道---楼坪隧道---桐子林隧道---王家坪隧道---窑坪隧道---沙坝隧道---石会隧道---武陵隧道---G65包茂高速---黔恩枢纽---高家湾隧道---茅草坪隧道---楠木沟隧道---情侣山隧道---瓦窑沟隧道---仰头山隧道---马鞍山隧道---熊家槽隧道---白岩脚隧道---花果山隧道---廖家坡隧道---吴家湾隧道---茶园坡隧道---牛塘隧道---椿木槽隧道---大岩坝隧道---李家槽隧道---宣恩互通---陈家坡隧道---黄土坡1号隧道---黄土坡2号隧道---大茅坡隧道---高罗隧道---当阳坪隧道---砂子坡隧道---茅坪隧道---庙岭界隧道---撮箕坡Ⅰ号隧道---撮箕坡Ⅱ号隧道---水畲村隧道---红岩溪隧道---观音岩隧道---永龙界隧道---大干溪1号隧道---大干溪Ⅳ号隧道---响米坳隧道---利布坳隧道---大坝隧道---海洛枢纽---科洞二号隧道---科洞1号隧道---老庄二号隧道---老庄一号隧道---青坪隧道---S10张花高速---舒家湾隧道---刘家峪隧道---楠木溪隧道---大栗坡隧道---刘家院子隧道---凉亭垭隧道---老屋湾隧道---岩门口隧道---关口垭隧道---G5513长张高速---岳麓西大道---岳麓大道---长沙市', NULL, '湘4', 1, 1, 11, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (61, '13187029351', '2023-02-09 11:50:45', '2023-02-01 12:00:00', '2023-02-09 12:00:00', '济南市,德州市,沧州市,天津城区,唐山市,秦皇岛市,葫芦岛市,锦州市,盘锦市,鞍山市,沈阳市', 2336, 2340, '是小諾吧民宿---尹家巷---按察司街---大明湖路---黑虎泉北路---明湖东路---历黄路---北园立交桥---济南北立交---华山枢纽立交---G2京沪高速出口---G1811黄石高速---G1811黄石高速出口---团泊南立交---G25长深高速---于家岭大桥---G25长深高速---S0105绕城高速出口---G1京哈高速---G1京哈高速出口---S2沈康高速---尚屯互通---沈阳法库财湖机场', NULL, '皖E00004', 1, 1, 3.6, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (62, '10000', '2023-02-09 13:01:04', '2023-02-04 12:00:00', '2023-03-06 12:00:00', '重庆市', 2341, 2342, NULL, '不推荐的路线', '湘5', 0, 1, 11, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (63, '10000', '2023-02-09 13:03:03', '2023-02-04 12:00:00', '2023-02-11 12:00:00', '重庆郊县,重庆城区', 2341, 2342, '彭水站---长滩大桥---彭水东互通---长滩隧道---彭水隧道---下塘隧道---高谷隧道---望江寺隧道---董家湾隧道---胡家湾隧道---石坎子隧道---柿子坪隧道---共和隧道---佛仙寺隧道---G65包茂高速---银盘隧道---中兴隧道---下石桥隧道---枫香隧道---武隆隧道---黄草岭隧道---大湾隧道---羊角隧道---白马隧道---长坝隧道---白云隧道---G65包茂高速---龙凤山隧道---炉场坡隧道---丰岩隧道---石龙隧道---接龙隧道---太平隧道---南湖隧道---G65包茂高速---南环互通---吉庆隧道---小泉隧道---渝黔高架桥---G75兰海高速---西环立交---新中梁山隧道---新宋家沟二号隧道---宋家沟一号隧道---G85银昆高速---缙云山隧道---G85银昆高速---璧山南互通---虎峰大道---璧山站', NULL, '湘5', 1, 1, 11, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (64, '13187029351', '2023-02-09 14:15:33', '2023-02-02 12:00:00', '2023-02-09 12:00:00', '长沙', 2341, 2342, NULL, '小龙', '皖E00001', 0, 1, 2.3, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (65, '13187029351', '2023-02-09 14:19:52', '2023-02-02 12:00:00', '2023-02-09 12:00:00', '衡阳市', 2341, 2342, NULL, '哒哒哒哒哒哒多多', '皖E00001', 0, 1, 3.2, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (66, '10000', '2023-02-09 14:20:28', '2023-02-09 12:00:00', '2023-02-17 12:00:00', '重庆市', 2341, 2342, NULL, '彭水-----重庆', '湘5', 0, 1, 11, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (67, '10000', '2023-02-09 14:34:35', '2023-02-03 12:00:00', '2023-02-11 12:00:00', '重庆', 2341, 2342, NULL, '重庆是是是---啊啊啊', '湘5', 0, 1, 11, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (68, '13187029351', '2023-02-09 14:45:56', '2023-02-02 12:00:00', '2023-02-09 12:00:00', '是是是', 2341, 2342, NULL, '少时诵诗书', '皖E00001', 0, 2, 3.3, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (69, '10000', '2023-02-10 10:45:24', '2023-02-03 12:00:00', '2023-02-10 12:00:00', '重庆郊县,重庆城区', 2341, 2342, '彭水站---长滩大桥---彭水东互通---长滩隧道---彭水隧道---下塘隧道---高谷隧道---望江寺隧道---董家湾隧道---胡家湾隧道---石坎子隧道---柿子坪隧道---共和隧道---佛仙寺隧道---G65包茂高速---银盘隧道---中兴隧道---下石桥隧道---枫香隧道---武隆隧道---黄草岭隧道---大湾隧道---羊角隧道---白马隧道---长坝隧道---白云隧道---G65包茂高速---龙凤山隧道---炉场坡隧道---丰岩隧道---石龙隧道---接龙隧道---太平隧道---南湖隧道---G65包茂高速---南环互通---吉庆隧道---小泉隧道---渝黔高架桥---G75兰海高速---西环立交---新中梁山隧道---新宋家沟二号隧道---宋家沟一号隧道---G85银昆高速---缙云山隧道---G85银昆高速---璧山南互通---虎峰大道---璧山站', NULL, '湘5', 1, 1, 99, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (70, '99999999999', '2023-02-10 15:27:13', '2023-02-04 12:00:00', '2023-03-04 12:00:00', '长沙市,岳阳市', 2242, 1236, '长沙收费站(G6021杭长高速出口)---G6021杭长高速---长沙北枢纽---G4京港澳高速---岳阳收费站(G4京港澳高速入口)', NULL, '湘1', 1, 4, 22, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (71, '99999999999', '2023-02-10 15:36:41', '2023-02-04 12:00:00', '2023-03-05 12:00:00', '常德市,益阳市,长沙市,湘潭市,株洲市', 2343, 2344, '张家界收费站入口(阳和方向)---关口垭隧道---G5513长张高速---简家坳枢纽---邓家湾隧道---龙洞隧道---李家冲隧道---毛栗冲隧道---狮子垄隧道---塔岭枢纽互通---G60沪昆高速---殷家坳枢纽---株洲西互通---动力谷大道---猎豹路---上汽路---株洲西互通---株洲西收费站入口(长沙方向)', NULL, '湘4', 1, 2, 77, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (72, '99999999999', '2023-02-10 15:39:25', '2023-02-04 12:00:00', '2023-03-05 12:00:00', '常德市,益阳市,长沙市,湘潭市,株洲市', 2345, 2346, '株洲市---天台路---黄河北路---神农大道---株洲大道---株洲西互通---马家河互通---湘潭大道---湖湘东路---湘潭市', NULL, '湘1', 1, 2, 55, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (73, '13187029351', '2023-02-11 12:45:10', '2023-02-04 12:00:00', '2023-03-02 12:00:00', '长沙市,湘潭市,株洲市,张家界市,常德市,益阳市', 2347, 2344, '张家界荷花国际机场航站楼---荷花路---机场路---大庸路---鼎泰路---澧兰中路---永昌路---体育馆路---永定大道---张家界互通---关口垭隧道---G5513长张高速---简家坳枢纽---邓家湾隧道---龙洞隧道---李家冲隧道---毛栗冲隧道---狮子垄隧道---塔岭枢纽互通---G60沪昆高速---殷家坳枢纽---株洲西互通---动力谷大道---猎豹路---上汽路---株洲西互通---株洲西收费站入口(长沙方向)', NULL, '皖E00001', 1, 2, 34, '1', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (74, '13187029351', '2023-02-12 00:42:25', '2023-02-07 12:00:00', '2023-02-09 12:00:00', '延边朝鲜族自治州,白山市,通化市', 2348, 2349, '比较大卷饼(中兴花园1期店)---学府街---南环路---G11鹤大高速入口---柞木台隧道---大蒲柴河隧道---白水滩隧道---二道岭隧道---后崴子隧道---荒沟门隧道---松阳隧道---长板坡隧道---G11鹤大高速---十道羊岔隧道---回头沟隧道---朝阳隧道---高丽沟隧道---兴林隧道---东南岔隧道---光华隧道---闹枝隧道---G11鹤大高速---马当隧道---二密隧道---太安隧道---黎明互通---河口隧道---湾湾川隧道---夹皮沟隧道---砬子沟隧道---前进隧道---矿山隧道---五女峰隧道---G1112集双高速---高台子隧道---G1112集双高速---迎宾岭隧道---G1112集双高速出口---黎明北街---Hi大明家庭火锅便利店', NULL, '皖E00007', 1, 2, 2.3, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (75, '13187029351', '2023-02-12 01:34:50', '2023-02-03 12:00:00', '2023-02-10 12:00:00', '长沙市', NULL, NULL, NULL, '长沙市---岳阳市---衡阳市', '皖E00007', 0, 2, 2.2, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (76, '13187029351', '2023-02-12 01:50:05', '2023-02-01 00:00:00', '2023-02-09 00:00:00', '长沙市', NULL, NULL, NULL, '长沙市---株洲市---常德市', '皖E00001', 0, 1, 11, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (77, '13187029351', '2023-02-12 14:28:39', '2023-02-02 00:00:00', '2023-03-03 00:00:00', '长沙市,湘潭市', 2350, 2351, '长沙站---东二环辅路---东二环入口---东二环---南二环---云栖路---S41长潭西高速---文星门路---泗州路---文星门路---广场东支路---湘潭站', NULL, '皖E00001', 1, 2, 43, '0', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (78, '13187029351', '2023-02-12 14:43:41', '2023-02-07 00:00:00', '2023-03-01 00:00:00', 'qin zhou shi', NULL, NULL, NULL, '66收费站---12高速---学学学收费站', '皖E00001', 0, 2, 12, '2', 0);
INSERT INTO `tbl_road_apply_logs` VALUES (79, '13187029351', '2023-02-12 14:50:31', '2023-02-01 00:00:00', '2023-03-21 00:00:00', '长沙市,湘潭市,株洲市', 2352, 2344, '长沙汽车东站-张家界中心汽车站---京珠西辅道---人民东路---人民东路浏阳河大桥---长善路---支四十二路---江榕路---花侯路---长沙大道---S40机场高速入口---雨花枢纽---株洲西互通---动力谷大道---猎豹路---上汽路---株洲西互通---株洲西收费站入口(长沙方向)', NULL, '皖E00001', 1, 1, 1, '0', 0);

-- ----------------------------
-- Table structure for tbl_road_nodes
-- ----------------------------
DROP TABLE IF EXISTS `tbl_road_nodes`;
CREATE TABLE `tbl_road_nodes`  (
  `road_nodes_id` int(0) NOT NULL AUTO_INCREMENT,
  `road_segs_id` int(0) NULL DEFAULT NULL,
  `high_roads_id` int(0) NULL DEFAULT NULL,
  `node_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `node_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `truck_height` float NULL DEFAULT NULL,
  `truck_width` float NULL DEFAULT NULL,
  `longitude` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `latitude` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `index_cities` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `node_state` tinyint(1) NULL DEFAULT NULL,
  `node_comment` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`road_nodes_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2353 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_road_nodes
-- ----------------------------
INSERT INTO `tbl_road_nodes` VALUES (1222, NULL, NULL, '云溪收费站(G0421许广高速入口)', '收费站', 20, 10, '113.260790', '29.440325', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1223, NULL, NULL, '收费站(城陵矶高速出口)', '收费站', 20, 10, '113.211458', '29.451746', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1224, NULL, NULL, '收费站(城陵矶高速入口)', '收费站', 20, 10, '113.211375', '29.451722', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1225, NULL, NULL, '君山收费站(G56杭瑞高速入口)', '收费站', 20, 10, '113.026043', '29.444476', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1226, NULL, NULL, '君山收费站(G56杭瑞高速出口)', '收费站', 20, 10, '113.025760', '29.444504', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1227, NULL, NULL, '许市收费站(G56杭瑞高速出口)', '收费站', 20, 10, '112.778969', '29.544258', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1228, NULL, NULL, '胥家桥收费站(城陵矶高速出口)', '收费站', 20, 10, '113.207551', '29.398978', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1229, NULL, NULL, '胥家桥收费站(城陵矶高速入口)', '收费站', 20, 10, '113.207718', '29.398994', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1230, NULL, NULL, '岳阳收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.275577', '29.271357', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1231, NULL, NULL, '许市收费站(G56杭瑞高速入口)', '收费站', 20, 10, '112.778825', '29.544100', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1232, NULL, NULL, '云溪收费站(G0421许广高速出口)', '收费站', 20, 10, '113.260889', '29.440213', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1233, NULL, NULL, '巴陵收费站(G0421许广高速出口)', '收费站', 20, 10, '113.240606', '29.520365', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1234, NULL, NULL, '巴陵收费站(G0421许广高速入口)', '收费站', 20, 10, '113.240683', '29.520219', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1235, NULL, NULL, '冷水铺收费站(G56杭瑞高速入口)', '收费站', 20, 10, '113.168449', '29.413764', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1236, NULL, NULL, '岳阳收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.274986', '29.271135', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1237, NULL, NULL, '冷水铺收费站(G56杭瑞高速出口)', '收费站', 20, 10, '113.168633', '29.413654', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1238, NULL, NULL, '岳阳东收费站(G0421许广高速出口)', '收费站', 20, 10, '113.242717', '29.318798', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1239, NULL, NULL, '岳阳东收费站(G0421许广高速入口)', '收费站', 20, 10, '113.242470', '29.318712', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1240, NULL, NULL, '忠防收费站(G56杭瑞高速出口)', '收费站', 20, 10, '113.464254', '29.329517', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1241, NULL, NULL, '桃林收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.403528', '29.336978', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1242, NULL, NULL, '桃林收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.403449', '29.336799', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1243, NULL, NULL, '临湘收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.440516', '29.461674', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1244, NULL, NULL, '临湘收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.440625', '29.461956', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1245, NULL, NULL, '金鸡收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.599851', '29.523860', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1246, NULL, NULL, '金鸡收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.599660', '29.523822', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1247, NULL, NULL, '华容西收费站(G56杭瑞高速出口)', '收费站', 20, 10, '112.513452', '29.534422', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1248, NULL, NULL, '收费站(平益高速入口)', '收费站', 20, 10, '113.088825', '28.756628', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1249, NULL, NULL, '收费站(平益高速出口)', '收费站', 20, 10, '113.088959', '28.756623', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1250, NULL, NULL, '收费站(平益高速入口)', '收费站', 20, 10, '112.912496', '28.715349', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1251, NULL, NULL, '收费站(平益高速出口)', '收费站', 20, 10, '112.912463', '28.715402', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1252, NULL, NULL, '收费站(平益高速入口)', '收费站', 20, 10, '112.613043', '28.625161', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1253, NULL, NULL, '收费站(平益高速出口)', '收费站', 20, 10, '112.613105', '28.625166', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1254, NULL, NULL, '安定收费站(G0422武深高速出口)', '收费站', 20, 10, '113.617565', '28.575142', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1255, NULL, NULL, '长寿收费站(平益高速出口)', '收费站', 20, 10, '113.916016', '28.742284', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1256, NULL, NULL, '加义收费站(平益高速入口)', '收费站', 20, 10, '113.863670', '28.685690', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1257, NULL, NULL, '加义收费站(平益高速出口)', '收费站', 20, 10, '113.863563', '28.685737', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1258, NULL, NULL, '浯口收费站(平益高速入口)', '收费站', 20, 10, '113.346702', '28.761706', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1259, NULL, NULL, '浯口收费站(平益高速出口)', '收费站', 20, 10, '113.346760', '28.761682', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1260, NULL, NULL, '平江收费站(平益高速出口)', '收费站', 20, 10, '113.589676', '28.659879', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1261, NULL, NULL, '平江收费站(平益高速入口)', '收费站', 20, 10, '113.589546', '28.659874', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1262, NULL, NULL, '瓮江收费站(平益高速出口)', '收费站', 20, 10, '113.452614', '28.707186', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1263, NULL, NULL, '瓮江收费站(平益高速入口)', '收费站', 20, 10, '113.452678', '28.707188', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1264, NULL, NULL, '龙门收费站(平益高速出口)', '收费站', 20, 10, '114.009655', '28.843131', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1265, NULL, NULL, '龙门收费站(平益高速入口)', '收费站', 20, 10, '114.009654', '28.843185', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1266, NULL, NULL, '长寿收费站(平益高速入口)', '收费站', 20, 10, '113.916161', '28.742284', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1267, NULL, NULL, '三市收费站(平益高速入口)', '收费站', 20, 10, '113.700548', '28.654067', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1268, NULL, NULL, '三市收费站(平益高速出口)', '收费站', 20, 10, '113.700488', '28.654070', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1269, NULL, NULL, '大荆收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.266365', '28.958633', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1270, NULL, NULL, '华容东收费站(G56杭瑞高速入口)', '收费站', 20, 10, '112.626290', '29.554241', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1271, NULL, NULL, '忠防收费站(G56杭瑞高速入口)', '收费站', 20, 10, '113.464093', '29.329569', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1272, NULL, NULL, '汨罗东收费站(平益高速出口)', '收费站', 20, 10, '113.162545', '28.757015', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1273, NULL, NULL, '汨罗东收费站(平益高速入口)', '收费站', 20, 10, '113.162379', '28.757029', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1274, NULL, NULL, '詹桥收费站(G56杭瑞高速入口)', '收费站', 20, 10, '113.590869', '29.305444', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1275, NULL, NULL, '詹桥收费站(G56杭瑞高速出口)', '收费站', 20, 10, '113.591033', '29.305376', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1276, NULL, NULL, '鹤龙湖收费站(平益高速出口)', '收费站', 20, 10, '112.767372', '28.675236', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1277, NULL, NULL, '鹤龙湖收费站(平益高速入口)', '收费站', 20, 10, '112.767314', '28.675258', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1278, NULL, NULL, '华容西收费站(G56杭瑞高速入口)', '收费站', 20, 10, '112.513419', '29.534631', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1279, NULL, NULL, '岳阳南收费站(G0421许广高速出口)', '收费站', 20, 10, '113.226607', '29.218754', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1280, NULL, NULL, '岳阳南收费站(G0421许广高速入口)', '收费站', 20, 10, '113.226462', '29.219004', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1281, NULL, NULL, '上塔市收费站(G0422武深高速出口)', '收费站', 20, 10, '113.775768', '29.071643', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1282, NULL, NULL, '华容东收费站(G56杭瑞高速出口)', '收费站', 20, 10, '112.626250', '29.554050', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1283, NULL, NULL, '黄沙街收费站(G0421许广高速入口)', '收费站', 20, 10, '113.119579', '29.008503', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1284, NULL, NULL, '黄沙街收费站(G0421许广高速出口)', '收费站', 20, 10, '113.119907', '29.008314', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1285, NULL, NULL, '汨罗北收费站(G0421许广高速出口)', '收费站', 20, 10, '113.048723', '28.919527', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1286, NULL, NULL, '汨罗北收费站(G0421许广高速入口)', '收费站', 20, 10, '113.048838', '28.919548', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1287, NULL, NULL, '荣家湾收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.257644', '29.110182', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1288, NULL, NULL, '湘阴东六塘收费站(G0421许广高速出口)', '收费站', 20, 10, '112.965006', '28.709918', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1289, NULL, NULL, '湘阴东六塘收费站(G0421许广高速入口)', '收费站', 20, 10, '112.965079', '28.709929', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1290, NULL, NULL, '新墙河岳阳县收费站(G0421许广高速入口)', '收费站', 20, 10, '113.176815', '29.127811', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1291, NULL, NULL, '新墙河岳阳县收费站(G0421许广高速出口)', '收费站', 20, 10, '113.176997', '29.127996', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1292, NULL, NULL, '屈原管理区收费站(G0421许广高速出口)', '收费站', 20, 10, '113.012083', '28.857764', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1293, NULL, NULL, '屈原管理区收费站(G0421许广高速入口)', '收费站', 20, 10, '113.012443', '28.857623', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1294, NULL, NULL, '湘阴南洋沙湖收费站(G0421许广高速出口)', '收费站', 20, 10, '112.931135', '28.625990', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1295, NULL, NULL, '大荆收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.266356', '28.958425', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1296, NULL, NULL, '湘阴南洋沙湖收费站(G0421许广高速入口)', '收费站', 20, 10, '112.930746', '28.625948', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1297, NULL, NULL, '南江收费站(G0422武深高速入口)', '收费站', 20, 10, '113.748289', '28.988156', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1298, NULL, NULL, '南江收费站(G0422武深高速出口)', '收费站', 20, 10, '113.748459', '28.988067', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1299, NULL, NULL, '梅仙收费站(G0422武深高速入口)', '收费站', 20, 10, '113.608768', '28.833863', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1300, NULL, NULL, '梅仙收费站(G0422武深高速出口)', '收费站', 20, 10, '113.608962', '28.833774', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1301, NULL, NULL, '平江收费站(G0422武深高速入口)', '收费站', 20, 10, '113.635457', '28.678986', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1302, NULL, NULL, '平江收费站(G0422武深高速出口)', '收费站', 20, 10, '113.635443', '28.679209', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1303, NULL, NULL, '安定收费站(G0422武深高速入口)', '收费站', 20, 10, '113.617546', '28.575292', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1304, NULL, NULL, '平江西收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.255152', '28.778224', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1305, NULL, NULL, '平江西收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.255098', '28.777958', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1306, NULL, NULL, '上塔市收费站(G0422武深高速入口)', '收费站', 20, 10, '113.775559', '29.071711', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1307, NULL, NULL, '屈原管理区收费站(G0421许广高速入口)', '收费站', 20, 10, '113.012308', '28.857678', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1308, NULL, NULL, '荣家湾收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.257469', '29.110083', '岳阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1309, NULL, NULL, '张家界西收费站(S10张花高速出口)', '收费站', 20, 10, '110.425034', '29.159479', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1310, NULL, NULL, '茅岩河收费站(S10张花高速出口)', '收费站', 20, 10, '110.324340', '29.147450', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1311, NULL, NULL, '茅岩河收费站(S10张花高速入口)', '收费站', 20, 10, '110.324436', '29.147330', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1312, NULL, NULL, '杨家界收费站(G5515张桑高速出口)', '收费站', 20, 10, '110.319135', '29.297290', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1313, NULL, NULL, '杨家界收费站(G5515张桑高速入口)', '收费站', 20, 10, '110.319052', '29.297670', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1314, NULL, NULL, '张家界西收费站(S10张花高速入口)', '收费站', 20, 10, '110.425154', '29.159194', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1315, NULL, NULL, '张家界收费站(G5513长张高速出口)', '收费站', 20, 10, '110.556400', '29.134277', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1316, NULL, NULL, '张家界收费站(S10张花高速入口)', '收费站', 20, 10, '110.556610', '29.134188', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1317, NULL, NULL, '慈利东收费站(G5513长张高速入口)', '收费站', 20, 10, '111.172906', '29.416427', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1318, NULL, NULL, '武陵源收费站(G5513长张高速出口)', '收费站', 20, 10, '110.716581', '29.256766', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1319, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '111.032624', '29.546782', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1320, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '111.032604', '29.546731', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1321, NULL, NULL, '收费站(G5515张南高速出口)', '收费站', 20, 10, '109.951624', '29.437848', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1322, NULL, NULL, '收费站(G5515张南高速入口)', '收费站', 20, 10, '109.951682', '29.437823', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1323, NULL, NULL, '苗市收费站(S12安慈高速出口)', '收费站', 20, 10, '111.286166', '29.547500', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1324, NULL, NULL, '苗市收费站(S12安慈高速入口)', '收费站', 20, 10, '111.286038', '29.547471', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1325, NULL, NULL, '岩泊渡收费站(G5513长张高速入口)', '收费站', 20, 10, '110.980973', '29.374982', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1326, NULL, NULL, '桑植东收费站(G5515张桑高速入口)', '收费站', 20, 10, '110.222423', '29.424964', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1327, NULL, NULL, '桑植东收费站(G5515张桑高速出口)', '收费站', 20, 10, '110.222473', '29.425074', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1328, NULL, NULL, '桑植西收费站(G5515张桑高速出口)', '收费站', 20, 10, '110.131582', '29.426915', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1329, NULL, NULL, '桑植西收费站(G5515张南高速入口)', '收费站', 20, 10, '110.131688', '29.426884', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1330, NULL, NULL, '慈利收费站(S12安慈高速入口北向)', '收费站', 20, 10, '111.179464', '29.426397', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1331, NULL, NULL, '慈利收费站(S12安慈高速出口常德方向)', '收费站', 20, 10, '111.179543', '29.426489', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1332, NULL, NULL, '岩泊渡收费站(G5513长张高速出口)', '收费站', 20, 10, '110.980800', '29.375002', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1333, NULL, NULL, '慈利西收费站(G5513长张高速入口)', '收费站', 20, 10, '111.092859', '29.420960', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1334, NULL, NULL, '慈利西收费站(G5513长张高速出口)', '收费站', 20, 10, '111.092719', '29.420863', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1335, NULL, NULL, '慈利东收费站(G5513长张高速出口)', '收费站', 20, 10, '111.172986', '29.416540', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1336, NULL, NULL, '武陵源收费站(G5513长张高速入口)', '收费站', 20, 10, '110.716465', '29.256662', '张家界市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1337, NULL, NULL, '怀化北收费站(S50长芷高速出口)', '收费站', 20, 10, '110.013449', '27.601217', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1338, NULL, NULL, '怀化北收费站(S50长芷高速入口)', '收费站', 20, 10, '110.013965', '27.601350', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1339, NULL, NULL, '怀化西收费站(G65包茂高速出口)', '收费站', 20, 10, '109.916264', '27.542459', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1340, NULL, NULL, '怀化西收费站(G65包茂高速入口)', '收费站', 20, 10, '109.916244', '27.542891', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1341, NULL, NULL, '杨村收费站(S97怀化绕城高速入口)', '收费站', 20, 10, '110.033086', '27.532856', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1342, NULL, NULL, '杨村收费站(S97怀化绕城高速出口)', '收费站', 20, 10, '110.033418', '27.532904', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1343, NULL, NULL, '怀化南收费站(S97怀化绕城高速出口)', '收费站', 20, 10, '109.974232', '27.501444', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1344, NULL, NULL, '怀化南收费站(S97怀化绕城高速入口)', '收费站', 20, 10, '109.973964', '27.501645', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1345, NULL, NULL, '池回收费站(S97怀化绕城高速出口池回方向)', '收费站', 20, 10, '109.949407', '27.484648', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1346, NULL, NULL, '池回收费站(S97怀化绕城高速入口池回高速方向)', '收费站', 20, 10, '109.948879', '27.485036', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1347, NULL, NULL, '中方收费站(G60沪昆高速出口)', '收费站', 20, 10, '109.934490', '27.371448', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1348, NULL, NULL, '芷江收费站(G60沪昆高速出口)', '收费站', 20, 10, '109.703018', '27.421972', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1349, NULL, NULL, '收费站(靖黎高速出口)', '收费站', 20, 10, '109.662786', '26.543023', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1350, NULL, NULL, '收费站(靖黎高速入口)', '收费站', 20, 10, '109.662713', '26.542991', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1351, NULL, NULL, '收费站(靖黎高速出口)', '收费站', 20, 10, '109.431198', '26.400543', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1352, NULL, NULL, '收费站(靖黎高速入口)', '收费站', 20, 10, '109.431313', '26.400572', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1353, NULL, NULL, '隆家堡收费站(G65包茂高速入口)', '收费站', 20, 10, '109.794020', '27.800658', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1354, NULL, NULL, '溆浦收费站(S50长芷高速入口)', '收费站', 20, 10, '110.562201', '27.911997', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1355, NULL, NULL, '收费站(S71铜怀高速入口)', '收费站', 20, 10, '109.615913', '27.562657', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1356, NULL, NULL, '收费站(S71铜怀高速出口)', '收费站', 20, 10, '109.615974', '27.562662', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1357, NULL, NULL, '收费站(S95沅辰高速入口)', '收费站', 20, 10, '110.212433', '27.877444', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1358, NULL, NULL, '收费站(S95沅辰高速出口)', '收费站', 20, 10, '110.212375', '27.877423', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1359, NULL, NULL, '收费站(S71铜怀高速入口)', '收费站', 20, 10, '109.543899', '27.597277', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1360, NULL, NULL, '收费站(S71铜怀高速出口)', '收费站', 20, 10, '109.543930', '27.597233', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1361, NULL, NULL, '收费站(S95沅辰高速入口)', '收费站', 20, 10, '110.224492', '27.997653', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1362, NULL, NULL, '收费站(S95沅辰高速出口)', '收费站', 20, 10, '110.224496', '27.997707', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1363, NULL, NULL, '收费站(S95沅辰高速出口)', '收费站', 20, 10, '110.244773', '28.105129', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1364, NULL, NULL, '收费站(S95沅辰高速入口)', '收费站', 20, 10, '110.244760', '28.105076', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1365, NULL, NULL, '收费站(S95沅辰高速入口)', '收费站', 20, 10, '110.259930', '28.232158', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1366, NULL, NULL, '收费站(S95沅辰高速出口)', '收费站', 20, 10, '110.259878', '28.232141', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1367, NULL, NULL, '陇城收费站(G65包茂高速入口)', '收费站', 20, 10, '109.782684', '26.040335', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1368, NULL, NULL, '大江口收费站(S50长芷高速入口)', '收费站', 20, 10, '110.382696', '27.854947', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1369, NULL, NULL, '收费站(G60沪昆高速入口)', '收费站', 20, 10, '110.306484', '27.280565', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1370, NULL, NULL, '收费站(G60沪昆高速出口)', '收费站', 20, 10, '110.306491', '27.280528', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1371, NULL, NULL, '中方收费站(G60沪昆高速入口)', '收费站', 20, 10, '109.934384', '27.371525', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1372, NULL, NULL, '三江收费站(S50长芷高速出口)', '收费站', 20, 10, '110.845810', '27.951451', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1373, NULL, NULL, '桐木收费站(G65包茂高速入口)', '收费站', 20, 10, '109.871429', '27.320312', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1374, NULL, NULL, '桐木收费站(G65包茂高速出口)', '收费站', 20, 10, '109.871300', '27.320203', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1375, NULL, NULL, '筲箕湾收费站(G56杭瑞高速出口)', '收费站', 20, 10, '110.320714', '28.276278', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1376, NULL, NULL, '沅陵收费站(G56杭瑞高速出口)', '收费站', 20, 10, '110.442105', '28.404111', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1377, NULL, NULL, '花桥东收费站(S50长芷高速入口)', '收费站', 20, 10, '110.136410', '27.699817', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1378, NULL, NULL, '安江收费站(G60沪昆高速出口)', '收费站', 20, 10, '110.144422', '27.272647', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1379, NULL, NULL, '麻阳收费站(G65包茂高速出口)', '收费站', 20, 10, '109.769462', '27.840191', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1380, NULL, NULL, '花桥东收费站(S50长芷高速出口)', '收费站', 20, 10, '110.136386', '27.699620', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1381, NULL, NULL, '罗旧收费站(S50长芷高速出口)', '收费站', 20, 10, '109.773125', '27.522441', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1382, NULL, NULL, '罗旧收费站(S50长芷高速入口)', '收费站', 20, 10, '109.773152', '27.522490', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1383, NULL, NULL, '江口收费站(G60沪昆高速出口)', '收费站', 20, 10, '110.420237', '27.204113', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1384, NULL, NULL, '靖州收费站(G65包茂高速出口)', '收费站', 20, 10, '109.694463', '26.612381', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1385, NULL, NULL, '溆浦收费站(S50长芷高速出口)', '收费站', 20, 10, '110.562105', '27.911787', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1386, NULL, NULL, '桥江收费站(S50长芷高速出口)', '收费站', 20, 10, '110.669988', '27.956755', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1387, NULL, NULL, '会同收费站(G65包茂高速出口)', '收费站', 20, 10, '109.746947', '26.857952', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1388, NULL, NULL, '甘棠收费站(G65包茂高速出口)', '收费站', 20, 10, '109.729061', '26.701600', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1389, NULL, NULL, '辰溪南收费站(S50长芷高速出口)', '收费站', 20, 10, '110.221577', '27.854940', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1390, NULL, NULL, '怀化东收费站(S97怀化绕城高速入口)', '收费站', 20, 10, '110.076773', '27.583032', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1391, NULL, NULL, '怀化东收费站(S97怀化绕城高速出口)', '收费站', 20, 10, '110.076990', '27.583239', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1392, NULL, NULL, '芷江北收费站(S50长芷高速出口)', '收费站', 20, 10, '109.696652', '27.473453', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1393, NULL, NULL, '芷江北收费站(S50长芷高速入口)', '收费站', 20, 10, '109.697054', '27.473784', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1394, NULL, NULL, '安江收费站(G60沪昆高速入口)', '收费站', 20, 10, '110.144379', '27.272817', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1395, NULL, NULL, '麻阳收费站(G65包茂高速入口)', '收费站', 20, 10, '109.769303', '27.840337', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1396, NULL, NULL, '洪江收费站(G65包茂高速入口)', '收费站', 20, 10, '109.793400', '27.234529', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1397, NULL, NULL, '洪江收费站(G65包茂高速出口)', '收费站', 20, 10, '109.793220', '27.234421', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1398, NULL, NULL, '江市收费站(G65包茂高速入口)', '收费站', 20, 10, '109.739340', '27.134626', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1399, NULL, NULL, '江市收费站(G65包茂高速出口)', '收费站', 20, 10, '109.739201', '27.134725', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1400, NULL, NULL, '坪村收费站(G65包茂高速出口)', '收费站', 20, 10, '109.752886', '26.960993', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1401, NULL, NULL, '坪村收费站(G65包茂高速入口)', '收费站', 20, 10, '109.753027', '26.960844', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1402, NULL, NULL, '会同收费站(G65包茂高速入口)', '收费站', 20, 10, '109.746692', '26.858019', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1403, NULL, NULL, '甘棠收费站(G65包茂高速入口)', '收费站', 20, 10, '109.728841', '26.701602', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1404, NULL, NULL, '沅陵收费站(G56杭瑞高速入口)', '收费站', 20, 10, '110.441989', '28.403987', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1405, NULL, NULL, '靖州收费站(G65包茂高速入口)', '收费站', 20, 10, '109.694465', '26.612191', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1406, NULL, NULL, '官庄收费站(G56杭瑞高速入口)', '收费站', 20, 10, '110.942840', '28.531807', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1407, NULL, NULL, '官庄收费站(G56杭瑞高速出口)', '收费站', 20, 10, '110.942824', '28.531923', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1408, NULL, NULL, '双江收费站(G65包茂高速出口)', '收费站', 20, 10, '109.812741', '26.163111', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1409, NULL, NULL, '双江收费站(G65包茂高速入口)', '收费站', 20, 10, '109.812641', '26.162960', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1410, NULL, NULL, '陇城收费站(G65包茂高速出口)', '收费站', 20, 10, '109.782480', '26.040416', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1411, NULL, NULL, '土桥收费站(G60沪昆高速入口)', '收费站', 20, 10, '109.521258', '27.419487', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1412, NULL, NULL, '隆家堡收费站(G65包茂高速出口)', '收费站', 20, 10, '109.793884', '27.800650', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1413, NULL, NULL, '辰溪南收费站(S95沅辰高速出口)', '收费站', 20, 10, '110.221327', '27.854888', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1414, NULL, NULL, '石羊哨收费站(G65包茂高速入口)', '收费站', 20, 10, '109.708598', '27.894234', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1415, NULL, NULL, '石羊哨收费站(G65包茂高速出口)', '收费站', 20, 10, '109.708475', '27.894287', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1416, NULL, NULL, '筲箕湾收费站(G56杭瑞高速入口)', '收费站', 20, 10, '110.320830', '28.276406', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1417, NULL, NULL, '马底驿收费站(G56杭瑞高速入口)', '收费站', 20, 10, '110.668564', '28.476199', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1418, NULL, NULL, '马底驿收费站(G56杭瑞高速出口)', '收费站', 20, 10, '110.668502', '28.476305', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1419, NULL, NULL, '万佛山收费站(G65包茂高速入口)', '收费站', 20, 10, '109.875093', '26.266887', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1420, NULL, NULL, '万佛山收费站(G65包茂高速出口)', '收费站', 20, 10, '109.875198', '26.266695', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1421, NULL, NULL, '芷江收费站(G60沪昆高速入口)', '收费站', 20, 10, '109.702845', '27.421940', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1422, NULL, NULL, '兴隆收费站(G60沪昆高速入口)', '收费站', 20, 10, '109.215819', '27.350032', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1423, NULL, NULL, '桥江收费站(S50长芷高速入口)', '收费站', 20, 10, '110.669958', '27.956926', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1424, NULL, NULL, '三江收费站(S50长芷高速入口)', '收费站', 20, 10, '110.845976', '27.951321', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1425, NULL, NULL, '大江口收费站(S50长芷高速出口)', '收费站', 20, 10, '110.382892', '27.854863', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1426, NULL, NULL, '土桥收费站(G60沪昆高速出口)', '收费站', 20, 10, '109.521381', '27.419434', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1427, NULL, NULL, '江口收费站(G60沪昆高速入口)', '收费站', 20, 10, '110.420310', '27.204236', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1428, NULL, NULL, '柳寨收费站(G60沪昆高速入口)', '收费站', 20, 10, '109.331959', '27.351159', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1429, NULL, NULL, '柳寨收费站(G60沪昆高速出口)', '收费站', 20, 10, '109.331933', '27.351232', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1430, NULL, NULL, '兴隆收费站(G60沪昆高速出口)', '收费站', 20, 10, '109.215844', '27.349903', '怀化市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1431, NULL, NULL, '吉首南收费站(G56杭瑞高速出口)', '收费站', 20, 10, '109.668640', '28.204136', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1432, NULL, NULL, '吉首收费站(G56杭瑞高速出口)', '收费站', 20, 10, '109.726450', '28.269058', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1433, NULL, NULL, '矮寨收费站(G65包茂高速入口)', '收费站', 20, 10, '109.564654', '28.336775', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1434, NULL, NULL, '矮寨收费站(G65包茂高速出口)', '收费站', 20, 10, '109.564771', '28.336656', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1435, NULL, NULL, '吉首东收费站(G56杭瑞高速入口)', '收费站', 20, 10, '109.787319', '28.262403', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1436, NULL, NULL, '吉首东收费站(G56杭瑞高速出口)', '收费站', 20, 10, '109.787471', '28.262386', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1437, NULL, NULL, '吉首南收费站(G56杭瑞高速入口)', '收费站', 20, 10, '109.668580', '28.203843', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1438, NULL, NULL, '吉首收费站(G65包茂高速入口)', '收费站', 20, 10, '109.726254', '28.268781', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1439, NULL, NULL, '吉首西收费站(G65包茂高速入口)', '收费站', 20, 10, '109.693371', '28.286227', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1440, NULL, NULL, '吉首西收费站(G65包茂高速出口)', '收费站', 20, 10, '109.692771', '28.286554', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1441, NULL, NULL, '吉首北收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.746389', '28.374462', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1442, NULL, NULL, '吉首北收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.746318', '28.374806', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1443, NULL, NULL, '永顺收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.778694', '28.983139', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1444, NULL, NULL, '黄丝桥收费站(G56杭瑞高速出口)', '收费站', 20, 10, '109.390171', '27.913636', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1445, NULL, NULL, '收费站(G56杭瑞高速出口)', '收费站', 20, 10, '109.528966', '27.945828', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1446, NULL, NULL, '收费站(G56杭瑞高速入口)', '收费站', 20, 10, '109.529023', '27.945853', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1447, NULL, NULL, '凤凰收费站(G56杭瑞高速出口)', '收费站', 20, 10, '109.602906', '27.978465', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1448, NULL, NULL, '古丈收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.932447', '28.591709', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1449, NULL, NULL, '古丈收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.932678', '28.591468', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1450, NULL, NULL, '花垣收费站(S10张花高速入口)', '收费站', 20, 10, '109.443196', '28.580963', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1451, NULL, NULL, '泸溪收费站(G56杭瑞高速入口)', '收费站', 20, 10, '110.163623', '28.251506', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1452, NULL, NULL, '农车收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.629665', '29.139758', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1453, NULL, NULL, '首车收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.692241', '29.074151', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1454, NULL, NULL, '首车收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.692567', '29.074254', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1455, NULL, NULL, '农车收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.629355', '29.139819', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1456, NULL, NULL, '龙山收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.480994', '29.456597', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1457, NULL, NULL, '龙山收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.481037', '29.456408', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1458, NULL, NULL, '永顺收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.778565', '28.983294', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1459, NULL, NULL, '青坪收费站(S10张花高速入口)', '收费站', 20, 10, '110.229152', '29.033863', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1460, NULL, NULL, '青坪收费站(S10张花高速出口)', '收费站', 20, 10, '110.228999', '29.033680', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1461, NULL, NULL, '花垣收费站(S10张花高速出口)', '收费站', 20, 10, '109.443133', '28.580735', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1462, NULL, NULL, '茅坪收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.543888', '29.363285', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1463, NULL, NULL, '茅坪收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.543958', '29.363557', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1464, NULL, NULL, '花垣东收费站(G65包茂高速入口)', '收费站', 20, 10, '109.495965', '28.466105', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1465, NULL, NULL, '花垣东收费站(G65包茂高速出口)', '收费站', 20, 10, '109.495947', '28.465935', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1466, NULL, NULL, '芙蓉镇收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.963781', '28.748584', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1467, NULL, NULL, '芙蓉镇收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.963413', '28.748461', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1468, NULL, NULL, '保靖西收费站(S10张花高速入口)', '收费站', 20, 10, '109.557282', '28.631041', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1469, NULL, NULL, '保靖西收费站(S10张花高速出口)', '收费站', 20, 10, '109.557160', '28.630965', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1470, NULL, NULL, '保靖东收费站(S10张花高速出口)', '收费站', 20, 10, '109.681014', '28.708293', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1471, NULL, NULL, '猛洞河收费站(S10张花高速出口)', '收费站', 20, 10, '109.918201', '28.912673', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1472, NULL, NULL, '花垣西收费站(G65包茂高速出口)', '收费站', 20, 10, '109.304595', '28.517921', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1473, NULL, NULL, '红岩溪收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.656338', '29.228866', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1474, NULL, NULL, '保靖东收费站(S10张花高速入口)', '收费站', 20, 10, '109.681274', '28.708247', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1475, NULL, NULL, '罗依溪收费站(S99龙吉高速出口)', '收费站', 20, 10, '109.992686', '28.670251', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1476, NULL, NULL, '罗依溪收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.992506', '28.670241', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1477, NULL, NULL, '花垣西收费站(G65包茂高速入口)', '收费站', 20, 10, '109.304373', '28.517760', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1478, NULL, NULL, '猛洞河收费站(S10张花高速入口)', '收费站', 20, 10, '109.918181', '28.912948', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1479, NULL, NULL, '红岩溪收费站(S99龙吉高速入口)', '收费站', 20, 10, '109.656008', '29.228955', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1480, NULL, NULL, '芙蓉镇西收费站(S10张花高速出口)', '收费站', 20, 10, '109.805460', '28.847683', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1481, NULL, NULL, '芙蓉镇东收费站(S10张花高速出口)', '收费站', 20, 10, '110.111173', '28.986990', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1482, NULL, NULL, '芙蓉镇西收费站(S10张花高速入口)', '收费站', 20, 10, '109.805184', '28.847650', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1483, NULL, NULL, '芙蓉镇东收费站(S10张花高速入口)', '收费站', 20, 10, '110.111153', '28.986868', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1484, NULL, NULL, '吉信收费站(G56杭瑞高速入口)', '收费站', 20, 10, '109.621017', '28.105979', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1485, NULL, NULL, '吉信收费站(G56杭瑞高速出口)', '收费站', 20, 10, '109.621060', '28.106140', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1486, NULL, NULL, '凤凰收费站(G56杭瑞高速入口)', '收费站', 20, 10, '109.603202', '27.978366', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1487, NULL, NULL, '黄丝桥收费站(G56杭瑞高速入口)', '收费站', 20, 10, '109.390405', '27.913650', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1488, NULL, NULL, '泸溪收费站(G56杭瑞高速出口)', '收费站', 20, 10, '110.163534', '28.251410', '湘西土家族苗族自治州', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1489, NULL, NULL, '常德收费站(G55二广高速入口)', '收费站', 20, 10, '111.771615', '29.010651', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1490, NULL, NULL, '常德收费站(G55二广高速出口)', '收费站', 20, 10, '111.771715', '29.011088', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1491, NULL, NULL, '桃源收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.529118', '28.851277', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1492, NULL, NULL, '桃源收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.529177', '28.851420', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1493, NULL, NULL, '常德南收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.631082', '28.943537', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1494, NULL, NULL, '德山收费站(G5513长张高速入口)', '收费站', 20, 10, '111.733068', '28.917433', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1495, NULL, NULL, '德山收费站(G5513长张高速出口)', '收费站', 20, 10, '111.733593', '28.917459', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1496, NULL, NULL, '周家店收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.877809', '29.220108', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1497, NULL, NULL, '双桥坪收费站(G55二广高速出口)', '收费站', 20, 10, '111.780537', '29.275742', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1498, NULL, NULL, '花岩溪收费站(G55二广高速入口)', '收费站', 20, 10, '111.683373', '28.744559', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1499, NULL, NULL, '花岩溪收费站(G55二广高速出口)', '收费站', 20, 10, '111.683183', '28.744790', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1500, NULL, NULL, '西洞庭收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.976200', '29.229376', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1501, NULL, NULL, '双桥坪收费站(G55二广高速入口)', '收费站', 20, 10, '111.780574', '29.275913', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1502, NULL, NULL, '西洞庭收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.976215', '29.229183', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1503, NULL, NULL, '常德南收费站(G5513长张高速出口)', '收费站', 20, 10, '111.631154', '28.943405', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1504, NULL, NULL, '常德北(太阳山)收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.705417', '29.128323', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1505, NULL, NULL, '常德北(太阳山)收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.705404', '29.128533', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1506, NULL, NULL, '常德东北(柳叶湖)收费站(G55二广高速入口)', '收费站', 20, 10, '111.768494', '29.103267', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1507, NULL, NULL, '常德高新区(灌溪)收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.631318', '29.122313', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1508, NULL, NULL, '常德高新区(灌溪)收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.631160', '29.122443', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1509, NULL, NULL, '周家店收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.878037', '29.220190', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1510, NULL, NULL, '常德东北(柳叶湖)收费站(G55二广高速出口)', '收费站', 20, 10, '111.768357', '29.103435', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1511, NULL, NULL, '架桥收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.515096', '29.189503', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1512, NULL, NULL, '临澧收费站(G55二广高速入口)', '收费站', 20, 10, '111.769782', '29.462911', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1513, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '111.065164', '29.663034', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1514, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '111.065071', '29.663022', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1515, NULL, NULL, '架桥收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.515031', '29.189405', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1516, NULL, NULL, '津市收费站(G55二广高速入口)', '收费站', 20, 10, '111.822657', '29.562910', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1517, NULL, NULL, '津市收费站(G55二广高速出口)', '收费站', 20, 10, '111.822587', '29.562703', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1518, NULL, NULL, '安乡收费站(G56杭瑞高速入口)', '收费站', 20, 10, '112.190079', '29.376348', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1519, NULL, NULL, '常德西收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.582016', '29.062314', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1520, NULL, NULL, '临澧收费站(G55二广高速出口)', '收费站', 20, 10, '111.769835', '29.463047', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1521, NULL, NULL, '石门收费站(S12安慈高速出口)', '收费站', 20, 10, '111.447878', '29.537713', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1522, NULL, NULL, '石门收费站(S12安慈高速入口)', '收费站', 20, 10, '111.447805', '29.537757', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1523, NULL, NULL, '安乡收费站(G56杭瑞高速出口)', '收费站', 20, 10, '112.190195', '29.376518', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1524, NULL, NULL, '热市收费站(G5513长张高速入口)', '收费站', 20, 10, '111.332858', '29.306314', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1525, NULL, NULL, '茶庵铺收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.153204', '28.611160', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1526, NULL, NULL, '茶庵铺收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.153089', '28.611096', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1527, NULL, NULL, '热市收费站(G5513长张高速出口)', '收费站', 20, 10, '111.332877', '29.306433', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1528, NULL, NULL, '桃花源收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.431099', '28.754317', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1529, NULL, NULL, '桃花源收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.431425', '28.754238', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1530, NULL, NULL, '乌云界收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.305913', '28.678022', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1531, NULL, NULL, '乌云界收费站(G56杭瑞高速出口)', '收费站', 20, 10, '111.305801', '28.677956', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1532, NULL, NULL, '收费站(G5517长常北线高速出口)', '收费站', 20, 10, '111.941997', '28.849126', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1533, NULL, NULL, '收费站(G5517长常北线高速入口)', '收费站', 20, 10, '111.942026', '28.849192', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1534, NULL, NULL, '澧县收费站(G55二广高速入口)', '收费站', 20, 10, '111.817865', '29.635799', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1535, NULL, NULL, '临澧北收费站(S12安慈高速入口)', '收费站', 20, 10, '111.637925', '29.521378', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1536, NULL, NULL, '临澧北收费站(S12安慈高速出口)', '收费站', 20, 10, '111.638017', '29.521352', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1537, NULL, NULL, '收费站(G5517长常北线高速入口)', '收费站', 20, 10, '112.142914', '28.749450', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1538, NULL, NULL, '收费站(G5517长常北线高速出口)', '收费站', 20, 10, '112.142951', '28.749500', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1539, NULL, NULL, '津市东收费站(S12安慈高速出口)', '收费站', 20, 10, '111.913354', '29.499603', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1540, NULL, NULL, '津市东收费站(S12安慈高速入口)', '收费站', 20, 10, '111.913309', '29.499647', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1541, NULL, NULL, '澧县南收费站(S12安慈高速入口)', '收费站', 20, 10, '111.726512', '29.567319', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1542, NULL, NULL, '澧县南收费站(S12安慈高速出口)', '收费站', 20, 10, '111.726598', '29.567369', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1543, NULL, NULL, '澧县收费站(G55二广高速出口)', '收费站', 20, 10, '111.818187', '29.635813', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1544, NULL, NULL, '复兴厂收费站(G55二广高速入口)', '收费站', 20, 10, '111.864562', '29.837603', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1545, NULL, NULL, '复兴厂收费站(G55二广高速出口)', '收费站', 20, 10, '111.864608', '29.837713', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1546, NULL, NULL, '谢家铺收费站(G5513长张高速入口)', '收费站', 20, 10, '111.806324', '28.858248', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1547, NULL, NULL, '谢家铺收费站(G5513长张高速出口)', '收费站', 20, 10, '111.806242', '28.858304', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1548, NULL, NULL, '军山铺收费站(G5513长张高速出口)', '收费站', 20, 10, '112.159831', '28.707506', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1549, NULL, NULL, '常德西收费站(G56杭瑞高速入口)', '收费站', 20, 10, '111.581970', '29.062472', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1550, NULL, NULL, '军山铺收费站(G5513长张高速入口)', '收费站', 20, 10, '112.159842', '28.707578', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1551, NULL, NULL, '安乡西(大鲸港)收费站(S12安慈高速入口)', '收费站', 20, 10, '112.130359', '29.389790', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1552, NULL, NULL, '安乡西(大鲸港)收费站(S12安慈高速出口)', '收费站', 20, 10, '112.130452', '29.389647', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1553, NULL, NULL, '太子庙(汉寿南)收费站(G5513长张高速入口)', '收费站', 20, 10, '111.957714', '28.796300', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1554, NULL, NULL, '太子庙(汉寿南)收费站(G5513长张高速出口)', '收费站', 20, 10, '111.957892', '28.796199', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1555, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '111.070979', '29.745202', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1556, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '111.070944', '29.745265', '常德市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1557, NULL, NULL, '湘潭北收费站(G60沪昆高速出口)', '收费站', 20, 10, '112.933917', '27.921753', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1558, NULL, NULL, '湘潭西收费站(G0421许广高速出口)', '收费站', 20, 10, '112.809583', '27.855962', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1559, NULL, NULL, '南谷收费站(G0421许广高速出口)', '收费站', 20, 10, '112.779515', '27.986666', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1560, NULL, NULL, '南谷收费站(G0421许广高速入口)', '收费站', 20, 10, '112.779484', '27.986847', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1561, NULL, NULL, '湘潭西收费站(G0421许广高速入口)', '收费站', 20, 10, '112.809732', '27.855932', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1562, NULL, NULL, '长潭西高速九华收费站(S41长潭西高速南向)', '收费站', 20, 10, '112.936616', '27.934551', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1563, NULL, NULL, '长潭西高速九华收费站(S41长潭西高速北向)', '收费站', 20, 10, '112.936902', '27.934607', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1564, NULL, NULL, '岳塘收费站(G60沪昆高速入口)', '收费站', 20, 10, '112.990888', '27.908873', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1565, NULL, NULL, '岳塘收费站(G60沪昆高速出口)', '收费站', 20, 10, '112.990604', '27.908800', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1566, NULL, NULL, '湘潭北收费站(G60沪昆高速入口)', '收费站', 20, 10, '112.933396', '27.921730', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1567, NULL, NULL, '昭山南收费站(G60沪昆高速出口西向)', '收费站', 20, 10, '113.022322', '27.927238', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1568, NULL, NULL, '昭山南收费站(G60沪昆高速入口东向)', '收费站', 20, 10, '113.023811', '27.927134', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1569, NULL, NULL, '马家河收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.020853', '27.850188', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1570, NULL, NULL, '株洲北收费站(S52醴易高速入口)', '收费站', 20, 10, '113.059951', '27.928529', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1571, NULL, NULL, '昭山收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.044742', '27.981122', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1572, NULL, NULL, '昭山收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.044798', '27.980990', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1573, NULL, NULL, '株洲北收费站(S52醴易高速出口)', '收费站', 20, 10, '113.059975', '27.928721', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1574, NULL, NULL, '马家河收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.020991', '27.849972', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1575, NULL, NULL, '射埠收费站(G0421许广高速入口)', '收费站', 20, 10, '112.767854', '27.658519', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1576, NULL, NULL, '楠竹山收费站(S01宁韶高速出口)', '收费站', 20, 10, '112.649252', '27.874453', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1577, NULL, NULL, '楠竹山收费站(S01宁韶高速入口)', '收费站', 20, 10, '112.649339', '27.874457', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1578, NULL, NULL, '射埠收费站(G0421许广高速出口)', '收费站', 20, 10, '112.767873', '27.658353', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1579, NULL, NULL, '杨嘉桥收费站(G0421许广高速出口)', '收费站', 20, 10, '112.809417', '27.765437', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1580, NULL, NULL, '杨嘉桥收费站(G0421许广高速入口)', '收费站', 20, 10, '112.809344', '27.765630', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1581, NULL, NULL, '回龙桥收费站(G0421许广高速入口)', '收费站', 20, 10, '112.702141', '27.561699', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1582, NULL, NULL, '回龙桥收费站(G0421许广高速出口)', '收费站', 20, 10, '112.701962', '27.561608', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1583, NULL, NULL, '青山桥收费站(G0421许广高速入口)', '收费站', 20, 10, '112.632795', '27.473465', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1584, NULL, NULL, '青山桥收费站(G0421许广高速出口)', '收费站', 20, 10, '112.632616', '27.473536', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1585, NULL, NULL, '韶山收费站(S01宁韶高速出口)', '收费站', 20, 10, '112.560150', '27.903429', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1586, NULL, NULL, '湘乡收费站(G60沪昆高速入口)', '收费站', 20, 10, '112.502852', '27.768424', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1587, NULL, NULL, '湘乡收费站(G60沪昆高速出口)', '收费站', 20, 10, '112.502596', '27.768449', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1588, NULL, NULL, '河岭收费站(G60醴娄高速出口)', '收费站', 20, 10, '112.863300', '27.635772', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1589, NULL, NULL, '河岭收费站(G60醴娄高速入口)', '收费站', 20, 10, '112.863303', '27.635826', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1590, NULL, NULL, '潭市收费站(G60沪昆高速出口)', '收费站', 20, 10, '112.350653', '27.740536', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1591, NULL, NULL, '韶山收费站(S01宁韶高速入口)', '收费站', 20, 10, '112.560173', '27.903153', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1592, NULL, NULL, '水府收费站(G60沪昆高速出口)', '收费站', 20, 10, '112.246326', '27.713878', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1593, NULL, NULL, '乌石收费站(G60醴娄高速出口)', '收费站', 20, 10, '112.661624', '27.638365', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1594, NULL, NULL, '乌石收费站(G60醴娄高速入口)', '收费站', 20, 10, '112.661564', '27.638352', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1595, NULL, NULL, '金石收费站(S50长芷高速出口)', '收费站', 20, 10, '112.442138', '27.984986', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1596, NULL, NULL, '潭市收费站(G60沪昆高速入口)', '收费站', 20, 10, '112.350485', '27.740367', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1597, NULL, NULL, '水府收费站(G60沪昆高速入口)', '收费站', 20, 10, '112.246117', '27.713925', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1598, NULL, NULL, '山枣收费站(G60醴娄高速入口)', '收费站', 20, 10, '112.453732', '27.645552', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1599, NULL, NULL, '山枣收费站(G60醴娄高速出口)', '收费站', 20, 10, '112.453890', '27.645632', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1600, NULL, NULL, '虞塘收费站(G60醴娄高速入口)', '收费站', 20, 10, '112.375278', '27.607003', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1601, NULL, NULL, '虞塘收费站(G60醴娄高速出口)', '收费站', 20, 10, '112.375397', '27.607009', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1602, NULL, NULL, '翻江收费站(S50长芷高速入口)', '收费站', 20, 10, '112.154720', '27.902076', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1603, NULL, NULL, '毛田收费站(S71华常高速出口)', '收费站', 20, 10, '112.096313', '27.666170', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1604, NULL, NULL, '毛田收费站(S71华常高速入口)', '收费站', 20, 10, '112.096410', '27.665954', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1605, NULL, NULL, '谭家山收费站(G60醴娄高速出口)', '收费站', 20, 10, '112.946740', '27.616685', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1606, NULL, NULL, '谭家山收费站(G60醴娄高速入口)', '收费站', 20, 10, '112.946708', '27.616607', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1607, NULL, NULL, '湘乡南收费站(G60醴娄高速入口)', '收费站', 20, 10, '112.538031', '27.665598', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1608, NULL, NULL, '湘乡南收费站(G60醴娄高速出口)', '收费站', 20, 10, '112.538077', '27.665630', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1609, NULL, NULL, '翻江收费站(S50长芷高速出口)', '收费站', 20, 10, '112.154824', '27.901833', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1610, NULL, NULL, '金石收费站(S50长芷高速入口)', '收费站', 20, 10, '112.442353', '27.984967', '湘潭市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1611, NULL, NULL, '郴州收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.064639', '25.774226', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1612, NULL, NULL, '郴州收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.065002', '25.774132', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1613, NULL, NULL, '良田收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.031996', '25.603980', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1614, NULL, NULL, '良田收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.032039', '25.604066', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1615, NULL, NULL, '郴州北收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.081124', '25.888779', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1616, NULL, NULL, '五里牌收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.056193', '25.962243', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1617, NULL, NULL, '五里牌收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.055894', '25.962328', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1618, NULL, NULL, '郴州北收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.081395', '25.888675', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1619, NULL, NULL, '黄沙收费站(S31宜连高速入口)', '收费站', 20, 10, '112.740005', '25.139270', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1620, NULL, NULL, '平和收费站(G76厦蓉高速入口)', '收费站', 20, 10, '113.118485', '25.529011', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1621, NULL, NULL, '平和收费站(G76厦蓉高速出口)', '收费站', 20, 10, '113.118395', '25.529164', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1622, NULL, NULL, '长村收费站(S31宜连高速入口)', '收费站', 20, 10, '112.783332', '25.218335', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1623, NULL, NULL, '里田收费站(G76厦蓉高速出口)', '收费站', 20, 10, '113.247022', '25.540234', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1624, NULL, NULL, '里田收费站(G76厦蓉高速入口)', '收费站', 20, 10, '113.247200', '25.540364', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1625, NULL, NULL, '梅田收费站(S31宜连高速入口)', '收费站', 20, 10, '112.824893', '25.301715', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1626, NULL, NULL, '梅田收费站(S31宜连高速出口)', '收费站', 20, 10, '112.825036', '25.301687', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1627, NULL, NULL, '长村收费站(S31宜连高速出口)', '收费站', 20, 10, '112.783438', '25.218155', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1628, NULL, NULL, '黄沙收费站(S31宜连高速出口)', '收费站', 20, 10, '112.739856', '25.139300', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1629, NULL, NULL, '宜章收费站(G4京港澳高速出口)', '收费站', 20, 10, '112.963758', '25.399456', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1630, NULL, NULL, '宜章南收费站(S31宜连高速入口)', '收费站', 20, 10, '112.931688', '25.368171', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1631, NULL, NULL, '宜章南收费站(S31宜连高速出口)', '收费站', 20, 10, '112.931711', '25.367978', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1632, NULL, NULL, '迎春收费站(G0421许广高速入口)', '收费站', 20, 10, '112.662096', '25.235521', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1633, NULL, NULL, '迎春收费站(G0421许广高速出口)', '收费站', 20, 10, '112.662256', '25.235356', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1634, NULL, NULL, '宜章收费站(G4京港澳高速入口)', '收费站', 20, 10, '112.963535', '25.399436', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1635, NULL, NULL, '郴州西收费站(G76厦蓉高速出口)', '收费站', 20, 10, '112.945462', '25.688677', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1636, NULL, NULL, '收费站(茶常高速入口)', '收费站', 20, 10, '113.273311', '26.581857', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1637, NULL, NULL, '收费站(茶常高速出口)', '收费站', 20, 10, '113.273371', '26.581869', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1638, NULL, NULL, '收费站(茶常高速出口)', '收费站', 20, 10, '113.368363', '26.594990', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1639, NULL, NULL, '收费站(茶常高速入口)', '收费站', 20, 10, '113.368374', '26.595062', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1640, NULL, NULL, '收费站(安仁支线入口)', '收费站', 20, 10, '113.297404', '26.730487', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1641, NULL, NULL, '收费站(安仁支线出口)', '收费站', 20, 10, '113.297393', '26.730561', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1642, NULL, NULL, '郴州西收费站(G76厦蓉高速入口)', '收费站', 20, 10, '112.945148', '25.688697', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1643, NULL, NULL, '收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.000312', '25.472690', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1644, NULL, NULL, '收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.000266', '25.472727', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1645, NULL, NULL, '郴州南收费站(G76厦蓉高速出口)', '收费站', 20, 10, '113.000773', '25.668415', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1646, NULL, NULL, '郴州南收费站(G76厦蓉高速入口)', '收费站', 20, 10, '113.000524', '25.668561', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1647, NULL, NULL, '龙潭收费站(G76厦蓉高速出口)', '收费站', 20, 10, '112.528246', '25.600768', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1648, NULL, NULL, '龙潭收费站(G76厦蓉高速入口)', '收费站', 20, 10, '112.528056', '25.600748', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1649, NULL, NULL, '桂阳收费站(G76厦蓉高速入口)', '收费站', 20, 10, '112.736262', '25.648542', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1650, NULL, NULL, '桂阳收费站(G76厦蓉高速出口)', '收费站', 20, 10, '112.736282', '25.648792', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1651, NULL, NULL, '嘉禾收费站(G76厦蓉高速出口)', '收费站', 20, 10, '112.365723', '25.548434', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1652, NULL, NULL, '永兴收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.013920', '26.041259', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1653, NULL, NULL, '永兴收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.013690', '26.041342', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1654, NULL, NULL, '马田收费站(G4京港澳高速入口)', '收费站', 20, 10, '112.926756', '26.111722', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1655, NULL, NULL, '马田收费站(G4京港澳高速出口)', '收费站', 20, 10, '112.926874', '26.111637', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1656, NULL, NULL, '行廊收费站(G0421许广高速入口)', '收费站', 20, 10, '112.496440', '25.625865', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1657, NULL, NULL, '行廊收费站(G0421许广高速出口)', '收费站', 20, 10, '112.496504', '25.625686', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1658, NULL, NULL, '流峰收费站(G0421许广高速出口)', '收费站', 20, 10, '112.509383', '25.961076', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1659, NULL, NULL, '沙田收费站(G0422武深高速入口)', '收费站', 20, 10, '113.824462', '25.855769', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1660, NULL, NULL, '沙田收费站(G0422武深高速出口)', '收费站', 20, 10, '113.824362', '25.855603', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1661, NULL, NULL, '八面山收费站(G0422武深高速入口)', '收费站', 20, 10, '113.783097', '25.988587', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1662, NULL, NULL, '汝城南收费站(G0422武深高速出口)', '收费站', 20, 10, '113.662253', '25.524033', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1663, NULL, NULL, '舂陵江收费站(G0421许广高速出口)', '收费站', 20, 10, '112.498867', '25.823643', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1664, NULL, NULL, '舂陵江收费站(G0421许广高速入口)', '收费站', 20, 10, '112.498495', '25.823724', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1665, NULL, NULL, '八面山收费站(G0422武深高速出口)', '收费站', 20, 10, '113.782888', '25.988549', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1666, NULL, NULL, '汝城北收费站(G76厦蓉高速出口)', '收费站', 20, 10, '113.709218', '25.591832', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1667, NULL, NULL, '沙洲收费站(G76厦蓉高速入口)', '收费站', 20, 10, '113.336461', '25.551094', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1668, NULL, NULL, '沙洲收费站(G76厦蓉高速出口)', '收费站', 20, 10, '113.336283', '25.551206', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1669, NULL, NULL, '岭秀收费站(G76厦蓉高速入口)', '收费站', 20, 10, '113.457237', '25.522611', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1670, NULL, NULL, '岭秀收费站(G76厦蓉高速出口)', '收费站', 20, 10, '113.457168', '25.522487', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1671, NULL, NULL, '集益收费站(G76厦蓉高速入口)', '收费站', 20, 10, '113.857525', '25.571781', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1672, NULL, NULL, '集益收费站(G76厦蓉高速出口)', '收费站', 20, 10, '113.857472', '25.571615', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1673, NULL, NULL, '田庄收费站(G0422武深高速入口)', '收费站', 20, 10, '113.707036', '25.691432', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1674, NULL, NULL, '田庄收费站(G0422武深高速出口)', '收费站', 20, 10, '113.706918', '25.691360', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1675, NULL, NULL, '汝城北收费站(G76厦蓉高速入口)', '收费站', 20, 10, '113.709420', '25.591778', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1676, NULL, NULL, '井坡收费站(G0422武深高速出口)', '收费站', 20, 10, '113.660144', '25.463236', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1677, NULL, NULL, '井坡收费站(G0422武深高速入口)', '收费站', 20, 10, '113.660100', '25.463439', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1678, NULL, NULL, '临武收费站(G0421许广高速出口)', '收费站', 20, 10, '112.602602', '25.294427', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1679, NULL, NULL, '汝城南收费站(G0422武深高速入口)', '收费站', 20, 10, '113.662182', '25.524287', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1680, NULL, NULL, '嘉禾收费站(G76厦蓉高速入口)', '收费站', 20, 10, '112.365464', '25.548366', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1681, NULL, NULL, '流峰收费站(G0421许广高速入口)', '收费站', 20, 10, '112.509030', '25.960991', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1682, NULL, NULL, '临武收费站(G0421许广高速入口)', '收费站', 20, 10, '112.602661', '25.294134', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1683, NULL, NULL, '麦市收费站(G0421许广高速出口)', '收费站', 20, 10, '112.464134', '25.496919', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1684, NULL, NULL, '麦市收费站(G0421许广高速入口)', '收费站', 20, 10, '112.464321', '25.497086', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1685, NULL, NULL, '楚江收费站(G0421许广高速入口)', '收费站', 20, 10, '112.484797', '25.367208', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1686, NULL, NULL, '楚江收费站(G0421许广高速出口)', '收费站', 20, 10, '112.484634', '25.367268', '郴州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1687, NULL, NULL, '邓石桥收费站(S71华常高速出口)', '收费站', 20, 10, '112.305093', '28.528550', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1688, NULL, NULL, '收费站(赫山枢纽入口)', '收费站', 20, 10, '112.504196', '28.346659', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1689, NULL, NULL, '收费站(赫山枢纽出口)', '收费站', 20, 10, '112.504394', '28.346716', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1690, NULL, NULL, '凤凰湖收费站(平益高速入口)', '收费站', 20, 10, '112.544884', '28.580173', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1691, NULL, NULL, '凤凰湖收费站(平益高速出口)', '收费站', 20, 10, '112.544922', '28.580221', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1692, NULL, NULL, '益阳收费站(G5513长张高速入口)', '收费站', 20, 10, '112.405288', '28.562534', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1693, NULL, NULL, '益阳收费站(G5513长张高速出口)', '收费站', 20, 10, '112.405281', '28.562819', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1694, NULL, NULL, '邓石桥收费站(S71华常高速入口)', '收费站', 20, 10, '112.304963', '28.528732', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1695, NULL, NULL, '金盆山收费站(S20平洞高速入口)', '收费站', 20, 10, '112.415557', '28.487146', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1696, NULL, NULL, '金盆山收费站(S20平洞高速出口)', '收费站', 20, 10, '112.415487', '28.487321', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1697, NULL, NULL, '收费站(G5517长常北线高速出口)', '收费站', 20, 10, '112.360157', '28.655139', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1698, NULL, NULL, '收费站(G5517长常北线高速入口)', '收费站', 20, 10, '112.360179', '28.655211', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1699, NULL, NULL, '新桥河收费站(S71华常高速入口)', '收费站', 20, 10, '112.243444', '28.595290', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1700, NULL, NULL, '新桥河收费站(S71华常高速出口)', '收费站', 20, 10, '112.243514', '28.595601', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1701, NULL, NULL, '收费站(G5517长常北线高速入口)', '收费站', 20, 10, '112.480252', '28.581774', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1702, NULL, NULL, '收费站(G5517长常北线高速出口)', '收费站', 20, 10, '112.480334', '28.581766', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1703, NULL, NULL, '迎丰桥收费站(S71华常高速出口)', '收费站', 20, 10, '112.247615', '28.678408', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1704, NULL, NULL, '泥江口收费站(S71华常高速出口)', '收费站', 20, 10, '112.336936', '28.374786', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1705, NULL, NULL, '泥江口收费站(S71华常高速入口)', '收费站', 20, 10, '112.336977', '28.374971', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1706, NULL, NULL, '幸福渠收费站(G5513长张高速入口)', '收费站', 20, 10, '112.357553', '28.612455', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1707, NULL, NULL, '幸福渠收费站(G5513长张高速出口)', '收费站', 20, 10, '112.357534', '28.612572', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1708, NULL, NULL, '益阳收费站(G5513长张高速出口)', '收费站', 20, 10, '112.545919', '28.329810', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1709, NULL, NULL, '益阳收费站(G5513长张高速入口)', '收费站', 20, 10, '112.546032', '28.329546', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1710, NULL, NULL, '泉交河收费站(G5513长张高速出口)', '收费站', 20, 10, '112.490262', '28.452013', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1711, NULL, NULL, '迎丰桥收费站(G5513长张高速入口)', '收费站', 20, 10, '112.247492', '28.678460', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1712, NULL, NULL, '泉交河收费站(G5513长张高速入口)', '收费站', 20, 10, '112.490334', '28.451787', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1713, NULL, NULL, '泞湖收费站(G5517长常北线高速出口)', '收费站', 20, 10, '112.551873', '28.468254', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1714, NULL, NULL, '泞湖收费站(G5517长常北线高速入口)', '收费站', 20, 10, '112.551770', '28.468276', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1715, NULL, NULL, '收费站(官新高速入口)', '收费站', 20, 10, '111.103279', '28.355243', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1716, NULL, NULL, '收费站(官新高速出口)', '收费站', 20, 10, '111.103279', '28.355189', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1717, NULL, NULL, '收费站(官新高速入口)', '收费站', 20, 10, '111.069701', '28.208592', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1718, NULL, NULL, '收费站(官新高速出口)', '收费站', 20, 10, '111.069854', '28.208532', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1719, NULL, NULL, '草尾收费站(S71华常高速入口)', '收费站', 20, 10, '112.350127', '29.055639', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1720, NULL, NULL, '草尾收费站(S71华常高速出口)', '收费站', 20, 10, '112.350200', '29.055653', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1721, NULL, NULL, '长塘收费站(G55二广高速入口)', '收费站', 20, 10, '111.764959', '28.353753', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1722, NULL, NULL, '长塘收费站(G55二广高速出口)', '收费站', 20, 10, '111.764769', '28.353572', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1723, NULL, NULL, '武潭收费站(G55二广高速出口)', '收费站', 20, 10, '111.718615', '28.540453', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1724, NULL, NULL, '武潭收费站(G55二广高速入口)', '收费站', 20, 10, '111.718858', '28.540673', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1725, NULL, NULL, '仙溪收费站(G55二广高速入口)', '收费站', 20, 10, '111.714528', '28.247644', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1726, NULL, NULL, '仙溪收费站(G55二广高速出口)', '收费站', 20, 10, '111.714804', '28.247380', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1727, NULL, NULL, '冷市收费站(S20平洞高速入口)', '收费站', 20, 10, '111.526881', '28.500953', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1728, NULL, NULL, '冷市收费站(S20平洞高速出口)', '收费站', 20, 10, '111.527093', '28.501287', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1729, NULL, NULL, '梅城收费站(G55二广高速出口)', '收费站', 20, 10, '111.678299', '28.140243', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1730, NULL, NULL, '梅城收费站(G55二广高速入口)', '收费站', 20, 10, '111.678451', '28.140269', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1731, NULL, NULL, '南县收费站(G56杭瑞高速入口)', '收费站', 20, 10, '112.376676', '29.384043', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1732, NULL, NULL, '南县收费站(G56杭瑞高速出口)', '收费站', 20, 10, '112.376446', '29.384026', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1733, NULL, NULL, '安化收费站(S20平洞高速出口)', '收费站', 20, 10, '111.231252', '28.421836', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1734, NULL, NULL, '双桥收费站(S20平洞高速东向)', '收费站', 20, 10, '111.107819', '28.370981', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1735, NULL, NULL, '双桥收费站(S20平洞高速西向)', '收费站', 20, 10, '111.107827', '28.371099', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1736, NULL, NULL, '桃花江收费站(S20平洞高速出口)', '收费站', 20, 10, '112.157351', '28.485432', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1737, NULL, NULL, '桃花江收费站(S20平洞高速入口)', '收费站', 20, 10, '112.157333', '28.485352', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1738, NULL, NULL, '沅江南收费站(S71华常高速出口)', '收费站', 20, 10, '112.303424', '28.738410', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1739, NULL, NULL, '灰山港收费站(S71华常高速出口)', '收费站', 20, 10, '112.304103', '28.272934', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1740, NULL, NULL, '灰山港收费站(S71华常高速入口)', '收费站', 20, 10, '112.304236', '28.272953', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1741, NULL, NULL, '沅江北收费站(S71华常高速出口)', '收费站', 20, 10, '112.304761', '28.860185', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1742, NULL, NULL, '沅江北收费站(S71华常高速入口)', '收费站', 20, 10, '112.304645', '28.860273', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1743, NULL, NULL, '大栗港收费站(S20平洞高速入口)', '收费站', 20, 10, '111.938343', '28.475716', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1744, NULL, NULL, '大栗港收费站(S20平洞高速出口)', '收费站', 20, 10, '111.938209', '28.475829', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1745, NULL, NULL, '大通湖收费站(S71华常高速出口)', '收费站', 20, 10, '112.375127', '29.131807', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1746, NULL, NULL, '大通湖收费站(S71华常高速入口)', '收费站', 20, 10, '112.375194', '29.131959', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1747, NULL, NULL, '马迹塘收费站(G55二广高速出口)', '收费站', 20, 10, '111.751980', '28.453512', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1748, NULL, NULL, '马迹塘收费站(G55二广高速入口)', '收费站', 20, 10, '111.751972', '28.453202', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1749, NULL, NULL, '羊角塘收费站(S20平洞高速入口)', '收费站', 20, 10, '111.628059', '28.508896', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1750, NULL, NULL, '羊角塘收费站(S20平洞高速出口)', '收费站', 20, 10, '111.628117', '28.508898', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1751, NULL, NULL, '南县西收费站(S71华常高速出口)', '收费站', 20, 10, '112.359728', '29.349404', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1752, NULL, NULL, '南县西收费站(S71华常高速入口)', '收费站', 20, 10, '112.359692', '29.349516', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1753, NULL, NULL, '清塘铺收费站(G55二广高速出口)', '收费站', 20, 10, '111.759492', '28.074667', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1754, NULL, NULL, '清塘铺收费站(G55二广高速入口)', '收费站', 20, 10, '111.759450', '28.074847', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1755, NULL, NULL, '龙塘东收费站(S20平洞高速出口)', '收费站', 20, 10, '111.414776', '28.460605', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1756, NULL, NULL, '龙塘东收费站(S20平洞高速入口)', '收费站', 20, 10, '111.414757', '28.460655', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1757, NULL, NULL, '马迹塘东收费站(S20平洞高速出口)', '收费站', 20, 10, '111.789698', '28.478478', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1758, NULL, NULL, '马迹塘东收费站(S20平洞高速出口)', '收费站', 20, 10, '111.789875', '28.478343', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1759, NULL, NULL, '马迹塘东收费站(S20平洞高速入口)', '收费站', 20, 10, '111.789909', '28.478306', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1760, NULL, NULL, '安化收费站(S20平洞高速入口东向)', '收费站', 20, 10, '111.231401', '28.421767', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1761, NULL, NULL, '安化收费站(S20平洞高速入口西向)', '收费站', 20, 10, '111.231345', '28.421782', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1762, NULL, NULL, '沅江南收费站(S71华常高速入口)', '收费站', 20, 10, '112.303457', '28.738443', '益阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1763, NULL, NULL, '娄底西收费站(S70娄新高速出口)', '收费站', 20, 10, '111.947200', '27.659087', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1764, NULL, NULL, '娄底收费站(S50长芷高速出口)', '收费站', 20, 10, '111.999791', '27.843088', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1765, NULL, NULL, '娄底收费站(S50长芷高速入口)', '收费站', 20, 10, '112.000241', '27.843149', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1766, NULL, NULL, '娄底南收费站(S70娄新高速入口)', '收费站', 20, 10, '112.035746', '27.641944', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1767, NULL, NULL, '娄底西收费站(S70娄新高速入口)', '收费站', 20, 10, '111.946764', '27.659184', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1768, NULL, NULL, '娄底南收费站(S70娄新高速出口)', '收费站', 20, 10, '112.036041', '27.642089', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1769, NULL, NULL, '娄底经开区收费站(S71华常高速入口)', '收费站', 20, 10, '112.092474', '27.753786', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1770, NULL, NULL, '娄底经开区收费站(S71华常高速出口)', '收费站', 20, 10, '112.092463', '27.753705', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1771, NULL, NULL, '扶洲收费站(S70娄新高速出口扶洲方向)', '收费站', 20, 10, '112.115732', '27.636815', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1772, NULL, NULL, '扶洲收费站(S70娄新高速入口G60方向)', '收费站', 20, 10, '112.115621', '27.636058', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1773, NULL, NULL, '新化收费站(S70娄新高速出口)', '收费站', 20, 10, '111.288792', '27.711450', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1774, NULL, NULL, '收费站(官新高速入口)', '收费站', 20, 10, '111.107193', '28.024430', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1775, NULL, NULL, '收费站(官新高速出口)', '收费站', 20, 10, '111.107317', '28.024430', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1776, NULL, NULL, '双峰收费站(G60沪昆高速出口)', '收费站', 20, 10, '112.074514', '27.514256', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1777, NULL, NULL, '涟源收费站(S70娄新高速出口)', '收费站', 20, 10, '111.691406', '27.664047', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1778, NULL, NULL, '杨市收费站(S70娄新高速出口)', '收费站', 20, 10, '111.824465', '27.623594', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1779, NULL, NULL, '杨市收费站(S70娄新高速入口)', '收费站', 20, 10, '111.824564', '27.623423', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1780, NULL, NULL, '三甲收费站(S70娄新高速出口)', '收费站', 20, 10, '111.630952', '27.651767', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1781, NULL, NULL, '双峰收费站(G60沪昆高速入口)', '收费站', 20, 10, '112.074418', '27.514395', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1782, NULL, NULL, '温塘收费站(S50长芷高速入口)', '收费站', 20, 10, '111.518405', '27.886003', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1783, NULL, NULL, '温塘收费站(S50长芷高速出口)', '收费站', 20, 10, '111.518404', '27.885883', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1784, NULL, NULL, '龙塘收费站(G55二广高速入口)', '收费站', 20, 10, '111.770655', '27.819544', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1785, NULL, NULL, '龙塘收费站(G55二广高速出口)', '收费站', 20, 10, '111.770542', '27.819678', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1786, NULL, NULL, '涟源收费站(S70娄新高速入口)', '收费站', 20, 10, '111.691255', '27.663836', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1787, NULL, NULL, '伏口收费站(G55二广高速出口)', '收费站', 20, 10, '111.786542', '27.919521', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1788, NULL, NULL, '伏口收费站(G55二广高速入口)', '收费站', 20, 10, '111.786357', '27.919445', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1789, NULL, NULL, '白马收费站(G55二广高速出口)', '收费站', 20, 10, '111.687125', '27.600375', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1790, NULL, NULL, '白马收费站(G55二广高速入口)', '收费站', 20, 10, '111.687500', '27.600464', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1791, NULL, NULL, '三甲收费站(S70娄新高速入口)', '收费站', 20, 10, '111.630733', '27.651682', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1792, NULL, NULL, '锁石收费站(S71华常高速入口)', '收费站', 20, 10, '112.127649', '27.320027', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1793, NULL, NULL, '锁石收费站(S71华常高速出口)', '收费站', 20, 10, '112.127622', '27.320340', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1794, NULL, NULL, '桥头河收费站(S50长芷高速出口)', '收费站', 20, 10, '111.860692', '27.809081', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1795, NULL, NULL, '新化收费站(S70娄新高速入口)', '收费站', 20, 10, '111.288531', '27.711478', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1796, NULL, NULL, '涟源北收费站(S50长芷高速出口)', '收费站', 20, 10, '111.723366', '27.774238', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1797, NULL, NULL, '涟源北收费站(S50长芷高速入口)', '收费站', 20, 10, '111.723774', '27.774189', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1798, NULL, NULL, '双峰东收费站(G60醴娄高速入口)', '收费站', 20, 10, '112.258767', '27.589751', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1799, NULL, NULL, '双峰东收费站(G60醴娄高速出口)', '收费站', 20, 10, '112.258707', '27.589762', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1800, NULL, NULL, '涟源东收费站(G55二广高速出口)', '收费站', 20, 10, '111.721029', '27.696647', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1801, NULL, NULL, '三塘铺收费站(G60沪昆高速出口)', '收费站', 20, 10, '111.980899', '27.414024', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1802, NULL, NULL, '湄江南收费站(S50长芷高速出口)', '收费站', 20, 10, '111.672347', '27.820118', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1803, NULL, NULL, '湄江南收费站(S50长芷高速入口)', '收费站', 20, 10, '111.672435', '27.820064', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1804, NULL, NULL, '车田江收费站(S50长芷高速入口)', '收费站', 20, 10, '111.599750', '27.869739', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1805, NULL, NULL, '车田江收费站(S50长芷高速出口)', '收费站', 20, 10, '111.599862', '27.869699', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1806, NULL, NULL, '油溪桥收费站(S50长芷高速入口)', '收费站', 20, 10, '111.394150', '27.924488', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1807, NULL, NULL, '油溪桥收费站(S50长芷高速出口)', '收费站', 20, 10, '111.393909', '27.924694', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1808, NULL, NULL, '涟源东收费站(G55二广高速入口)', '收费站', 20, 10, '111.721171', '27.696410', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1809, NULL, NULL, '三塘铺收费站(G60沪昆高速入口)', '收费站', 20, 10, '111.980929', '27.413868', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1810, NULL, NULL, '双峰西收费站(S71华常高速入口)', '收费站', 20, 10, '112.136703', '27.434130', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1811, NULL, NULL, '双峰西收费站(S71华常高速出口)', '收费站', 20, 10, '112.136801', '27.433986', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1812, NULL, NULL, '金竹山收费站(S70娄新高速入口)', '收费站', 20, 10, '111.482306', '27.605016', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1813, NULL, NULL, '金竹山收费站(S70娄新高速出口)', '收费站', 20, 10, '111.482629', '27.604930', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1814, NULL, NULL, '紫鹊界收费站(S70娄新高速出口)', '收费站', 20, 10, '111.181279', '27.757620', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1815, NULL, NULL, '紫鹊界收费站(S70娄新高速入口)', '收费站', 20, 10, '111.181096', '27.757567', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1816, NULL, NULL, '大熊山收费站(S70娄新高速入口)', '收费站', 20, 10, '111.163887', '27.856594', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1817, NULL, NULL, '大熊山收费站(S70娄新高速出口)', '收费站', 20, 10, '111.163903', '27.856411', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1818, NULL, NULL, '苏溪湖收费站(S50长芷高速出口)', '收费站', 20, 10, '111.116827', '27.954054', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1819, NULL, NULL, '苏溪湖收费站(S50长芷高速入口)', '收费站', 20, 10, '111.116769', '27.953874', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1820, NULL, NULL, '冷水江南收费站(S70娄新高速入口)', '收费站', 20, 10, '111.438976', '27.629627', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1821, NULL, NULL, '梅山龙宫收费站(S50长芷高速入口)', '收费站', 20, 10, '111.303379', '27.925425', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1822, NULL, NULL, '梅山龙宫收费站(S50长芷高速出口)', '收费站', 20, 10, '111.303537', '27.925576', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1823, NULL, NULL, '冷水江南收费站(S70娄新高速出口)', '收费站', 20, 10, '111.439128', '27.629454', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1824, NULL, NULL, '桥头河收费站(S50长芷高速入口)', '收费站', 20, 10, '111.860922', '27.809206', '娄底市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1825, NULL, NULL, '永州收费站(G72泉南高速入口)', '收费站', 20, 10, '111.629479', '26.292606', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1826, NULL, NULL, '永州收费站(G72泉南高速出口)', '收费站', 20, 10, '111.629695', '26.292591', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1827, NULL, NULL, '珠山收费站(G72泉南高速出口)', '收费站', 20, 10, '111.336439', '26.117005', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1828, NULL, NULL, '珠山收费站(G72泉南高速入口)', '收费站', 20, 10, '111.336576', '26.117083', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1829, NULL, NULL, '收费站(永零高速入口)', '收费站', 20, 10, '111.522065', '26.277025', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1830, NULL, NULL, '收费站(永零高速出口)', '收费站', 20, 10, '111.522118', '26.277035', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1831, NULL, NULL, '收费站(衡永高速入口)', '收费站', 20, 10, '111.601252', '26.595407', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1832, NULL, NULL, '收费站(衡永高速出口)', '收费站', 20, 10, '111.601300', '26.595432', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1833, NULL, NULL, '收费站(衡永高速出口)', '收费站', 20, 10, '111.682708', '26.624003', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1834, NULL, NULL, '收费站(衡永高速入口)', '收费站', 20, 10, '111.682766', '26.623985', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1835, NULL, NULL, '收费站(永零高速出口)', '收费站', 20, 10, '111.541042', '26.194078', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1836, NULL, NULL, '收费站(永零高速入口)', '收费站', 20, 10, '111.540982', '26.194065', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1837, NULL, NULL, '永州东收费站(G55二广高速出口)', '收费站', 20, 10, '111.686340', '26.420985', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1838, NULL, NULL, '收费站(永零高速入口)', '收费站', 20, 10, '111.608244', '26.103703', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1839, NULL, NULL, '收费站(永零高速出口)', '收费站', 20, 10, '111.608274', '26.103799', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1840, NULL, NULL, '永州北收费站(G55二广高速出口)', '收费站', 20, 10, '111.583115', '26.526854', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1841, NULL, NULL, '黄田铺收费站(G72泉南高速入口)', '收费站', 20, 10, '111.464204', '26.228819', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1842, NULL, NULL, '黄田铺收费站(G72泉南高速出口)', '收费站', 20, 10, '111.464372', '26.228789', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1843, NULL, NULL, '花桥收费站(G55二广高速入口)', '收费站', 20, 10, '111.500310', '26.661940', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1844, NULL, NULL, '花桥收费站(G55二广高速出口)', '收费站', 20, 10, '111.500243', '26.662021', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1845, NULL, NULL, '永州东收费站(G55二广高速入口)', '收费站', 20, 10, '111.686356', '26.420779', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1846, NULL, NULL, '永州北收费站(G55二广高速入口)', '收费站', 20, 10, '111.583300', '26.526919', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1847, NULL, NULL, '白水收费站(G72泉南高速出口)', '收费站', 20, 10, '111.968394', '26.437998', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1848, NULL, NULL, '收费站(永零高速入口)', '收费站', 20, 10, '111.524091', '26.339802', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1849, NULL, NULL, '收费站(永零高速出口)', '收费站', 20, 10, '111.524122', '26.339756', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1850, NULL, NULL, '收费站(永新高速出口)', '收费站', 20, 10, '111.439041', '26.435820', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1851, NULL, NULL, '收费站(永新高速入口)', '收费站', 20, 10, '111.438945', '26.435882', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1852, NULL, NULL, '道州收费站(S81道贺高速出口)', '收费站', 20, 10, '111.593237', '25.468953', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1853, NULL, NULL, '收费站(永新高速出口)', '收费站', 20, 10, '111.329869', '26.419459', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1854, NULL, NULL, '收费站(永新高速入口)', '收费站', 20, 10, '111.329934', '26.419460', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1855, NULL, NULL, '收费站(衡永高速出口)', '收费站', 20, 10, '111.825278', '26.642561', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1856, NULL, NULL, '收费站(衡永高速入口)', '收费站', 20, 10, '111.825307', '26.642608', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1857, NULL, NULL, '收费站(衡永高速出口)', '收费站', 20, 10, '111.896972', '26.668080', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1858, NULL, NULL, '收费站(衡永高速入口)', '收费站', 20, 10, '111.896996', '26.668031', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1859, NULL, NULL, '蓝山收费站(G55二广高速出口)', '收费站', 20, 10, '112.175075', '25.399045', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1860, NULL, NULL, '宁远南收费站(G76厦蓉高速出口)', '收费站', 20, 10, '111.957390', '25.495465', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1861, NULL, NULL, '收费站(永新高速出口东向)', '收费站', 20, 10, '111.525638', '26.444554', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1862, NULL, NULL, '收费站(永新高速入口西向)', '收费站', 20, 10, '111.525624', '26.444606', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1863, NULL, NULL, '潘市收费站(G72泉南高速入口)', '收费站', 20, 10, '112.059997', '26.501941', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1864, NULL, NULL, '宁远收费站(G55二广高速入口)', '收费站', 20, 10, '112.058309', '25.699222', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1865, NULL, NULL, '平田收费站(G55二广高速入口)', '收费站', 20, 10, '111.969218', '25.850665', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1866, NULL, NULL, '平田收费站(G55二广高速出口)', '收费站', 20, 10, '111.969450', '25.850590', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1867, NULL, NULL, '宁远收费站(G55二广高速出口)', '收费站', 20, 10, '112.058073', '25.699234', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1868, NULL, NULL, '梅岗收费站(G76厦蓉高速入口)', '收费站', 20, 10, '111.812157', '25.473724', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1869, NULL, NULL, '梅岗收费站(G76厦蓉高速出口)', '收费站', 20, 10, '111.811929', '25.473631', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1870, NULL, NULL, '楠市收费站(G76厦蓉高速入口)', '收费站', 20, 10, '112.163415', '25.516681', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1871, NULL, NULL, '楠市收费站(G76厦蓉高速出口)', '收费站', 20, 10, '112.163225', '25.516751', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1872, NULL, NULL, '蓝山收费站(G55二广高速入口)', '收费站', 20, 10, '112.175075', '25.399276', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1873, NULL, NULL, '江华收费站(S81道贺高速出口)', '收费站', 20, 10, '111.529828', '25.250766', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1874, NULL, NULL, '江华收费站(S81道贺高速入口)', '收费站', 20, 10, '111.529781', '25.250948', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1875, NULL, NULL, '长铺收费站(G55二广高速入口)', '收费站', 20, 10, '112.168466', '25.283463', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1876, NULL, NULL, '长铺收费站(G55二广高速出口)', '收费站', 20, 10, '112.168438', '25.283687', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1877, NULL, NULL, '江永收费站(S81道贺高速出口)', '收费站', 20, 10, '111.420422', '25.170739', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1878, NULL, NULL, '江永收费站(S81道贺高速入口)', '收费站', 20, 10, '111.420189', '25.170877', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1879, NULL, NULL, '阳明山收费站(G55二广高速入口)', '收费站', 20, 10, '111.838294', '26.061722', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1880, NULL, NULL, '阳明山收费站(G55二广高速出口)', '收费站', 20, 10, '111.838543', '26.061802', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1881, NULL, NULL, '宁远东收费站(G55二广高速入口)', '收费站', 20, 10, '112.096526', '25.604485', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1882, NULL, NULL, '宁远东收费站(G55二广高速出口)', '收费站', 20, 10, '112.096693', '25.604529', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1883, NULL, NULL, '回龙圩收费站(S81道贺高速出口)', '收费站', 20, 10, '111.353090', '25.112388', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1884, NULL, NULL, '潘市收费站(G72泉南高速出口)', '收费站', 20, 10, '112.060109', '26.502056', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1885, NULL, NULL, '道州收费站(S81道贺高速入口)', '收费站', 20, 10, '111.593132', '25.468731', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1886, NULL, NULL, '仙子脚收费站(G76厦蓉高速入口)', '收费站', 20, 10, '111.356670', '25.633898', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1887, NULL, NULL, '白水收费站(G72泉南高速入口)', '收费站', 20, 10, '111.968379', '26.438176', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1888, NULL, NULL, '仙子脚收费站(G76厦蓉高速出口)', '收费站', 20, 10, '111.356772', '25.633721', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1889, NULL, NULL, '道州西收费站(G76厦蓉高速入口)', '收费站', 20, 10, '111.472059', '25.527609', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1890, NULL, NULL, '道州东收费站(G76厦蓉高速出口)', '收费站', 20, 10, '111.719423', '25.471089', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1891, NULL, NULL, '道州西收费站(G76厦蓉高速出口)', '收费站', 20, 10, '111.472201', '25.527438', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1892, NULL, NULL, '道州南收费站(S81道贺高速出口)', '收费站', 20, 10, '111.546101', '25.359153', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1893, NULL, NULL, '道州南收费站(S81道贺高速入口)', '收费站', 20, 10, '111.546281', '25.359231', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1894, NULL, NULL, '道州东收费站(G76厦蓉高速入口)', '收费站', 20, 10, '111.719440', '25.470893', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1895, NULL, NULL, '大忠桥收费站(G72泉南高速入口)', '收费站', 20, 10, '111.855557', '26.381353', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1896, NULL, NULL, '大忠桥收费站(G72泉南高速出口)', '收费站', 20, 10, '111.855381', '26.381396', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1897, NULL, NULL, '宁远南收费站(G76厦蓉高速入口)', '收费站', 20, 10, '111.957282', '25.495631', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1898, NULL, NULL, '回龙圩收费站(S81道贺高速入口)', '收费站', 20, 10, '111.352975', '25.112197', '永州市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1899, NULL, NULL, '邵阳南收费站(G60沪昆高速出口)', '收费站', 20, 10, '111.442032', '27.190786', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1900, NULL, NULL, '邵阳南收费站(G60沪昆高速入口)', '收费站', 20, 10, '111.441529', '27.190743', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1901, NULL, NULL, '邵阳东收费站(G60沪昆高速入口)', '收费站', 20, 10, '111.577823', '27.228031', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1902, NULL, NULL, '邵阳东收费站(G60沪昆高速出口)', '收费站', 20, 10, '111.578319', '27.228391', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1903, NULL, NULL, '邵东收费站(G60沪昆高速出口)', '收费站', 20, 10, '111.724120', '27.265962', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1904, NULL, NULL, '廉桥收费站(G60沪昆高速出口)', '收费站', 20, 10, '111.833359', '27.318397', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1905, NULL, NULL, '收费站(白新高速入口)', '收费站', 20, 10, '111.207084', '26.845728', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1906, NULL, NULL, '收费站(白新高速出口)', '收费站', 20, 10, '111.207025', '26.845743', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1907, NULL, NULL, '收费站(白新高速入口)', '收费站', 20, 10, '111.150474', '26.746593', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1908, NULL, NULL, '收费站(白新高速出口)', '收费站', 20, 10, '111.150414', '26.746580', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1909, NULL, NULL, '收费站(永新高速出口)', '收费站', 20, 10, '111.199326', '26.659222', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1910, NULL, NULL, '收费站(永新高速入口)', '收费站', 20, 10, '111.199347', '26.659310', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1911, NULL, NULL, '收费站(永新高速出口)', '收费站', 20, 10, '111.237804', '26.524011', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1912, NULL, NULL, '收费站(永新高速入口)', '收费站', 20, 10, '111.237840', '26.523944', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1913, NULL, NULL, '收费站(龙城高速入口)', '收费站', 20, 10, '110.297387', '26.278615', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1914, NULL, NULL, '收费站(龙城高速出口)', '收费站', 20, 10, '110.297360', '26.278576', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1915, NULL, NULL, '黄桥收费站(G60沪昆高速出口)', '收费站', 20, 10, '110.843448', '27.036510', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1916, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '110.933755', '27.482230', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1917, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '110.933702', '27.482200', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1918, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '110.941056', '27.569087', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1919, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '110.941014', '27.569052', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1920, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '111.031322', '26.778341', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1921, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '111.031265', '26.778362', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1922, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '111.004387', '26.605866', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1923, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '111.004409', '26.605916', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1924, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '110.984446', '26.562607', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1925, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '110.984401', '26.562641', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1926, NULL, NULL, '收费站(G59呼北高速出口)', '收费站', 20, 10, '110.904976', '26.474442', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1927, NULL, NULL, '收费站(G59呼北高速入口)', '收费站', 20, 10, '110.904938', '26.474389', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1928, NULL, NULL, '邵东收费站(G60沪昆高速入口)', '收费站', 20, 10, '111.724210', '27.266168', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1929, NULL, NULL, '廉桥收费站(G60沪昆高速入口)', '收费站', 20, 10, '111.833285', '27.318389', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1930, NULL, NULL, '杨桥收费站(S80衡邵高速出口)', '收费站', 20, 10, '111.964050', '27.152300', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1931, NULL, NULL, '杨桥收费站(S80衡邵高速入口)', '收费站', 20, 10, '111.964105', '27.152100', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1932, NULL, NULL, '新邵收费站(S80衡邵高速入口)', '收费站', 20, 10, '111.487470', '27.390536', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1933, NULL, NULL, '新邵收费站(S80衡邵高速出口)', '收费站', 20, 10, '111.487589', '27.390640', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1934, NULL, NULL, '寸石收费站(G55二广高速出口)', '收费站', 20, 10, '111.568972', '27.469021', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1935, NULL, NULL, '寸石收费站(S75邵坪高速入口)', '收费站', 20, 10, '111.569065', '27.469207', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1936, NULL, NULL, '大同收费站(S75邵坪高速入口)', '收费站', 20, 10, '111.526933', '27.560229', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1937, NULL, NULL, '大同收费站(S75邵坪高速出口)', '收费站', 20, 10, '111.526997', '27.560375', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1938, NULL, NULL, '白仓收费站(G55二广高速入口)', '收费站', 20, 10, '111.347212', '26.894834', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1939, NULL, NULL, '白仓收费站(G55二广高速出口)', '收费站', 20, 10, '111.347209', '26.894961', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1940, NULL, NULL, '蒋坊收费站(S91洞城高速出口)', '收费站', 20, 10, '110.343505', '26.506680', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1941, NULL, NULL, '坪上收费站(S70娄新高速出口)', '收费站', 20, 10, '111.544191', '27.590759', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1942, NULL, NULL, '坪上收费站(S70娄新高速入口)', '收费站', 20, 10, '111.544372', '27.590895', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1943, NULL, NULL, '寨市收费站(S86宁靖高速出口)', '收费站', 20, 10, '110.059319', '26.506362', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1944, NULL, NULL, '蒋坊收费站(S91洞城高速入口)', '收费站', 20, 10, '110.343357', '26.506670', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1945, NULL, NULL, '绥宁收费站(S86宁靖高速出口)', '收费站', 20, 10, '110.145815', '26.538941', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1946, NULL, NULL, '绥宁收费站(S86宁靖高速入口)', '收费站', 20, 10, '110.145746', '26.538898', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1947, NULL, NULL, '隆回收费站(G60沪昆高速出口)', '收费站', 20, 10, '111.000342', '27.099649', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1948, NULL, NULL, '隆回收费站(G60沪昆高速入口)', '收费站', 20, 10, '111.000160', '27.099655', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1949, NULL, NULL, '邵阳西收费站(G55二广高速入口)', '收费站', 20, 10, '111.388381', '27.288665', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1950, NULL, NULL, '邵阳西收费站(G55二广高速出口)', '收费站', 20, 10, '111.388332', '27.288442', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1951, NULL, NULL, '新宁收费站(G59呼北高速出口)', '收费站', 20, 10, '110.793280', '26.468702', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1952, NULL, NULL, '新宁收费站(G59呼北高速入口)', '收费站', 20, 10, '110.793146', '26.468901', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1953, NULL, NULL, '西岩收费站(S91洞城高速出口)', '收费站', 20, 10, '110.496099', '26.642501', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1954, NULL, NULL, '西岩收费站(S91洞城高速入口)', '收费站', 20, 10, '110.496357', '26.642679', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1955, NULL, NULL, '关峡收费站(S86宁靖高速入口)', '收费站', 20, 10, '110.279654', '26.529460', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1956, NULL, NULL, '关峡收费站(S86宁靖高速出口)', '收费站', 20, 10, '110.279745', '26.529503', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1957, NULL, NULL, '城步收费站(S91洞城高速出口)', '收费站', 20, 10, '110.334409', '26.415086', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1958, NULL, NULL, '城步收费站(S91洞城高速入口)', '收费站', 20, 10, '110.334575', '26.415175', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1959, NULL, NULL, '寨市收费站(S86宁靖高速入口)', '收费站', 20, 10, '110.059176', '26.506520', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1960, NULL, NULL, '新邵西收费站(G55二广高速出口)', '收费站', 20, 10, '111.417688', '27.335457', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1961, NULL, NULL, '邵阳北收费站(S75邵坪高速入口)', '收费站', 20, 10, '111.562503', '27.307299', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1962, NULL, NULL, '新邵西收费站(G55二广高速入口)', '收费站', 20, 10, '111.417744', '27.335711', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1963, NULL, NULL, '邵阳北收费站(S75邵坪高速出口)', '收费站', 20, 10, '111.562339', '27.307575', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1964, NULL, NULL, '邵东南收费站(S80衡邵高速入口)', '收费站', 20, 10, '111.721132', '27.182542', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1965, NULL, NULL, '邵东南收费站(S80衡邵高速出口)', '收费站', 20, 10, '111.721072', '27.182386', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1966, NULL, NULL, '周旺铺收费站(G60沪昆高速出口)', '收费站', 20, 10, '111.198090', '27.182340', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1967, NULL, NULL, '邵阳县收费站(G55二广高速出口)', '收费站', 20, 10, '111.359676', '27.011446', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1968, NULL, NULL, '周旺铺收费站(G60沪昆高速入口)', '收费站', 20, 10, '111.198045', '27.182214', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1969, NULL, NULL, '邵阳县收费站(G55二广高速入口)', '收费站', 20, 10, '111.359547', '27.011293', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1970, NULL, NULL, '武冈东收费站(G59呼北高速入口)', '收费站', 20, 10, '110.697904', '26.743400', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1971, NULL, NULL, '武冈东收费站(G59呼北高速出口)', '收费站', 20, 10, '110.697853', '26.743582', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1972, NULL, NULL, '武冈西收费站(S91洞城高速入口)', '收费站', 20, 10, '110.603178', '26.751464', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1973, NULL, NULL, '司马冲收费站(G59呼北高速出口)', '收费站', 20, 10, '110.742941', '26.608620', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1974, NULL, NULL, '武冈西收费站(S91洞城高速出口)', '收费站', 20, 10, '110.603073', '26.751301', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1975, NULL, NULL, '司马冲收费站(G59呼北高速入口)', '收费站', 20, 10, '110.742939', '26.608405', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1976, NULL, NULL, '八角寨收费站(G59呼北高速出口)', '收费站', 20, 10, '110.816953', '26.291667', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1977, NULL, NULL, '八角寨收费站(G59呼北高速入口)', '收费站', 20, 10, '110.817026', '26.291806', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1978, NULL, NULL, '乐安铺收费站(G65包茂高速入口)', '收费站', 20, 10, '109.898913', '26.466558', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1979, NULL, NULL, '乐安铺收费站(G65包茂高速出口)', '收费站', 20, 10, '109.899048', '26.466433', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1980, NULL, NULL, '大水收费站(S91洞城高速出口)', '收费站', 20, 10, '110.690527', '27.013957', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1981, NULL, NULL, '大水收费站(S91洞城高速入口)', '收费站', 20, 10, '110.690525', '27.013803', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1982, NULL, NULL, '洞口收费站(G60沪昆高速出口)', '收费站', 20, 10, '110.590160', '27.041038', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1983, NULL, NULL, '高沙收费站(S91洞城高速出口)', '收费站', 20, 10, '110.688708', '26.938346', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1984, NULL, NULL, '黄桥收费站(G60沪昆高速入口)', '收费站', 20, 10, '110.843551', '27.036448', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1985, NULL, NULL, '高沙收费站(S91洞城高速入口)', '收费站', 20, 10, '110.688656', '26.938165', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1986, NULL, NULL, '洞口收费站(G60沪昆高速入口)', '收费站', 20, 10, '110.590040', '27.040854', '邵阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1987, NULL, NULL, '株洲收费站(S21长株高速北向)', '收费站', 20, 10, '113.163975', '27.929972', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1988, NULL, NULL, '株洲收费站(S21长株高速南向)', '收费站', 20, 10, '113.163720', '27.930006', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1989, NULL, NULL, '株洲东收费站(G60沪昆高速入口)', '收费站', 20, 10, '113.225827', '27.903786', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1990, NULL, NULL, '株洲东收费站(G60沪昆高速出口)', '收费站', 20, 10, '113.225768', '27.903912', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1991, NULL, NULL, '株洲西收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.058223', '27.810841', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1992, NULL, NULL, '朱亭收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.033895', '27.363964', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1993, NULL, NULL, '芷钱桥收费站(G60沪昆高速出口)', '收费站', 20, 10, '113.346710', '27.849397', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1994, NULL, NULL, '云龙北收费站(S21长株高速入口)', '收费站', 20, 10, '113.182242', '27.992669', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1995, NULL, NULL, '云龙北收费站(S21长株高速出口)', '收费站', 20, 10, '113.182479', '27.992610', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1996, NULL, NULL, '三门收费站(G60醴娄高速出口)', '收费站', 20, 10, '113.078024', '27.608710', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1997, NULL, NULL, '三门收费站(G60醴娄高速入口)', '收费站', 20, 10, '113.078048', '27.608758', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1998, NULL, NULL, '芷钱桥收费站(G60沪昆高速入口)', '收费站', 20, 10, '113.346604', '27.849278', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (1999, NULL, NULL, '白关收费站(S52醴易高速东南向)', '收费站', 20, 10, '113.270480', '27.809239', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2000, NULL, NULL, '白关收费站(S52醴易高速西北向)', '收费站', 20, 10, '113.270662', '27.809445', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2001, NULL, NULL, '姚家坝收费站(S52醴易高速入口)', '收费站', 20, 10, '113.333424', '27.778257', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2002, NULL, NULL, '姚家坝收费站(S52醴易高速出口)', '收费站', 20, 10, '113.333521', '27.778158', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2003, NULL, NULL, '王拾万收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.032691', '27.498781', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2004, NULL, NULL, '王拾万收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.032667', '27.498897', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2005, NULL, NULL, '渌口区收费站(G60醴娄高速入口)', '收费站', 20, 10, '113.147175', '27.606735', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2006, NULL, NULL, '渌口区收费站(G60醴娄高速出口)', '收费站', 20, 10, '113.147234', '27.606751', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2007, NULL, NULL, '朱亭收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.033833', '27.363871', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2008, NULL, NULL, '收费站(茶常高速出口)', '收费站', 20, 10, '113.502115', '26.712349', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2009, NULL, NULL, '收费站(茶常高速入口)', '收费站', 20, 10, '113.502066', '26.712315', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2010, NULL, NULL, '醴陵北收费站(G60沪昆高速入口)', '收费站', 20, 10, '113.485198', '27.706261', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2011, NULL, NULL, '醴陵东收费站(G60沪昆高速入口)', '收费站', 20, 10, '113.569081', '27.697070', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2012, NULL, NULL, '醴陵北收费站(G60沪昆高速出口)', '收费站', 20, 10, '113.484999', '27.706327', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2013, NULL, NULL, '收费站(G60醴娄高速入口)', '收费站', 20, 10, '113.562389', '27.642889', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2014, NULL, NULL, '收费站(G60醴娄高速出口)', '收费站', 20, 10, '113.562335', '27.642918', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2015, NULL, NULL, '收费站(G0422武深高速出口)', '收费站', 20, 10, '113.458353', '27.781424', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2016, NULL, NULL, '收费站(G0422武深高速入口)', '收费站', 20, 10, '113.458412', '27.781446', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2017, NULL, NULL, '株洲西收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.058316', '27.810718', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2018, NULL, NULL, '攸县收费站(G72泉南高速入口)', '收费站', 20, 10, '113.324540', '26.854033', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2019, NULL, NULL, '攸县收费站(G72泉南高速出口)', '收费站', 20, 10, '113.324421', '26.853986', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2020, NULL, NULL, '光明收费站(G72泉南高速入口)', '收费站', 20, 10, '113.775942', '26.948669', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2021, NULL, NULL, '光明收费站(G72泉南高速出口)', '收费站', 20, 10, '113.775713', '26.948616', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2022, NULL, NULL, '腰陂收费站(G72泉南高速出口)', '收费站', 20, 10, '113.665289', '26.869527', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2023, NULL, NULL, '腰陂收费站(G72泉南高速入口)', '收费站', 20, 10, '113.665123', '26.869399', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2024, NULL, NULL, '石亭收费站(G60醴娄高速入口)', '收费站', 20, 10, '113.238908', '27.610264', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2025, NULL, NULL, '石亭收费站(G60醴娄高速出口)', '收费站', 20, 10, '113.238945', '27.610224', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2026, NULL, NULL, '茶山收费站(G60醴娄高速入口)', '收费站', 20, 10, '113.366192', '27.608384', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2027, NULL, NULL, '茶山收费站(G60醴娄高速出口)', '收费站', 20, 10, '113.366508', '27.608585', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2028, NULL, NULL, '醴陵南收费(G60醴娄高速出口)', '收费站', 20, 10, '113.512823', '27.612499', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2029, NULL, NULL, '醴陵南收费(G60醴娄高速入口)', '收费站', 20, 10, '113.512834', '27.612446', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2030, NULL, NULL, '嘉树收费站(G0422武深高速入口)', '收费站', 20, 10, '113.449695', '27.561193', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2031, NULL, NULL, '泗汾收费站(G0422武深高速出口)', '收费站', 20, 10, '113.488084', '27.491950', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2032, NULL, NULL, '醴陵东收费站(G60沪昆高速出口)', '收费站', 20, 10, '113.569392', '27.696864', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2033, NULL, NULL, '虎踞收费站(G0422武深高速出口)', '收费站', 20, 10, '113.378212', '26.889467', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2034, NULL, NULL, '虎踞收费站(G0422武深高速入口)', '收费站', 20, 10, '113.378169', '26.889660', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2035, NULL, NULL, '茶陵收费站(G0422武深高速入口)', '收费站', 20, 10, '113.524267', '26.761879', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2036, NULL, NULL, '茶陵东收费站(G72泉南高速入口)', '收费站', 20, 10, '113.589110', '26.788556', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2037, NULL, NULL, '茶陵东收费站(G72泉南高速出口)', '收费站', 20, 10, '113.589238', '26.788700', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2038, NULL, NULL, '浣溪收费站(G0422武深高速入口)', '收费站', 20, 10, '113.608487', '26.554236', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2039, NULL, NULL, '浣溪收费站(G0422武深高速出口)', '收费站', 20, 10, '113.608573', '26.554237', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2040, NULL, NULL, '霞阳收费站(G1517莆炎高速入口)', '收费站', 20, 10, '113.802728', '26.515082', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2041, NULL, NULL, '霞阳收费站(G1517莆炎高速出口)', '收费站', 20, 10, '113.802585', '26.514882', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2042, NULL, NULL, '船形收费站(G0422武深高速入口)', '收费站', 20, 10, '113.669599', '26.308284', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2043, NULL, NULL, '船形收费站(G0422武深高速出口)', '收费站', 20, 10, '113.669712', '26.308214', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2044, NULL, NULL, '中村收费站(G0422武深高速入口)', '收费站', 20, 10, '113.764018', '26.195373', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2045, NULL, NULL, '中村收费站(G0422武深高速出口)', '收费站', 20, 10, '113.763831', '26.195369', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2046, NULL, NULL, '嘉树收费站(G0422武深高速出口)', '收费站', 20, 10, '113.449933', '27.561436', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2047, NULL, NULL, '枫林市收费站(G0422武深高速出口)', '收费站', 20, 10, '113.429786', '27.839919', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2048, NULL, NULL, '泗汾收费站(G0422武深高速入口)', '收费站', 20, 10, '113.488070', '27.492168', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2049, NULL, NULL, '枫林市收费站(G0422武深高速入口)', '收费站', 20, 10, '113.429904', '27.839817', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2050, NULL, NULL, '醴陵西收费站(G0422武深高速入口)', '收费站', 20, 10, '113.455734', '27.674486', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2051, NULL, NULL, '醴陵西收费站(G0422武深高速出口)', '收费站', 20, 10, '113.455678', '27.674265', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2052, NULL, NULL, '皇图岭收费站(G0422武深高速出口)', '收费站', 20, 10, '113.520295', '27.357035', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2053, NULL, NULL, '皇图岭收费站(G0422武深高速入口)', '收费站', 20, 10, '113.520246', '27.356792', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2054, NULL, NULL, '攸县东收费站(G0422武深高速出口)', '收费站', 20, 10, '113.414357', '27.012699', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2055, NULL, NULL, '酒埠江收费站(G0422武深高速入口)', '收费站', 20, 10, '113.503457', '27.219076', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2056, NULL, NULL, '酒埠江收费站(G0422武深高速出口)', '收费站', 20, 10, '113.503202', '27.218973', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2057, NULL, NULL, '钟佳桥收费站(G0422武深高速入口)', '收费站', 20, 10, '113.461794', '27.143766', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2058, NULL, NULL, '钟佳桥收费站(G0422武深高速出口)', '收费站', 20, 10, '113.461734', '27.143983', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2059, NULL, NULL, '攸县东收费站(G0422武深高速入口)', '收费站', 20, 10, '113.414345', '27.012460', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2060, NULL, NULL, '炎陵西收费站(G0422武深高速入口)', '收费站', 20, 10, '113.671194', '26.497900', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2061, NULL, NULL, '神农谷收费站(G1517莆炎高速出口)', '收费站', 20, 10, '113.850781', '26.566755', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2062, NULL, NULL, '炎陵西收费站(G0422武深高速出口)', '收费站', 20, 10, '113.671371', '26.497801', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2063, NULL, NULL, '神农谷收费站(G1517莆炎高速入口)', '收费站', 20, 10, '113.850689', '26.566866', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2064, NULL, NULL, '炎帝陵收费站(G0422武深高速出口)', '收费站', 20, 10, '113.660627', '26.414624', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2065, NULL, NULL, '炎帝陵收费站(G0422武深高速入口)', '收费站', 20, 10, '113.660645', '26.414849', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2066, NULL, NULL, '湖南高速神农谷收费站', '收费站', 20, 10, '113.850773', '26.566731', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2067, NULL, NULL, '醴陵经开区收费站(S52醴易高速入口)', '收费站', 20, 10, '113.464919', '27.714053', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2068, NULL, NULL, '醴陵经开区收费站(S52醴易高速出口)', '收费站', 20, 10, '113.464874', '27.713936', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2069, NULL, NULL, '茶陵收费站(G0422武深高速出口)', '收费站', 20, 10, '113.524306', '26.761744', '株洲市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2070, NULL, NULL, '收费站(G0421许广高速出口)', '收费站', 20, 10, '112.549152', '26.818468', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2071, NULL, NULL, '收费站(G0421许广高速入口)', '收费站', 20, 10, '112.549110', '26.818501', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2072, NULL, NULL, '石鼓收费站(S51南岳高速出口)', '收费站', 20, 10, '112.593785', '26.948978', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2073, NULL, NULL, '雁峰收费站(G72泉南高速出口)', '收费站', 20, 10, '112.586168', '26.810148', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2074, NULL, NULL, '雁峰收费站(G72泉南高速入口)', '收费站', 20, 10, '112.585989', '26.810221', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2075, NULL, NULL, '蒸湘收费站(G0421许广高速出口)', '收费站', 20, 10, '112.525432', '26.876942', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2076, NULL, NULL, '珠晖北收费站(S80衡邵高速出口)', '收费站', 20, 10, '112.698023', '26.957305', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2077, NULL, NULL, '珠晖南收费站(G72泉南高速出口)', '收费站', 20, 10, '112.678210', '26.816967', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2078, NULL, NULL, '蒸湘收费站(G0421许广高速入口)', '收费站', 20, 10, '112.525477', '26.877119', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2079, NULL, NULL, '南岳收费站(S51南岳高速西北向)', '收费站', 20, 10, '112.732485', '27.201429', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2080, NULL, NULL, '珠晖南收费站(G72泉南高速入口)', '收费站', 20, 10, '112.677964', '26.816954', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2081, NULL, NULL, '珠晖北收费站(S80衡邵高速入口)', '收费站', 20, 10, '112.698229', '26.957145', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2082, NULL, NULL, '南岳收费站(S51南岳高速东南向)', '收费站', 20, 10, '112.732081', '27.201258', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2083, NULL, NULL, '石鼓收费站(S51南岳高速入口)', '收费站', 20, 10, '112.594557', '26.949079', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2084, NULL, NULL, '松木收费站(S80衡邵高速出口)', '收费站', 20, 10, '112.634993', '26.960250', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2085, NULL, NULL, '松木收费站(S80衡邵高速入口)', '收费站', 20, 10, '112.635047', '26.960479', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2086, NULL, NULL, '衡阳西渡收费站(S80衡邵高速出口)', '收费站', 20, 10, '112.433632', '26.979858', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2087, NULL, NULL, '石市收费站(G0421许广高速出口)', '收费站', 20, 10, '112.484244', '27.182876', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2088, NULL, NULL, '白果收费站(G0421许广高速入口)', '收费站', 20, 10, '112.607869', '27.389488', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2089, NULL, NULL, '白果收费站(G0421许广高速出口)', '收费站', 20, 10, '112.607731', '27.389392', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2090, NULL, NULL, '大浦收费站(G4京港澳高速出口)', '收费站', 20, 10, '112.839250', '27.009678', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2091, NULL, NULL, '收费站(衡永高速出口)', '收费站', 20, 10, '112.351084', '26.812108', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2092, NULL, NULL, '收费站(衡永高速入口)', '收费站', 20, 10, '112.351025', '26.812094', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2093, NULL, NULL, '收费站(茶常高速出口)', '收费站', 20, 10, '112.817456', '26.490391', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2094, NULL, NULL, '收费站(茶常高速入口)', '收费站', 20, 10, '112.817502', '26.490461', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2095, NULL, NULL, '收费站(衡永高速出口)', '收费站', 20, 10, '112.087320', '26.738708', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2096, NULL, NULL, '收费站(衡永高速入口)', '收费站', 20, 10, '112.087276', '26.738751', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2097, NULL, NULL, '收费站(白南高速出口)', '收费站', 20, 10, '112.720296', '27.376610', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2098, NULL, NULL, '收费站(白南高速入口)', '收费站', 20, 10, '112.720299', '27.376495', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2099, NULL, NULL, '收费站(茶常高速入口)', '收费站', 20, 10, '112.953227', '26.529127', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2100, NULL, NULL, '收费站(茶常高速出口)', '收费站', 20, 10, '112.953125', '26.529079', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2101, NULL, NULL, '收费站(衡永高速入口)', '收费站', 20, 10, '112.008732', '26.711946', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2102, NULL, NULL, '收费站(衡永高速出口)', '收费站', 20, 10, '112.008786', '26.711918', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2103, NULL, NULL, '收费站(茶常高速入口)', '收费站', 20, 10, '113.092298', '26.516644', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2104, NULL, NULL, '收费站(茶常高速出口)', '收费站', 20, 10, '113.092208', '26.516659', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2105, NULL, NULL, '东湖收费站(G0421许广高速入口)', '收费站', 20, 10, '112.560936', '27.313147', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2106, NULL, NULL, '冠市收费站(G4京港澳高速出口)', '收费站', 20, 10, '112.874948', '26.728592', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2107, NULL, NULL, '东湖收费站(G0421许广高速出口)', '收费站', 20, 10, '112.560821', '27.312985', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2108, NULL, NULL, '楠木收费站(S80衡邵高速入口)', '收费站', 20, 10, '112.791087', '26.974220', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2109, NULL, NULL, '楠木收费站(S80衡邵高速出口)', '收费站', 20, 10, '112.791184', '26.974297', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2110, NULL, NULL, '高湖收费站(G72泉南高速出口)', '收费站', 20, 10, '113.182624', '26.951799', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2111, NULL, NULL, '衡东收费站(G72泉南高速出口)', '收费站', 20, 10, '112.979458', '27.020850', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2112, NULL, NULL, '新塘收费站(G4京港澳高速入口)', '收费站', 20, 10, '112.887478', '27.211604', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2113, NULL, NULL, '收费站(S92茶常高速入口)', '收费站', 20, 10, '112.656680', '26.486729', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2114, NULL, NULL, '收费站(S92茶常高速出口)', '收费站', 20, 10, '112.656649', '26.486677', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2115, NULL, NULL, '收费站(S92茶常高速出口)', '收费站', 20, 10, '112.548534', '26.480136', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2116, NULL, NULL, '收费站(S92茶常高速入口)', '收费站', 20, 10, '112.548475', '26.480030', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2117, NULL, NULL, '大浦收费站(G4京港澳高速入口)', '收费站', 20, 10, '112.839128', '27.009569', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2118, NULL, NULL, '新塘收费站(G4京港澳高速出口)', '收费站', 20, 10, '112.887511', '27.211807', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2119, NULL, NULL, '高湖收费站(G72泉南高速入口)', '收费站', 20, 10, '113.182715', '26.951679', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2120, NULL, NULL, '收费站(衡山支线高速出口)', '收费站', 20, 10, '112.868799', '27.288307', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2121, NULL, NULL, '收费站(衡山支线高速入口)', '收费站', 20, 10, '112.868869', '27.288183', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2122, NULL, NULL, '新场市收费站(S78南岳高速东延线出口)', '收费站', 20, 10, '112.819910', '27.148182', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2123, NULL, NULL, '新场市收费站(S78南岳高速东延线入口)', '收费站', 20, 10, '112.819692', '27.148227', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2124, NULL, NULL, '衡东收费站(G72泉南高速入口)', '收费站', 20, 10, '112.979308', '27.020888', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2125, NULL, NULL, '归阳收费站(G72泉南高速入口)', '收费站', 20, 10, '112.201208', '26.557116', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2126, NULL, NULL, '杨山收费站(S78南岳高速东延线入口)', '收费站', 20, 10, '112.898041', '27.072099', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2127, NULL, NULL, '杨山收费站(S78南岳高速东延线出口)', '收费站', 20, 10, '112.898229', '27.071906', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2128, NULL, NULL, '新河收费站(S71华常高速出口)', '收费站', 20, 10, '112.336606', '26.507051', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2129, NULL, NULL, '新河收费站(S71华常高速入口)', '收费站', 20, 10, '112.336600', '26.506909', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2130, NULL, NULL, '演陂收费站(S80衡邵高速入口)', '收费站', 20, 10, '112.272030', '27.063118', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2131, NULL, NULL, '演陂收费站(S80衡邵高速出口)', '收费站', 20, 10, '112.272137', '27.063242', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2132, NULL, NULL, '洪市收费站(S71华常高速出口)', '收费站', 20, 10, '112.228802', '27.166858', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2133, NULL, NULL, '洪市收费站(S71华常高速入口)', '收费站', 20, 10, '112.228729', '27.167150', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2134, NULL, NULL, '金兰收费站(S80衡邵高速出口)', '收费站', 20, 10, '112.108297', '27.133165', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2135, NULL, NULL, '金兰收费站(S80衡邵高速入口)', '收费站', 20, 10, '112.108220', '27.133307', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2136, NULL, NULL, '檀山咀收费站(S71华常高速入口)', '收费站', 20, 10, '112.249386', '26.972924', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2137, NULL, NULL, '檀山咀收费站(S71华常高速出口)', '收费站', 20, 10, '112.249053', '26.972967', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2138, NULL, NULL, '常宁北收费站(S71华常高速出口)', '收费站', 20, 10, '112.416776', '26.464246', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2139, NULL, NULL, '常宁北收费站(S71华常高速入口)', '收费站', 20, 10, '112.416836', '26.464107', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2140, NULL, NULL, '祁东南收费站(S71华常高速出口)', '收费站', 20, 10, '112.261871', '26.536669', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2141, NULL, NULL, '祁东南收费站(S71华常高速入口)', '收费站', 20, 10, '112.261938', '26.536558', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2142, NULL, NULL, '新市收费站(G4京港澳高速出口)', '收费站', 20, 10, '112.935046', '26.605021', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2143, NULL, NULL, '耒阳收费站(G4京港澳高速入口)', '收费站', 20, 10, '112.898430', '26.432254', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2144, NULL, NULL, '耒阳收费站(G4京港澳高速出口)', '收费站', 20, 10, '112.898229', '26.432255', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2145, NULL, NULL, '公平收费站(G4京港澳高速入口)', '收费站', 20, 10, '112.873286', '26.213224', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2146, NULL, NULL, '公平收费站(G4京港澳高速出口)', '收费站', 20, 10, '112.873309', '26.213364', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2147, NULL, NULL, '石市收费站(G0421许广高速入口)', '收费站', 20, 10, '112.484354', '27.183037', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2148, NULL, NULL, '常宁收费站(G0421许广高速入口)', '收费站', 20, 10, '112.453643', '26.381070', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2149, NULL, NULL, '归阳收费站(G72泉南高速出口)', '收费站', 20, 10, '112.201378', '26.557100', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2150, NULL, NULL, '新市收费站(G4京港澳高速入口)', '收费站', 20, 10, '112.935049', '26.604942', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2151, NULL, NULL, '古城收费站(G72泉南高速入口)', '收费站', 20, 10, '112.773355', '26.839595', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2152, NULL, NULL, '古城收费站(G72泉南高速出口)', '收费站', 20, 10, '112.773508', '26.839674', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2153, NULL, NULL, '硫市收费站(G72泉南高速入口)', '收费站', 20, 10, '112.466640', '26.706136', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2154, NULL, NULL, '硫市收费站(G72泉南高速出口)', '收费站', 20, 10, '112.466789', '26.706185', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2155, NULL, NULL, '衡阳西渡收费站(S80衡邵高速入口)', '收费站', 20, 10, '112.433812', '26.979786', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2156, NULL, NULL, '岐山收费站(S71华常高速入口)', '收费站', 20, 10, '112.225341', '26.853078', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2157, NULL, NULL, '岐山收费站(S71华常高速出口)', '收费站', 20, 10, '112.225466', '26.853265', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2158, NULL, NULL, '常宁收费站(G0421许广高速出口)', '收费站', 20, 10, '112.453604', '26.381346', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2159, NULL, NULL, '庙前收费站(G0421许广高速出口)', '收费站', 20, 10, '112.484981', '26.193274', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2160, NULL, NULL, '庙前收费站(G0421许广高速入口)', '收费站', 20, 10, '112.484960', '26.193104', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2161, NULL, NULL, '水口山收费站(G0421许广高速出口)', '收费站', 20, 10, '112.556018', '26.570876', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2162, NULL, NULL, '冠市收费站(G4京港澳高速入口)', '收费站', 20, 10, '112.875073', '26.728541', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2163, NULL, NULL, '车江收费站(G0421许广高速入口)', '收费站', 20, 10, '112.539859', '26.730683', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2164, NULL, NULL, '车江收费站(G0421许广高速出口)', '收费站', 20, 10, '112.539982', '26.730552', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2165, NULL, NULL, '水口山收费站(G0421许广高速入口)', '收费站', 20, 10, '112.555824', '26.570713', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2166, NULL, NULL, '鸟江收费站(S71华常高速出口)', '收费站', 20, 10, '112.231679', '26.608960', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2167, NULL, NULL, '鸟江收费站(S71华常高速入口)', '收费站', 20, 10, '112.231753', '26.608728', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2168, NULL, NULL, '粮市收费站(G72泉南高速入口)', '收费站', 20, 10, '112.304929', '26.586189', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2169, NULL, NULL, '粮市收费站(G72泉南高速出口)', '收费站', 20, 10, '112.304769', '26.586178', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2170, NULL, NULL, '白鹤铺收费站(S71华常高速入口)', '收费站', 20, 10, '112.209292', '26.778529', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2171, NULL, NULL, '白鹤铺收费站(S71华常高速出口)', '收费站', 20, 10, '112.209275', '26.778727', '衡阳市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2172, NULL, NULL, '长沙西收费站(岳麓西大道东向)', '收费站', 20, 10, '112.784277', '28.244547', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2173, NULL, NULL, '长潭西高速长沙学士收费站', '收费站', 20, 10, '112.893219', '28.141266', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2174, NULL, NULL, '金桥收费站(岳麓西大道出口)', '收费站', 20, 10, '112.849822', '28.237114', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2175, NULL, NULL, '乌山收费站(G0421许广高速出口)', '收费站', 20, 10, '112.746580', '28.297980', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2176, NULL, NULL, '乌山收费站(G0421许广高速入口)', '收费站', 20, 10, '112.746631', '28.298124', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2177, NULL, NULL, '白泉收费站(S41长潭西高速出口)', '收费站', 20, 10, '112.896735', '27.996428', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2178, NULL, NULL, '白泉收费站(S41长潭西高速入口)', '收费站', 20, 10, '112.896737', '27.996374', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2179, NULL, NULL, '莲花收费站(G0421许广高速出口)', '收费站', 20, 10, '112.753784', '28.097869', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2180, NULL, NULL, '莲花收费站(G0421许广高速入口)', '收费站', 20, 10, '112.753836', '28.097700', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2181, NULL, NULL, '望城收费站(G0421许广高速入口)', '收费站', 20, 10, '112.786311', '28.401959', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2182, NULL, NULL, '望城收费站(G0421许广高速出口)', '收费站', 20, 10, '112.786400', '28.401793', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2183, NULL, NULL, '铜官收费站(G0421许广高速入口)', '收费站', 20, 10, '112.841309', '28.451451', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2184, NULL, NULL, '铜官收费站(G0421许广高速出口)', '收费站', 20, 10, '112.841587', '28.451456', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2185, NULL, NULL, '岳麓收费站(S41长潭西高速入口)', '收费站', 20, 10, '112.883651', '28.121815', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2186, NULL, NULL, '长沙西收费站(G5513长张高速西向)', '收费站', 20, 10, '112.784183', '28.245102', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2187, NULL, NULL, '栗山塘收费站(S41长潭西高速入口)', '收费站', 20, 10, '112.859245', '28.086306', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2188, NULL, NULL, '栗山塘收费站(S41长潭西高速出口)', '收费站', 20, 10, '112.859267', '28.086358', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2189, NULL, NULL, '白箬铺收费站(G0421许广高速入口)', '收费站', 20, 10, '112.759800', '28.205175', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2190, NULL, NULL, '白箬铺收费站(G0421许广高速出口)', '收费站', 20, 10, '112.759902', '28.205352', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2191, NULL, NULL, '星城收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '112.879165', '28.300152', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2192, NULL, NULL, '金桥收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '112.849835', '28.237332', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2193, NULL, NULL, '星城收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '112.879050', '28.300237', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2194, NULL, NULL, '坪塘收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '112.926879', '28.108270', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2195, NULL, NULL, '金凤收费站(S50长芷高速入口西南向)', '收费站', 20, 10, '112.779831', '28.089568', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2196, NULL, NULL, '黄花塘收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '112.856024', '28.197289', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2197, NULL, NULL, '黄花塘收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '112.855833', '28.197255', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2198, NULL, NULL, '友仁收费站(G5513长张高速入口西北向)', '收费站', 20, 10, '112.727966', '28.258864', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2199, NULL, NULL, '友仁收费站(G5513长张高速入口东南向)', '收费站', 20, 10, '112.727087', '28.256636', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2200, NULL, NULL, '金凤收费站(S50长芷高速出口望城方向)', '收费站', 20, 10, '112.780515', '28.088972', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2201, NULL, NULL, '长潭西高速学士收费站(S41长潭西高速出口)', '收费站', 20, 10, '112.892737', '28.140561', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2202, NULL, NULL, '友仁收费站(G5513长张高速出口白箬铺方向)', '收费站', 20, 10, '112.727074', '28.256598', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2203, NULL, NULL, '友仁收费站(G5513长张高速出口白箬铺方向)', '收费站', 20, 10, '112.727937', '28.258809', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2204, NULL, NULL, '长潭西高速学士收费站(S41长潭西高速入口)', '收费站', 20, 10, '112.893219', '28.141266', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2205, NULL, NULL, '坪塘收费站(湘府路高架出口)', '收费站', 20, 10, '112.926981', '28.108293', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2206, NULL, NULL, '李家塘收费站(G0401长沙绕城高速出口湘潭方向)', '收费站', 20, 10, '113.057734', '28.092058', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2207, NULL, NULL, '收费站(S41长潭西高速出口)', '收费站', 20, 10, '112.878165', '28.057743', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2208, NULL, NULL, '收费站(S41长潭西高速入口)', '收费站', 20, 10, '112.878210', '28.057774', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2209, NULL, NULL, '岳麓收费站(S41长潭西高速出口)', '收费站', 20, 10, '112.883465', '28.121189', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2210, NULL, NULL, '新港收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '112.953128', '28.324971', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2211, NULL, NULL, '大托收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '112.969741', '28.091355', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2212, NULL, NULL, '收费站(G0401长沙绕城高速出口沙坪方向)', '收费站', 20, 10, '113.003096', '28.316134', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2213, NULL, NULL, '白竹收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.087285', '28.094729', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2214, NULL, NULL, '白竹收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.087235', '28.094576', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2215, NULL, NULL, '茶亭收费站(G0421许广高速出口)', '收费站', 20, 10, '112.901374', '28.494452', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2216, NULL, NULL, '茶亭收费站(G0421许广高速入口)', '收费站', 20, 10, '112.901428', '28.494601', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2217, NULL, NULL, '黄金园收费站(G5517长常北线高速入口)', '收费站', 20, 10, '112.805837', '28.280435', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2218, NULL, NULL, '黄金园收费站(G5517长常北线高速出口)', '收费站', 20, 10, '112.806041', '28.280427', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2219, NULL, NULL, '雨花收费站(S40机场高速出口)', '收费站', 20, 10, '113.052690', '28.166818', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2220, NULL, NULL, '雨花收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.052843', '28.166590', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2221, NULL, NULL, '岳麓山风景名胜区东门停车场入口收费站', '收费站', 20, 10, '112.949249', '28.193372', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2222, NULL, NULL, '新港收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '112.952953', '28.324785', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2223, NULL, NULL, '望城经开区收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '112.859792', '28.292683', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2224, NULL, NULL, '团头收费站(S21长株高速入口)', '收费站', 20, 10, '113.179158', '28.049320', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2225, NULL, NULL, '团头收费站(S21长株高速出口)', '收费站', 20, 10, '113.179341', '28.049384', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2226, NULL, NULL, '大托收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '112.969575', '28.091282', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2227, NULL, NULL, '观音岩收费站(G5517长常北线高速入口西向)', '收费站', 20, 10, '112.852204', '28.276490', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2228, NULL, NULL, '捞刀河收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.003489', '28.316162', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2229, NULL, NULL, '望城经开区收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '112.859465', '28.292818', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2230, NULL, NULL, '观音岩收费站(G5517长常北线高速出口银星路方向)', '收费站', 20, 10, '112.852174', '28.276372', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2231, NULL, NULL, '万家丽南收费站(G0401长沙绕城高速入口西向)', '收费站', 20, 10, '113.037110', '28.085312', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2232, NULL, NULL, '捞刀河收费站(G0401长沙绕城高速出口望城区方向)', '收费站', 20, 10, '113.003406', '28.316154', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2233, NULL, NULL, '万家丽南收费站(G0401长沙绕城高速入口东向)', '收费站', 20, 10, '113.038788', '28.083027', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2234, NULL, NULL, '万家丽南收费站(G0401长沙绕城高速出口雨花经开区方向)', '收费站', 20, 10, '113.039939', '28.084507', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2235, NULL, NULL, '收费站(江杉高速出口)', '收费站', 20, 10, '113.284757', '28.085929', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2236, NULL, NULL, '收费站(江杉高速入口)', '收费站', 20, 10, '113.284928', '28.085953', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2237, NULL, NULL, '收费站', '收费站', 20, 10, '112.530090', '28.136412', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2238, NULL, NULL, '洞井收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.003262', '28.085933', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2239, NULL, NULL, '洞井收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.003373', '28.086004', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2240, NULL, NULL, '星沙收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.077043', '28.267605', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2241, NULL, NULL, '星沙收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.077041', '28.267323', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2242, NULL, NULL, '长沙收费站(G6021杭长高速出口)', '收费站', 20, 10, '113.064693', '28.230237', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2243, NULL, NULL, '广福收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.191283', '28.458624', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2244, NULL, NULL, '广福收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.191329', '28.458810', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2245, NULL, NULL, '板仓收费站(G4京港澳高速出口)', '收费站', 20, 10, '113.241504', '28.583569', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2246, NULL, NULL, '板仓收费站(G4京港澳高速入口)', '收费站', 20, 10, '113.241424', '28.583414', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2247, NULL, NULL, '㮾梨收费站(S40机场高速出口)', '收费站', 20, 10, '113.137570', '28.178207', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2248, NULL, NULL, '㮾梨收费站(S40机场高速入口)', '收费站', 20, 10, '113.137649', '28.178096', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2249, NULL, NULL, '长沙东收费站(S40机场高速西向)', '收费站', 20, 10, '113.201926', '28.186351', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2250, NULL, NULL, '长沙东收费站(S40机场高速东向)', '收费站', 20, 10, '113.201886', '28.186059', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2251, NULL, NULL, '黄花收费站(G6021杭长高速入口)', '收费站', 20, 10, '113.213054', '28.222875', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2252, NULL, NULL, '黄花收费站(G6021杭长高速出口)', '收费站', 20, 10, '113.212857', '28.222772', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2253, NULL, NULL, '仙人市收费站(S21长株高速入口)', '收费站', 20, 10, '113.182624', '28.068906', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2254, NULL, NULL, '仙人市收费站(S21长株高速出口)', '收费站', 20, 10, '113.182672', '28.068785', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2255, NULL, NULL, '江背收费站(G0422武深高速出口)', '收费站', 20, 10, '113.364520', '28.112852', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2256, NULL, NULL, '江背收费站(G0422武深高速入口)', '收费站', 20, 10, '113.364681', '28.112631', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2257, NULL, NULL, '㮾梨东收费站(S40机场高速出口)', '收费站', 20, 10, '113.163843', '28.182026', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2258, NULL, NULL, '收费站(S01宁韶高速入口)', '收费站', 20, 10, '112.554072', '28.070722', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2259, NULL, NULL, '收费站(S01宁韶高速入口)', '收费站', 20, 10, '112.530057', '28.136455', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2260, NULL, NULL, '收费站(S01宁韶高速出口)', '收费站', 20, 10, '112.553922', '28.070916', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2261, NULL, NULL, '干杉收费站(江杉高速出口)', '收费站', 20, 10, '113.184523', '28.115481', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2262, NULL, NULL, '干杉收费站(江杉高速入口)', '收费站', 20, 10, '113.184451', '28.115335', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2263, NULL, NULL, '长龙收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.158790', '28.247566', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2264, NULL, NULL, '长龙收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.158627', '28.247470', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2265, NULL, NULL, '松雅湖收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.125434', '28.301212', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2266, NULL, NULL, '松雅湖收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.125585', '28.301378', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2267, NULL, NULL, '朗梨东收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.163589', '28.182088', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2268, NULL, NULL, '鹿芝岭收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.126585', '28.103718', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2269, NULL, NULL, '鹿芝岭收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.126376', '28.103798', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2270, NULL, NULL, '万家丽北收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.072543', '28.313612', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2271, NULL, NULL, '㮾梨东收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.164506', '28.181959', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2272, NULL, NULL, '㮾梨东收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.164000', '28.181876', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2273, NULL, NULL, '宁乡北收费站(G5517长常北线高速出口)', '收费站', 20, 10, '112.617096', '28.414195', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2274, NULL, NULL, '收费站(G0401长沙绕城高速入口)', '收费站', 20, 10, '113.073462', '28.320163', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2275, NULL, NULL, '收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.073999', '28.320088', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2276, NULL, NULL, '唐市收费站(S71华常高速入口)', '收费站', 20, 10, '112.158323', '28.031050', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2277, NULL, NULL, '唐市收费站(S71华常高速出口)', '收费站', 20, 10, '112.158302', '28.030889', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2278, NULL, NULL, '大屯营收费站(S01宁韶高速出口)', '收费站', 20, 10, '112.589111', '27.969557', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2279, NULL, NULL, '横市收费站(S71华常高速出口)', '收费站', 20, 10, '112.186984', '28.154847', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2280, NULL, NULL, '横市收费站(S71华常高速入口)', '收费站', 20, 10, '112.186959', '28.154990', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2281, NULL, NULL, '花明楼收费站(S50长芷高速出口)', '收费站', 20, 10, '112.658103', '28.045541', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2282, NULL, NULL, '大瑶收费站(S19浏洪高速入口)', '收费站', 20, 10, '113.710160', '27.991365', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2283, NULL, NULL, '花明楼收费站(S50长芷高速入口)', '收费站', 20, 10, '112.658379', '28.045623', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2284, NULL, NULL, '宁乡收费站(G5513长张高速入口)', '收费站', 20, 10, '112.576883', '28.311060', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2285, NULL, NULL, '宁乡收费站(G5513长张高速出口)', '收费站', 20, 10, '112.576755', '28.311201', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2286, NULL, NULL, '大屯营收费站(S01宁韶高速入口)', '收费站', 20, 10, '112.589207', '27.969591', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2287, NULL, NULL, '浏阳东收费站(G6021杭长高速出口)', '收费站', 20, 10, '113.660643', '28.244854', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2288, NULL, NULL, '金洲北收费站(G5517长常北线高速出口)', '收费站', 20, 10, '112.714474', '28.337509', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2289, NULL, NULL, '金洲北收费站(G5517长常北线高速入口)', '收费站', 20, 10, '112.714511', '28.337464', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2290, NULL, NULL, '宁乡北收费站(G5517长常北线高速入口)', '收费站', 20, 10, '112.616790', '28.414284', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2291, NULL, NULL, '万家丽北收费站(G0401长沙绕城高速出口)', '收费站', 20, 10, '113.071956', '28.313481', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2292, NULL, NULL, '三一收费站(G6021杭长高速出口三一方向)', '收费站', 20, 10, '113.099504', '28.234135', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2293, NULL, NULL, '三一收费站(G6021杭长高速入口黄花方向)', '收费站', 20, 10, '113.099555', '28.233721', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2294, NULL, NULL, '永安收费站(G6021杭长高速入口)', '收费站', 20, 10, '113.290870', '28.205827', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2295, NULL, NULL, '浏阳西收费站(S19浏洪高速出口)', '收费站', 20, 10, '113.569764', '28.152509', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2296, NULL, NULL, '大瑶收费站(S19浏洪高速出口)', '收费站', 20, 10, '113.709937', '27.991222', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2297, NULL, NULL, '永安收费站(G6021杭长高速出口)', '收费站', 20, 10, '113.290873', '28.205680', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2298, NULL, NULL, '洞阳收费站(G6021杭长高速入口)', '收费站', 20, 10, '113.409019', '28.189373', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2299, NULL, NULL, '北盛收费站(G0422武深高速出口)', '收费站', 20, 10, '113.383622', '28.270504', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2300, NULL, NULL, '沙市收费站(G0422武深高速入口)', '收费站', 20, 10, '113.421998', '28.354779', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2301, NULL, NULL, '沙市收费站(G0422武深高速出口)', '收费站', 20, 10, '113.422113', '28.354552', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2302, NULL, NULL, '蕉溪收费站(G6021杭长高速入口)', '收费站', 20, 10, '113.528395', '28.226820', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2303, NULL, NULL, '浏阳南收费站(S19浏洪高速入口)', '收费站', 20, 10, '113.640618', '28.100531', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2304, NULL, NULL, '洞阳收费站(G6021杭长高速出口)', '收费站', 20, 10, '113.409167', '28.189299', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2305, NULL, NULL, '浏阳西收费站(S19浏洪高速入口)', '收费站', 20, 10, '113.569875', '28.152783', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2306, NULL, NULL, '跃龙收费站(G0422武深高速出口)', '收费站', 20, 10, '113.383605', '28.050283', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2307, NULL, NULL, '社港收费站(G0422武深高速入口)', '收费站', 20, 10, '113.529320', '28.447150', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2308, NULL, NULL, '普迹收费站(G0422武深高速出口)', '收费站', 20, 10, '113.384574', '27.967897', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2309, NULL, NULL, '蕉溪收费站(G6021杭长高速出口)', '收费站', 20, 10, '113.528496', '28.226676', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2310, NULL, NULL, '社港收费站(G0422武深高速出口)', '收费站', 20, 10, '113.529165', '28.447025', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2311, NULL, NULL, '古港收费站(G6021杭长高速入口)', '收费站', 20, 10, '113.786964', '28.300457', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2312, NULL, NULL, '浏阳南收费站(S19浏洪高速出口)', '收费站', 20, 10, '113.640759', '28.100407', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2313, NULL, NULL, '古港收费站(G6021杭长高速出口)', '收费站', 20, 10, '113.786838', '28.300332', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2314, NULL, NULL, '北盛收费站(G0422武深高速入口)', '收费站', 20, 10, '113.383313', '28.270637', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2315, NULL, NULL, '跃龙收费站(G0422武深高速入口)', '收费站', 20, 10, '113.383576', '28.050094', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2316, NULL, NULL, '普迹收费站(G0422武深高速入口)', '收费站', 20, 10, '113.384723', '27.967743', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2317, NULL, NULL, '官渡收费站(G6021杭长高速出口)', '收费站', 20, 10, '113.910317', '28.339335', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2318, NULL, NULL, '官渡收费站(G6021杭长高速入口)', '收费站', 20, 10, '113.910034', '28.339340', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2319, NULL, NULL, '浏阳东收费站(G6021杭长高速入口)', '收费站', 20, 10, '113.660987', '28.245012', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2320, NULL, NULL, '大围山南收费站(G6021杭长高速入口)', '收费站', 20, 10, '114.090628', '28.330332', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2321, NULL, NULL, '大围山南收费站(G6021杭长高速出口)', '收费站', 20, 10, '114.090233', '28.330377', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2322, NULL, NULL, '道林收费站(S50长芷高速出口)', '收费站', 20, 10, '112.746665', '28.060487', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2323, NULL, NULL, '道林收费站(S50长芷高速入口)', '收费站', 20, 10, '112.750069', '28.061869', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2324, NULL, NULL, '灰汤收费站(S50长芷高速出口)', '收费站', 20, 10, '112.345608', '27.972250', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2325, NULL, NULL, '灰汤收费站(S50长芷高速入口)', '收费站', 20, 10, '112.345306', '27.972088', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2326, NULL, NULL, '心田收费站(S71华常高速出口)', '收费站', 20, 10, '112.130976', '27.929863', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2327, NULL, NULL, '心田收费站(S71华常高速入口)', '收费站', 20, 10, '112.130975', '27.929682', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2328, NULL, NULL, '道林收费站(G0421许广高速出口)', '收费站', 20, 10, '112.750171', '28.061891', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2329, NULL, NULL, '道林收费站(G0421许广高速入口)', '收费站', 20, 10, '112.746587', '28.060425', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2330, NULL, NULL, '金洲收费站(G5513长张高速出口)', '收费站', 20, 10, '112.661419', '28.276774', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2331, NULL, NULL, '金洲收费站(G5513长张高速入口)', '收费站', 20, 10, '112.661633', '28.276732', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2332, NULL, NULL, '关山收费站(G5513长张高速入口西向)', '收费站', 20, 10, '112.690510', '28.274082', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2333, NULL, NULL, '关山收费站(G5513长张高速入口长沙方向)', '收费站', 20, 10, '112.689190', '28.271725', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2334, NULL, NULL, '关山收费站(G5513长张高速出口宁乡高新区方向)', '收费站', 20, 10, '112.689231', '28.271867', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2335, NULL, NULL, '关山收费站(G5513长张高速出口宁乡高新区方向)', '收费站', 20, 10, '112.690535', '28.274182', '长沙市', 1, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2336, NULL, NULL, '是小諾吧民宿', NULL, NULL, NULL, '117.031767', '36.668185', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2337, NULL, NULL, '时代花园南路', NULL, NULL, NULL, '116.202262', '39.918711', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2338, NULL, NULL, '重庆市', NULL, NULL, NULL, '106.550483', '29.563707', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2339, NULL, NULL, '长沙市', NULL, NULL, NULL, '112.938882', '28.228304', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2340, NULL, NULL, '沈阳法库财湖机场', NULL, NULL, NULL, '123.377853', '42.392152', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2341, NULL, NULL, '彭水站', NULL, NULL, NULL, '108.235662', '29.360546', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2342, NULL, NULL, '璧山站', NULL, NULL, NULL, '106.224251', '29.50415', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2343, NULL, NULL, '张家界收费站入口(阳和方向)', NULL, NULL, NULL, '110.562806', '29.140472', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2344, NULL, NULL, '株洲西收费站入口(长沙方向)', NULL, NULL, NULL, '113.055265', '27.810895', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2345, NULL, NULL, '株洲市', NULL, NULL, NULL, '113.132783', '27.828862', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2346, NULL, NULL, '湘潭市', NULL, NULL, NULL, '112.945439', '27.83136', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2347, NULL, NULL, '张家界荷花国际机场航站楼', NULL, NULL, NULL, '110.445286', '29.104783', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2348, NULL, NULL, '比较大卷饼(中兴花园1期店)', NULL, NULL, NULL, '128.223485', '43.360125', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2349, NULL, NULL, 'Hi大明家庭火锅便利店', NULL, NULL, NULL, '126.192038', '41.121809', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2350, NULL, NULL, '长沙站', NULL, NULL, NULL, '113.014607', '28.193727', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2351, NULL, NULL, '湘潭站', NULL, NULL, NULL, '112.912985', '27.87537', NULL, NULL, NULL);
INSERT INTO `tbl_road_nodes` VALUES (2352, NULL, NULL, '长沙汽车东站-张家界中心汽车站', NULL, NULL, NULL, '113.057457', '28.200596', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for tbl_road_segs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_road_segs`;
CREATE TABLE `tbl_road_segs`  (
  `road_segs_id` int(0) NOT NULL AUTO_INCREMENT,
  `high_roads_id` int(0) NOT NULL,
  `road_begin` int(0) NULL DEFAULT NULL,
  `road_end` int(0) NULL DEFAULT NULL,
  `total_mileage` float NULL DEFAULT NULL,
  `road_seg_cost` float NULL DEFAULT NULL,
  `truck_length` float NULL DEFAULT NULL,
  `truck_width` float NULL DEFAULT NULL,
  `truck_height` float NULL DEFAULT NULL,
  `truck_weight` float NULL DEFAULT NULL,
  `truck_axes` smallint(0) NULL DEFAULT NULL,
  `truck_tyres` smallint(0) NULL DEFAULT NULL,
  `road_seg_status` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `road_seg_comment` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`road_segs_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_road_segs
-- ----------------------------
INSERT INTO `tbl_road_segs` VALUES (1, 5, 7, 10, 67, 2, 20, 10, 10, 100, 20, 20, '1', '冰雹');
INSERT INTO `tbl_road_segs` VALUES (2, 5, 2, 4, 86, 97, 20, 10, 10, 100, 20, 20, '1', '晴天');
INSERT INTO `tbl_road_segs` VALUES (3, 4, 8, 7, 63, 59, 20, 10, 10, 100, 20, 20, '1', '冰雹');
INSERT INTO `tbl_road_segs` VALUES (4, 1, 8, 8, 22, 92, 20, 10, 10, 100, 20, 20, '1', '暴雨');
INSERT INTO `tbl_road_segs` VALUES (5, 5, 3, 10, 81, 20, 20, 10, 10, 100, 20, 20, '1', '雨天');
INSERT INTO `tbl_road_segs` VALUES (6, 7, 4, 1, 96, 3, 20, 10, 10, 100, 20, 20, '1', '雨天');
INSERT INTO `tbl_road_segs` VALUES (7, 7, 7, 2, 1, 32, 20, 10, 10, 100, 20, 20, '1', '冰雹');
INSERT INTO `tbl_road_segs` VALUES (8, 5, 9, 5, 36, 26, 20, 10, 10, 100, 20, 20, '1', '暴雨');
INSERT INTO `tbl_road_segs` VALUES (9, 8, 7, 5, 96, 86, 20, 10, 10, 100, 20, 20, '1', '冰雹');
INSERT INTO `tbl_road_segs` VALUES (10, 3, 3, 4, 91, 70, 20, 10, 10, 100, 20, 20, '1', '雨天');

-- ----------------------------
-- Table structure for tbl_road_stone
-- ----------------------------
DROP TABLE IF EXISTS `tbl_road_stone`;
CREATE TABLE `tbl_road_stone`  (
  `road_nodes_id` int(0) NOT NULL AUTO_INCREMENT,
  `road_segs_id` int(0) NULL DEFAULT NULL,
  `high_roads_id` int(0) NOT NULL,
  `node_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `longitude` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `latitude` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `index_cities` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `node_comment` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`road_nodes_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_road_stone
-- ----------------------------
INSERT INTO `tbl_road_stone` VALUES (1, 9, 8, '无独有偶', '124.74', '26.47', '即墨', '暴雨');
INSERT INTO `tbl_road_stone` VALUES (2, 8, 5, '脚踏实地', '112.07', '23.47', '齐齐哈尔', '晴天');
INSERT INTO `tbl_road_stone` VALUES (3, 5, 2, '不翼而飞', '118.39', '35.18', '昆明', '雨天');
INSERT INTO `tbl_road_stone` VALUES (4, 5, 2, '风口浪尖', '111.67', '46.14', '锦州', '小雨');
INSERT INTO `tbl_road_stone` VALUES (5, 2, 10, '家喻户晓', '126.51', '35.02', '寿光', '多云');
INSERT INTO `tbl_road_stone` VALUES (6, 2, 4, '举足轻重', '111.50', '45.06', '河源', '小雨');
INSERT INTO `tbl_road_stone` VALUES (7, 3, 2, '任重道远', '101.99', '40.75', '抚顺', '雪天');
INSERT INTO `tbl_road_stone` VALUES (8, 10, 10, '无独有偶', '118.97', '46.60', '厦门', '雪天');
INSERT INTO `tbl_road_stone` VALUES (9, 8, 1, '水涨船高', '111.45', '33.33', '九江', '台风天');
INSERT INTO `tbl_road_stone` VALUES (10, 4, 4, '厉行节约', '122.55', '29.66', '盘锦', '台风天');

-- ----------------------------
-- Table structure for tbl_roads
-- ----------------------------
DROP TABLE IF EXISTS `tbl_roads`;
CREATE TABLE `tbl_roads`  (
  `roads_id` int(0) NOT NULL AUTO_INCREMENT,
  `road_begin` int(0) NULL DEFAULT NULL,
  `road_end` int(0) NULL DEFAULT NULL,
  `suggest_road` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `total_mileage` float NULL DEFAULT NULL,
  `pass_cities` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `road_cost` float NULL DEFAULT NULL,
  `truck_length` float NULL DEFAULT NULL,
  `truck_width` float NULL DEFAULT NULL,
  `truck_height` float NULL DEFAULT NULL,
  `truck_weight` float NULL DEFAULT NULL,
  `truck_axes` smallint(0) NULL DEFAULT NULL,
  `truck_tyres` smallint(0) NULL DEFAULT NULL,
  `road_status` tinyint(1) NULL DEFAULT NULL COMMENT '0是阻塞，1是可通行',
  `road_comment` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`roads_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_roads
-- ----------------------------
INSERT INTO `tbl_roads` VALUES (6, 1222, 1225, '\"常德高新区（灌溪）收费站---G56杭瑞高速---G5513长张高速---张家界收费站\"', 20, '常德市', 18, 50, 10, 10, 100, 20, 20, 1, '晴天');
INSERT INTO `tbl_roads` VALUES (7, 1222, 1226, '\"常德高新区（灌溪）收费站---G56杭瑞高速---G5513长张高速---G55二广高速---永州北收费站\"', 21, '海阳市、长沙市', 82, 50, 10, 10, 100, 20, 20, 0, '小雨');
INSERT INTO `tbl_roads` VALUES (8, 1224, 1225, '\"攸县东收费站---G0422武深高速---平江收费站\"', 73, '湘潭市', 48, 50, 10, 10, 100, 20, 20, 0, '小雨');

-- ----------------------------
-- Table structure for tbl_sys_informs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_informs`;
CREATE TABLE `tbl_sys_informs`  (
  `sys_informs_id` int(0) NOT NULL AUTO_INCREMENT,
  `sender_username` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `receiver_username` varchar(11) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `send_date` datetime(0) NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `send_info` varchar(256) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `deleted` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`sys_informs_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 563 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_sys_informs
-- ----------------------------
INSERT INTO `tbl_sys_informs` VALUES (1, '13124038733', '15699320040', '2022-11-21 18:15:02', '美', 0);
INSERT INTO `tbl_sys_informs` VALUES (2, '15600348099', '15298925712', '2022-11-21 18:15:03', '唱', 0);
INSERT INTO `tbl_sys_informs` VALUES (3, '13298506856', '15191023759', '2022-11-21 18:15:03', '篮球', 0);
INSERT INTO `tbl_sys_informs` VALUES (4, '14531897142', '17260557885', '2022-11-21 18:15:04', '篮球', 0);
INSERT INTO `tbl_sys_informs` VALUES (5, '18934903515', '13726422667', '2022-11-21 18:15:09', '太', 0);
INSERT INTO `tbl_sys_informs` VALUES (6, '13507775216', '13983410138', '2022-11-21 18:15:05', '唱', 0);
INSERT INTO `tbl_sys_informs` VALUES (7, '17875892565', '15064337435', '2022-11-21 18:15:07', '两年半', 0);
INSERT INTO `tbl_sys_informs` VALUES (8, '17118211457', '17166810214', '2022-11-21 18:15:07', '练习', 0);
INSERT INTO `tbl_sys_informs` VALUES (9, '15356606030', '15545307151', '2022-11-21 18:15:10', 'rap', 0);
INSERT INTO `tbl_sys_informs` VALUES (10, '15545307151', '14720823350', '2022-11-21 18:15:11', '两年半', 0);
INSERT INTO `tbl_sys_informs` VALUES (11, '13593208343', '13593208343', '2022-11-21 18:15:11', '喂喂喂', 0);
INSERT INTO `tbl_sys_informs` VALUES (30, '17160580103', '15781397208', '2022-11-21 18:15:12', '6666666666999999999', 0);
INSERT INTO `tbl_sys_informs` VALUES (31, '17160580103', '17090512999', '2022-11-21 18:15:14', '6666666666999999999', 0);
INSERT INTO `tbl_sys_informs` VALUES (32, '17160580103', '15503334578', '2022-11-21 18:15:14', '6666666666999999999', 0);
INSERT INTO `tbl_sys_informs` VALUES (33, '17160580103', '17006298324', '2022-11-21 18:15:18', '6666666666999999999', 0);
INSERT INTO `tbl_sys_informs` VALUES (34, '17160580103', '13599927387', '2022-11-21 18:15:16', '6666666666999999999', 0);
INSERT INTO `tbl_sys_informs` VALUES (35, '17160580103', '13827384739', '2022-11-21 18:15:16', '6666666666999999999', 0);
INSERT INTO `tbl_sys_informs` VALUES (36, '17160580103', '18393255154', '2022-11-21 18:15:19', '6666666666999999999', 0);
INSERT INTO `tbl_sys_informs` VALUES (52, '17160580103', '15699030591', '2022-11-21 18:15:19', '1234', 0);
INSERT INTO `tbl_sys_informs` VALUES (82, '17160580103', '15699030591', '2022-11-21 18:15:20', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (83, '17160580103', '17158136759', '2022-11-21 18:15:21', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (84, '17160580103', '14762026367', '2022-11-21 18:15:21', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (85, '17160580103', '15567141285', '2022-11-21 18:15:22', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (86, '17160580103', '17010752589', '2022-11-21 18:15:23', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (87, '17160580103', '17181219115', '2022-11-21 18:15:23', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (88, '17160580103', '17751205856', '2022-11-21 18:15:25', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (89, '17160580103', '15673049735', '2022-11-21 18:15:27', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (90, '17160580103', '17188996993', '2022-11-21 18:15:28', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (91, '17160580103', '13392873847', '2022-11-21 18:15:30', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (92, '17160580103', '13878787123', '2022-11-21 18:15:32', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (93, '17160580103', '13676127861', '2022-11-21 18:15:31', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (95, '17160580103', '15405040131', '2022-11-21 18:15:34', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (97, '17160580103', '18766636464', '2022-11-21 18:15:34', '时间问题，得抓紧', 0);
INSERT INTO `tbl_sys_informs` VALUES (98, '13187029351', '15699030591', '2022-11-21 18:15:35', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (99, '13187029351', '17158136759', '2022-11-21 18:15:38', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (100, '13187029351', '14762026367', '2022-11-21 18:15:40', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (101, '13187029351', '18886410372', '2022-11-21 18:15:43', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (102, '13187029351', '17010752589', '2022-11-21 18:15:45', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (103, '13187029351', '17181219115', '2022-11-21 18:15:44', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (104, '13187029351', '17751205856', '2022-11-21 18:15:46', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (105, '13187029351', '15673049735', '2022-11-21 18:15:46', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (106, '13187029351', '17188996993', '2022-11-21 18:15:47', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (107, '13187029351', '13392873847', '2022-11-21 18:15:48', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (108, '13187029351', '13878787123', '2022-11-21 18:15:48', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (109, '13187029351', '13676127861', '2022-11-21 18:15:49', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (110, '13187029351', '22222222222', '2022-11-21 18:15:49', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (111, '13187029351', '15405040131', '2022-11-21 18:15:50', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (112, '13187029351', '18766636464', '2022-11-21 18:15:51', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (113, '13187029351', '17633383745', '2022-11-21 18:15:51', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (114, '13187029351', '18788863547', '2022-11-21 18:15:52', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (115, '13187029351', '18677736456', '2022-11-21 18:15:53', 'gigio', 0);
INSERT INTO `tbl_sys_informs` VALUES (129, '18677761646', '23232323232', '2022-11-21 00:00:00', '审核员系统通知消息测试', 0);
INSERT INTO `tbl_sys_informs` VALUES (130, '18677761646', '18766645434', '2022-11-21 00:00:00', '审核员系统通知消息测试', 0);
INSERT INTO `tbl_sys_informs` VALUES (131, '18677761646', '18677797905', '2022-11-21 00:00:00', '审核员系统通知消息测试', 0);
INSERT INTO `tbl_sys_informs` VALUES (132, '18677761646', '18677734564', '2022-11-21 00:00:00', '审核员系统通知消息测试', 0);
INSERT INTO `tbl_sys_informs` VALUES (134, '18677761646', '12345678999', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (135, '18677761646', '11111111111', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (136, '18677761646', '22222222222', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (137, '18677761646', '33333333333', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (138, '18677761646', '44444444444', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (139, '18677761646', '55555555555', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (140, '18677761646', '66666666666', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (141, '18677761646', '77777777777', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (142, '18677761646', '88888888888', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (143, '18677761646', '99999999999', '2022-11-21 00:00:00', 'deleted呢，日期呢', 0);
INSERT INTO `tbl_sys_informs` VALUES (145, '18677761646', '12121212121', '2022-11-21 00:00:00', 'whywhy', 0);
INSERT INTO `tbl_sys_informs` VALUES (146, '18677761646', '23232323232', '2022-11-21 00:00:00', 'whywhy', 0);
INSERT INTO `tbl_sys_informs` VALUES (147, '18677761646', '18766645434', '2022-11-21 00:00:00', 'whywhy', 0);
INSERT INTO `tbl_sys_informs` VALUES (148, '18677761646', '18677797905', '2022-11-21 00:00:00', 'whywhy', 0);
INSERT INTO `tbl_sys_informs` VALUES (149, '18677761646', '18677734564', '2022-11-21 00:00:00', 'whywhy', 0);
INSERT INTO `tbl_sys_informs` VALUES (150, '18677761646', '15405040131', '2022-11-21 00:00:00', 'whywhy', 0);
INSERT INTO `tbl_sys_informs` VALUES (152, '18677761646', '12345678999', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (153, '18677761646', '11111111111', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (154, '18677761646', '22222222222', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (155, '18677761646', '33333333333', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (156, '18677761646', '44444444444', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (157, '18677761646', '55555555555', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (158, '18677761646', '66666666666', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (159, '18677761646', '77777777777', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (160, '18677761646', '88888888888', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (161, '18677761646', '99999999999', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (162, '18677761646', '15404050431', '2022-11-21 19:56:46', '别吓我', 0);
INSERT INTO `tbl_sys_informs` VALUES (163, '18677761646', '18225374869', '2022-11-23 13:15:34', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (164, '18677761646', '18677761646', '2022-11-23 13:15:34', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (165, '18677761646', '12345678999', '2022-11-23 13:15:34', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (166, '18677761646', '11111111111', '2022-11-23 13:15:34', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (167, '18677761646', '22222222222', '2022-11-23 13:15:34', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (168, '18677761646', '33333333333', '2022-11-23 13:15:34', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (169, '18677761646', '44444444444', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (170, '18677761646', '55555555555', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (171, '18677761646', '66666666666', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (172, '18677761646', '77777777777', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (173, '18677761646', '88888888888', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (174, '18677761646', '99999999999', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (175, '18677761646', '12121212121', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (176, '18677761646', '23232323232', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (177, '18677761646', '18766645434', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (178, '18677761646', '18677797905', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (179, '18677761646', '18677734564', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (180, '18677761646', '15405040131', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (181, '18677761646', '15405040311', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (182, '18677761646', '15404050431', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (183, '18677761646', '15405040531', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (185, '18677761646', '18393255154', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (186, '18677761646', '13285659856', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (187, '18677761646', '13569856985', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (188, '18677761646', '13185645698', '2022-11-23 13:15:35', '123', 0);
INSERT INTO `tbl_sys_informs` VALUES (189, '18677761646', '12345678999', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (190, '18677761646', '11111111111', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (191, '18677761646', '22222222222', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (192, '18677761646', '33333333333', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (193, '18677761646', '44444444444', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (194, '18677761646', '55555555555', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (195, '18677761646', '66666666666', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (196, '18677761646', '77777777777', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (197, '18677761646', '88888888888', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (198, '18677761646', '99999999999', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (199, '18677761646', '15404050431', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (200, '18677761646', '15405040531', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (202, '18677761646', '18393255154', '2022-11-23 13:23:41', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (203, '18677761646', '13285659856', '2022-11-23 13:23:42', '喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威喂喂喂威威', 0);
INSERT INTO `tbl_sys_informs` VALUES (206, '18677761646', '18225374869', '2022-11-23 13:42:32', 'dcd当初我的错我的错我的错的外侧如此侮辱', 0);
INSERT INTO `tbl_sys_informs` VALUES (207, '18677761646', '12121212121', '2022-11-23 13:42:32', 'dcd当初我的错我的错我的错的外侧如此侮辱', 0);
INSERT INTO `tbl_sys_informs` VALUES (208, '18677761646', '23232323232', '2022-11-23 13:42:32', 'dcd当初我的错我的错我的错的外侧如此侮辱', 0);
INSERT INTO `tbl_sys_informs` VALUES (209, '18677761646', '18766645434', '2022-11-23 13:42:32', 'dcd当初我的错我的错我的错的外侧如此侮辱', 0);
INSERT INTO `tbl_sys_informs` VALUES (211, '18677761646', '18677734564', '2022-11-23 13:42:32', 'dcd当初我的错我的错我的错的外侧如此侮辱', 0);
INSERT INTO `tbl_sys_informs` VALUES (214, '18677761646', '18225374869', '2022-11-23 17:00:22', '审核员呃呃呃呃', 0);
INSERT INTO `tbl_sys_informs` VALUES (215, '18677761646', '23232323232', '2022-11-23 17:00:22', '审核员呃呃呃呃', 0);
INSERT INTO `tbl_sys_informs` VALUES (216, '18677761646', '18766645434', '2022-11-23 17:00:22', '审核员呃呃呃呃', 0);
INSERT INTO `tbl_sys_informs` VALUES (217, '18677761646', '18677797905', '2022-11-23 17:00:22', '审核员呃呃呃呃', 0);
INSERT INTO `tbl_sys_informs` VALUES (218, '18677761646', '18677734564', '2022-11-23 17:00:22', '审核员呃呃呃呃', 0);
INSERT INTO `tbl_sys_informs` VALUES (219, '18677761646', '12345678999', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (220, '18677761646', '11111111111', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (221, '18677761646', '22222222222', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (222, '18677761646', '33333333333', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (223, '18677761646', '44444444444', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (224, '18677761646', '55555555555', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (225, '18677761646', '66666666666', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (226, '18677761646', '77777777777', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (227, '18677761646', '88888888888', '2022-11-23 17:00:59', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (228, '18677761646', '99999999999', '2022-11-23 17:01:00', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (229, '18677761646', '15404050431', '2022-11-23 17:01:00', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (230, '18677761646', '15405040531', '2022-11-23 17:01:00', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (232, '18677761646', '18393255154', '2022-11-23 17:01:00', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (233, '18677761646', '13285659856', '2022-11-23 17:01:00', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (234, '18677761646', '13569856985', '2022-11-23 17:01:00', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (235, '18677761646', '13185645698', '2022-11-23 17:01:00', '司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (236, '18677761646', '18225374869', '2022-11-23 17:02:40', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (237, '18677761646', '18677761646', '2022-11-23 17:02:40', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (238, '18677761646', '12345678999', '2022-11-23 17:02:40', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (239, '18677761646', '11111111111', '2022-11-23 17:02:40', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (240, '18677761646', '22222222222', '2022-11-23 17:02:40', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (241, '18677761646', '33333333333', '2022-11-23 17:02:40', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (242, '18677761646', '44444444444', '2022-11-23 17:02:40', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (243, '18677761646', '55555555555', '2022-11-23 17:02:40', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (244, '18677761646', '66666666666', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (245, '18677761646', '77777777777', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (246, '18677761646', '88888888888', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (247, '18677761646', '99999999999', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (248, '18677761646', '23232323232', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (249, '18677761646', '18766645434', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (250, '18677761646', '18677797905', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (251, '18677761646', '18677734564', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (252, '18677761646', '15404050431', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (253, '18677761646', '15405040531', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (255, '18677761646', '18393255154', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (256, '18677761646', '13285659856', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (257, '18677761646', '13569856985', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (258, '18677761646', '13185645698', '2022-11-23 17:02:41', '快给我收到', 0);
INSERT INTO `tbl_sys_informs` VALUES (259, '18766645434', '12345678999', '2022-11-24 15:29:25', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (260, '18766645434', '11111111111', '2022-11-24 15:29:25', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (261, '18766645434', '22222222222', '2022-11-24 15:29:25', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (262, '18766645434', '33333333333', '2022-11-24 15:29:25', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (263, '18766645434', '44444444444', '2022-11-24 15:29:25', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (264, '18766645434', '55555555555', '2022-11-24 15:29:25', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (265, '18766645434', '66666666666', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (266, '18766645434', '77777777777', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (267, '18766645434', '88888888888', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (268, '18766645434', '99999999999', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (269, '18766645434', '15404050431', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (270, '18766645434', '15405040531', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (272, '18766645434', '18393255154', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (273, '18766645434', '13285659856', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (274, '18766645434', '13569856985', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (275, '18766645434', '13185645698', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (276, '18766645434', '13187029350', '2022-11-24 15:29:26', '你好啊', 0);
INSERT INTO `tbl_sys_informs` VALUES (352, 'admin', '13594282718', '2023-02-08 13:56:59', '111', 0);
INSERT INTO `tbl_sys_informs` VALUES (354, 'admin', '23232323232', '2023-02-08 14:04:18', '1111', 0);
INSERT INTO `tbl_sys_informs` VALUES (355, 'admin', '18677797905', '2023-02-08 14:04:18', '1111', 0);
INSERT INTO `tbl_sys_informs` VALUES (356, 'admin', '18677734564', '2023-02-08 14:04:18', '1111', 0);
INSERT INTO `tbl_sys_informs` VALUES (357, 'admin', '13187029351', '2023-02-08 14:04:18', '1111', 0);
INSERT INTO `tbl_sys_informs` VALUES (358, 'admin', '13594282718', '2023-02-08 14:04:18', '1111', 0);
INSERT INTO `tbl_sys_informs` VALUES (360, 'admin', '23232323232', '2023-02-08 14:05:59', '1', 0);
INSERT INTO `tbl_sys_informs` VALUES (361, 'admin', '18677797905', '2023-02-08 14:05:59', '1', 0);
INSERT INTO `tbl_sys_informs` VALUES (362, 'admin', '18677734564', '2023-02-08 14:05:59', '1', 0);
INSERT INTO `tbl_sys_informs` VALUES (363, 'admin', '13187029351', '2023-02-08 14:05:59', '1', 0);
INSERT INTO `tbl_sys_informs` VALUES (364, 'admin', '13594282718', '2023-02-08 14:06:00', '1', 0);
INSERT INTO `tbl_sys_informs` VALUES (366, 'admin', '23232323232', '2023-02-08 14:06:24', '112', 0);
INSERT INTO `tbl_sys_informs` VALUES (367, 'admin', '18677797905', '2023-02-08 14:06:24', '112', 0);
INSERT INTO `tbl_sys_informs` VALUES (368, 'admin', '18677734564', '2023-02-08 14:06:24', '112', 0);
INSERT INTO `tbl_sys_informs` VALUES (369, 'admin', '13187029351', '2023-02-08 14:06:24', '112', 0);
INSERT INTO `tbl_sys_informs` VALUES (370, 'admin', '13594282718', '2023-02-08 14:06:24', '112', 0);
INSERT INTO `tbl_sys_informs` VALUES (400, 'admin', '23232323232', '2023-02-09 14:25:48', '111', 0);
INSERT INTO `tbl_sys_informs` VALUES (401, 'admin', '18677797905', '2023-02-09 14:25:48', '111', 0);
INSERT INTO `tbl_sys_informs` VALUES (402, 'admin', '18677734564', '2023-02-09 14:25:48', '111', 0);
INSERT INTO `tbl_sys_informs` VALUES (403, 'admin', '13594282718', '2023-02-09 14:25:48', '111', 0);
INSERT INTO `tbl_sys_informs` VALUES (404, 'admin', '10001', '2023-02-10 12:43:50', '嘿嘿嘿', 0);
INSERT INTO `tbl_sys_informs` VALUES (447, 'admin', '10000', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (448, 'admin', '12345678999', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (449, 'admin', '11111111111', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (450, 'admin', '22222222222', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (451, 'admin', '33333333333', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (452, 'admin', '44444444444', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (453, 'admin', '55555555555', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (454, 'admin', '66666666666', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (455, 'admin', '77777777777', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (456, 'admin', '88888888888', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (457, 'admin', '99999999999', '2023-02-10 13:09:19', '司机司机', 0);
INSERT INTO `tbl_sys_informs` VALUES (468, 'admin', '10000', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (469, 'admin', '12345678999', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (470, 'admin', '11111111111', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (471, 'admin', '22222222222', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (472, 'admin', '33333333333', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (473, 'admin', '44444444444', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (474, 'admin', '55555555555', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (475, 'admin', '66666666666', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (476, 'admin', '77777777777', '2023-02-10 13:11:08', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (477, 'admin', '88888888888', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (478, 'admin', '99999999999', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (479, 'admin', '15404050431', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (480, 'admin', '15405040531', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (481, 'admin', '13187029351', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (482, 'admin', '18393255154', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (483, 'admin', '13285659856', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (484, 'admin', '13569856985', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (485, 'admin', '13185645698', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (486, 'admin', '13187029350', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (487, 'admin', '18677761640', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (488, 'admin', '18677745298', '2023-02-10 13:11:09', '再试一次', 0);
INSERT INTO `tbl_sys_informs` VALUES (489, 'admin', 'admin', '2023-02-10 13:11:44', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (490, 'admin', '10000', '2023-02-10 13:11:44', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (491, 'admin', '10001', '2023-02-10 13:11:44', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (492, 'admin', '18677761646', '2023-02-10 13:11:44', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (493, 'admin', '12345678999', '2023-02-10 13:11:44', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (494, 'admin', '11111111111', '2023-02-10 13:11:44', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (495, 'admin', '22222222222', '2023-02-10 13:11:44', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (496, 'admin', '33333333333', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (497, 'admin', '44444444444', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (498, 'admin', '55555555555', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (499, 'admin', '66666666666', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (500, 'admin', '77777777777', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (501, 'admin', '88888888888', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (502, 'admin', '99999999999', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (503, 'admin', '23232323232', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (504, 'admin', '18766645434', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (505, 'admin', '18677797905', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (506, 'admin', '18677734564', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (507, 'admin', '15404050431', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (508, 'admin', '15405040531', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (509, 'admin', '13187029351', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (510, 'admin', '18393255154', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (511, 'admin', '13285659856', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (512, 'admin', '13569856985', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (513, 'admin', '13185645698', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (514, 'admin', '13187029350', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (515, 'admin', NULL, '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (516, 'admin', '13594282718', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (517, 'admin', '18677761640', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (518, 'admin', '18677745298', '2023-02-10 13:11:45', '胜多负少', 0);
INSERT INTO `tbl_sys_informs` VALUES (519, 'admin', '10001', '2023-02-10 13:12:11', '阿斯顿发送到发送到', 0);
INSERT INTO `tbl_sys_informs` VALUES (520, 'admin', '23232323232', '2023-02-10 13:12:11', '阿斯顿发送到发送到', 0);
INSERT INTO `tbl_sys_informs` VALUES (521, 'admin', '18677797905', '2023-02-10 13:12:11', '阿斯顿发送到发送到', 0);
INSERT INTO `tbl_sys_informs` VALUES (523, 'admin', '13594282718', '2023-02-10 13:12:11', '阿斯顿发送到发送到', 0);
INSERT INTO `tbl_sys_informs` VALUES (524, 'admin', '10000', '2023-02-11 11:34:17', '司机测试1', 0);
INSERT INTO `tbl_sys_informs` VALUES (525, 'admin', '13187029351', '2023-02-11 11:34:17', '司机测试1', 0);
INSERT INTO `tbl_sys_informs` VALUES (526, 'admin', '12345678999', '2023-02-11 11:34:17', '司机测试1', 0);
INSERT INTO `tbl_sys_informs` VALUES (527, 'admin', '99999999999', '2023-02-11 11:34:17', '司机测试1', 0);
INSERT INTO `tbl_sys_informs` VALUES (528, 'admin', '10001', '2023-02-11 11:34:43', '审核员测试1', 0);
INSERT INTO `tbl_sys_informs` VALUES (529, 'admin', '23232323232', '2023-02-11 11:34:43', '审核员测试1', 0);
INSERT INTO `tbl_sys_informs` VALUES (530, 'admin', '18677797905', '2023-02-11 11:34:43', '审核员测试1', 0);
INSERT INTO `tbl_sys_informs` VALUES (531, 'admin', '18677734564', '2023-02-11 11:34:43', '审核员测试1', 0);
INSERT INTO `tbl_sys_informs` VALUES (532, 'admin', '12312312312', '2023-02-12 01:30:00', 'test2.12', 0);
INSERT INTO `tbl_sys_informs` VALUES (533, 'admin', '10001', '2023-02-12 01:30:00', 'test2.12', 0);
INSERT INTO `tbl_sys_informs` VALUES (534, 'admin', '18225374869', '2023-02-12 01:30:00', 'test2.12', 0);
INSERT INTO `tbl_sys_informs` VALUES (535, 'admin', '23232323232', '2023-02-12 01:30:00', 'test2.12', 0);
INSERT INTO `tbl_sys_informs` VALUES (536, 'admin', '18677797905', '2023-02-12 01:30:00', 'test2.12', 0);
INSERT INTO `tbl_sys_informs` VALUES (537, 'admin', '18677734564', '2023-02-12 01:30:00', 'test2.12', 0);
INSERT INTO `tbl_sys_informs` VALUES (538, 'admin', '13187029351', '2023-02-12 14:52:28', '   123', 0);
INSERT INTO `tbl_sys_informs` VALUES (539, 'admin', '13855793753', '2023-02-12 14:52:28', '   123', 0);
INSERT INTO `tbl_sys_informs` VALUES (540, 'admin', '13815215061', '2023-02-12 14:52:28', '   123', 0);
INSERT INTO `tbl_sys_informs` VALUES (541, 'admin', '18677761641', '2023-02-12 14:52:28', '   123', 0);
INSERT INTO `tbl_sys_informs` VALUES (542, 'admin', '13594282711', '2023-02-12 14:52:28', '   123', 0);
INSERT INTO `tbl_sys_informs` VALUES (543, 'admin', '10000', '2023-02-12 14:52:28', '   123', 0);
INSERT INTO `tbl_sys_informs` VALUES (544, 'admin', '12345678999', '2023-02-12 14:52:28', '   123', 0);
INSERT INTO `tbl_sys_informs` VALUES (545, 'admin', '99999999999', '2023-02-12 14:52:28', '   123', 0);
INSERT INTO `tbl_sys_informs` VALUES (546, 'admin', 'admin', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (547, 'admin', '10000', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (548, 'admin', '10001', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (549, 'admin', '18677761646', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (550, 'admin', '12345678999', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (551, 'admin', '99999999999', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (552, 'admin', '23232323232', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (553, 'admin', '18766645434', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (554, 'admin', '18677797905', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (555, 'admin', '18677734564', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (556, 'admin', '13594282711', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (557, 'admin', '18677761641', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (558, 'admin', '18225374869', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (559, 'admin', '13815215061', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (560, 'admin', '13855793753', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (561, 'admin', '13187029351', '2023-02-12 15:00:19', '发给所有人', 0);
INSERT INTO `tbl_sys_informs` VALUES (562, 'admin', '12312312312', '2023-02-12 15:00:19', '发给所有人', 0);

-- ----------------------------
-- Table structure for tbl_sys_logs
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_logs`;
CREATE TABLE `tbl_sys_logs`  (
  `sys_logs_id` int(0) NOT NULL AUTO_INCREMENT,
  `road_apply_id` int(0) NULL DEFAULT NULL,
  `auditor_id` int(0) NULL DEFAULT NULL,
  `road_id` int(0) NULL DEFAULT NULL,
  PRIMARY KEY (`sys_logs_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_sys_logs
-- ----------------------------
INSERT INTO `tbl_sys_logs` VALUES (1, 7, 7, 8);
INSERT INTO `tbl_sys_logs` VALUES (2, 3, 1, 2);
INSERT INTO `tbl_sys_logs` VALUES (3, 2, 10, 2);
INSERT INTO `tbl_sys_logs` VALUES (4, 2, 1, 8);
INSERT INTO `tbl_sys_logs` VALUES (5, 6, 5, 5);
INSERT INTO `tbl_sys_logs` VALUES (6, 8, 3, 1);
INSERT INTO `tbl_sys_logs` VALUES (7, 2, 3, 7);
INSERT INTO `tbl_sys_logs` VALUES (8, 4, 10, 1);
INSERT INTO `tbl_sys_logs` VALUES (9, 10, 8, 8);
INSERT INTO `tbl_sys_logs` VALUES (10, 5, 2, 10);

-- ----------------------------
-- Table structure for tbl_trucks
-- ----------------------------
DROP TABLE IF EXISTS `tbl_trucks`;
CREATE TABLE `tbl_trucks`  (
  `trucks_id` int(0) NOT NULL AUTO_INCREMENT,
  `truck_license` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id_card` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `truck_length` float NULL DEFAULT NULL,
  `truck_width` float NULL DEFAULT NULL,
  `truck_height` float NULL DEFAULT NULL,
  `truck_weight` float NULL DEFAULT NULL,
  `truck_cargo_weight` float NULL DEFAULT NULL,
  `truck_axes` smallint(0) NULL DEFAULT NULL,
  `truck_tyres` smallint(0) NULL DEFAULT NULL,
  `init_date` datetime(0) NULL DEFAULT NULL,
  `current_condition` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`trucks_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_trucks
-- ----------------------------
INSERT INTO `tbl_trucks` VALUES (1, '皖E00001', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-02-10 00:00:00', '后胎略微磨损');
INSERT INTO `tbl_trucks` VALUES (2, '皖E00002', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-08-18 00:00:00', '左视镜刮花');
INSERT INTO `tbl_trucks` VALUES (3, '皖E00003', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-06-13 00:00:00', '对对对');
INSERT INTO `tbl_trucks` VALUES (4, '皖E00004', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-01-24 00:00:00', '左轮胎、有一点点问题');
INSERT INTO `tbl_trucks` VALUES (5, '皖E00005', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-01-25 00:00:00', '好得很');
INSERT INTO `tbl_trucks` VALUES (6, '皖E00006', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-10-19 00:00:00', '蔡徐坤');
INSERT INTO `tbl_trucks` VALUES (7, '皖E00007', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-05-30 00:00:00', '太');
INSERT INTO `tbl_trucks` VALUES (8, '皖E00008', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-06-08 00:00:00', '两年半');
INSERT INTO `tbl_trucks` VALUES (9, '皖E00009', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-01-02 00:00:00', '蔡徐坤');
INSERT INTO `tbl_trucks` VALUES (10, '皖E00010', '622427200208070418', 10, 4, 4, 20, 20, 10, 10, '2022-03-24 00:00:00', '蔡徐坤');
INSERT INTO `tbl_trucks` VALUES (11, '湘5', '622427200208070419', 1, 2, 3, 4, 5, 6, 19, '2023-02-15 14:38:40', '好车');

-- ----------------------------
-- Table structure for tbl_users
-- ----------------------------
DROP TABLE IF EXISTS `tbl_users`;
CREATE TABLE `tbl_users`  (
  `user_id` int(0) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `real_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `id_card` varchar(18) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `register_date` datetime(0) NULL DEFAULT NULL,
  `apply_times` int(0) NULL DEFAULT NULL,
  `user_type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `image` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 121 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of tbl_users
-- ----------------------------
INSERT INTO `tbl_users` VALUES (1, 'admin', 'ze2qfl8b33v3l4qtdujl@98261249284229d989008c289d9616a9', '管理员', '500227200112230031', '2023-01-21 11:45:01', 0, '管理员', NULL);
INSERT INTO `tbl_users` VALUES (2, '10000', 'd5b4xdqcksudkfetjy2j@25cced50a795ce070b50b62331c9b5a3', '司机sxl', '622427200208070419', '2023-01-21 11:46:54', 0, '司机', '/usr/image/driver/default0.jpg');
INSERT INTO `tbl_users` VALUES (3, '10001', 'd5b4xdqcksudkfetjy2j@25cced50a795ce070b50b62331c9b5a3', '黄森哥', '622427200208070419', '2023-02-12 00:41:24', 0, '审核员', '/usr/image/auditor/default0.jpg');
INSERT INTO `tbl_users` VALUES (45, '18677761646', '0738vv9ig2oz7mukdf0d@5f90a1c9b90538820b88cbe6014d3f37', '育铭', '450732333030303811', '2022-11-21 18:00:48', 0, '管理员', '/usr/image/manager/18677761646.jpg');
INSERT INTO `tbl_users` VALUES (46, '12345678999', '5zzlleghqth89wspb0qc@08097cc2d3eacab6bae2550c49a01246', '呆瓜', '450732333030303812', '2022-11-21 18:00:48', 0, '司机', '/usr/image/driver/default0.jpg');
INSERT INTO `tbl_users` VALUES (55, '99999999999', 'qjrshnr4kj4ruqdhzy2q@f431191461f6c331875861a66ba06394', '曹育铭', '450721200104272656', '2022-11-21 18:00:48', 0, '司机', '/usr/image/driver/99999999999.jpg');
INSERT INTO `tbl_users` VALUES (57, '23232323232', 'c4mq93tjew7e1jx9z1zw@d592bc1c513e3868f29f833781e9236b', '神情', '450732333030303823', '2022-11-21 18:00:48', 0, '审核员', '/usr/image/auditor/default0.jpg');
INSERT INTO `tbl_users` VALUES (58, '18766645434', 'rna9mlagzhrtx0bm7hfe@ebba176dd7cf1947454c58f5ecf2a1f7', '霹雳', '450732333030303824', '2022-11-21 18:00:48', 0, '管理员', '/usr/image/auditor/default0.jpg');
INSERT INTO `tbl_users` VALUES (59, '18677797905', 'le704gk3xuyls3c24iwu@b629816794b0825365c59e93876a80d7', '威震', '450732333030303825', '2022-11-21 18:00:48', 0, '审核员', '/usr/image/auditor/default0.jpg');
INSERT INTO `tbl_users` VALUES (60, '18677734564', 'actp8j497cjml4s03tyz@98a95acdf5c9b3b8e409443d6e168129', '神威', '450732333030303826', '2022-11-21 18:00:48', 0, '审核员', '/usr/image/auditor/default0.jpg');
INSERT INTO `tbl_users` VALUES (110, '13594282711', 'jd1b0ig9i4ao8citctn7@5e3eaf38b4ebeaee9cb6da9034990568', NULL, NULL, '2023-02-11 12:56:21', 0, '司机', '/usr/image/driver/default0.jpg');
INSERT INTO `tbl_users` VALUES (111, '18677761641', 'pa5mk0dnguc847n73nrh@53524ba70278db67a05efc9fafdfe853', '嘎嘎', '450721202102262984', '2023-02-11 13:06:24', 0, '司机', '/usr/image/driver/18677761641.jpg');
INSERT INTO `tbl_users` VALUES (114, '18225374869', '4y4t2c0yekp21fsbxvvs@3afe441f9edbe3636a4921025e65df0c', '三木', '500244200107125056', '2023-02-11 21:05:05', 0, '审核员', '/usr/image/auditor/18225374869.jpg');
INSERT INTO `tbl_users` VALUES (115, '13815215061', 'r8siemhrvjhvjs69rovf@a6e20642b8911bc783073adaf0c17011', NULL, NULL, '2023-02-11 21:06:29', 0, '司机', '/usr/image/driver/default0.jpg');
INSERT INTO `tbl_users` VALUES (116, '13855793753', '9cwumwczlq9e3j6oj25p@fe31ee6b20ba5029b8b1db78ad311d6c', NULL, NULL, '2023-02-11 22:45:51', 0, '司机', '/usr/image/driver/13855793753.jpg');
INSERT INTO `tbl_users` VALUES (117, '13187029351', '6pk3wiumhoj8s7mwfzyw@6549d61cbed25b4dea1028ee623c750a', '小龙a', '622427200208070418', '2023-02-11 23:04:30', 0, '司机', '/usr/image/driver/13187029351.jpg');
INSERT INTO `tbl_users` VALUES (119, '12312312312', 'p3cvbh6m5ty4k6ws8m6a@b3b70df68ddbc36f0494e2cda1c84b09', NULL, NULL, '2023-02-12 01:23:19', 0, '审核员', 'src/main/resources/image/auditor/default0.jpg');

SET FOREIGN_KEY_CHECKS = 1;
