# 大件运输电子地图
### 技术简介
前端：vue3 + Axios + Vue Router + Element + Echarts  
后端：Springboot2 + MySQL + Mybatis + MybatisPlus + Redis 

# 快速启动
1、导入sql文件

2、修改mysql配置

3、修改redis配置

4、本地运行测试即可

# 默认账号
管理员 admin 666666  
司机 10000 6666  
司机 13187029351 123456  
审核员 10001 6666

# 成功启动截图
### 管理员
![img.png](/image/管理员.png)
### 司机
![img.png](/image/司机.png)
### 审核员
![img.png](/image/审核员.png)

# 注意
1、本项目使用了短信服务 和 高德地图平台api，当你启动本项目时可能这些服务已过期，需要自己更换。  
2、本项目最初设计 为了实现 自己智能实现推荐路线，设计了许多路线相关的数据库表，  
比如：tbl_road_nodes, tbl_blocked_segs、 tbl_roads、tbl_high_roads、tbl_highroad_nodes、tbl_road_segs、tbl_road_stone 
但是最后迫于难度过高，只能调用高德地图api，于是有一些路线相关的表被废弃，只剩下三张根路线相关的表有用：  
tbl_road_nodes, tbl_blocked_segs和 tbl_roads
- tbl_road_nodes是路线节点表，储存路线的起点终点，SQL文件内该表存有湖南省所有收费站数据。此外，此表储存用户输入的，不在该表中的起始结点。
- tbl_blocked_segs是路线阻断表，储存路线的阻断信息。当路线是用户输入的，可以阻断，保存阻断信息。
- tbl_roads 是路线表，当用户手动输入路线会存入该表。 

3、此外，本项目由于时间、人力等原因，有tbl_sys_logs、tbl_login_logs表也没用上，综上，所有用上的表为：  
tbl_audit_logs、tbl_road_apply_logs、tbl_blocked_segs、tbl_road_nodes,  tbl_roads、tbl_sys_informs、tbl_trucks、tbl_users
如果本项目有后续，那就请继续完善吧。